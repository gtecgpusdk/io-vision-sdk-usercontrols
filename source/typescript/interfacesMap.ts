/** WARNING: this file has been automatically generated from typescript interfaces, which exist in this package. */

/* tslint:disable: variable-name no-use-before-declare only-arrow-functions */
namespace Io.VisionSDK.UserControls.Interfaces {
    "use strict";
    export let IAreaStrategy : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "setAreaStatus",
                "setMouseViewPosition",
                "getMouseViewPosition",
                "getMouseAreaPosition",
                "getMouseAreaMove",
                "getMouseViewMove",
                "getMouseLastAreaMove",
                "getMouseLastViewMove",
                "getMouseLastViewAxisChange",
                "getPointViewPosition",
                "getViewSize",
                "setViewSize",
                "setAreaConstraints",
                "IndicateAreaResize",
                "ApplyAreaResize",
                "CancelResize",
                "UpdateDimensionsIndicator",
                "getAreaResizeDirection",
                "getResizeAnchor",
                "getResizeAreaDimensions",
                "setAreaSize",
                "getAreaSize",
                "setZoomPercentage",
                "getScale",
                "getZoomMultiplier",
                "ResetAreaPosition",
                "getAreaPosition",
                "setAreaPosition",
                "getPointViewOverflow",
                "getMinOverflowPoint",
                "getMaxOverflowPoint",
                "ToggleScroll",
                "getPointScrollVector",
                "getPointsScrollVector",
                "IsPointsBoundableByView",
                "IsScrollActive",
                "IsResizeActive",
                "IsViewHovered"
            ]);
        }();
}

namespace Io.VisionSDK.UserControls.Interfaces {
    "use strict";
    export let IValidator : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
            ]);
        }();
}

namespace Io.VisionSDK.UserControls.Interfaces.Events {
    "use strict";
    export let IConnectorHolderEvents : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "setOnConnectorUpdate",
                "setOnConnectorMouseUp",
                "setOnConnectorMouseDown"
            ], Com.Wui.Framework.Gui.Interfaces.Events.IBasePanelEvents);
        }();
}

namespace Io.VisionSDK.UserControls.Interfaces.Events {
    "use strict";
    export let IConnectorHolderEventsHandler : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
            ], Com.Wui.Framework.Commons.Interfaces.IEventsHandler);
        }();
}

namespace Io.VisionSDK.UserControls.Interfaces.Events {
    "use strict";
    export let IKernelNodeEvents : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "setOnMove",
                "setOnConnectorMouseUp",
                "setOnConnectorMouseDown",
                "setOnConnectorMouseEnter",
                "setOnConnectorMouseLeave",
                "setOnMouseEnter",
                "setOnMouseLeave",
                "setOnTooltipChange",
                "setBeforeRemove"
            ], Com.Wui.Framework.Gui.Interfaces.Events.IBasePanelEvents);
        }();
}

namespace Io.VisionSDK.UserControls.Interfaces.Events {
    "use strict";
    export let IKernelNodeEventsHandler : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
            ], Com.Wui.Framework.Commons.Interfaces.IEventsHandler);
        }();
}

namespace Io.VisionSDK.UserControls.Interfaces.Events {
    "use strict";
    export let IPointerIconEvents : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "setOnDrag",
                "setOnMove"
            ], Com.Wui.Framework.Gui.Interfaces.Events.IFormsObjectEvents);
        }();
}

namespace Io.VisionSDK.UserControls.Interfaces {
    "use strict";
    export let IDropDownButton : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "Value"
            ], Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiCommons);
        }();
}

namespace Io.VisionSDK.UserControls.Interfaces {
    "use strict";
    export let IKernelNode : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "Move",
                "ParentNode",
                "ChildNodes",
                "AddInput",
                "AddOutput",
                "Help",
                "IconType",
                "Type",
                "Selected",
                "UniqueIdPrefix",
                "UniqueId",
                "NamePrefix",
                "Name",
                "getGroupId",
                "GroupOffset",
                "Column",
                "Row",
                "getOutput",
                "getInput",
                "getInputs",
                "getOutputs",
                "getInputConnectors",
                "getOutputConnectors",
                "Remove",
                "TrackConnector",
                "ShowInputConnectors",
                "ShowOutputConnectors",
                "HideConnectors",
                "IsConnectorsVisible",
                "ScaleVisibleConnectors",
                "MoveInterface",
                "RemoveInterface",
                "getAttributes",
                "setAttributeValue",
                "setAttribute",
                "getGridPosition",
                "getAbsolutePosition",
                "getAbsoluteConnectorPosition",
                "GridSize",
                "getEvents",
                "setConnectorPosition",
                "RegisterEvents"
            ], Com.Wui.Framework.Gui.Interfaces.UserControls.IIcon);
        }();
}

namespace Io.VisionSDK.UserControls.Interfaces {
    "use strict";
    export let IKernelNodeConnector : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "Index",
                "SuperSampleScale",
                "GuiType",
                "Position"
            ], Com.Wui.Framework.Gui.Interfaces.Primitives.IBaseGuiObject);
        }();
}

/* tslint:enable */
