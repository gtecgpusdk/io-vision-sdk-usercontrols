/* tslint:disable:no-reference */
/// <reference path="../../dependencies/com-wui-framework-commons/source/typescript/reference.d.ts" />
/// <reference path="../../dependencies/com-wui-framework-gui/source/typescript/reference.d.ts" />
/// <reference path="../../dependencies/com-wui-framework-usercontrols/source/typescript/reference.d.ts" />

// generated-code-start
/// <reference path="interfacesMap.ts" />
/// <reference path="Io/VisionSDK/UserControls/Interfaces/Events/IConnectorHolderEvents.ts" />
/// <reference path="Io/VisionSDK/UserControls/Interfaces/Events/IConnectorHolderEventsHandler.ts" />
/// <reference path="Io/VisionSDK/UserControls/Interfaces/Events/IKernelNodeEvents.ts" />
/// <reference path="Io/VisionSDK/UserControls/Interfaces/Events/IKernelNodeEventsHandler.ts" />
/// <reference path="Io/VisionSDK/UserControls/Interfaces/Events/IPointerIconEvents.ts" />
/// <reference path="Io/VisionSDK/UserControls/Interfaces/IAreaStrategy.ts" />
/// <reference path="Io/VisionSDK/UserControls/Interfaces/IBounds.ts" />
/// <reference path="Io/VisionSDK/UserControls/Interfaces/IDropDownButton.ts" />
/// <reference path="Io/VisionSDK/UserControls/Interfaces/IGridState.ts" />
/// <reference path="Io/VisionSDK/UserControls/Interfaces/IKernelNodeConnector.ts" />
/// <reference path="Io/VisionSDK/UserControls/Interfaces/IVector.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/Enums/Components/ToolTipType.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/Enums/Components/SelectBoxType.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/Enums/UserControls/KernelNodeType.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/Enums/UserControls/KernelNodeInterfaceType.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/Enums/UserControls/ImageButtonType.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/Enums/UserControls/DropDownListType.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/Enums/UserControls/ImageType.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/Enums/UserControls/InputLabelType.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/Enums/UserControls/NumberPickerType.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/Enums/UserControls/TextFieldType.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/Enums/UserControls/BSPSelectorType.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/Enums/UserControls/ButtonType.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/Enums/UserControls/DialogType.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/Enums/UserControls/KernelNodeButtonType.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/Enums/UserControls/NotificationMessageType.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/Enums/UserControls/PointerIconType.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/Enums/UserControls/ProgressBarType.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/Enums/UserControls/TextAreaType.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/Enums/UserControls/CropBoxType.ts" />
/// <reference path="Io/VisionSDK/UserControls/Enums/ParameterRangeType.ts" />
/// <reference path="Io/VisionSDK/UserControls/Enums/DragNodeType.ts" />
/// <reference path="Io/VisionSDK/UserControls/Enums/ConnectionStatus.ts" />
/// <reference path="Io/VisionSDK/UserControls/Enums/ConnectorHolderStatus.ts" />
/// <reference path="Io/VisionSDK/UserControls/Enums/DropDownButtonType.ts" />
/// <reference path="Io/VisionSDK/UserControls/HttpProcessor/HttpResolver.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/Components/ScrollBar.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/Enums/UserControls/IconType.ts" />
/// <reference path="Io/VisionSDK/UserControls/Enums/Events/ConnectorHolderEventType.ts" />
/// <reference path="Io/VisionSDK/UserControls/Enums/Events/KernelNodeEventType.ts" />
/// <reference path="Io/VisionSDK/UserControls/Enums/InterfaceDataType.ts" />
/// <reference path="Io/VisionSDK/UserControls/Structures/PropertyListFormArgs.ts" />
/// <reference path="Io/VisionSDK/UserControls/Loader.ts" />
/// <reference path="Io/VisionSDK/UserControls/Interfaces/IAreaState.ts" />
/// <reference path="Io/VisionSDK/UserControls/RuntimeTests/UserControlsTestPanelViewer.ts" />
/// <reference path="Io/VisionSDK/UserControls/Structures/PropertyFormArgs.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/Components/SelectBox.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/Components/CropBox.ts" />
/// <reference path="Io/VisionSDK/UserControls/Structures/PropertyPickerFormArgs.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/UserControls/HorizontalPanelHolder.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/UserControls/VerticalPanelHolder.ts" />
/// <reference path="Io/VisionSDK/UserControls/Events/Args/KernelNodeEventArgs.ts" />
/// <reference path="Io/VisionSDK/UserControls/Events/Args/ConnectorHolderEventArgs.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/Viewers/Components/ScrollBarViewer.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/Viewers/Components/CropBoxViewer.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/Viewers/Components/SelectBoxViewer.ts" />
/// <reference path="Io/VisionSDK/UserControls/Interfaces/IKernelNode.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/Viewers/UserControls/HorizontalPanelHolderViewer.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/Viewers/UserControls/VerticalPanelHolderViewer.ts" />
/// <reference path="Io/VisionSDK/UserControls/Events/ElementEventsManager.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/Components/KernelNodeConnector.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/UserControls/DropDownButton.ts" />
/// <reference path="Io/VisionSDK/UserControls/Utils/AreaManager.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/Components/ToolTip.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/UserControls/Accordion.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/Viewers/Components/KernelNodeConnectorViewer.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/UserControls/Label.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/UserControls/CheckBox.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/UserControls/RadioBox.ts" />
/// <reference path="Io/VisionSDK/UserControls/Primitives/AbstractGuiObject.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/UserControls/InputLabel.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/UserControls/Dialog.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/Viewers/UserControls/DropDownButtonViewer.ts" />
/// <reference path="Io/VisionSDK/UserControls/Structures/KernelNodeInterface.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/UserControls/Icon.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/UserControls/ImageButton.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/UserControls/NumberPicker.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/UserControls/DirectoryBrowser.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/UserControls/InformativeButton.ts" />
/// <reference path="Io/VisionSDK/UserControls/Interfaces/IConnection.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/UserControls/DropDownList.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/UserControls/Button.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/Viewers/Components/ToolTipViewer.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/Viewers/UserControls/AccordionViewer.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/Viewers/UserControls/CheckBoxViewer.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/Viewers/UserControls/LabelViewer.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/Viewers/UserControls/RadioBoxViewer.ts" />
/// <reference path="Io/VisionSDK/UserControls/Utils/KernelNodeRegistry.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/UserControls/Image.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/Viewers/UserControls/IconViewer.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/Viewers/UserControls/InformativeButtonViewer.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/Viewers/UserControls/InputLabelViewer.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/UserControls/NotificationMessage.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/Viewers/UserControls/NumberPickerViewer.ts" />
/// <reference path="Io/VisionSDK/UserControls/Primitives/BasePanel.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/UserControls/KernelNodeButton.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/UserControls/TextArea.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/Viewers/UserControls/DropDownListViewer.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/Viewers/UserControls/ImageViewer.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/UserControls/BSPSelector.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/UserControls/ProgressBar.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/Viewers/UserControls/NotificationMessageViewer.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/UserControls/InstallationItem.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/Viewers/UserControls/DialogViewer.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/Viewers/UserControls/ImageButtonViewer.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/Viewers/UserControls/BSPSelectorViewer.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/Viewers/UserControls/ButtonViewer.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/Viewers/UserControls/DirectoryBrowserViewer.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/Viewers/UserControls/InstallationItemViewer.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/Viewers/UserControls/TextAreaViewer.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/Viewers/UserControls/ProgressBarViewer.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/UserControls/PointerIcon.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/Viewers/UserControls/PointerIconViewer.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/UserControls/FormInput.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/UserControls/TextField.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/Viewers/UserControls/FormInputViewer.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/Viewers/UserControls/TextFieldViewer.ts" />
/// <reference path="Io/VisionSDK/UserControls/Utils/GridManager.ts" />
/// <reference path="Io/VisionSDK/UserControls/Utils/ConnectionManager.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/UserControls/KernelNode.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/Viewers/UserControls/KernelNodeViewer.ts" />
/// <reference path="Io/VisionSDK/UserControls/Utils/KernelNodeUtils.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/Viewers/UserControls/KernelNodeButtonViewer.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/UserControls/ConnectorHolder.ts" />
/// <reference path="Io/VisionSDK/UserControls/BaseInterface/Viewers/UserControls/ConnectorHolderViewer.ts" />
/// <reference path="Io/VisionSDK/UserControls/RuntimeTests/UserControlsTestPanel.ts" />
/// <reference path="Io/VisionSDK/UserControls/Index.ts" />
// generated-code-end
