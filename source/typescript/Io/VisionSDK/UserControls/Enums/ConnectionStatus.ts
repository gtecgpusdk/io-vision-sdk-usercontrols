/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.Enums {
    "use strict";

    export class ConnectionStatus extends Com.Wui.Framework.Commons.Primitives.BaseEnum {
        public static readonly INACTIVE : string = "inactive";
        public static readonly ACTIVE : string = "active";
        public static readonly ERROR : string = "error";
    }
}
