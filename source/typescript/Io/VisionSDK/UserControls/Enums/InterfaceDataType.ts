/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.Enums {
    "use strict";

    import BaseEnum = Com.Wui.Framework.Commons.Primitives.BaseEnum;

    export class InterfaceDataType extends BaseEnum {
        public static readonly DELEGATED : string = "Delegated";
    }
}
