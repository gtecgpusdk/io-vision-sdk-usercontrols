/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.Enums {
    "use strict";

    export class ParameterRangeType extends Com.Wui.Framework.Commons.Primitives.BaseEnum {
        public static readonly EXCLUSIVE : string = "Exclusive";
        public static readonly INCLUSIVE : string = "Inclusive";
        public static readonly EXCLUSIVE_MIN_INCLUSIVE_MAX : string = "ExclusiveMinInclusiveMax";
        public static readonly INCLUSIVE_MIN_EXCLUSIVE_MAX : string = "InclusiveMinExclusiveMax";
    }
}
