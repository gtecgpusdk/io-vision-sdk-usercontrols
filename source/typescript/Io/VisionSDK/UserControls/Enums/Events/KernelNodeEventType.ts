/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.Enums.Events {
    "use strict";
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;

    export class KernelNodeEventType extends EventType {
        public static readonly ON_MOVE : string = "OnMove";
        public static readonly BEFORE_REMOVE : string = "BeforeRemove";
        public static readonly ON_CONNECTOR_MOUSE_DOWN : string = "OnConnectorMouseDown";
        public static readonly ON_CONNECTOR_MOUSE_UP : string = "OnConnectorMouseUp";
        public static readonly ON_MOUSE_ENTER : string = "OnMouseEnter";
        public static readonly ON_MOUSE_LEAVE : string = "OnMouseLeave";
        public static readonly ON_CONNECTOR_MOUSE_ENTER : string = "OnConnectorMouseEnter";
        public static readonly ON_CONNECTOR_MOUSE_LEAVE : string = "OnConnectorMouseLeave";
        public static readonly ON_TOOLTIP_CHANGE : string = "OnTooltipChange";
    }
}
