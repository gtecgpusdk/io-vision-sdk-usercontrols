/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.HttpProcessor {
    "use strict";

    export class HttpResolver extends Com.Wui.Framework.UserControls.HttpProcessor.HttpResolver {

        protected getStartupResolvers() : any {
            const resolvers : any = super.getStartupResolvers();
            resolvers["/"] = Io.VisionSDK.UserControls.Index;
            resolvers["/index"] = Io.VisionSDK.UserControls.Index;
            resolvers["/web/"] = Io.VisionSDK.UserControls.Index;
            return resolvers;
        }
    }
}
