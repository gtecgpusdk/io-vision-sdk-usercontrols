/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.BaseInterface.Components {
    "use strict";
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;
    import BaseGuiObject = Com.Wui.Framework.Gui.Primitives.BaseGuiObject;
    import KernelNodeInterfaceType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.KernelNodeInterfaceType;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import GuiObjectManager = Com.Wui.Framework.Gui.GuiObjectManager;
    import IVector = Io.VisionSDK.UserControls.Interfaces.IVector;
    import IKernelNodeConnector = Io.VisionSDK.UserControls.Interfaces.IKernelNodeConnector;

    export class KernelNodeConnector extends BaseGuiObject implements IKernelNodeConnector {
        private index : number;
        private guiType : any;
        private position : IVector;
        private superSampleScale : number;

        constructor($index : number) {
            super();
            this.guiType = KernelNodeInterfaceType.OUTPUT;
            this.index = $index;
            this.position = null;
        }

        public Index($value? : number) : number {
            return this.index = Property.Integer(this.index, $value);
        }

        public SuperSampleScale($value? : number) : number {
            if (ObjectValidator.IsSet($value)) {
                this.superSampleScale = $value;
                const setScale : any = () : void => {
                    if (this.superSampleScale === 1) {
                        ElementManager.ClearCssProperty(this.Id() + "_GuiWrapper", "transform");
                        ElementManager.ClearCssProperty(this.Id(), "transform");
                    } else {
                        ElementManager.setCssProperty(this.Id() + "_GuiWrapper", "transform", "scale(" + 1 / this.superSampleScale + ")");
                        ElementManager.setCssProperty(this.Id(), "transform", "scale(" + this.superSampleScale + ")");
                    }

                };
                if (this.IsCompleted()) {
                    setScale();
                } else {
                    this.getEvents().setOnComplete(() : void => {
                        setScale();
                    });
                }
            }
            return this.superSampleScale;
        }

        /**
         * @param {KernelNodeInterfaceType} [$kernelNodeInterfaceType] Specify type of element look and feel.
         * @return {KernelNodeInterfaceType} Returns type of element's look and feel.
         */
        public GuiType($kernelNodeInterfaceType? : KernelNodeInterfaceType) : KernelNodeInterfaceType {
            if (ObjectValidator.IsSet($kernelNodeInterfaceType)) {
                this.guiType = this.guiTypeValueSetter($kernelNodeInterfaceType);
                if (ElementManager.IsVisible(this.Id())) {
                    ElementManager.setClassName(this.Id() + "_Type", this.guiType.toString());
                }
            }
            return this.guiType;
        }

        public Position($value? : IVector) : IVector {
            if (ObjectValidator.IsSet($value)) {
                this.position = $value;
                let thisElement : HTMLElement = ElementManager.getElement(this.Id() + "_GuiWrapper");
                if (!ObjectValidator.IsEmptyOrNull(thisElement)) {
                    thisElement = thisElement.parentElement;
                    const xInt : number = Math.floor($value.x);
                    const yInt : number = Math.floor($value.y);
                    thisElement.style.left = xInt + "px";
                    thisElement.style.top = yInt + "px";
                    if (!ObjectValidator.IsEmptyOrNull(this.superSampleScale) && this.superSampleScale !== 1) {
                        ElementManager.setCssProperty(this.Id(), "top", ($value.y - yInt) * this.superSampleScale + "px");
                        ElementManager.setCssProperty(this.Id(), "left", ($value.x - xInt) * this.superSampleScale + "px");
                    } else {
                        ElementManager.ClearCssProperty(this.Id(), "top");
                        ElementManager.ClearCssProperty(this.Id(), "left");
                    }
                }
            }
            return this.position;
        }

        protected guiTypeValueSetter($value : any) : any {
            return Property.EnumType(this.guiType, $value, KernelNodeInterfaceType);
        }

        protected innerCode() : IGuiElement {
            let isMouseOver : boolean = false;
            this.getEvents().setOnMouseOver(($eventArgs : EventArgs, $manager : GuiObjectManager) : void => {
                $manager.setActive(this, true);
                isMouseOver = true;
            });

            this.getEvents().setOnMouseOut(($eventArgs : EventArgs, $manager : GuiObjectManager) : void => {
                isMouseOver = false;
                this.getEventsManager().FireAsynchronousMethod(() : void => {
                    if (!isMouseOver) {
                        $manager.setActive(this, false);
                    }
                }, 10);
            });

            this.getEvents().setOnMouseDown(($eventArgs : EventArgs, $manager : GuiObjectManager) : void => {
                this.getEventsManager().FireAsynchronousMethod(() : void => {
                    $manager.setActive(this, false);
                }, 10);
            });

            return super.innerCode();
        }

        protected innerHtml() : IGuiElement {
            return this.addElement().Add(this.addElement(this.Id() + "_Type")
                .GuiTypeTag("KernelNodeConnector").StyleClassName(this.GuiType().toString()));
        }

        protected cssContainerName() : string {
            return "KernelNodeConnector";
        }
    }
}
