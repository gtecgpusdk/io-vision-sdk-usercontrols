/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.BaseInterface.Components {
    "use strict";
    import ToolTipType = Io.VisionSDK.UserControls.BaseInterface.Enums.Components.ToolTipType;
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import WindowManager = Com.Wui.Framework.Gui.Utils.WindowManager;
    import MoveEventArgs = Com.Wui.Framework.Gui.Events.Args.MoveEventArgs;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import GuiObjectManager = Com.Wui.Framework.Gui.GuiObjectManager;
    import IGuiCommons = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiCommons;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ValueProgressManager = Com.Wui.Framework.Gui.Utils.ValueProgressManager;
    import ValueProgressEventArgs = Com.Wui.Framework.Gui.Events.Args.ValueProgressEventArgs;
    import Reflection = Com.Wui.Framework.Commons.Utils.Reflection;

    export class ToolTip extends Com.Wui.Framework.UserControls.BaseInterface.Components.ToolTip {
        private static globalInstance : ToolTip;
        private static lastMove : MoveEventArgs;
        private static isHoveredCallback : any;
        private static showHandle : number;

        public static IsGlobalEnabled() : boolean {
            return !ObjectValidator.IsEmptyOrNull(ToolTip.globalInstance);
        }

        public static ShowGlobal($text? : string, $timeout : number = 500, $isHoveredCallback? : () => boolean) : void {
            if (ToolTip.IsGlobalEnabled()) {
                ToolTip.globalInstance.getEventsManager().FireAsynchronousMethod(() : void => {
                    const manipulatorArgs : ValueProgressEventArgs = ValueProgressManager.get(ToolTip.globalInstance.Id());
                    const currOpacity : number = ObjectValidator.IsEmptyOrNull(manipulatorArgs) ? 0 : manipulatorArgs.CurrentValue();
                    const hovered : IGuiCommons = GuiObjectManager.getInstanceSingleton().getHovered();
                    const isHoveredCallback : any = ObjectValidator.IsSet($isHoveredCallback) ? $isHoveredCallback : () => {
                        const currHovered : IGuiCommons = GuiObjectManager.getInstanceSingleton().getHovered();
                        return !ObjectValidator.IsEmptyOrNull(hovered) &&
                            !ObjectValidator.IsEmptyOrNull(currHovered) &&
                            hovered.Id() === currHovered.Id();
                    };

                    if (currOpacity === 0) {
                        if (!ObjectValidator.IsEmptyOrNull(hovered)) {
                            ToolTip.showHandle = ToolTip.globalInstance.getEventsManager().FireAsynchronousMethod(() : void => {
                                if (isHoveredCallback()) {
                                    ToolTip.isHoveredCallback = isHoveredCallback;
                                    ToolTip.Show(ToolTip.globalInstance);
                                    ToolTip.globalInstance.Text($text);
                                    ToolTip.Move(ToolTip.globalInstance, ToolTip.lastMove);
                                }
                            }, true, $timeout);
                        }
                    } else {
                        const tick : number = 50;
                        ToolTip.globalInstance.getEventsManager().FireAsynchronousMethod(() : void => {
                            ToolTip.ShowGlobal($text, Math.max(0, $timeout - tick), isHoveredCallback);
                        }, true, tick);
                    }
                }, true);
            } else {
                LogIt.Error("ToolTip global instance doesn't exist.");
            }
        }

        public static HideGlobal($force? : boolean) : void {
            if (ToolTip.IsGlobalEnabled()) {
                if ($force) {
                    clearInterval(ToolTip.showHandle);
                }
                ElementManager.StopOpacityChange(ToolTip.globalInstance);
                ToolTip.isHoveredCallback = () : boolean => {
                    return false;
                };
                ToolTip.Hide(ToolTip.globalInstance);
            }
        }

        public static getInstanceSingleton() : ToolTip {
            if (!ObjectValidator.IsSet(ToolTip.globalInstance)) {
                const tooltipClass : any = Reflection.getInstance().getClass(this.ClassName());
                ToolTip.globalInstance = new tooltipClass();
                ToolTip.globalInstance.Enabled(true);
                WindowManager.getEvents().setOnMove(($eventArgs : MoveEventArgs) : void => {
                    if (ToolTip.IsVisible(ToolTip.globalInstance)) {
                        if (ToolTip.isHoveredCallback()) {
                            ToolTip.Move(ToolTip.globalInstance, $eventArgs);
                        } else {
                            ToolTip.HideGlobal();
                            ToolTip.isHoveredCallback = () : boolean => {
                                return false;
                            };
                        }
                    }
                    ToolTip.lastMove = $eventArgs;
                });
            }
            ToolTip.isHoveredCallback = () : boolean => {
                return false;
            };
            return ToolTip.globalInstance;
        }

        private static IsVisible($instance : ToolTip) : boolean {
            return ElementManager.getCssIntegerValue($instance.Id(), "z-index") > 0;
        }

        constructor($text? : string, $toolTipType? : ToolTipType, $id? : string) {
            super($text, $toolTipType, $id);
        }

        /**
         * @param {ToolTipType} [$toolTipType] Specify type of element's look and feel.
         * @return {ToolTipType} Returns type of element's look and feel.
         */
        public GuiType($toolTipType? : ToolTipType) : ToolTipType {
            return <ToolTipType>super.GuiType($toolTipType);
        }

        protected guiTypeValueSetter($value : any) : any {
            return Property.EnumType(this.GuiType(), $value, ToolTipType, ToolTipType.GENERAL);
        }

        protected styleClassNameSetterValidator($value : string) : boolean {
            if (!ToolTipType.Contains($value)) {
                return super.styleClassNameSetterValidator($value);
            }
            Echo.Println("Use GuiType method for set of tooltip type instead of StyleClassName method.");
            return false;
        }
    }
}
