/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.BaseInterface.Components {
    "use strict";
    import SelectBoxType = Io.VisionSDK.UserControls.BaseInterface.Enums.Components.SelectBoxType;
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;

    export class SelectBox extends Com.Wui.Framework.UserControls.BaseInterface.Components.SelectBox {

        constructor($selectBoxType? : SelectBoxType, $id? : string) {
            super($selectBoxType, $id);
        }

        /**
         * @param {SelectBoxType} [$selectBoxType] Specify type of element's look and feel.
         * @return {SelectBoxType} Returns type of element's look and feel.
         */
        public GuiType($selectBoxType? : SelectBoxType) : SelectBoxType {
            return <SelectBoxType>super.GuiType($selectBoxType);
        }

        protected guiTypeValueSetter($value : any) : any {
            return Property.EnumType(this.GuiType(), $value, SelectBoxType, SelectBoxType.GENERAL);
        }

        protected styleClassNameSetterValidator($value : string) : boolean {
            if (!SelectBoxType.Contains($value)) {
                return super.styleClassNameSetterValidator($value);
            }
            Echo.Println("Use GuiType method for set of select box type instead of StyleClassName method.");
            return false;
        }

        protected getScrollBarClass() : any {
            return ScrollBar;
        }
    }
}
