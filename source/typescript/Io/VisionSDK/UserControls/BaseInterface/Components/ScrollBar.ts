/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.BaseInterface.Components {
    "use strict";
    import OrientationType = Com.Wui.Framework.Gui.Enums.OrientationType;

    export class ScrollBar extends Com.Wui.Framework.UserControls.BaseInterface.Components.ScrollBar {

        constructor($orientationType : OrientationType, $id? : string) {
            super($orientationType, $id);
        }
    }
}
