/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.BaseInterface.Components {
    "use strict";
    import IGuiCommons = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiCommons;
    import CropBoxType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.CropBoxType;
    import Property = Com.Wui.Framework.Commons.Utils.Property;

    export class CropBox extends Com.Wui.Framework.UserControls.BaseInterface.Components.CropBox {

        constructor($cropBoxType? : CropBoxType, $owner? : string | IGuiCommons, $id? : string) {
            super($cropBoxType, $owner, $id);
        }

        protected guiTypeValueSetter($value : any) : any {
            return Property.EnumType(this.GuiType(), $value, CropBoxType, CropBoxType.GREEN);
        }
    }
}
