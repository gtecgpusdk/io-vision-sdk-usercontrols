/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.BaseInterface.UserControls {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import ToolTip = Io.VisionSDK.UserControls.BaseInterface.Components.ToolTip;
    import ScrollBar = Io.VisionSDK.UserControls.BaseInterface.Components.ScrollBar;

    export class DirectoryBrowser extends Com.Wui.Framework.UserControls.BaseInterface.UserControls.DirectoryBrowser {

        /**
         * @return {IToolTip} This method should return class with
         * interface Com.Wui.Framework.Gui.Interfaces.Components.IToolTip
         */
        protected getTitleClass() : any {
            return ToolTip;
        }

        /**
         * @return {IScrollBar} This method should return class with
         * interface Com.Wui.Framework.Gui.Interfaces.Components.IScrollBar
         */
        protected getScrollBarClass() : any {
            return ScrollBar;
        }
    }
}
