/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.BaseInterface.UserControls {
    "use strict";
    import ButtonType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.ButtonType;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import ToolTip = Io.VisionSDK.UserControls.BaseInterface.Components.ToolTip;
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import GuiObjectManager = Com.Wui.Framework.Gui.GuiObjectManager;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;

    export class Button extends Com.Wui.Framework.UserControls.BaseInterface.UserControls.Button {
        private selected : boolean;

        public static TurnOn($element : Button, $manager : GuiObjectManager) : void {
            if ($element.Enabled() && !$manager.IsActive($element) && !$element.Selected()) {
                ElementManager.TurnOn($element.Id() + "_Status");
            }
        }

        public static TurnOff($element : Button, $manager : GuiObjectManager) : void {
            if ($element.Enabled() && !$manager.IsActive($element) && !$element.Selected()) {
                ElementManager.TurnOff($element.Id() + "_Status");
            }
        }

        public static TurnActive($element : Button, $manager : GuiObjectManager) : void {
            if ($element.Enabled()) {
                ElementManager.TurnActive($element.Id() + "_Status");
            }
        }

        constructor($buttonType? : ButtonType, $id? : string) {
            super($buttonType, $id);
        }

        public Selected($value? : boolean) : boolean {
            if (ObjectValidator.IsSet($value)) {
                this.selected = $value;
                if (this.selected) {
                    Button.TurnActive(this, GuiObjectManager.getInstanceSingleton());
                } else {
                    Button.TurnOff(this, GuiObjectManager.getInstanceSingleton());
                }
            }
            return this.selected;
        }

        /**
         * @param {ButtonType} [$buttonType] Specify type of element look and feel.
         * @return {ButtonType} Returns type of element's look and feel.
         */
        public GuiType($buttonType? : ButtonType) : ButtonType {
            return <ButtonType>super.GuiType($buttonType);
        }

        /**
         * @return {IToolTip} This method should return class with
         * interface Com.Wui.Framework.Gui.Interfaces.Components.IToolTip
         */
        protected getTitleClass() : any {
            return ToolTip;
        }

        protected guiTypeValueSetter($value : any) : any {
            return Property.EnumType(this.GuiType(), $value, ButtonType, ButtonType.WHITE_MEDIUM);
        }

        protected innerCode() : IGuiElement {
            this.getEvents().setOnMouseOver(($eventArgs : EventArgs, $manager : GuiObjectManager) => {
                if (ToolTip.IsGlobalEnabled()) {
                    if (ElementManager.getElement(this.Id() + "_Text").scrollWidth
                        > ElementManager.getElement(this.Id() + "_Text").offsetWidth) {
                        $eventArgs.StopAllPropagation();
                        $manager.setHovered(this, true);
                        ToolTip.ShowGlobal(this.Text());
                    }
                }
            });

            this.getEvents().setOnMouseOut(() : void => {
                ToolTip.HideGlobal();
            });

            return super.innerCode();
        }

        protected styleClassNameSetterValidator($value : string) : boolean {
            if (!ButtonType.Contains($value)) {
                return super.styleClassNameSetterValidator($value);
            }
            Echo.Println("Use GuiType method for set of button type instead of StyleClassName method.");
            return false;
        }
    }
}
