/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.BaseInterface.UserControls {
    "use strict";
    import TextFieldType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.TextFieldType;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import ToolTip = Io.VisionSDK.UserControls.BaseInterface.Components.ToolTip;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import SelectBox = Io.VisionSDK.UserControls.BaseInterface.Components.SelectBox;
    import SelectBoxType = Io.VisionSDK.UserControls.BaseInterface.Enums.Components.SelectBoxType;
    import IconType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.IconType;
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;
    import Reflection = Com.Wui.Framework.Commons.Utils.Reflection;
    import GuiObjectManager = Com.Wui.Framework.Gui.GuiObjectManager;
    import FormsObject = Com.Wui.Framework.UserControls.Primitives.FormsObject;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import MouseEventArgs = Com.Wui.Framework.Gui.Events.Args.MouseEventArgs;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ElementEventsManager = Io.VisionSDK.UserControls.Events.ElementEventsManager;

    export class TextField extends Com.Wui.Framework.UserControls.BaseInterface.UserControls.TextField {

        /**
         * @param {FormsObject} $element Specify element, which should be handled.
         * @param {GuiObjectManager} [$manager] Specify instance of GuiObjectManager.
         * @param {Reflection} [$reflection] Specify instance of Reflection.
         * @return {void}
         */
        public static TurnActive($element : FormsObject, $manager? : GuiObjectManager, $reflection? : Reflection) : void {
            if (!($element.Error() || ElementManager.getClassName($element.Id() + "_Status") === "ReadOnly")) {
                ElementManager.TurnActive($element.Id() + "_Status");
            }
        }

        protected static onHoverEventHandler($eventArgs : MouseEventArgs, $manager : GuiObjectManager) : void {
            // enabling propagation
        }

        constructor($textFieldType? : TextFieldType, $id? : string) {
            super($textFieldType, $id);
            this.GuiType($textFieldType);
        }

        /**
         * @param {TextFieldType} [$textFieldType] Specify type of element look and feel.
         * @return {TextFieldType} Returns type of element's look and feel.
         */
        public GuiType($textFieldType? : TextFieldType) : TextFieldType {
            if (ObjectValidator.IsSet($textFieldType)) {
                const type : TextFieldType = <TextFieldType>super.GuiType($textFieldType);
                switch (type) {
                case TextFieldType.GENERAL:
                    this.autocompleteBox.GuiType(SelectBoxType.GENERAL);
                    break;
                default:
                    break;
                }
                return type;
            }
            return <TextFieldType>super.GuiType();
        }

        protected innerCode() : IGuiElement {
            this.getEvents().setOnChange(($eventArgs : EventArgs, $manager : GuiObjectManager) => {
                const thisClass : any = Reflection.getInstance().getClass(this.getClassName());
                thisClass.setErrorStyle(this);
                if ($manager.IsActive(this)) {
                    thisClass.TurnActive(this);
                }
            });

            const handleTooltip : any = ($eventArgs : EventArgs, $manager : GuiObjectManager) : void => {
                if (ToolTip.IsGlobalEnabled()) {
                    const inputId : string = this.Id() + (this.Enabled() ? "_Input" : "_DisabledInput");
                    if (ElementManager.getElement(inputId).scrollWidth
                        > ElementManager.getElement(inputId).offsetWidth) {
                        $eventArgs.StopAllPropagation();
                        $manager.setHovered(this, true);
                        ToolTip.ShowGlobal(this.Value());
                    }
                }
            };

            this.getEvents().setOnMouseOver(($eventArgs : EventArgs, $manager : GuiObjectManager) : void => {
                handleTooltip($eventArgs, $manager);
            });
            const manager : ElementEventsManager = new ElementEventsManager(this.Id() + "_Disabled");
            manager.setOnMouseOver(($eventArgs : EventArgs, $manager : GuiObjectManager) : void => {
                handleTooltip($eventArgs, $manager);
            });
            this.getEvents().setOnComplete(() : void => {
                manager.Subscribe();
            });

            return super.innerCode();
        }

        /**
         * @return {IToolTip} This method should return class with
         * interface Com.Wui.Framework.Gui.Interfaces.Components.IToolTip
         */
        protected getTitleClass() : any {
            return ToolTip;
        }

        protected guiTypeValueSetter($value : any) : any {
            return Property.EnumType(this.GuiType(), $value, TextFieldType, TextFieldType.GENERAL);
        }

        protected styleClassNameSetterValidator($value : string) : boolean {
            if (!TextFieldType.Contains($value)) {
                return super.styleClassNameSetterValidator($value);
            }
            Echo.Println("Use GuiType method for set of textField type instead of StyleClassName method.");
            return false;
        }

        /**
         * @return {ISelectBox} This method should return class with
         * interface Com.Wui.Framework.Gui.Interfaces.Components.ISelectBox
         */
        protected getSelectBoxClass() : any {
            return SelectBox;
        }

        protected getAutocompleteIconName() : IconType {
            return IconType.SPINNER_GRAPHENE_SMALL;
        }
    }
}
