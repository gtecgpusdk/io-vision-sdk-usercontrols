/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.BaseInterface.UserControls {
    "use strict";
    import BasePanelViewer = Com.Wui.Framework.Gui.Primitives.BasePanelViewer;
    import BasePanelViewerArgs = Com.Wui.Framework.Gui.Primitives.BasePanelViewerArgs;
    import ScrollBar = Io.VisionSDK.UserControls.BaseInterface.Components.ScrollBar;

    /**
     * HorizontalPanelHolder class renders BasePanel holder, which is suitable for external manipulation at horizontal orientation.
     */
    export class HorizontalPanelHolder extends Com.Wui.Framework.UserControls.BaseInterface.UserControls.HorizontalPanelHolder {

        /**
         * @param {BasePanelViewer} $bodyClass Specify class name of panel viewer, which should be handled.
         * @param {BasePanelViewerArgs} [$args] Specify arguments for handled panel object viewer.
         * @param {HorizontalPanelHolderType} [$holderType] Specify type of element look and feel.
         * @param {string} [$id] Force set element id instead of generated one.
         */
        constructor($bodyClass : BasePanelViewer, $args? : BasePanelViewerArgs, $holderType? : any, $id? : string) {
            super($bodyClass, $args, $holderType, $id);
            this.minWidth = 20;
            this.minHeight = 20;
            this.Width(this.minWidth);
        }

        /**
         * @return {IScrollBar} This method should return class with
         * interface Com.Wui.Framework.Gui.Interfaces.Components.IScrollBar
         */
        protected getScrollBarClass() : any {
            return ScrollBar;
        }

        /**
         * @return {ILabel} This method should return class with
         * interface Com.Wui.Framework.Gui.Interfaces.UserControls.ILabel
         */
        protected getLabelClass() : any {
            return Label;
        }

        /**
         * @return {IImageButton} This method should return class with
         * interface Com.Wui.Framework.Gui.Interfaces.UserControls.IImageButton
         */
        protected getImageButtonClass() : any {
            return ImageButton;
        }
    }
}
