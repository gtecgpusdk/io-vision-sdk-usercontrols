/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.BaseInterface.UserControls {
    "use strict";
    import DialogType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.DialogType;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import ToolTip = Io.VisionSDK.UserControls.BaseInterface.Components.ToolTip;

    /**
     * Dialog class renders floating panel container with modal or modeless behaviour.
     */
    export class Dialog extends Com.Wui.Framework.UserControls.BaseInterface.UserControls.Dialog {

        /**
         * @param {DialogType} $dialogType Specify type of element look and feel.
         * @param {string} [$id] Force set element id instead of generated one.
         */
        constructor($dialogType : DialogType, $id? : string) {
            super($dialogType, $id);

            this.headerIcon.Visible(false);
            this.headerText.Visible(false);
            this.closeButton.Visible(false);
        }

        /**
         * @param {DialogType} [$dialogType] Specify type of element look and feel.
         * @return {DialogType} Returns type of element's look and feel.
         */
        public GuiType($dialogType? : DialogType) : DialogType {
            return <DialogType>super.GuiType($dialogType);
        }

        protected guiTypeValueSetter($value : any) : any {
            return Property.EnumType(this.GuiType(), $value, DialogType, DialogType.BLUE);
        }

        protected styleClassNameSetterValidator($value : string) : boolean {
            if (!DialogType.Contains($value)) {
                return super.styleClassNameSetterValidator($value);
            }
            Echo.Println("Use GuiType method for set of Dialog type instead of StyleClassName method.");
            return false;
        }

        /**
         * @return {IToolTip} This method should return class with
         * interface Com.Wui.Framework.Gui.Interfaces.Components.IToolTip
         */
        protected getTitleClass() : any {
            return ToolTip;
        }

        /**
         * @return {IImageButton} This method should return class with
         * interface Com.Wui.Framework.Gui.Interfaces.Components.IImageButton
         */
        protected getCloseButtonClass() : any {
            return ImageButton;
        }

        /**
         * @return {IIcon} This method should return class with
         * interface Com.Wui.Framework.Gui.Interfaces.Components.IIcon
         */
        protected getHeaderIconClass() : any {
            return Icon;
        }

        /**
         * @return {ILabel} This method should return class with
         * interface Com.Wui.Framework.Gui.Interfaces.Components.ILabel
         */
        protected getHeaderLabelClass() : any {
            return Label;
        }
    }
}
