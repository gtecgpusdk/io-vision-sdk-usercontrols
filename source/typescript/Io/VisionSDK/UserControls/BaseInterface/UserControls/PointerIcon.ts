/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.BaseInterface.UserControls {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import ToolTip = Io.VisionSDK.UserControls.BaseInterface.Components.ToolTip;
    import PointerIconType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.PointerIconType;
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;
    import GuiOptionType = Com.Wui.Framework.Gui.Enums.GuiOptionType;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import ElementOffset = Com.Wui.Framework.Gui.Structures.ElementOffset;
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;
    import PositionType = Com.Wui.Framework.Gui.Enums.PositionType;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import Size = Com.Wui.Framework.Gui.Structures.Size;
    import ValueProgressEventArgs = Com.Wui.Framework.Gui.Events.Args.ValueProgressEventArgs;
    import ValueProgressManager = Com.Wui.Framework.Gui.Utils.ValueProgressManager;
    import DirectionType = Com.Wui.Framework.Gui.Enums.DirectionType;
    import ProgressType = Com.Wui.Framework.Gui.Enums.ProgressType;
    import IPointerIconEvents = Io.VisionSDK.UserControls.Interfaces.Events.IPointerIconEvents;
    import IMouseEventsHandler = Com.Wui.Framework.Gui.Interfaces.Events.IMouseEventsHandler;

    export class PointerIcon extends Icon {
        private lastFired : number;

        constructor($id? : string) {
            super(PointerIconType.OFF, $id);
        }

        /**
         * @param {PointerIconType} [$type] Specify type of element look and feel.
         * @return {PointerIconType} Returns type of element's look and feel.
         */
        public IconType($type? : PointerIconType) : PointerIconType {
            return <PointerIconType>super.IconType($type);
        }

        public getEvents() : IPointerIconEvents {
            const events : IPointerIconEvents = <IPointerIconEvents>super.getEvents();
            events.setOnClick = ($handler : IMouseEventsHandler) : void => {
                this.getEventsManager().setEvent(this, EventType.ON_CLICK, $handler);
            };
            events.setOnDoubleClick = ($handler : IMouseEventsHandler) : void => {
                this.getEventsManager().setEvent(this, EventType.ON_DOUBLE_CLICK, $handler);
            };
            events.setOnDrag = ($handler : IMouseEventsHandler) : void => {
                this.getEventsManager().setEvent(this, EventType.ON_DRAG, $handler);
            };
            events.setOnMove = ($handler : IMouseEventsHandler) : void => {
                this.getEventsManager().setEvent(this, EventType.ON_MOUSE_MOVE, $handler);
            };
            this.getEvents = () : IPointerIconEvents => {
                return events;
            };
            return events;
        }

        public Click($text? : string) : void {
            this.Visible(true);
            this.setTitle($text);
            this.IconType(PointerIconType.ON);
            this.ReleaseEvents();
            this.lastFired = this.getEventsManager().FireAsynchronousMethod(() : void => {
                this.Restore();
                this.getEventsManager().FireEvent(this, EventType.ON_CLICK);
            }, 200);
        }

        public DoubleClick($text? : string) : void {
            this.Visible(true);
            this.setTitle($text);
            this.IconType(PointerIconType.ON);
            this.ReleaseEvents();
            this.lastFired = this.getEventsManager().FireAsynchronousMethod(() : void => {
                this.IconType(PointerIconType.OFF);
                this.lastFired = this.getEventsManager().FireAsynchronousMethod(() : void => {
                    this.IconType(PointerIconType.ON);
                    this.lastFired = this.getEventsManager().FireAsynchronousMethod(() : void => {
                        this.Restore();
                        this.getEventsManager().FireEvent(this, EventType.ON_DOUBLE_CLICK);
                    }, 100);
                }, 100);
            }, 100);
        }

        public Drag($text? : string) : void {
            this.Visible(true);
            this.setTitle($text);
            this.IconType(PointerIconType.ACTIVCE);
            this.getEventsManager().FireEvent(this, EventType.ON_DRAG);
        }

        public Restore() : void {
            this.IconType(PointerIconType.OFF);
            this.ReleaseEvents();
            this.lastFired = this.getEventsManager().FireAsynchronousMethod(() : void => {
                this.Title().Visible(false);
            }, 300);
        }

        public MoveTo($dX : number, $dY : number,
                      $animationMath? : ($current : number, $end? : number, $dX? : number, $dY? : number) => number) : void {
            const element : HTMLElement = ElementManager.getElement(this.guiContentId());
            const size : Size = new Size(this.Id() + "_Type", true);
            const position : ElementOffset = new ElementOffset(element.offsetTop, element.offsetLeft);
            const manipulatorArgs : ValueProgressEventArgs = ValueProgressManager.get(this.Id() + "MoveTo");
            manipulatorArgs.DirectionType(DirectionType.UP);
            manipulatorArgs.ProgressType(ProgressType.LINEAR);
            manipulatorArgs.RangeStart(0);
            manipulatorArgs.RangeEnd(100);
            manipulatorArgs.CurrentValue(0);
            manipulatorArgs.Step(1);
            manipulatorArgs.ChangeEventType(EventType.ON_CHANGE);
            manipulatorArgs.CompleteEventType(EventType.ON_COMPLETE);

            let calculation : any = ($current : number, $end? : number, $dX? : number, $dY? : number) : number => {
                return $current / $end;
            };
            if (!ObjectValidator.IsEmptyOrNull($animationMath)) {
                calculation = $animationMath;
            }
            this.getEventsManager().setEvent(this.Id() + "MoveTo", EventType.ON_CHANGE,
                ($eventArgs : ValueProgressEventArgs) : void => {
                    const scale : number = calculation($eventArgs.CurrentValue(), $eventArgs.RangeEnd(), $dX, $dY);
                    this.setPosition(position.Top() + size.Height() / 2 + $dY * scale, position.Left() + size.Width() / 2 + $dX * scale);
                });

            this.getEventsManager().setEvent(this.Id() + "MoveTo", EventType.ON_COMPLETE, () : void => {
                this.getEventsManager().FireEvent(this, EventType.ON_MOUSE_MOVE);
            });

            ValueProgressManager.Execute(manipulatorArgs);
        }

        public ReleaseEvents() : void {
            if (!ObjectValidator.IsEmptyOrNull(this.lastFired)) {
                clearTimeout(this.lastFired);
            }
            ValueProgressManager.Remove(this.Id() + "MoveTo");
        }

        public setPosition($top : number, $left : number) : void {
            const size : Size = new Size(this.Id() + "_Type", true);
            super.setPosition($top - size.Height() / 2, $left - size.Width() / 2, PositionType.FIXED);
        }

        /**
         * @return {IToolTip} This method should return class with
         * interface Com.Wui.Framework.Gui.Interfaces.Components.IToolTip
         */
        protected getTitleClass() : any {
            return ToolTip;
        }

        protected iconTypeValueSetter($value : any) : any {
            return Property.EnumType(this.IconType(), $value, PointerIconType, PointerIconType.OFF);
        }

        protected styleClassNameSetterValidator($value : string) : boolean {
            if (!PointerIconType.Contains($value)) {
                return super.styleClassNameSetterValidator($value);
            }
            Echo.Println("Use PointerIconType method for set of icon type instead of StyleClassName method.");
            return false;
        }

        protected guiContentId() : string {
            return this.Id() + "_GuiWrapper";
        }

        protected innerCode() : IGuiElement {
            this.getGuiOptions().Add(GuiOptionType.TOOL_TIP);
            this.Visible(false);

            return super.innerCode();
        }

        protected cssContainerName() : string {
            return this.getClassNameWithoutNamespace();
        }

        private setTitle($text : string) : void {
            if (!ObjectValidator.IsEmptyOrNull($text)) {
                this.Title().Text($text);
                this.Title().Visible(true);
            } else {
                this.Title().Visible(false);
            }
        }
    }
}
