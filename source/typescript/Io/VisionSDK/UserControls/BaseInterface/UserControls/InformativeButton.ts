/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.BaseInterface.UserControls {
    "use strict";
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import ToolTip = Io.VisionSDK.UserControls.BaseInterface.Components.ToolTip;
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import GuiObjectManager = Com.Wui.Framework.Gui.GuiObjectManager;
    import GeneralCssNames = Com.Wui.Framework.Gui.Enums.GeneralCssNames;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;

    export class InformativeButton extends Com.Wui.Framework.UserControls.Primitives.FormsObject {
        private text : string;
        private description : string;
        private selected : boolean;

        public static TurnOn($element : InformativeButton, $manager : GuiObjectManager) : void {
            if ($element.Enabled() && !$manager.IsActive($element)) {
                ElementManager.TurnOn($element.Id() + "_Status");
            }
        }

        public static TurnOff($element : InformativeButton, $manager : GuiObjectManager) : void {
            if (!$element.selected && $element.Enabled() && !$manager.IsActive($element)) {
                ElementManager.TurnOff($element.Id() + "_Status");
            }
        }

        public static TurnActive($element : InformativeButton) : void {
            if (!$element.selected && $element.Enabled()) {
                ElementManager.TurnActive($element.Id() + "_Status");
            }
        }

        constructor($id? : string) {
            super($id);
            this.text = "";
            this.description = "";
        }

        /**
         * @param {string} [$value] Specify text, which should be displayed as item value.
         * @return {string} Returns element's text value.
         */
        public Text($value? : string) : string {
            this.text = Property.String(this.text, $value);
            if (this.IsCompleted() && ObjectValidator.IsSet($value)) {
                ElementManager.setInnerHtml(this.Id() + "_" + GeneralCssNames.TEXT, this.text);
            }
            return this.text;
        }

        public Selected($value? : boolean) : boolean {
            this.selected = Property.Boolean(this.selected, $value);
            if ($value) {
                InformativeButton.TurnOn(this, GuiObjectManager.getInstanceSingleton());
            } else if (ObjectValidator.IsSet($value)) {
                InformativeButton.TurnOff(this, GuiObjectManager.getInstanceSingleton());
            }
            return this.selected;
        }

        /**
         * @param {string} [$value] Specify text, which should be displayed under item's value as a description.
         * @return {string} Returns element's text value.
         */
        public Description($value? : string) : string {
            this.description = Property.String(this.description, $value);
            if (this.IsCompleted() && ObjectValidator.IsSet($value)) {
                ElementManager.setInnerHtml(this.Id() + "_" + GeneralCssNames.TEXT, this.description);
            }
            return this.description;
        }

        protected innerCode() : IGuiElement {
            return super.innerCode();
        }

        protected innerHtml() : IGuiElement {
            return this.addElement(this.Id() + "_Status").StyleClassName(this.statusCss())
                .Add(this.addElement(this.Id() + "_Body").StyleClassName("BodyEnvelop")
                    .Add(this.addElement().StyleClassName("Body")
                        .Add(this.addElement(this.Id() + "_" + GeneralCssNames.TEXT).StyleClassName(GeneralCssNames.TEXT)
                            .Add(this.Text())
                        )
                        .Add(this.addElement().StyleClassName("Description")
                            .Add(this.Description())
                        )
                    )
                ).Add(this.addElement().setAttribute("clear", "both"));
        }

        protected getTitleClass() : any {
            return ToolTip;
        }
    }
}
