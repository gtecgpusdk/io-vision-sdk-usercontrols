/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.BaseInterface.UserControls {
    "use strict";
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import GeneralCssNames = Com.Wui.Framework.Gui.Enums.GeneralCssNames;
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import ElementOffset = Com.Wui.Framework.Gui.Structures.ElementOffset;
    import MouseEventArgs = Com.Wui.Framework.Gui.Events.Args.MouseEventArgs;
    import GuiOptionType = Com.Wui.Framework.Gui.Enums.GuiOptionType;
    import ElementEventsManager = Io.VisionSDK.UserControls.Events.ElementEventsManager;
    import KernelNodeEventType = Io.VisionSDK.UserControls.Enums.Events.KernelNodeEventType;
    import IKernelNodeEvents = Io.VisionSDK.UserControls.Interfaces.Events.IKernelNodeEvents;
    import KernelNodeInterfaceType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.KernelNodeInterfaceType;
    import KernelNodeEventArgs = Io.VisionSDK.UserControls.Events.Args.KernelNodeEventArgs;
    import GuiObjectManager = Com.Wui.Framework.Gui.GuiObjectManager;
    import KernelNodeType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.KernelNodeType;
    import KernelNodeConnector = Io.VisionSDK.UserControls.BaseInterface.Components.KernelNodeConnector;
    import FormsObject = Com.Wui.Framework.UserControls.Primitives.FormsObject;
    import Size = Com.Wui.Framework.Gui.Structures.Size;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import IKernelNodeNotifications = Io.VisionSDK.UserControls.Interfaces.IKernelNodeNotifications;
    import Output = Io.VisionSDK.UserControls.Interfaces.Output;
    import Input = Io.VisionSDK.UserControls.Interfaces.Input;
    import IKernelNodeAttribute = Io.VisionSDK.UserControls.Interfaces.IKernelNodeAttribute;
    import IKernelNodeHelp = Io.VisionSDK.UserControls.Interfaces.IKernelNodeHelp;
    import KernelNodeInterface = Io.VisionSDK.UserControls.Structures.KernelNodeInterface;
    import IConnectorVisibilityArgs = Io.VisionSDK.UserControls.Interfaces.IConnectorVisibilityArgs;
    import IConnectionValidatorStatus = Io.VisionSDK.UserControls.Interfaces.IConnectionValidatorStatus;
    import IKernelNode = Io.VisionSDK.UserControls.Interfaces.IKernelNode;
    import IKernelNodeConnector = Io.VisionSDK.UserControls.Interfaces.IKernelNodeConnector;
    import InterfaceDataType = Io.VisionSDK.UserControls.Enums.InterfaceDataType;
    import IVector = Io.VisionSDK.UserControls.Interfaces.IVector;

    /**
     * KernelNode class provides structure describing kernel node in visual graph.
     */
    export class KernelNode extends Icon implements IKernelNode {
        public static readonly envelopSize : number = 80;
        public static readonly radius : number = 19;
        public static readonly activeRadius : number = 24;
        private static readonly defaultGridSize : number = 4;
        private static readonly minConnectorScale : number = 1;
        private static readonly maxConnectorScale : number = 1.5;
        private static readonly centerOffset : number = 14;
        private static registerEventsHandler : ($node : IKernelNode) => void;
        private static typeToNodeMapper : ($type : KernelNodeType) => IKernelNode;
        private static notifications : IKernelNodeNotifications = <any>{};
        private uniqueId : string;
        private uniqueIdPrefix : string;
        private namePrefix : string;
        private name : string;
        private type : string;
        private index : number;
        private column : number;
        private row : number;
        private readonly outputs : ArrayList<Output>;
        private readonly inputs : ArrayList<Input>;
        private selected : boolean;
        private readonly attributes : ArrayList<IKernelNodeAttribute>;
        private help : IKernelNodeHelp;
        private mouseEnterEventFired : boolean;
        private isEnabled : boolean;
        private isConnectorsVisible : boolean;
        private readonly inputConnectors : ArrayList<IKernelNodeConnector>;
        private readonly outputConnectors : ArrayList<IKernelNodeConnector>;
        private errorMessage : string;
        private parentNode : IKernelNode;
        private childNodes : ArrayList<IKernelNode>;
        private scale : number;
        private readonly groupId : string;
        private gridSize : Size;
        private absolutePosition : IVector;
        private groupOffset : ElementOffset;

        public static RegisterEventsHandler($handler? : ($node : IKernelNode) => void) : ($node : IKernelNode) => void {
            if (ObjectValidator.IsSet($handler)) {
                KernelNode.registerEventsHandler = $handler;
            } else if (ObjectValidator.IsEmptyOrNull(KernelNode.registerEventsHandler)) {
                KernelNode.registerEventsHandler = ($node : IKernelNode) : void => {
                    // placeholder for overriding
                };
            }
            return KernelNode.registerEventsHandler;
        }

        public static getTypedInterface<T extends Output | Input>($startInterface : T) : T {
            if (ObjectValidator.IsEmptyOrNull($startInterface) || $startInterface.dataType !== InterfaceDataType.DELEGATED) {
                return $startInterface;
            }
            let target : any = $startInterface;
            [target.delegator, target.innerInput, target.innerOutput]
                .forEach(($interface : KernelNodeInterface) : boolean => {
                    if (!ObjectValidator.IsEmptyOrNull($interface)) {
                        target = $interface;
                        return false;
                    }
                });
            if (!ObjectValidator.IsEmptyOrNull(target.output) || !ObjectValidator.IsEmptyOrNull(target.inputs)) {
                return <T>this.getTypedInterface(target instanceof Input ? target.output : target.inputs.getFirst());
            } else {
                return $startInterface;
            }
        }

        public static TypeToNodeMapper($value? : ($type : KernelNodeType) => IKernelNode) : ($type : KernelNodeType) => IKernelNode {
            return this.typeToNodeMapper = ObjectValidator.IsSet($value) ? $value : this.typeToNodeMapper;
        }

        public static TurnOn($element : KernelNode) : void {
            ElementManager.setClassName($element.Id() + "_KernelNodeEnvelop", GeneralCssNames.ON);
        }

        public static TurnOff($element : KernelNode) : void {
            if (!$element.isConnectorsVisible) {
                ElementManager.setClassName($element.Id() + "_KernelNodeEnvelop", GeneralCssNames.OFF);
            }
        }

        public static TurnActive($element : KernelNode) : void {
            ElementManager.setClassName($element.Id() + "_KernelNodeEnvelop", GeneralCssNames.ACTIVE);
        }

        public static getGridPosition($row : number, $column : number) : IVector {
            return {
                x: ($column + 0.5) * KernelNode.envelopSize,
                y: ($row + 0.5) * KernelNode.envelopSize - 1 /*column border correction*/
            };
        }

        public static Notifications($value? : IKernelNodeNotifications) : IKernelNodeNotifications {
            if (ObjectValidator.IsSet($value)) {
                KernelNode.notifications = $value;
            }
            return KernelNode.notifications;
        }

        protected static setErrorStyle($element : FormsObject) : void {
            if (!ObjectValidator.IsEmptyOrNull($element)) {
                if ($element.Error()) {
                    ElementManager.setClassName($element.Id() + "_Status", GeneralCssNames.ERROR);
                } else {
                    ElementManager.setClassName($element.Id() + "_Status", GeneralCssNames.ENABLE);
                }
            }
        }

        /**
         * @param {KernelNodeType} $type Specify kernel node visual type
         * @param {number} $row Specify row index in visual graph
         * @param {number} $column Specify column index in visual graph
         * @param {string} $groupId Specify group name identifier.
         */
        constructor($type : KernelNodeType, $row : number, $column : number, $groupId? : string) {
            super($type);
            this.help = new IKernelNodeHelp();
            this.uniqueIdPrefix = "vxNode";
            this.namePrefix = "vxNode";
            this.Column($column);
            this.Row($row);
            this.outputs = new ArrayList<Output>();
            this.inputs = new ArrayList<Input>();
            this.outputConnectors = new ArrayList<IKernelNodeConnector>();
            this.inputConnectors = new ArrayList<IKernelNodeConnector>();
            this.index = null;
            this.uniqueId = null;
            this.selected = false;
            this.attributes = new ArrayList<IKernelNodeAttribute>();
            this.getGuiOptions().Add(GuiOptionType.DISABLE);
            this.mouseEnterEventFired = false;
            this.isEnabled = true;
            this.isConnectorsVisible = false;
            this.errorMessage = "";
            this.parentNode = null;
            this.childNodes = new ArrayList<IKernelNode>();
            this.groupId = $groupId;
            this.gridSize = new Size();
            this.gridSize.Width(KernelNode.defaultGridSize);
            this.gridSize.Height(KernelNode.defaultGridSize);
            this.absolutePosition = new IVector();
            this.groupOffset = new ElementOffset();
            this.scale = 1;
        }

        public Move($position : IVector) : void {
            this.absolutePosition = {x: $position.x, y: $position.y};
            ElementManager.setCssProperty(this, "top", this.absolutePosition.y - KernelNode.radius);
            ElementManager.setCssProperty(this, "left", this.absolutePosition.x - KernelNode.radius);
            this.getEventsManager().FireEvent(this, KernelNodeEventType.ON_MOVE, undefined, false);
        }

        public ParentNode($value? : IKernelNode) : IKernelNode {
            return this.parentNode = ObjectValidator.IsSet($value) ? $value : this.parentNode;
        }

        public ChildNodes($value? : ArrayList<IKernelNode>) : ArrayList<IKernelNode> {
            return this.childNodes = ObjectValidator.IsSet($value) ? $value : this.childNodes;
        }

        public AddInput($dataType : string) : void {
            const input : Input = new Input();
            input.node = this;
            input.position = this.inputs.Length();
            input.dataType = $dataType;
            this.inputs.Add(input, input.position);
            if (this.inputConnectors.Length() < this.inputs.Length()) {
                const connector : KernelNodeConnector = new KernelNodeConnector(this.inputConnectors.Length());
                connector.GuiType(KernelNodeInterfaceType.INPUT);
                this.inputConnectors.Add(connector);
            }
        }

        public AddOutput($dataType : string) : void {
            const output : Output = new Output();
            output.node = this;
            output.position = this.outputs.Length();
            output.dataType = $dataType;
            this.outputs.Add(output, output.position);
            if (this.outputConnectors.Length() < this.outputs.Length()) {
                const connector : IKernelNodeConnector = new KernelNodeConnector(this.outputConnectors.Length());
                connector.GuiType(KernelNodeInterfaceType.OUTPUT);
                this.outputConnectors.Add(connector);
            }
        }

        public Help($value? : IKernelNodeHelp) : IKernelNodeHelp {
            if (ObjectValidator.IsSet($value)) {
                this.help = $value;
            }
            return this.help;
        }

        public IconType($type? : KernelNodeType) : KernelNodeType {
            return <KernelNodeType>super.IconType($type);
        }

        public Type($value? : string) : string {
            return this.type = Property.String(this.type, $value);
        }

        public Selected($value? : boolean) : boolean {
            this.selected = Property.Boolean(this.selected, $value);
            if (!ObjectValidator.IsEmptyOrNull($value)) {
                if (this.selected) {
                    ElementManager.setClassName(this.Id() + "_Status", "Selected");
                } else if (this.Enabled()) {
                    ElementManager.setClassName(this.Id() + "_Status", GeneralCssNames.ENABLE);
                } else {
                    ElementManager.setClassName(this.Id() + "_Status", GeneralCssNames.DISABLE);
                    this.HideConnectors();
                }
            }
            return this.selected;
        }

        public Enabled($value? : boolean) : boolean {
            this.isEnabled = Property.Boolean(this.isEnabled, $value);
            if (ObjectValidator.IsSet($value) && ElementManager.IsVisible(this.Id())) {
                if (!this.selected) {
                    ElementManager.Enabled(this.Id(), this.isEnabled);
                    if (this.isEnabled) {
                        ElementManager.setClassName(this.Id() + "_Status", GeneralCssNames.ENABLE);
                    } else {
                        this.HideConnectors();
                        ElementManager.setClassName(this.Id() + "_Status", GeneralCssNames.DISABLE);
                    }
                }
            }
            return this.isEnabled;
        }

        public Error($value? : boolean) : boolean {
            const error : boolean = super.Error($value);
            if (!error) {
                this.errorMessage = "";
            }
            return error;
        }

        public UniqueIdPrefix($value? : string) : string {
            return this.uniqueIdPrefix = Property.String(this.uniqueIdPrefix, $value);
        }

        public UniqueId($value? : string) : string {
            return this.uniqueId = Property.String(this.uniqueId, $value);
        }

        public NamePrefix($value? : string) : string {
            return this.namePrefix = Property.String(this.namePrefix, $value);
        }

        public Name($value? : string) : string {
            return this.name = Property.String(this.name, $value);
        }

        public getGroupId() : string {
            return this.groupId;
        }

        public GroupOffset($value ? : ElementOffset) : ElementOffset {
            return this.groupOffset = ObjectValidator.IsSet($value) ? $value : this.groupOffset;
        }

        public Column($value? : number) : number {
            return this.column = Property.Integer(this.column, $value);
        }

        public Row($value? : number) : number {
            return this.row = Property.Integer(this.row, $value);
        }

        public getOutput($position : number) : Output {
            return this.outputs.getItem($position);
        }

        public getInput($position : number) : Input {
            return this.inputs.getItem($position);
        }

        public getInputs() : ArrayList<Input> {
            return this.inputs;
        }

        public getOutputs() : ArrayList<Output> {
            return this.outputs;
        }

        public getInputConnectors() : ArrayList<IKernelNodeConnector> {
            return this.inputConnectors;
        }

        public getOutputConnectors() : ArrayList<IKernelNodeConnector> {
            return this.outputConnectors;
        }

        public Remove($removeRefs : boolean = true) : void {
            const eventArgs : KernelNodeEventArgs = new KernelNodeEventArgs();
            eventArgs.Owner(this);
            eventArgs.Status($removeRefs);
            this.getEventsManager().FireEvent(this, KernelNodeEventType.BEFORE_REMOVE, eventArgs, false);

            if (!ObjectValidator.IsEmptyOrNull(ElementManager.getElement(this.Id() + "_GuiWrapper"))) {
                const wrapperParent : Node = ElementManager.getElement(this.Id() + "_GuiWrapper").parentNode;
                if (!ObjectValidator.IsEmptyOrNull(wrapperParent) && !ObjectValidator.IsEmptyOrNull(wrapperParent.parentNode)) {
                    wrapperParent.parentNode.removeChild(wrapperParent);
                }
            }

            this.getEventsManager().Clear(this.Id());
            const guiManager : GuiObjectManager = this.getGuiManager();
            this.inputConnectors.foreach(($connector : IKernelNodeConnector) : void => {
                guiManager.Clear($connector);
            });
            this.outputConnectors.foreach(($connector : IKernelNodeConnector) : void => {
                guiManager.Clear($connector);
            });
            guiManager.Clear(this);
        }

        public TrackConnector($vector : IVector) : void {
            const con : IKernelNodeConnector = this.outputConnectors.getFirst();
            con.Visible(true);
            let mag : number = Math.sqrt(Math.pow($vector.y, 2) + Math.pow($vector.x, 2));
            if (mag === 0) {
                mag = 1;
            }
            const top : number = $vector.y / mag * KernelNode.activeRadius;
            const left : number = $vector.x / mag * KernelNode.activeRadius;

            con.SuperSampleScale(100);
            con.Position({x: left + KernelNode.centerOffset, y: top + KernelNode.centerOffset});
        }

        public ShowInputConnectors($args? : IConnectorVisibilityArgs) : void {
            let visibleConnectors : number = 0;
            const handlers : any[] = [];
            if (this.Selected() || this.Enabled()) {
                if (ObjectValidator.IsSet($args)) {
                    this.scale = $args.scale;
                }

                let drawIndex : number = 0;
                for (let index : number = 0; index < this.inputConnectors.Length(); index++) {
                    const connector : IKernelNodeConnector = this.inputConnectors.getItem(index);
                    let hideConnector : boolean = true;
                    const typedInput : Input = KernelNode.getTypedInterface(this.inputs.getItem(index));
                    const dataType = typedInput.dataType;
                    const isDelegated : boolean = $args.dataType === InterfaceDataType.DELEGATED
                        || dataType === InterfaceDataType.DELEGATED;

                    if (ObjectValidator.IsEmptyOrNull(this.inputs.getItem(index).output) && (isDelegated || dataType === $args.dataType)) {
                        if (ObjectValidator.IsEmptyOrNull($args) ||
                            ObjectValidator.IsEmptyOrNull($args.sourceNode) && ObjectValidator.IsEmptyOrNull($args.sourceConnector) ||
                            ObjectValidator.IsEmptyOrNull($args.validator)) {
                            hideConnector = false;
                        }

                        if (isDelegated) {
                            hideConnector = false;
                        } else if (hideConnector) {
                            const validatorStatus : IConnectionValidatorStatus = $args.validator(
                                $args.delegatedOutput.node, typedInput.node,
                                $args.delegatedOutput.position, typedInput.position, true, false);
                            hideConnector = !validatorStatus.status;
                            if (hideConnector) {
                                this.errorMessage = validatorStatus.message;
                                if (ObjectValidator.IsEmptyOrNull(this.errorMessage)) {
                                    this.errorMessage = KernelNode.notifications.no_compatible_node_connection_exists;
                                }
                            } else {
                                this.errorMessage = "";
                            }
                        }
                    }
                    if (!hideConnector) {
                        this.isConnectorsVisible = true;
                        connector.Visible(true);
                        connector.Index(index);
                        visibleConnectors++;
                        handlers.push(() : void => {
                            this.scaleConnector(connector, this.scale);
                            this.setConnectorPosition(connector, drawIndex++, visibleConnectors, 255, -150);
                        });
                    } else {
                        connector.Visible(false);
                    }
                }

                handlers.forEach(($handler : any) : void => {
                    $handler();
                });
                if (!this.isConnectorsVisible && ObjectValidator.IsEmptyOrNull(this.errorMessage)) {
                    this.errorMessage = KernelNode.notifications.no_compatible_node_connection_exists;
                }
            }
        }

        public ShowOutputConnectors($args? : IConnectorVisibilityArgs) : void {
            if (ObjectValidator.IsSet($args)) {
                this.scale = $args.scale;
            }

            if (this.Selected() || this.Enabled()) {
                for (let i = 0; i < this.outputConnectors.Length(); i++) {
                    this.isConnectorsVisible = true;
                    const connector : IKernelNodeConnector = this.outputConnectors.getItem(i);
                    connector.Visible(true);
                    this.scaleConnector(connector, this.scale);
                    this.setConnectorPosition(connector, i, this.getOutputs().Length(), 285, 150);
                }
            }
        }

        public HideConnectors() : void {
            this.isConnectorsVisible = false;
            this.inputConnectors.foreach(($connector : IKernelNodeConnector) : void => {
                $connector.Visible(false);
            });
            this.outputConnectors.foreach(($connector : IKernelNodeConnector) : void => {
                $connector.Visible(false);
            });
        }

        public IsConnectorsVisible() : boolean {
            return this.isConnectorsVisible;
        }

        public ScaleVisibleConnectors($scale : number) : void {
            this.scale = $scale;
            this.inputConnectors.foreach(($connector : IKernelNodeConnector) : void => {
                if ($connector.Visible()) {
                    this.scaleConnector($connector, this.scale);
                }
            });
            this.outputConnectors.foreach(($connector : IKernelNodeConnector) : void => {
                if ($connector.Visible()) {
                    this.scaleConnector($connector, this.scale);
                }
            });
        }

        public MoveInterface($fromIndex : number, $toIndex : number, $type : KernelNodeInterfaceType) : void {
            const increment : number = $fromIndex > $toIndex ? -1 : 1;
            let swapFrom : number = $fromIndex;
            let swapTo : number = $fromIndex + increment;
            while (swapTo >= $toIndex && increment < 0 || swapTo <= $toIndex && increment > 0) {
                this.swapInterface(swapFrom, swapTo, $type);
                swapFrom += increment;
                swapTo += increment;
            }
            this.reIndexInterfaces($type);
        }

        public RemoveInterface($index : number, $type : KernelNodeInterfaceType) : void {
            if ($type === KernelNodeInterfaceType.OUTPUT) {
                const output : Output = this.getOutput($index);
                output.inputs.foreach(($input : Input) : void => {
                    $input.output = null;
                });
                this.getOutputs().RemoveAt($index);
                this.getOutputs().Reindex();
                this.outputConnectors.RemoveAt($index);
                this.outputConnectors.Reindex();
            } else if ($type === KernelNodeInterfaceType.INPUT) {
                const input : Input = this.getInput($index);
                if (!ObjectValidator.IsEmptyOrNull(input.output)) {
                    input.output.inputs.RemoveAt(input.output.inputs.IndexOf(input));
                }
                this.getInputs().RemoveAt($index);
                this.getInputs().Reindex();
                this.inputConnectors.RemoveAt($index);
                this.inputConnectors.Reindex();
            }
            this.reIndexInterfaces($type);
        }

        public getAttributes() : ArrayList<IKernelNodeAttribute> {
            return this.attributes;
        }

        public setAttributeValue($name : string, $value : any) : void {
            this.attributes.getItem($name).value = $value;
        }

        public setAttribute($value : IKernelNodeAttribute) : void {
            this.attributes.Add($value, $value.name);
        }

        public getGridPosition() : IVector {
            return KernelNode.getGridPosition(this.Row(), this.Column());
        }

        public getAbsolutePosition() : IVector {
            return this.absolutePosition;
        }

        public getAbsoluteConnectorPosition($index : number, $type : KernelNodeInterfaceType) : IVector {
            const targetConnector : IKernelNodeConnector
                = ($type === KernelNodeInterfaceType.INPUT ? this.getInputConnectors() : this.getOutputConnectors()).getItem($index);
            return {
                x: targetConnector.Position().x + this.absolutePosition.x - KernelNode.activeRadius / 2,
                y: targetConnector.Position().y + this.absolutePosition.y - KernelNode.activeRadius / 2
            };
        }

        public GridSize($value? : Size) : Size {
            if (ObjectValidator.IsSet($value)) {
                this.gridSize = $value;
                if (this.gridSize.Height() === 0 || this.gridSize.Width() === 0) {
                    this.gridSize.Width(KernelNode.defaultGridSize);
                    this.gridSize.Height(KernelNode.defaultGridSize);
                }
            }
            return this.gridSize;
        }

        public ToString($prefix : string = "", $htmlTag : boolean = true) : string {
            return $prefix + "type: " + this.Type() + " name: " + this.Name() + " unique id: " + this.UniqueId();
        }

        public getEvents() : IKernelNodeEvents {
            return <IKernelNodeEvents>super.getEvents();
        }

        public setConnectorPosition($connector : IKernelNodeConnector,
                                    $index : number, $availableNum : number,
                                    $angleOffset : number, $angleSpace : number) : void {

            const theta = Math.PI * $angleSpace / 180 / $availableNum;
            const angle = Math.PI * $angleOffset / 180 + theta * $index + theta / 2;
            $connector.SuperSampleScale(1);
            $connector.Position({
                x: KernelNode.activeRadius * Math.cos(angle) + KernelNode.centerOffset,
                y: KernelNode.activeRadius * Math.sin(angle) + KernelNode.centerOffset
            });
        }

        public RegisterEvents() : void {
            KernelNode.RegisterEventsHandler()(this);
        }

        protected innerHtml() : IGuiElement {
            const envelop : IGuiElement = this.addElement(this.Id() + "_KernelNodeEnvelop").StyleClassName(GeneralCssNames.OFF)
                .Add(this.addElement(this.Id() + "_IconEnvelop").StyleClassName("IconEnvelop")
                    .Add(this.addElement(this.Id() + "_Icon").StyleClassName(GeneralCssNames.ICON)));

            const connectors = this.addElement(this.Id() + "_ConnectorsEnvelop")
                .StyleClassName("ConnectorsEnvelop");
            this.inputConnectors.foreach(($connector : KernelNodeConnector) => {
                connectors.Add($connector);
            });
            this.outputConnectors.foreach(($connector : KernelNodeConnector) => {
                connectors.Add($connector);
            });
            envelop.Add(connectors);

            return this.addElement(this.Id() + "_Status").StyleClassName(this.statusCss())
                .Add(this.addElement(this.Id() + "_Type").GuiTypeTag("Icon").StyleClassName(this.IconType())
                    .Add(envelop));
        }

        protected innerCode() : IGuiElement {
            const generateEventArgs : any = ($connector : KernelNodeConnector) : KernelNodeEventArgs => {
                const eventArgs : KernelNodeEventArgs = new KernelNodeEventArgs();
                eventArgs.Owner(this);
                eventArgs.Index($connector.GuiType() === KernelNodeInterfaceType.INPUT ?
                    this.inputConnectors.IndexOf($connector) : this.outputConnectors.IndexOf($connector));
                eventArgs.DataType($connector.GuiType() === KernelNodeInterfaceType.INPUT ?
                    this.inputs.getItem(eventArgs.Index()).dataType : this.outputs.getItem(eventArgs.Index()).dataType);
                eventArgs.ConnectorType($connector.GuiType());
                return eventArgs;
            };

            const setNodeOn : any = () => {
                GuiObjectManager.getInstanceSingleton().setHovered(this, true);
                KernelNode.TurnOn(this);
            };

            const setNodeOff : any = () => {
                GuiObjectManager.getInstanceSingleton().setHovered(this, false);
                KernelNode.TurnOff(this);
            };

            let isMouseOver : boolean = false;
            const registerConnectorEvents : any = (connector : KernelNodeConnector) : void => {
                connector.getEvents().setOnMouseUp(() => {
                    this.getEventsManager().FireEvent(this, KernelNodeEventType.ON_CONNECTOR_MOUSE_UP,
                        generateEventArgs(connector));
                });
                connector.getEvents().setOnMouseDown(($eventArgs : MouseEventArgs) => {
                    $eventArgs.StopAllPropagation();
                    this.getEventsManager().FireEvent(this, KernelNodeEventType.ON_CONNECTOR_MOUSE_DOWN,
                        generateEventArgs(connector));
                });
                connector.getEvents().setOnMouseOver(($eventArgs : MouseEventArgs) => {
                    $eventArgs.StopAllPropagation();

                    let text : string = connector.GuiType() + " " + connector.Index();
                    if (this.type === KernelNodeType.VXGROUP) {
                        if (connector.GuiType() === KernelNodeInterfaceType.OUTPUT) {
                            const typedOutput : Output =
                                KernelNode.getTypedInterface(this.getOutput(connector.Index()).innerInput.output);
                            if (!ObjectValidator.IsEmptyOrNull(typedOutput)) {
                                const help : string = typedOutput.node.Help().outputs[typedOutput.position];
                                if (!ObjectValidator.IsEmptyOrNull(help)) {
                                    text = help;
                                }
                            }
                        } else if (connector.GuiType() === KernelNodeInterfaceType.INPUT) {
                            const typedInput : Input =
                                KernelNode.getTypedInterface(this.getInput(connector.Index()).innerOutput.inputs.getFirst());
                            if (!ObjectValidator.IsEmptyOrNull(typedInput)) {
                                const help : string = typedInput.node.Help().inputs[typedInput.position];
                                if (!ObjectValidator.IsEmptyOrNull(help)) {
                                    text = help;
                                }
                            }
                        }
                    } else {
                        if (connector.GuiType() === KernelNodeInterfaceType.OUTPUT
                            && !ObjectValidator.IsEmptyOrNull(this.Help().outputs[connector.Index()])) {
                            text = this.Help().outputs[connector.Index()];
                        } else if (connector.GuiType() === KernelNodeInterfaceType.INPUT
                            && !ObjectValidator.IsEmptyOrNull(this.Help().inputs[connector.Index()])) {
                            text = this.Help().inputs[connector.Index()];
                        }
                    }

                    const eventArgs : KernelNodeEventArgs = new KernelNodeEventArgs();
                    eventArgs.Owner(this);
                    eventArgs.TooltipText(text);
                    eventArgs.ConnectorType(connector.GuiType());
                    eventArgs.Index(connector.Index());
                    this.getEventsManager()
                        .FireEvent(this, KernelNodeEventType.ON_CONNECTOR_MOUSE_ENTER, generateEventArgs(connector));
                    if (!this.mouseEnterEventFired) {
                        setNodeOn();
                        this.getEventsManager().FireEvent(this, KernelNodeEventType.ON_MOUSE_ENTER, $eventArgs);
                        this.mouseEnterEventFired = true;
                    }
                    this.getEventsManager().FireEvent(this, KernelNodeEventType.ON_TOOLTIP_CHANGE, eventArgs);
                    isMouseOver = true;
                });

                connector.getEvents().setOnMouseOut(($eventArgs : MouseEventArgs) => {
                    $eventArgs.StopAllPropagation();

                    isMouseOver = false;
                    this.getEventsManager()
                        .FireEvent(this, KernelNodeEventType.ON_CONNECTOR_MOUSE_LEAVE, generateEventArgs(connector));
                    this.getEventsManager().FireAsynchronousMethod(() => {
                        if (!isMouseOver) {
                            setNodeOff();
                            this.getEventsManager().FireEvent(this, KernelNodeEventType.ON_MOUSE_LEAVE, $eventArgs);
                            this.mouseEnterEventFired = false;
                        }
                    });
                });
                connector.getEvents().Subscribe(true);
            };

            this.inputConnectors.foreach(($connector : KernelNodeConnector) => {
                registerConnectorEvents($connector);
                $connector.Visible(false);
                this[$connector.Id()] = $connector;
            });

            this.outputConnectors.foreach(($connector : KernelNodeConnector) => {
                registerConnectorEvents($connector);
                $connector.Visible(false);
                this[$connector.Id()] = $connector;
            });

            this.getEvents().setOnMouseOver(($eventArgs : MouseEventArgs) => {
                $eventArgs.StopAllPropagation();
                const eventArgs : KernelNodeEventArgs = new KernelNodeEventArgs();
                eventArgs.Owner(this);
                if (this.Error() && !ObjectValidator.IsEmptyOrNull(this.errorMessage)) {
                    eventArgs.TooltipText(this.errorMessage);
                } else if ((this.Selected() || this.Enabled()) && !ObjectValidator.IsEmptyOrNull(this.Help().tooltip)) {
                    eventArgs.TooltipText(this.Help().tooltip);
                } else {
                    eventArgs.TooltipText(this.Name());
                }

                if (!this.mouseEnterEventFired) {
                    setNodeOn();
                    this.getEventsManager().FireEvent(this, KernelNodeEventType.ON_MOUSE_ENTER, $eventArgs);
                    this.mouseEnterEventFired = true;
                }
                this.getEventsManager().FireEvent(this, KernelNodeEventType.ON_TOOLTIP_CHANGE, eventArgs);
                isMouseOver = true;
            });

            this.getEventsManager().FireEvent(this, KernelNodeEventType.ON_MOUSE_LEAVE, null);

            this.getEvents().setOnMouseOut(($eventArgs : MouseEventArgs) : void => {
                $eventArgs.StopAllPropagation();
                isMouseOver = false;
                this.getEventsManager().FireAsynchronousMethod(() => {
                    if (!isMouseOver) {
                        setNodeOff();
                        this.getEventsManager().FireEvent(this, KernelNodeEventType.ON_MOUSE_LEAVE, $eventArgs);
                        this.mouseEnterEventFired = false;
                    }
                });
            });

            this.getEvents().setOnMouseDown(() : void => {
                KernelNode.TurnActive(this);
            });

            this.getEvents().setOnMouseUp(($eventArgs : EventArgs, $manager : GuiObjectManager) : void => {
                $manager.setHovered(this, true);
                KernelNode.TurnOn(this);
            });

            return super.innerCode();
        }

        protected getEventsManagerClass() : any {
            return ElementEventsManager;
        }

        protected cssContainerName() : string {
            return "KernelNode";
        }

        protected statusCss() : string {
            if (this.selected) {
                return "Selected";
            } else {
                return GeneralCssNames.ENABLE.toString();
            }
        }

        protected excludeIdentityHashData() : string[] {
            const exclude : string[] = super.excludeIdentityHashData();
            exclude.push("inputs", "outputs", "uniqueNodes", "parent", "owner", "inputs", "outputs", "valuesPersistence", "errorFlags",
                "events");
            return exclude;
        }

        private scaleConnector($connector : IKernelNodeConnector, $scale : number) : void {
            const normalizedScale : number = Math.min(Math.max($scale, KernelNode.minConnectorScale),
                KernelNode.maxConnectorScale);
            const resizeHandler : any = () : void => {
                ElementManager.setCssProperty($connector.Id() + "_Type", "transform", "scale(" + normalizedScale + ")");
            };
            if ($connector.IsLoaded()) {
                resizeHandler();
            } else {
                $connector.getEvents().setOnLoad(resizeHandler);
            }
        }

        private swapInterface($index1 : number, $index2 : number, $type : KernelNodeInterfaceType) : void {
            if ($type === KernelNodeInterfaceType.INPUT) {
                const input1 : Input = this.getInput($index1);
                const input2 : Input = this.getInput($index2);
                this.getInputs().Add(input1, $index2);
                this.getInputs().Add(input2, $index1);
            } else if ($type === KernelNodeInterfaceType.OUTPUT) {
                const output1 : Output = this.getOutput($index1);
                const output2 : Output = this.getOutput($index2);
                this.getOutputs().Add(output1, $index2);
                this.getOutputs().Add(output2, $index1);
            }
        }

        private reIndexInterfaces($type : KernelNodeInterfaceType) : void {
            if ($type === KernelNodeInterfaceType.INPUT) {
                this.getInputs().foreach(($input : Input, $index : number) : void => {
                    $input.position = $index;
                    if (!ObjectValidator.IsEmptyOrNull($input.innerOutput)) {
                        $input.innerOutput.position = $index;
                    }
                });
            } else if ($type === KernelNodeInterfaceType.OUTPUT) {
                this.getOutputs().foreach(($output : Output, $index : number) : void => {
                    $output.position = $index;
                    if (!ObjectValidator.IsEmptyOrNull($output.innerInput)) {
                        $output.innerInput.position = $index;
                    }
                });
            }
        }
    }
}
