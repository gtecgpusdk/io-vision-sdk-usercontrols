/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.BaseInterface.UserControls {
    "use strict";
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import ToolTip = Io.VisionSDK.UserControls.BaseInterface.Components.ToolTip;
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import ImageButtonType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.ImageButtonType;
    import GuiObjectManager = Com.Wui.Framework.Gui.GuiObjectManager;
    import KernelNodeType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.KernelNodeType;
    import KernelNodeButtonType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.KernelNodeButtonType;
    import GeneralCssNames = Com.Wui.Framework.Gui.Enums.GeneralCssNames;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;

    export class KernelNodeButton extends Com.Wui.Framework.UserControls.Primitives.FormsObject {
        private text : string;
        private width : number;
        private kernelImageButton : ImageButton;
        private guiType : KernelNodeButtonType;
        private selected : boolean;

        public static TurnOn($element : KernelNodeButton, $manager : GuiObjectManager) : void {
            if ($element.Enabled() && !$manager.IsActive($element)) {
                ElementManager.TurnOn($element.Id() + "_Status");
                ImageButton.TurnOn($element.kernelImageButton, $manager);
            }
        }

        public static TurnOff($element : KernelNodeButton, $manager : GuiObjectManager) : void {
            if ($element.Enabled() && !$manager.IsActive($element) && !$element.Selected()) {
                ElementManager.TurnOff($element.Id() + "_Status");
                ImageButton.TurnOff($element.kernelImageButton, $manager);
            }
        }

        public static TurnActive($element : KernelNodeButton, $manager : GuiObjectManager) : void {
            if ($element.Enabled() && !$element.Selected()) {
                ElementManager.TurnActive($element.Id() + "_Status");
                ImageButton.TurnOn($element.kernelImageButton, $manager);
            }
        }

        constructor($iconType : KernelNodeType, $id? : string) {
            super($id);
            this.kernelImageButton = new ImageButton(ImageButtonType.KERNEL_NODE);
            this.kernelImageButton.IconName($iconType);
            this.guiType = this.guiTypeValueSetter(KernelNodeButtonType.INFORMATIVE);
        }

        /**
         * @param {KernelNodeButtonType} [$kernelNodeButtonType] Specify type of element look and feel.
         * @return {KernelNodeButtonType} Returns type of element's look and feel.
         */
        public GuiType($kernelNodeButtonType? : KernelNodeButtonType) : KernelNodeButtonType {
            if (ObjectValidator.IsSet($kernelNodeButtonType)) {
                this.guiType = this.guiTypeValueSetter($kernelNodeButtonType);
                if (ElementManager.Exists(this.Id() + "_Type")) {
                    ElementManager.setClassName(this.Id() + "_Type", this.guiType.toString());
                }
            }
            return this.guiType;
        }

        /**
         * @param {string} [$value] Specify text, which should be displayed as item value.
         * @return {string} Returns element's text value.
         */
        public Text($value? : string) : string {
            return this.text = Property.String(this.text, $value);
        }

        public IconName($value? : KernelNodeType) : KernelNodeType {
            return this.kernelImageButton.IconName($value);
        }

        public Selected($value? : boolean) : boolean {
            if (ObjectValidator.IsSet($value)) {
                this.kernelImageButton.Selected($value);
                this.selected = $value;
                if (this.selected) {
                    KernelNodeButton.TurnOn(this, GuiObjectManager.getInstanceSingleton());
                } else {
                    KernelNodeButton.TurnOff(this, GuiObjectManager.getInstanceSingleton());
                }
            }
            return this.selected;
        }

        /**
         * @param {number} [$value] Specify element's width.
         * @return {number} Returns element's width value.
         */
        public Width($value? : number) : number {
            if (ObjectValidator.IsSet($value)) {
                if (this.width !== $value) {
                    this.width = Property.PositiveInteger(this.width, $value);
                    if (this.IsCompleted()) {
                        ElementManager.setWidth(this.Id() + "_Body", this.width);
                    }
                }
            }
            return this.width;
        }

        protected guiTypeValueSetter($value : any) : any {
            return Property.EnumType(this.GuiType(), $value, KernelNodeButtonType, KernelNodeButtonType.INFORMATIVE);
        }

        protected innerCode() : IGuiElement {
            this.getEvents().setOnLoad(() : void => {
                ElementManager.setClassName(this.Id() + "_Type", this.guiType.toString());
            });

            this.getEvents().setOnMouseOver(($eventArgs : EventArgs, $manager : GuiObjectManager) : void => {
                if (ToolTip.IsGlobalEnabled()) {
                    if (this.GuiType() === KernelNodeButtonType.SIMPLE) {
                        $manager.setHovered(this, true);
                        ToolTip.ShowGlobal(this.text, 0);
                    } else if (ElementManager.getElement(this.Id() + "_Text").scrollWidth
                        > ElementManager.getElement(this.Id() + "_Text").offsetWidth) {
                        $manager.setHovered(this, true);
                        ToolTip.ShowGlobal(this.text);
                    } else {
                        ToolTip.HideGlobal(true);
                    }
                }
            });
            this.kernelImageButton.getEvents().setOnMouseOut(() : void => {
                ToolTip.HideGlobal();
            });

            this.getEvents().setOnMouseOut(() : void => {
                ToolTip.HideGlobal();
            });
            this.getEvents().setOnMouseDown(() : void => {
                ToolTip.HideGlobal(true);
            });
            return super.innerCode();
        }

        protected innerHtml() : IGuiElement {
            return this.addElement(this.Id() + "_Type").StyleClassName(this.GuiType())
                .Add(this.addElement(this.Id() + "_Status")
                    .StyleClassName(this.statusCss())
                    .Add(this.addElement(this.Id() + "_Body").StyleClassName("BodyEnvelop")
                        .Width(this.width)
                        .Add(this.addElement().StyleClassName("Body")
                            .Add(this.kernelImageButton)
                            .Add(this.addElement(this.Id() + "_Text").StyleClassName(GeneralCssNames.TEXT)
                                .Add(this.Text())
                            )
                        )
                    )
                ).Add(this.addElement().setAttribute("clear", "both"));
        }

        protected getTitleClass() : any {
            return undefined;
        }
    }
}
