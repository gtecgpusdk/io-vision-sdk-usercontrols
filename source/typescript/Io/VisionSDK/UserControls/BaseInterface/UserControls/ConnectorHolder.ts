/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.BaseInterface.UserControls {
    "use strict";
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import GeneralCssNames = Com.Wui.Framework.Gui.Enums.GeneralCssNames;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import Size = Com.Wui.Framework.Gui.Structures.Size;
    import KernelNodeConnector = Io.VisionSDK.UserControls.BaseInterface.Components.KernelNodeConnector;
    import KernelNodeInterfaceType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.KernelNodeInterfaceType;
    import IConnectionStack = Io.VisionSDK.UserControls.Interfaces.IConnectionStack;
    import IConnection = Io.VisionSDK.UserControls.Interfaces.IConnection;
    import ElementEventsManager = Io.VisionSDK.UserControls.Events.ElementEventsManager;
    import KernelNodeEventArgs = Io.VisionSDK.UserControls.Events.Args.KernelNodeEventArgs;
    import KernelNodeEventType = Io.VisionSDK.UserControls.Enums.Events.KernelNodeEventType;
    import KernelNodeType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.KernelNodeType;
    import GuiObjectManager = Com.Wui.Framework.Gui.GuiObjectManager;
    import IconType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.IconType;
    import WindowManager = Com.Wui.Framework.Gui.Utils.WindowManager;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import KeyEventArgs = Com.Wui.Framework.Gui.Events.Args.KeyEventArgs;
    import KeyMap = Com.Wui.Framework.Gui.Enums.KeyMap;
    import ToolTip = Io.VisionSDK.UserControls.BaseInterface.Components.ToolTip;
    import ConnectionManager = Io.VisionSDK.UserControls.Utils.ConnectionManager;
    import ConnectorHolderEventArgs = Io.VisionSDK.UserControls.Events.Args.ConnectorHolderEventArgs;
    import ConnectorHolderEventType = Io.VisionSDK.UserControls.Enums.Events.ConnectorHolderEventType;
    import IConnectorHolderEvents = Io.VisionSDK.UserControls.Interfaces.Events.IConnectorHolderEvents;
    import KernelNodeInterface = Io.VisionSDK.UserControls.Structures.KernelNodeInterface;
    import Output = Io.VisionSDK.UserControls.Interfaces.Output;
    import Input = Io.VisionSDK.UserControls.Interfaces.Input;
    import IKernelNode = Io.VisionSDK.UserControls.Interfaces.IKernelNode;
    import ConnectorHolderStatus = Io.VisionSDK.UserControls.Enums.ConnectorHolderStatus;
    import InterfaceDataType = Io.VisionSDK.UserControls.Enums.InterfaceDataType;
    import IVector = Io.VisionSDK.UserControls.Interfaces.IVector;
    import GridManager = Io.VisionSDK.UserControls.Utils.GridManager;

    export class ConnectorHolder extends Com.Wui.Framework.UserControls.Primitives.BaseGuiObject {
        private static readonly maxConnectorNum : number = 6;
        private static readonly minConnectorScale : number = 1;
        private static readonly maxConnectorScale : number = 1.5;
        private static readonly defaultConnectorSize : number = 10;
        private readonly guiType : KernelNodeInterfaceType;
        private readonly addButtonBefore : Icon;
        private readonly addButtonAfter : Icon;
        private connectors : ArrayList<KernelNodeConnector>;
        private connectorEnvelops : ArrayList<IGuiElement>;
        private lastVisible : KernelNodeConnector;
        private selectedConnector : number;
        private draggedConnector : number;
        private isButtonsVisible : boolean;
        private connectionManager : ConnectionManager;
        private gridManager : GridManager;
        private moveStartIndex : number;
        private moveEndIndex : number;

        public static Update($handleVisibility : boolean = true, $animate? : boolean) : void {
            if (ObjectValidator.IsSet($animate)) {
                this.AnimateConnectors($animate);
            }
            const holders : ArrayList<ConnectorHolder> =
                <ArrayList<ConnectorHolder>>GuiObjectManager.getInstanceSingleton().getType(ConnectorHolder);
            holders.foreach(($holder : ConnectorHolder) : void => {
                if ($handleVisibility) {
                    $holder.HandleVisibility();
                } else {
                    $holder.Snap();
                }
            });
        }

        private static AnimateConnectors($value : boolean) : void {
            const holders : ArrayList<ConnectorHolder> =
                <ArrayList<ConnectorHolder>>GuiObjectManager.getInstanceSingleton().getType(ConnectorHolder);
            holders.foreach(($holder : ConnectorHolder) : void => {
                $holder.animateConnectors($value);
            });
        }

        /**
         * @param {KernelNodeInterfaceType} [$guiType] Set arguments defining element behaviour.
         * @param {ConnectionManager} [$connectionManager] Set connection manager.
         * @param {string} [$id] Force set element id instead of generated one.
         */
        constructor($guiType : KernelNodeInterfaceType, $connectionManager : ConnectionManager, $id? : string) {
            super($id);
            this.guiType = this.guiTypeValueSetter($guiType);
            this.connectors = new ArrayList<KernelNodeConnector>();
            this.addButtonBefore = new Icon(IconType.PLUS);
            this.addButtonAfter = new Icon(IconType.PLUS);
            this.connectorEnvelops = new ArrayList<IGuiElement>();
            this.selectedConnector = null;
            this.draggedConnector = null;
            this.isButtonsVisible = false;
            this.connectionManager = $connectionManager;
            this.gridManager = this.connectionManager.getGridManager();
            this.moveStartIndex = null;
            this.moveEndIndex = null;
        }

        public HandleVisibility() : void {
            const root : IKernelNode = this.gridManager.Root();
            if (!ObjectValidator.IsEmptyOrNull(root) && root.Type() === KernelNodeType.VXGROUP) {
                const interfaces : ArrayList<KernelNodeInterface> = this.guiType === KernelNodeInterfaceType.OUTPUT ?
                    root.getInputs() : root.getOutputs();
                this.lastVisible = null;
                this.connectors.foreach(($connector : KernelNodeConnector, $index : number) : void => {
                    const isVisible : boolean = !ObjectValidator.IsEmptyOrNull(interfaces.getItem($index));
                    ElementManager.setCssProperty($connector.Id() + "_ConnectorEnvelop", "display",
                        isVisible ? "block" : "none");
                    if (isVisible) {
                        this.lastVisible = $connector;
                    }
                });

                this.isButtonsVisible = !ObjectValidator.IsEmptyOrNull(this.lastVisible) &&
                    this.lastVisible.Index() !== ConnectorHolder.maxConnectorNum - 1;
                [this.addButtonBefore, this.addButtonAfter].forEach(($button : Icon) : void => {
                    ElementManager.setCssProperty($button.Id() + "_Icon", "transform", this.isButtonsVisible ?
                        "scale(1)" : "scale(0)");
                });

                ElementManager.setClassName(this.Id() + "_Status", ObjectValidator.IsEmptyOrNull(this.lastVisible) ?
                    GeneralCssNames.DISABLE : GeneralCssNames.ENABLE);

                this.Visible(true);
                this.Snap();
            } else {
                this.Visible(false);
            }
        }

        public Snap() : void {
            const root : IKernelNode = this.gridManager.Root();
            if (!ObjectValidator.IsEmptyOrNull(root) && root.Type() === KernelNodeType.VXGROUP) {
                const envelopId : string = this.Id() + "_Type";
                const connectorWidth : number = ElementManager.getOffsetWidth(envelopId, false, false);
                const visibleConnectors : number[] = [];
                const envelopSize : Size = this.gridManager.getViewSize();
                const anchorSize : Size = this.gridManager.getAreaSize();
                const anchorOffset : IVector = this.gridManager.getAreaPosition();
                const zoomMult : number = this.gridManager.getZoomMultiplier();

                this.scaleConnectors();

                const yAxisHandler : any = ($envelopId : string, $height : number) : number => {
                    let topOffset : number = anchorSize.Height() / 2 - $height / 2;
                    const topOverflow : number = topOffset + anchorOffset.y / zoomMult;
                    const bottomOverflow : number = topOffset + (anchorOffset.y - envelopSize.Height()) / zoomMult + $height;
                    if (topOverflow < 0) {
                        topOffset -= topOverflow;
                    } else if (bottomOverflow > 0) {
                        topOffset -= bottomOverflow;
                    }
                    topOffset = Math.min(Math.max(0, topOffset), anchorSize.Height() - $height);
                    ElementManager.setCssProperty($envelopId, "top", topOffset);
                    return topOffset;
                };

                const getConnectorsHeight : any = () : number => {
                    let height : number = 0;
                    this.connectors.foreach(($connector : KernelNodeConnector, $index : number) : void => {
                        const connectorHeight : number = ElementManager.getOffsetHeight($connector.Id() + "_ConnectorEnvelop");
                        if (connectorHeight !== 0) {
                            visibleConnectors.push($index);
                            height += connectorHeight;
                        }
                    });
                    height = Math.max(height,
                        ElementManager.getCssIntegerValue(this.Id() + "_Connectors", "min-height"));
                    return height;
                };

                const getButtonsHeight : any = () : number => {
                    let height : number = 0;
                    [this.addButtonBefore, this.addButtonAfter].forEach(($button : Icon) : void => {
                        height += ElementManager.getOffsetHeight($button.Id());
                    });
                    return height;
                };

                if (this.guiType === KernelNodeInterfaceType.OUTPUT) {
                    if (this.Visible()) {
                        const connectorsHeight : number = getConnectorsHeight();
                        const buttonsHeight : number = getButtonsHeight();

                        let leftOffset : number = Math.min(-anchorOffset.x / zoomMult, anchorSize.Width()
                            - connectorWidth);
                        if (leftOffset > -connectorWidth) {
                            ElementManager.setCssProperty(envelopId, "left", leftOffset);
                            ElementManager.setClassName(this.Id() + "_SnapStatus", GeneralCssNames.ON);
                            leftOffset += connectorWidth;
                        } else {
                            ElementManager.ClearCssProperty(envelopId, "left");
                            ElementManager.setClassName(this.Id() + "_SnapStatus", GeneralCssNames.OFF);
                            leftOffset = 0;
                        }
                        const topOffset : number = yAxisHandler(envelopId, connectorsHeight + buttonsHeight);
                        const outputHeight : number = connectorsHeight / visibleConnectors.length;

                        visibleConnectors.forEach(($value : number, $index : number) : void => {
                            const output : Output = root.getInput($index).innerOutput;
                            output.GridPosition({
                                x: leftOffset,
                                y: buttonsHeight / 2 + topOffset + ($index + .5) * outputHeight
                            });
                        });
                    }
                } else if (this.guiType === KernelNodeInterfaceType.INPUT) {
                    if (this.Visible()) {
                        const connectorsHeight : number = getConnectorsHeight();
                        const buttonsHeight : number = getButtonsHeight();

                        let rightOffset : number = Math.min(anchorSize.Width() + (anchorOffset.x - envelopSize.Width()) /
                            zoomMult, anchorSize.Width() - connectorWidth);
                        if (rightOffset > -connectorWidth) {
                            ElementManager.setCssProperty(envelopId, "right", rightOffset);
                            ElementManager.setClassName(this.Id() + "_SnapStatus", GeneralCssNames.ON);
                            rightOffset += connectorWidth;
                        } else {
                            ElementManager.ClearCssProperty(envelopId, "right");
                            ElementManager.setClassName(this.Id() + "_SnapStatus", GeneralCssNames.OFF);
                            rightOffset = 0;
                        }
                        const topOffset : number = yAxisHandler(envelopId, connectorsHeight + buttonsHeight);
                        const inputHeight : number = connectorsHeight / visibleConnectors.length;
                        const leftOffset : number = anchorSize.Width() - rightOffset;

                        visibleConnectors.forEach(($value : number, $index : number) : void => {
                            const input : Input = root.getOutput($index).innerInput;
                            input.GridPosition({
                                x: leftOffset,
                                y: buttonsHeight / 2 + topOffset + ($index + .5) * inputHeight
                            });
                        });
                    }
                }
                this.UpdateConnectorsStatus();
            }
        }

        public CreateConnections() : void {
            const root : IKernelNode = this.gridManager.Root();
            if (root.Type() === KernelNodeType.VXGROUP) {
                if (this.guiType === KernelNodeInterfaceType.OUTPUT) {
                    root.getInputs().foreach(($input : Input) : void => {
                        $input.innerOutput.inputs.foreach(($input : Input) : void => {
                            this.connectionManager.CreateConnection($input);
                        });
                    });
                } else if (this.guiType === KernelNodeInterfaceType.INPUT) {
                    root.getOutputs().foreach(($output : Output) : void => {
                        const innerNodeOutput : Output = $output.innerInput.output;
                        if (!ObjectValidator.IsEmptyOrNull(innerNodeOutput)) {
                            innerNodeOutput.inputs.foreach(($input : Input) : void => {
                                this.connectionManager.CreateConnection($input);
                            });
                        }
                    });
                }
            }
        }

        public UpdateConnectorsStatus() : void {
            const root : IKernelNode = this.gridManager.Root();
            (<any>(this.guiType === KernelNodeInterfaceType.OUTPUT ? root.getInputs() : root.getOutputs()))
                .foreach(($interface : KernelNodeInterface) : void => {
                    this.UpdateConnectorStatus($interface.position);
                });
        }

        public UpdateConnectorStatus($index : number, $value? : ConnectorHolderStatus, $animate? : boolean) : void {
            const envelop : string = this.Id() + "_Type";
            const connectorWidth : number = ElementManager.getOffsetWidth(envelop, false, false);
            const connectorOffset : number = ConnectorHolder.defaultConnectorSize - this.getConnectorSize() / 2;
            const connector : KernelNodeConnector = this.connectors.getItem($index);
            const root : IKernelNode = this.gridManager.Root();

            if (ObjectValidator.IsSet($animate)) {
                ConnectorHolder.AnimateConnectors($animate);
            }

            const setConnectorPos : any = ($value : number) : void => {
                if (connector.IsLoaded()) {
                    ElementManager.setCssProperty(connector.Id() + "_Type",
                        this.guiType === KernelNodeInterfaceType.OUTPUT ? "margin-left" : "margin-right", $value);
                } else {
                    connector.getEvents().setOnLoad(() : void => {
                        ElementManager.setCssProperty(connector.Id() + "_Type",
                            this.guiType === KernelNodeInterfaceType.OUTPUT ? "margin-left" : "margin-right", $value);
                    });
                }
            };

            const resetConnectorPos : any = () : void => {
                const centeredMargin : number = (connectorWidth - this.getConnectorSize()) / 2 - 1;

                if (connector.IsLoaded()) {
                    ElementManager.setCssProperty(connector.Id() + "_Type",
                        this.guiType === KernelNodeInterfaceType.OUTPUT ? "margin-left" : "margin-right", centeredMargin);
                } else {
                    connector.getEvents().setOnLoad(() : void => {
                        ElementManager.setCssProperty(connector.Id() + "_Type",
                            this.guiType === KernelNodeInterfaceType.OUTPUT ? "margin-left" : "margin-right", centeredMargin);
                    });
                }
            };

            const changeStatus : any = ($value : ConnectorHolderStatus, $pos : number) : void => {
                switch ($value) {
                case ConnectorHolderStatus.ON:
                    ElementManager.ClearCssProperty(connector.Id(), "pointer-events");
                    ElementManager.setOpacity(connector.Id(), 100);
                    resetConnectorPos();
                    break;
                case ConnectorHolderStatus.ACTIVE:
                    ElementManager.ClearCssProperty(connector.Id(), "pointer-events");
                    ElementManager.setOpacity(connector.Id(), 100);
                    setConnectorPos($pos);
                    break;
                case ConnectorHolderStatus.OFF:
                    ElementManager.setCssProperty(connector.Id(), "pointer-events", "none");
                    ElementManager.setOpacity(connector.Id(), 0);
                    break;
                }
            };

            if (this.guiType === KernelNodeInterfaceType.OUTPUT) {
                const input : Input = root.getInputs().getItem($index);
                const innerOutput : Output = input.innerOutput;
                const pos : IVector = innerOutput.GridPosition();
                const stackHandlers : ArrayList<any> = new ArrayList<any>();

                const value : ConnectorHolderStatus = ObjectValidator.IsSet($value) ?
                    $value : (!ObjectValidator.IsEmptyOrNull(innerOutput.inputs) ? ConnectorHolderStatus.ACTIVE : ConnectorHolderStatus.ON);
                changeStatus(value, connectorWidth / 2 + connectorOffset);

                innerOutput.inputs.foreach(($innerNodeInput : Input) : void => {
                    const connection : IConnection = $innerNodeInput.connection;
                    if (!ObjectValidator.IsEmptyOrNull(connection)) {
                        this.connectionManager.setConnectionStartPoint(connection.id, pos);
                        const stack : IConnectionStack = connection.stack;
                        if (!ObjectValidator.IsEmptyOrNull(stack)) {
                            stackHandlers.Add(() : void => {
                                this.connectionManager.setConnectionStartPoint(stack.id, pos);
                            }, stack.id);
                        }
                    }
                });
                stackHandlers.foreach(($handler : any) : void => {
                    $handler();
                });
            } else if (this.guiType === KernelNodeInterfaceType.INPUT) {
                const innerInput : Input = root.getOutputs().getItem($index).innerInput;
                const pos : IVector = innerInput.GridPosition();

                const value : ConnectorHolderStatus = ObjectValidator.IsSet($value) ?
                    $value : (!ObjectValidator.IsEmptyOrNull(innerInput.output) ? ConnectorHolderStatus.ACTIVE : ConnectorHolderStatus.ON);
                changeStatus(value, connectorWidth / 2 + connectorOffset);

                if (!ObjectValidator.IsEmptyOrNull(innerInput.connection)) {
                    this.connectionManager.setConnectionEndPoint(innerInput.connection.id, pos);
                }
            }
        }

        public getEvents() : IConnectorHolderEvents {
            return <IConnectorHolderEvents>super.getEvents();
        }

        protected guiTypeValueSetter($value : KernelNodeInterfaceType) : KernelNodeInterfaceType {
            return Property.EnumType(this.guiType, $value, KernelNodeInterfaceType, KernelNodeInterfaceType.OUTPUT);
        }

        protected styleClassNameSetterValidator($value : string) : boolean {
            if (!KernelNodeInterfaceType.Contains($value)) {
                return super.styleClassNameSetterValidator($value);
            }
            Echo.Println("Use GuiType method for set of connector holder type instead of StyleClassName method.");
            return false;
        }

        protected getEventsManagerClass() : any {
            return ElementEventsManager;
        }

        protected innerCode() : IGuiElement {
            const generateEventArgs : any = ($connector : KernelNodeConnector) : KernelNodeEventArgs => {
                const eventArgs : KernelNodeEventArgs = new KernelNodeEventArgs();
                eventArgs.Owner(this.gridManager.Root());
                eventArgs.Index($connector.Index());
                eventArgs.DataType(InterfaceDataType.DELEGATED);
                eventArgs.ConnectorType($connector.GuiType());
                return eventArgs;
            };

            for (let i = 0; i < ConnectorHolder.maxConnectorNum; i++) {
                const connector : KernelNodeConnector = new KernelNodeConnector(this.connectors.Length());
                connector.GuiType(this.guiType);
                connector.Visible(true);
                this.connectors.Add(connector);

                this[connector.Id()] = connector;
                connector.getEvents().setOnMouseUp(() : void => {
                    this.getEventsManager().FireEvent(this, KernelNodeEventType.ON_CONNECTOR_MOUSE_UP, generateEventArgs(connector));
                });
                connector.getEvents().setOnMouseDown(($eventArgs : EventArgs) : void => {
                    $eventArgs.StopAllPropagation();
                    ConnectorHolder.AnimateConnectors(true);
                    deselectAll();
                    this.getEventsManager().FireEvent(this, KernelNodeEventType.ON_CONNECTOR_MOUSE_DOWN, generateEventArgs(connector));
                });
                this.connectorEnvelops.Add(this.addElement(connector.Id() + "_ConnectorEnvelop")
                    .GuiTypeTag("ConnectorEnvelop").Add(connector));
            }

            const createConnector : any = () : void => {
                const root : IKernelNode = this.gridManager.Root();
                if (this.guiType === KernelNodeInterfaceType.OUTPUT) {
                    root.AddInput(InterfaceDataType.DELEGATED);
                    const input : Input = root.getInputs().getLast();
                    input.innerOutput = new Output();
                    input.innerOutput.delegator = input;
                    input.innerOutput.node = root;
                    input.innerOutput.position = input.position;
                    input.innerOutput.dataType = InterfaceDataType.DELEGATED;
                } else if (this.guiType === KernelNodeInterfaceType.INPUT) {
                    root.AddOutput(InterfaceDataType.DELEGATED);
                    const output : Output = root.getOutputs().getLast();
                    output.innerInput = new Input();
                    output.innerInput.delegator = output;
                    output.innerInput.node = root;
                    output.innerInput.position = output.position;
                    output.innerInput.dataType = InterfaceDataType.DELEGATED;
                }
                this.HandleVisibility();
            };

            this.addButtonBefore.getEvents().setOnClick(() : void => {
                this.animateConnectors(false);
                createConnector();
                if (!ObjectValidator.IsEmptyOrNull(this.lastVisible)) {
                    this.moveConnector(this.lastVisible.Index(), 0);
                }
                this.Snap();

                const eventArgs : ConnectorHolderEventArgs = new ConnectorHolderEventArgs();
                eventArgs.ToIndex(0);
                eventArgs.InterfaceType(this.guiType);
                this.getEventsManager().FireEvent(this, ConnectorHolderEventType.ON_CONNECTOR_UPDATE, eventArgs, false);
            });

            this.addButtonAfter.getEvents().setOnClick(() : void => {
                const root : IKernelNode = this.gridManager.Root();
                this.animateConnectors(false);
                createConnector();

                const eventArgs : ConnectorHolderEventArgs = new ConnectorHolderEventArgs();
                eventArgs.ToIndex((this.guiType === KernelNodeInterfaceType.OUTPUT ?
                    root.getInputs().Length() : root.getOutputs().Length()) - 1);
                eventArgs.InterfaceType(this.guiType);
                this.getEventsManager().FireEvent(this, ConnectorHolderEventType.ON_CONNECTOR_UPDATE, eventArgs, false);
            });

            WindowManager.getEvents().setOnMove(() : void => {
                this.getEventsManager().FireAsynchronousMethod(() : void => {
                    const holders : ArrayList<ConnectorHolder> =
                        <ArrayList<ConnectorHolder>>GuiObjectManager.getInstanceSingleton().getType(ConnectorHolder);

                    holders.foreach(($holder : ConnectorHolder) : void => {
                        if (!ObjectValidator.IsEmptyOrNull($holder.draggedConnector)) {
                            $holder.animateConnectors(false);
                            const root : IKernelNode = $holder.gridManager.Root();
                            const interfaces : ArrayList<KernelNodeInterface> = $holder.guiType === KernelNodeInterfaceType.INPUT ?
                                root.getOutputs() : root.getInputs();

                            const mousePos : IVector = $holder.gridManager.getMouseAreaPosition();
                            const stepStartIndex : number = $holder.draggedConnector;
                            if (ObjectValidator.IsEmptyOrNull($holder.moveStartIndex)) {
                                $holder.moveStartIndex = stepStartIndex;
                            }
                            $holder.moveEndIndex = stepStartIndex;
                            $holder.connectors.foreach(($connector : KernelNodeConnector) : void => {
                                const target : any = interfaces.getItem($connector.Index());
                                if (!ObjectValidator.IsEmptyOrNull(target)) {
                                    const innerTarget : KernelNodeInterface = $holder.guiType === KernelNodeInterfaceType.INPUT ?
                                        target.innerInput : target.innerOutput;
                                    const connectorHeight : number = ElementManager.getOffsetHeight($connector.Id() + "_ConnectorEnvelop");
                                    if ((mousePos.y > innerTarget.GridPosition().y - connectorHeight / 2
                                        || $connector.Index() === 0) &&
                                        (mousePos.y < innerTarget.GridPosition().y + connectorHeight / 2
                                            || $connector.Index() === interfaces.Length() - 1)) {
                                        $holder.moveEndIndex = $connector.Index();
                                    }
                                }
                            });

                            if (stepStartIndex !== $holder.moveEndIndex) {
                                $holder.moveConnector(stepStartIndex, $holder.moveEndIndex);
                                $holder.Snap();
                            }
                        }
                    });
                });

            });

            const deselectAll : any = () : void => {
                const holders : ArrayList<ConnectorHolder> =
                    <ArrayList<ConnectorHolder>>GuiObjectManager.getInstanceSingleton().getType(ConnectorHolder);
                holders.foreach(($holder : ConnectorHolder) : void => {
                    $holder.deselect();
                });
            };

            WindowManager.getEvents().setOnMouseUp(() : void => {
                const holders : ArrayList<ConnectorHolder> =
                    <ArrayList<ConnectorHolder>>GuiObjectManager.getInstanceSingleton().getType(ConnectorHolder);
                holders.foreach(($holder : ConnectorHolder) : void => {
                    $holder.draggedConnector = null;
                    if (!ObjectValidator.IsEmptyOrNull($holder.moveStartIndex) && $holder.moveStartIndex !== $holder.moveEndIndex) {
                        const eventArgs : ConnectorHolderEventArgs = new ConnectorHolderEventArgs();
                        eventArgs.FromIndex($holder.moveStartIndex);
                        eventArgs.ToIndex($holder.moveEndIndex);
                        eventArgs.InterfaceType($holder.guiType);
                        $holder.getEventsManager().FireEvent($holder, ConnectorHolderEventType.ON_CONNECTOR_UPDATE, eventArgs, false);
                    }
                    $holder.moveStartIndex = null;
                    $holder.moveEndIndex = null;
                });
            });

            WindowManager.getEvents().setOnMouseDown(() : void => {
                deselectAll();
            });

            WindowManager.getEvents().setOnKeyUp(($eventArgs : KeyEventArgs) : void => {
                switch ($eventArgs.getKeyCode()) {
                case KeyMap.DELETE:
                    const holders : ArrayList<ConnectorHolder> =
                        <ArrayList<ConnectorHolder>>GuiObjectManager.getInstanceSingleton().getType(ConnectorHolder);
                    holders.foreach(($holder : ConnectorHolder) : void => {
                        $holder.removeSelected();
                    });
                    break;
                }
            });

            this.getEvents().setOnComplete(() : void => {
                let hoveredEnvelop : string = "";
                this.connectorEnvelops.foreach(($envelop : IGuiElement, $index : number) : void => {
                    const index = $index;
                    const id : string = $envelop.getId();
                    const events : ElementEventsManager = new ElementEventsManager(id);

                    events.setOnMouseOver(() : void => {
                        const root : IKernelNode = this.gridManager.Root();
                        hoveredEnvelop = id;
                        let text : string = "Inner" + this.guiType + " " + $index;
                        if (this.guiType === KernelNodeInterfaceType.OUTPUT) {
                            const typedOutput : Output =
                                KernelNode.getTypedInterface(root.getInput($index).output);
                            if (!ObjectValidator.IsEmptyOrNull(typedOutput)) {
                                const help : string = typedOutput.node.Help().outputs[typedOutput.position];
                                if (!ObjectValidator.IsEmptyOrNull(help)) {
                                    text = help;
                                }
                            }
                        } else if (this.guiType === KernelNodeInterfaceType.INPUT) {
                            const typedInput : Input =
                                KernelNode.getTypedInterface(root.getOutput($index).inputs.getFirst());
                            if (!ObjectValidator.IsEmptyOrNull(typedInput)) {
                                const help : string = typedInput.node.Help().inputs[typedInput.position];
                                if (!ObjectValidator.IsEmptyOrNull(help)) {
                                    text = help;
                                }
                            }
                        }
                        ToolTip.ShowGlobal(text, 500, () : boolean => {
                            return hoveredEnvelop === id;
                        });
                        if (this.selectedConnector !== $index) {
                            ElementManager.setClassName(id, GeneralCssNames.ON);
                        }
                    });
                    events.setOnMouseOut(() : void => {
                        hoveredEnvelop = null;
                        if (this.selectedConnector !== $index) {
                            ElementManager.setClassName(id, GeneralCssNames.OFF);
                        }
                    });
                    events.setOnMouseDown(($eventArgs : EventArgs) : void => {
                        $eventArgs.StopAllPropagation();
                        deselectAll();
                        this.selectConnector(index);
                        this.draggedConnector = index;
                    });
                    events.setOnMouseUp(() : void => {
                        this.draggedConnector = null;
                    });
                    events.Subscribe();
                });
            });

            let isCreateActive : boolean = false;
            const events : ElementEventsManager = new ElementEventsManager(this.Id() + "_Connectors");
            events.setOnMouseOver(() : void => {
                if (ObjectValidator.IsEmptyOrNull(this.lastVisible)) {
                    isCreateActive = true;
                    ToolTip.ShowGlobal("Create connector", 1000, () : boolean => {
                        return isCreateActive;
                    });
                }
            });
            events.setOnMouseOut(() : void => {
                if (ObjectValidator.IsEmptyOrNull(this.lastVisible)) {
                    isCreateActive = false;
                }
            });
            events.setOnClick(() : void => {
                if (ObjectValidator.IsEmptyOrNull(this.lastVisible)) {
                    isCreateActive = false;
                    ConnectorHolder.AnimateConnectors(false);
                    createConnector();

                    const eventArgs : ConnectorHolderEventArgs = new ConnectorHolderEventArgs();
                    eventArgs.ToIndex(0);
                    eventArgs.InterfaceType(this.guiType);
                    this.getEventsManager().FireEvent(this, ConnectorHolderEventType.ON_CONNECTOR_UPDATE, eventArgs, false);

                    ElementManager.setCssProperty(this.connectorEnvelops.getFirst().getId(), "pointer-events", "none");
                    this.getEventsManager().FireAsynchronousMethod(() : void => {
                        ElementManager.ClearCssProperty(this.connectorEnvelops.getFirst().getId(), "pointer-events");
                    }, true, 300);
                }
            });
            events.Subscribe();
            return super.innerCode();
        }

        protected innerHtml() : IGuiElement {
            const connectors = this.addElement(this.Id() + "_Connectors").StyleClassName("Connectors");
            this.connectorEnvelops.foreach(($envelop : IGuiElement) : void => {
                connectors.Add($envelop);
            });

            return this.addElement(this.Id() + "_Type").GuiTypeTag(this.getGuiTypeTag()).StyleClassName(this.guiType)
                .Add(this.addElement(this.Id() + "_Animation").StyleClassName(GeneralCssNames.OFF)
                    .Add(this.addElement(this.Id() + "_Status").StyleClassName(GeneralCssNames.DISABLE)
                        .Add(this.addElement(this.Id() + "_SnapStatus").StyleClassName(GeneralCssNames.ON)
                            .Add(this.addElement().StyleClassName("BeforeButtonEnvelop").Add(this.addButtonBefore))
                            .Add(connectors)
                            .Add(this.addElement().StyleClassName("AfterButtonEnvelop").Add(this.addButtonAfter)))));
        }

        private animateConnectors($value : boolean) : void {
            ElementManager.setClassName(this.Id() + "_Animation", $value ? GeneralCssNames.ON : GeneralCssNames.OFF);
        }

        private selectConnector($index : number) : void {
            this.deselect();
            ElementManager.setClassName(this.connectorEnvelops.getItem($index).getId(), GeneralCssNames.ACTIVE);
            this.selectedConnector = $index;
        }

        private deselect() : void {
            if (!ObjectValidator.IsEmptyOrNull(this.selectedConnector)) {
                const envelop : IGuiElement = this.connectorEnvelops.getItem(this.selectedConnector);
                ElementManager.setClassName(envelop.getId(), GeneralCssNames.OFF);
                this.selectedConnector = null;
            }
        }

        private moveConnector($fromIndex : number, $toIndex : number) : void {
            const root : IKernelNode = this.gridManager.Root();
            root.MoveInterface($fromIndex, $toIndex, this.guiType === KernelNodeInterfaceType.OUTPUT ?
                KernelNodeInterfaceType.INPUT : KernelNodeInterfaceType.OUTPUT);
            if (this.selectedConnector === $fromIndex) {
                this.selectConnector($toIndex);
                if (this.draggedConnector === $fromIndex) {
                    this.draggedConnector = $toIndex;
                }
            } else if (this.selectedConnector === $toIndex) {
                this.selectConnector($fromIndex);
                if (this.draggedConnector === $toIndex) {
                    this.draggedConnector = $fromIndex;
                }
            }
        }

        private removeSelected() {
            if (!ObjectValidator.IsEmptyOrNull(this.selectedConnector) && ObjectValidator.IsEmptyOrNull(this.draggedConnector)) {
                const eventArgs : ConnectorHolderEventArgs = new ConnectorHolderEventArgs();
                eventArgs.FromIndex(this.selectedConnector);
                eventArgs.InterfaceType(this.guiType);
                this.getEventsManager().FireEvent(this, ConnectorHolderEventType.ON_CONNECTOR_UPDATE, eventArgs, false);

                const root : IKernelNode = this.gridManager.Root();
                if (this.guiType === KernelNodeInterfaceType.OUTPUT) {
                    const outerInput : Input = root.getInput(this.selectedConnector);
                    outerInput.innerOutput.inputs.ToArray().forEach(($input : Input) : void => {
                        if (!ObjectValidator.IsEmptyOrNull($input.connection)) {
                            this.connectionManager.RemoveConnection($input.connection.id);
                        }
                    });
                    root.RemoveInterface(this.selectedConnector, KernelNodeInterfaceType.INPUT);
                } else if (this.guiType === KernelNodeInterfaceType.INPUT) {
                    const outerOutput : Output = root.getOutput(this.selectedConnector);
                    const input : Input = outerOutput.innerInput;
                    if (!ObjectValidator.IsEmptyOrNull(input.connection)) {
                        this.connectionManager.RemoveConnection(input.connection.id);
                    }
                    root.RemoveInterface(this.selectedConnector, KernelNodeInterfaceType.OUTPUT);
                }
                ElementManager.setClassName(this.connectors.getItem(this.selectedConnector).Id() + "_ConnectorEnvelop",
                    GeneralCssNames.OFF);

                this.selectedConnector = null;
                ConnectorHolder.Update(true);
            }
        }

        private getConnectorSize() : number {
            const normalizedScale : number = Math.min(Math.max(this.gridManager.getScale(), ConnectorHolder.minConnectorScale),
                ConnectorHolder.maxConnectorScale);
            return ConnectorHolder.defaultConnectorSize * normalizedScale;
        }

        private scaleConnectors() : void {
            const scaledSize : number = this.getConnectorSize();
            this.connectors.foreach(($connector : KernelNodeConnector) : void => {
                ElementManager.setCssProperty($connector.Id() + "_Type", "width", scaledSize);
                ElementManager.setCssProperty($connector.Id() + "_Type", "height", scaledSize);
            });
        }
    }
}
