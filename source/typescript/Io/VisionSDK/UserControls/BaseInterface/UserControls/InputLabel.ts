/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.BaseInterface.UserControls {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import ToolTip = Io.VisionSDK.UserControls.BaseInterface.Components.ToolTip;
    import InputLabelType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.InputLabelType;

    export class InputLabel extends Com.Wui.Framework.UserControls.BaseInterface.UserControls.InputLabel {

        constructor($inputLabelType? : InputLabelType, $text? : string, $id? : string) {
            super($inputLabelType, $text, $id);
        }

        /**
         * @param {InputLabelType} [$inputLabelType] Specify type of element look and feel.
         * @return {InputLabelType} Returns type of element's look and feel.
         */
        public GuiType($inputLabelType? : InputLabelType) : InputLabelType {
            return <InputLabelType>super.GuiType($inputLabelType);
        }

        /**
         * @return {IToolTip} This method should return class with
         * interface Com.Wui.Framework.Gui.Interfaces.Components.IToolTip
         */
        protected getTitleClass() : any {
            return ToolTip;
        }

        protected guiTypeValueSetter($value : any) : any {
            return Property.EnumType(this.GuiType(), $value, InputLabelType, InputLabelType.PROPERTY);
        }

        protected styleClassNameSetterValidator($value : string) : boolean {
            if (!InputLabelType.Contains($value)) {
                return super.styleClassNameSetterValidator($value);
            }
            Echo.Println("Use GuiType method for set of inputLabel type instead of StyleClassName method.");
            return false;
        }
    }
}
