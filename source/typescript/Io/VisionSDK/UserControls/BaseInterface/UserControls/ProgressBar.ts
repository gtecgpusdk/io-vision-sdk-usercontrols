/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.BaseInterface.UserControls {
    "use strict";
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import ProgressBarType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.ProgressBarType;
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;
    import ImageType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.ImageType;
    import ImageOutputArgs = Com.Wui.Framework.Gui.ImageProcessor.ImageOutputArgs;
    import ToolTip = Io.VisionSDK.UserControls.BaseInterface.Components.ToolTip;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import ValueProgressEventArgs = Com.Wui.Framework.Gui.Events.Args.ValueProgressEventArgs;
    import ValueProgressManager = Com.Wui.Framework.Gui.Utils.ValueProgressManager;
    import DirectionType = Com.Wui.Framework.Gui.Enums.DirectionType;
    import ProgressType = Com.Wui.Framework.Gui.Enums.ProgressType;
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;

    /**
     * ProgressBar class renders element for capturing of process progression
     */
    export class ProgressBar extends Com.Wui.Framework.UserControls.BaseInterface.UserControls.ProgressBar {
        private readonly background : Image;
        private isMarqueeEnabled : boolean;
        private readonly barText : Label;

        /**
         * @param {ProgressBarType} [$progressBarType] Specify type of element look and feel.
         * @param {string} [$id] Force set element id instead of generated one.
         */
        constructor($progressBarType? : ProgressBarType, $id? : string) {
            super($progressBarType, $id);

            this.background = new Image(ImageType.STATIC_BAR);
            this.isMarqueeEnabled = false;
            this.barText = new Label();
        }

        public Value($value? : number) : number {
            if (this.IsCompleted() && this.isMarqueeEnabled) {
                if (ObjectValidator.IsSet($value)) {
                    this.getEventsManager().setEvent(this, this.getClassName() + "_marqueStop_" + EventType.ON_COMPLETE,
                        () : void => {
                            this.isMarqueeEnabled = false;
                            super.Value($value);
                        });
                }
                return super.Value();
            }
            return super.Value($value);
        }

        public Text($value? : string) : string {
            if (ObjectValidator.IsSet($value)) {
                this.barText.Visible(!ObjectValidator.IsEmptyOrNull($value));
            }
            return this.barText.Value($value);
        }

        public Visible($value? : boolean) : boolean {
            if ($value) {
                if (this.GuiType() === ProgressBarType.MARQUEE && ElementManager.Exists(this.Id() + "_Bar")) {
                    ElementManager.setWidth(this.Id() + "_Bar", 0);
                    this.GuiType(ProgressBarType.GENERAL);
                    this.GuiType(ProgressBarType.MARQUEE);
                }
            }
            return super.Visible($value);
        }

        /**
         * @param {ProgressBarType} [$progressBarType] Specify type of element look and feel.
         * @return {ProgressBarType} Returns type of element's look and feel.
         */
        public GuiType($progressBarType? : ProgressBarType) : ProgressBarType {
            if ($progressBarType === ProgressBarType.MARQUEE) {
                this.isMarqueeEnabled = true;
            }
            if (ObjectValidator.IsSet($progressBarType) && this.IsCompleted() && super.GuiType() !== $progressBarType) {
                const manipulatorArgs : ValueProgressEventArgs = ValueProgressManager.get(this.Id());
                manipulatorArgs.Owner(this);
                manipulatorArgs.ProgressType(ProgressType.LINEAR);
                manipulatorArgs.RangeStart(0);
                manipulatorArgs.Step(3);

                const marqueeWidth : number = Math.ceil(this.Width() * 0.25);
                const currentWidth : number = ElementManager.getElement(this.Id() + "_Bar").offsetWidth;
                const position : number = ElementManager.getElement(this.Id() + "_Bar").offsetLeft;
                const barOffset : number = ElementManager.getElement(this.Id() + "_BarEnvelop").offsetLeft;

                if ($progressBarType === ProgressBarType.MARQUEE) {
                    manipulatorArgs.CurrentValue(0);
                    manipulatorArgs.RangeEnd(currentWidth - marqueeWidth);
                    manipulatorArgs.DirectionType(DirectionType.UP);

                    manipulatorArgs.ChangeEventType(this.getClassName() + "_marqueStart_" + EventType.ON_CHANGE);
                    manipulatorArgs.CompleteEventType(this.getClassName() + "_marqueStart_" + EventType.ON_COMPLETE);

                    this.getEventsManager().setEvent(this, this.getClassName() + "_marqueStart_" + EventType.ON_CHANGE,
                        ($eventArgs : ValueProgressEventArgs) : void => {
                            if (this.isMarqueeEnabled) {
                                ElementManager.setCssProperty(this.Id() + "_Bar", "left", $eventArgs.CurrentValue());
                                ElementManager.setWidth(this.Id() + "_Bar", currentWidth - $eventArgs.CurrentValue());
                            }
                        });

                    this.getEventsManager().setEvent(this, this.getClassName() + "_marqueStart_" + EventType.ON_COMPLETE,
                        ($eventArgs : ValueProgressEventArgs) : void => {
                            if (this.isMarqueeEnabled) {
                                ElementManager.setWidth(this.Id() + "_Bar", marqueeWidth);

                                $eventArgs.RangeStart(0);
                                $eventArgs.RangeEnd(this.Width() - marqueeWidth - barOffset);
                                $eventArgs.DirectionType(DirectionType.UP);
                                $eventArgs.Step(1);

                                $eventArgs.ChangeEventType(this.getClassName() + "_marque_" + EventType.ON_CHANGE);
                                $eventArgs.CompleteEventType(this.getClassName() + "_marque_" + EventType.ON_COMPLETE);

                                this.getEventsManager().setEvent(this, this.getClassName() + "_marque_" + EventType.ON_CHANGE,
                                    ($eventArgs : ValueProgressEventArgs) : void => {
                                        if (this.isMarqueeEnabled && this.Visible()) {
                                            ElementManager.setCssProperty(this.Id() + "_Bar", "left", $eventArgs.CurrentValue());
                                        }
                                    });

                                this.getEventsManager().setEvent(this, this.getClassName() + "_marque_" + EventType.ON_COMPLETE,
                                    ($eventArgs : ValueProgressEventArgs) : void => {
                                        if (this.isMarqueeEnabled) {
                                            if ($eventArgs.DirectionType() === DirectionType.UP) {
                                                $eventArgs.DirectionType(DirectionType.DOWN);
                                            } else {
                                                $eventArgs.CurrentValue(0);
                                                $eventArgs.RangeStart(0);
                                                $eventArgs.RangeEnd(this.Width() - marqueeWidth - barOffset);
                                                $eventArgs.DirectionType(DirectionType.UP);
                                            }
                                            this.getEvents().FireAsynchronousMethod(() : void => {
                                                ValueProgressManager.Execute($eventArgs);
                                            }, 50);
                                        }
                                    });
                                ValueProgressManager.Execute(manipulatorArgs);
                            }
                        });
                    ValueProgressManager.Execute(manipulatorArgs);
                } else {
                    if (position > 0) {
                        manipulatorArgs.CurrentValue(position);
                        manipulatorArgs.RangeEnd(position + currentWidth);
                        manipulatorArgs.DirectionType(DirectionType.DOWN);
                        manipulatorArgs.ChangeEventType(this.getClassName() + "_marqueStop_" + EventType.ON_CHANGE);
                        manipulatorArgs.CompleteEventType(this.getClassName() + "_marqueStop_" + EventType.ON_COMPLETE);

                        this.getEventsManager().setEvent(this, this.getClassName() + "_marqueStop_" + EventType.ON_CHANGE,
                            ($eventArgs : ValueProgressEventArgs) : void => {
                                if (this.isMarqueeEnabled) {
                                    ElementManager.setCssProperty(this.Id() + "_Bar", "left", $eventArgs.CurrentValue());
                                    ElementManager.setWidth(this.Id() + "_Bar", currentWidth + position - $eventArgs.CurrentValue());
                                }
                            });

                        this.getEventsManager().setEvent(this, this.getClassName() + "_marqueStop_" + EventType.ON_COMPLETE,
                            ($eventArgs : ValueProgressEventArgs) : void => {
                                if (this.isMarqueeEnabled) {
                                    ValueProgressManager.Remove(this.Id());
                                    $eventArgs.CurrentValue(
                                        (this.RangeEnd() - this.RangeStart()) *
                                        (100 / this.Width() * ElementManager.getElement(this.Id() + "_Bar").offsetWidth) / 100);
                                }
                                ElementManager.setCssProperty(this.Id() + "_Bar", "left", 0);
                            });

                        ValueProgressManager.Execute(manipulatorArgs);
                    }
                }
            }
            return <ProgressBarType>super.GuiType($progressBarType);
        }

        public Width($value? : number) : number {
            const width : number = super.Width($value);
            if (ObjectValidator.IsSet($value) && this.IsCompleted()) {
                const args : ImageOutputArgs = this.background.OutputArgs();
                args.setSize($value, this.background.getHeight());
                this.background.OutputArgs(args);
                if (this.GuiType() === ProgressBarType.MARQUEE) {
                    this.GuiType(ProgressBarType.GENERAL);
                    this.GuiType(ProgressBarType.MARQUEE);
                }
            }
            return width;
        }

        /**
         * @return {IToolTip} This method should return class with
         * interface Com.Wui.Framework.Gui.Interfaces.Components.IToolTip
         */
        protected getTitleClass() : any {
            return ToolTip;
        }

        protected guiTypeValueSetter($value : any) : any {
            return Property.EnumType(this.GuiType(), $value, ProgressBarType, ProgressBarType.GENERAL);
        }

        protected styleClassNameSetterValidator($value : string) : boolean {
            if (!ProgressBarType.Contains($value)) {
                return super.styleClassNameSetterValidator($value);
            }
            Echo.Println("Use GuiType method for set of ProgressBar type instead of StyleClassName method.");
            return false;
        }

        protected innerCode() : IGuiElement {
            const barArgs : ImageOutputArgs = new ImageOutputArgs();
            barArgs.setSize(this.Width(), 18);
            this.background.OutputArgs(barArgs);
            this.barText.Visible(false);

            this.getEvents().setOnComplete(() : void => {
                if (this.GuiType() === ProgressBarType.MARQUEE) {
                    this.GuiType(ProgressBarType.GENERAL);
                    this.GuiType(ProgressBarType.MARQUEE);
                }
            });

            return super.innerCode();
        }

        protected innerHtml() : IGuiElement {
            const innerHtml : IGuiElement = super.innerHtml();
            (<IGuiElement>(<any>innerHtml)
                .getChildElement(0)
                .getChildElement(0)
                .getChildElement(1)
                .getChildElement(0)
                .getChildElement(0)
                .getChildElement(1))
                .Add(this.background);

            (<IGuiElement>(<any>innerHtml)
                .getChildElement(0)
                .getChildElement(0)
                .getChildElement(1))
                .Add(this.barText);

            return innerHtml;
        }
    }
}
