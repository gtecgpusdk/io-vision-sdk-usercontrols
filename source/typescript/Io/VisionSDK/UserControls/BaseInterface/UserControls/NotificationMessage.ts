/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.BaseInterface.UserControls {
    "use strict";
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import GeneralCssNames = Com.Wui.Framework.Gui.Enums.GeneralCssNames;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;
    import ImageButtonType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.ImageButtonType;
    import ToolTip = Io.VisionSDK.UserControls.BaseInterface.Components.ToolTip;
    import NotificationMessageType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.NotificationMessageType;

    /**
     * NotificationMessage class renders message suitable as content for notifications handler.
     */
    export class NotificationMessage extends Com.Wui.Framework.UserControls.Primitives.BaseGuiObject {
        public closeButton : ImageButton;
        private prefix : string;
        private text : string;
        private width : number;
        private guiType : NotificationMessageType;

        private static resize($element : NotificationMessage) : void {
            ElementManager.ClearCssProperty($element.Id(), "width");
            ElementManager.setWidth($element.Id(),
                ($element.Width() === -1 ? ElementManager.getElement($element.Id()).offsetWidth : $element.Width())
                - ElementManager.getCssIntegerValue($element.Id(), "border-left-width")
                - ElementManager.getCssIntegerValue($element.Id(), "border-right-width"));
        }

        private static resizeEventHandler($eventArgs : EventArgs) : void {
            NotificationMessage.resize(<NotificationMessage>$eventArgs.Owner());
        }

        /**
         * @param {string} [$text] Specify notification message.
         * @param {NotificationMessageType} [$messageType] Specify element's type of look and feel.
         * @param {string} [$id] Force set element id instead of generated one.
         */
        constructor($text? : string, $messageType? : NotificationMessageType, $id? : string) {
            super($id);
            this.guiType = this.guiTypeValueSetter($messageType);
            this.prefix = "";
            this.Text($text);
            this.closeButton = new ImageButton(ImageButtonType.ROUND_CLOSE);
        }

        /**
         * @param {string} [$value] Set text value, which should be displayed as message content.
         * @return {string} Returns element's message text value.
         */
        public Text($value? : string) : string {
            this.text = Property.NullString(this.text, $value);
            if (ObjectValidator.IsSet($value)) {
                if (this.IsLoaded()) {
                    ElementManager.setInnerHtml(this.Id() + "_Text", $value);
                    if (ElementManager.IsVisible(this.Id())) {
                        NotificationMessage.resize(this);
                    }
                }
            }
            return this.text;
        }

        /**
         * @param {string} [$value] Set text value, which should be displayed as message prefix.
         * @return {string} Returns element's message prefix text value.
         */
        public Prefix($value? : string) : string {
            if (!ObjectValidator.IsSet(this.prefix)) {
                this.prefix = "";
            }
            this.prefix = Property.NullString(this.prefix, $value);
            if (ObjectValidator.IsSet($value)) {
                if (this.IsLoaded()) {
                    ElementManager.setInnerHtml(this.Id() + "_Prefix", $value);
                    if (ElementManager.IsVisible(this.Id())) {
                        NotificationMessage.resize(this);
                    }
                }
            }
            return this.prefix;
        }

        /**
         * @param {number} [$value] Specify element's width value.
         * If value is -1, element will has auto size, otherwise width has to be more that 100.
         * @return {number} Returns element's width value.
         */
        public Width($value? : number) : number {
            this.width = Property.PositiveInteger(this.width, $value, 100);
            if (ObjectValidator.IsSet($value)) {
                if ($value === -1) {
                    if (!this.IsLoaded()) {
                        this.width = $value;
                    } else {
                        ElementManager.ClearCssProperty(this.Id(), "width");
                        this.width = -1;
                    }
                }

                if (ElementManager.IsVisible(this.Id())) {
                    NotificationMessage.resize(this);
                }
            } else {
                if (this.width === -1 && this.IsLoaded()) {
                    this.width = ElementManager.getElement(this.Id()).offsetWidth;
                }
            }
            return this.width;
        }

        /**
         * @param {NotificationMessageType} [$messageType] Specify type of element look and feel.
         * @return {NotificationMessageType} Returns type of element's look and feel.
         */
        public GuiType($messageType? : NotificationMessageType) : NotificationMessageType {
            if (ObjectValidator.IsSet($messageType)) {
                this.guiType = this.guiTypeValueSetter($messageType);
                if (ElementManager.IsVisible(this.Id())) {
                    ElementManager.setClassName(this.Id() + "_Type", this.guiType.toString());
                }
            }
            return this.guiType;
        }

        /**
         * @return {IToolTip} This method should return class with
         * interface Com.Wui.Framework.Gui.Interfaces.Components.IToolTip
         */
        protected getTitleClass() : any {
            return ToolTip;
        }

        protected guiTypeValueSetter($value : NotificationMessageType) : NotificationMessageType {
            return Property.EnumType(this.guiType, $value, NotificationMessageType, NotificationMessageType.GENERAL_INFO);
        }

        protected styleClassNameSetterValidator($value : string) : boolean {
            if (!NotificationMessageType.Contains($value)) {
                return super.styleClassNameSetterValidator($value);
            }
            Echo.Println("Use GuiType method for set of notification message type instead of StyleClassName method.");
            return false;
        }

        protected innerCode() : IGuiElement {
            this.DisableAsynchronousDraw();
            this.closeButton.DisableAsynchronousDraw();
            this.closeButton.getEvents().setOnClick(($eventArgs : EventArgs) : void => {
                const element : ImageButton = <ImageButton>$eventArgs.Owner();
                const parent : NotificationMessage = <NotificationMessage>element.Parent();
                parent.Visible(false);
            });

            this.getEvents().setOnLoad(NotificationMessage.resizeEventHandler);
            this.getEvents().setOnShow(NotificationMessage.resizeEventHandler);

            return super.innerCode();
        }

        protected innerHtml() : IGuiElement {
            return this.addElement(this.Id() + "_Type").StyleClassName(this.guiType).GuiTypeTag(this.getGuiTypeTag())
                .Add(this.addElement(this.Id() + "_Icon").StyleClassName(GeneralCssNames.ICON))
                .Add(this.addElement(this.Id() + "_Message")
                    .StyleClassName("Message")
                    .Add(this.addElement(this.Id() + "_Prefix")
                        .StyleClassName("Prefix")
                        .Add(this.Prefix())
                    )
                    .Add(this.addElement(this.Id() + "_Text")
                        .StyleClassName(GeneralCssNames.TEXT)
                        .Add(this.text)
                    )
                )
                .Add(this.closeButton);
        }

        protected excludeCacheData() : string[] {
            const exclude : string[] = super.excludeCacheData();
            if (ObjectValidator.IsEmptyOrNull(this.Prefix())) {
                exclude.push("prefix");
            }
            return exclude;
        }
    }
}
