/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.BaseInterface.UserControls {
    "use strict";
    import ToolTip = Io.VisionSDK.UserControls.BaseInterface.Components.ToolTip;

    export class Label extends Com.Wui.Framework.UserControls.BaseInterface.UserControls.Label {

        protected getTitleClass() : any {
            return ToolTip;
        }
    }
}
