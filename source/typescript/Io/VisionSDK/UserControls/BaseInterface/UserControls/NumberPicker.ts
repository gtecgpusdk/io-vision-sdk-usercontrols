/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.BaseInterface.UserControls {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import NumberPickerType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.NumberPickerType;
    import ToolTip = Io.VisionSDK.UserControls.BaseInterface.Components.ToolTip;
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;

    /**
     * NumberPicker class renders element for easy pick of number from specified range of numbers
     */
    export class NumberPicker extends Com.Wui.Framework.UserControls.BaseInterface.UserControls.NumberPicker {

        /**
         * @param {NumberPickerType} [$numberPickerType] Specify type of element look and feel.
         * @param {string} [$id] Force set element id instead of generated one.
         */
        constructor($numberPickerType? : NumberPickerType, $id? : string) {
            super($numberPickerType, $id);
        }

        /**
         * @param {NumberPickerType} [$numberPickerType] Specify type of element look and feel.
         * @return {NumberPickerType} Returns type of element's look and feel.
         */
        public GuiType($numberPickerType? : NumberPickerType) : NumberPickerType {
            return <NumberPickerType>super.GuiType($numberPickerType);
        }

        protected guiTypeValueSetter($value : any) : any {
            return Property.EnumType(this.GuiType(), $value, NumberPickerType, NumberPickerType.PROPERTY);
        }

        protected styleClassNameSetterValidator($value : string) : boolean {
            if (!NumberPickerType.Contains($value)) {
                return super.styleClassNameSetterValidator($value);
            }
            Echo.Println("Use GuiType method for set of NumberPicker type instead of StyleClassName method.");
            return false;
        }

        /**
         * @return {IToolTip} This method should return class with
         * interface Com.Wui.Framework.Gui.Interfaces.Components.IToolTip
         */
        protected getTitleClass() : any {
            return ToolTip;
        }

        protected innerCode() : IGuiElement {
            this.getEvents().setOnMouseOver(() : void => {
                NumberPicker.PointerOn(this);
            });
            this.getEvents().setOnMouseOut(() : void => {
                NumberPicker.PointerOff(this);
            });
            return super.innerCode();
        }
    }
}
