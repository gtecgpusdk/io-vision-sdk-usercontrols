/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.BaseInterface.UserControls {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import ToolTip = Io.VisionSDK.UserControls.BaseInterface.Components.ToolTip;
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;
    import Reflection = Com.Wui.Framework.Commons.Utils.Reflection;
    import GuiObjectManager = Com.Wui.Framework.Gui.GuiObjectManager;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import MouseEventArgs = Com.Wui.Framework.Gui.Events.Args.MouseEventArgs;
    import TextAreaType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.TextAreaType;
    import ScrollBar = Io.VisionSDK.UserControls.BaseInterface.Components.ScrollBar;

    export class TextArea extends Com.Wui.Framework.UserControls.BaseInterface.UserControls.TextArea {

        protected static onHoverEventHandler($eventArgs : MouseEventArgs, $manager : GuiObjectManager) : void {
            // enabling propagation
        }

        constructor($textAreaType? : TextAreaType, $id? : string) {
            super($textAreaType, $id);
            this.GuiType($textAreaType);
        }

        /**
         * @param {TextAreaType} [$textFieldType] Specify type of element look and feel.
         * @return {TextAreaType} Returns type of element's look and feel.
         */
        public GuiType($textFieldType? : TextAreaType) : TextAreaType {
            return <TextAreaType>super.GuiType();
        }

        protected innerCode() : IGuiElement {
            this.getEvents().setOnChange(($eventArgs : EventArgs, $manager : GuiObjectManager) => {
                const thisClass : any = Reflection.getInstance().getClass(this.getClassName());
                thisClass.setErrorStyle(this);
                if ($manager.IsActive(this)) {
                    thisClass.TurnActive(this);
                }
            });

            return super.innerCode();
        }

        /**
         * @return {IToolTip} This method should return class with
         * interface Com.Wui.Framework.Gui.Interfaces.Components.IToolTip
         */
        protected getTitleClass() : any {
            return ToolTip;
        }

        protected getScrollBarClass() : any {
            return ScrollBar;
        }

        protected guiTypeValueSetter($value : any) : any {
            return Property.EnumType(this.GuiType(), $value, TextAreaType, TextAreaType.GENERAL);
        }

        protected styleClassNameSetterValidator($value : string) : boolean {
            if (!TextAreaType.Contains($value)) {
                return super.styleClassNameSetterValidator($value);
            }
            Echo.Println("Use GuiType method for set of textField type instead of StyleClassName method.");
            return false;

        }
    }
}
