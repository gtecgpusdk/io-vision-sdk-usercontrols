/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.BaseInterface.UserControls {
    "use strict";
    import ImageButtonType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.ImageButtonType;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import ToolTip = Io.VisionSDK.UserControls.BaseInterface.Components.ToolTip;
    import MouseEventArgs = Com.Wui.Framework.Gui.Events.Args.MouseEventArgs;
    import GuiObjectManager = Com.Wui.Framework.Gui.GuiObjectManager;
    import Reflection = Com.Wui.Framework.Commons.Utils.Reflection;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;

    export class ImageButton extends Com.Wui.Framework.UserControls.BaseInterface.UserControls.ImageButton {
        private selected : boolean;

        /**
         * @param {ImageButton} $element Specify element, which should be handled.
         * @param {GuiObjectManager} [$manager] Specify instance of GuiObjectManager.
         * @param {Reflection} [$reflection] Specify instance of Reflection.
         * @return {void}
         */
        public static TurnOn($element : ImageButton, $manager? : GuiObjectManager, $reflection? : Reflection) : void {
            Com.Wui.Framework.UserControls.BaseInterface.UserControls.ImageButton.TurnOn($element, $manager, $reflection);
        }

        /**
         * @param {ImageButton} $element Specify element, which should be handled.
         * @param {GuiObjectManager} [$manager] Specify instance of GuiObjectManager.
         * @param {Reflection} [$reflection] Specify instance of Reflection.
         * @return {void}
         */
        public static TurnOff($element : ImageButton, $manager? : GuiObjectManager, $reflection? : Reflection) : void {
            if (!$element.Selected()) {
                Com.Wui.Framework.UserControls.BaseInterface.UserControls.ImageButton.TurnOff($element, $manager, $reflection);
            }
        }

        /**
         * @param {ImageButton} $element Specify element, which should be handled.
         * @param {GuiObjectManager} [$manager] Specify instance of GuiObjectManager.
         * @param {Reflection} [$reflection] Specify instance of Reflection.
         * @return {void}
         */
        public static TurnActive($element : ImageButton, $manager? : GuiObjectManager,
                                 $reflection? : Reflection) : void {
            if (!$element.Selected()) {
                Com.Wui.Framework.UserControls.BaseInterface.UserControls.ImageButton.TurnActive($element, $manager, $reflection);
            }
        }

        protected static onHoverEventHandler($eventArgs : MouseEventArgs, $manager : GuiObjectManager) : void {
            // enabling propagation
        }

        public Selected($value? : boolean) : boolean {
            if (ObjectValidator.IsSet($value)) {
                this.selected = $value;
                if (this.selected) {
                    ImageButton.TurnOn(this, GuiObjectManager.getInstanceSingleton());
                } else {
                    ImageButton.TurnOff(this, GuiObjectManager.getInstanceSingleton());
                }
            }
            return this.selected;
        }

        /**
         * @param {ImageButtonType} [$imageButtonType] Specify type of element look and feel.
         * @return {ImageButtonType} Returns type of element's look and feel.
         */
        public GuiType($imageButtonType? : ImageButtonType) : ImageButtonType {
            return <ImageButtonType>super.GuiType($imageButtonType);
        }

        /**
         * @return {IToolTip} This method should return class with
         * interface Com.Wui.Framework.Gui.Interfaces.Components.IToolTip
         */
        protected getTitleClass() : any {
            return ToolTip;
        }

        protected guiTypeValueSetter($value : any) : any {
            return Property.EnumType(this.GuiType(), $value, ImageButtonType, ImageButtonType.APP_CLOSE);
        }

        protected styleClassNameSetterValidator($value : string) : boolean {
            if (!ImageButtonType.Contains($value)) {
                return super.styleClassNameSetterValidator($value);
            }
            Echo.Println("Use GuiType method for set of image button type instead of StyleClassName method.");
            return false;
        }
    }
}
