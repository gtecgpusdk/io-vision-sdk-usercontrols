/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.BaseInterface.UserControls {
    "use strict";
    import FormsObject = Com.Wui.Framework.UserControls.Primitives.FormsObject;
    import ToolTip = Io.VisionSDK.UserControls.BaseInterface.Components.ToolTip;
    import BaseFormInputArgs = Com.Wui.Framework.UserControls.Structures.BaseFormInputArgs;
    import CheckBoxFormArgs = Com.Wui.Framework.UserControls.Structures.CheckBoxFormArgs;
    import TextFieldFormArgs = Com.Wui.Framework.UserControls.Structures.TextFieldFormArgs;
    import TextFieldType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.TextFieldType;
    import NotificationType = Com.Wui.Framework.UserControls.BaseInterface.Enums.Components.NotificationType;
    import PropertyFormArgs = Io.VisionSDK.UserControls.Structures.PropertyFormArgs;
    import InputLabelType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.InputLabelType;
    import PropertyListFormArgs = Io.VisionSDK.UserControls.Structures.PropertyListFormArgs;
    import DropDownListType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.DropDownListType;
    import PropertyPickerFormArgs = Io.VisionSDK.UserControls.Structures.PropertyPickerFormArgs;
    import NumberPickerType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.NumberPickerType;
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;
    import KeyEventArgs = Com.Wui.Framework.Gui.Events.Args.KeyEventArgs;
    import KeyEventHandler = Com.Wui.Framework.Gui.Utils.KeyEventHandler;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;

    export class FormInput extends Com.Wui.Framework.UserControls.BaseInterface.UserControls.FormInput {

        public getInput() : FormsObject {
            return this.getInputElement();
        }

        public setWidth($value : number) : void {
            const config : BaseFormInputArgs = this.Configuration();
            config.Width($value);
            this.resizeHandler(config);
        }

        /**
         * @return {IToolTip} This method should return class with
         * interface Com.Wui.Framework.Gui.Interfaces.Components.IToolTip
         */
        protected getTitleClass() : any {
            return ToolTip;
        }

        protected registerGroup($args? : BaseFormInputArgs) : void {
            if ($args.IsMemberOf(PropertyFormArgs) ||
                $args.IsMemberOf(PropertyListFormArgs) ||
                $args.IsMemberOf(PropertyPickerFormArgs)) {
                this.registerElement(new InputLabel(InputLabelType.PROPERTY, "", this.Id() + "_Name"), "label");
                this.registerElement(new Label("*", this.Id() + "_Required"), "requiredLabel");
            }
            if ($args.IsMemberOf(PropertyFormArgs)) {
                this.registerElement(new TextField(TextFieldType.PROPERTY), "input");
            } else if ($args.IsMemberOf(PropertyListFormArgs)) {
                this.registerElement(new DropDownList(DropDownListType.PROPERTY), "input");
            } else if ($args.IsMemberOf(PropertyPickerFormArgs)) {
                const numberPicker : NumberPicker = new NumberPicker(NumberPickerType.PROPERTY);
                const args : PropertyPickerFormArgs = <PropertyPickerFormArgs>$args;
                numberPicker.RangeStart(args.RangeStart());
                numberPicker.RangeEnd(args.RangeEnd());
                numberPicker.ValueStep(args.Step());
                numberPicker.DecimalPlaces(args.Size());
                numberPicker.Value(args.Value());
                this.registerElement(numberPicker, "input");
            } else if ($args.IsMemberOf(CheckBoxFormArgs)) {
                this.registerElement(new CheckBox(), "input");
                this.registerElement(new Label("*", this.Id() + "_Required"), "requiredLabel");
            } else {
                this.registerElement(new Label("", this.Id() + "_Name"), "label");
                this.registerElement(new Label("*", this.Id() + "_Required"), "requiredLabel");
                if ($args.IsMemberOf(TextFieldFormArgs)) {
                    this.registerElement(new TextField(TextFieldType.GENERAL), "input");
                    this.getInputElement().Notification().GuiType(NotificationType.GENERAL);
                }
            }
            if (this.getInputElement().IsMemberOf(TextField)) {
                (<TextField>this.getInputElement()).ReadOnly((<PropertyFormArgs>$args).ReadOnly());
            }
        }

        protected innerCode() : IGuiElement {
            if (this.Configuration().IsMemberOf(PropertyFormArgs)) {
                this.getInput().getEvents().setEvent(EventType.ON_KEY_PRESS, ($eventArgs : KeyEventArgs) : void => {
                    if (((<PropertyFormArgs>this.Configuration()).IntegerOnly() &&
                        !KeyEventHandler.IsInteger($eventArgs.NativeEventArgs())) ||
                        ((<PropertyFormArgs>this.Configuration()).FloatOnly() &&
                            !KeyEventHandler.IsDouble($eventArgs.NativeEventArgs()))) {
                        $eventArgs.PreventDefault();
                    }
                });
                this.getInput().getEvents().setEvent(EventType.ON_KEY_UP, ($eventArgs : KeyEventArgs) : void => {
                    const eventArgs : KeyEventArgs = new KeyEventArgs($eventArgs.NativeEventArgs());
                    eventArgs.Owner(parent);
                    this.getEventsManager().FireEvent(this, EventType.ON_CHANGE, eventArgs);
                });
            } else if (this.Configuration().IsMemberOf(PropertyPickerFormArgs) &&
                (<PropertyPickerFormArgs>this.Configuration()).IntegerOnly()) {
                (<NumberPicker>this.getInput()).DecimalPlaces(0);
            }
            return super.innerCode();
        }

        protected resizeHandler($args? : BaseFormInputArgs) : void {
            if ($args.IsMemberOf(PropertyFormArgs) ||
                $args.IsMemberOf(PropertyListFormArgs)) {
                (<InputLabel>this.getElement("label")).Width((<PropertyFormArgs>$args).Width() / 2);
                (<TextField>this.getInputElement()).Width((<PropertyFormArgs>$args).Width() / 2);
            }
            if ($args.IsMemberOf(PropertyPickerFormArgs)) {
                (<InputLabel>this.getElement("label")).Width((<PropertyFormArgs>$args).Width() / 2);
                const input : NumberPicker = <NumberPicker>this.getInputElement();
                input.Width((<PropertyFormArgs>$args).Width() / 2 - 20);
            }
        }
    }
}
