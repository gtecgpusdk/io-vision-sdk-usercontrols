/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.BaseInterface.UserControls {
    "use strict";
    import IconType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.IconType;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import ToolTip = Io.VisionSDK.UserControls.BaseInterface.Components.ToolTip;

    export class Icon extends Com.Wui.Framework.UserControls.BaseInterface.UserControls.Icon {

        constructor($iconType? : IconType, $id? : string) {
            super($iconType, $id);
        }

        /**
         * @param {IconType} [$type] Specify type of element look and feel.
         * @return {IconType} Returns type of element's look and feel.
         */
        public IconType($type? : IconType) : IconType {
            return <IconType>super.IconType($type);
        }

        /**
         * @return {IToolTip} This method should return class with
         * interface Com.Wui.Framework.Gui.Interfaces.Components.IToolTip
         */
        protected getTitleClass() : any {
            return ToolTip;
        }

        protected iconTypeValueSetter($value : any) : any {
            return Property.EnumType(this.IconType(), $value, IconType, IconType.SPINNER_WHITE_SMALL);
        }

        protected styleClassNameSetterValidator($value : string) : boolean {
            if (!IconType.Contains($value)) {
                return super.styleClassNameSetterValidator($value);
            }
            Echo.Println("Use IconType method for set of icon type instead of StyleClassName method.");
            return false;
        }
    }
}
