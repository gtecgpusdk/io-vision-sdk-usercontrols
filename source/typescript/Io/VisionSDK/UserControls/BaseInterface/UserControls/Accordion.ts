/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.BaseInterface.UserControls {
    "use strict";
    import AccordionType = Com.Wui.Framework.UserControls.BaseInterface.Enums.UserControls.AccordionType;
    import VerticalPanelHolderViewer = Io.VisionSDK.UserControls.BaseInterface.Viewers.UserControls.VerticalPanelHolderViewer;
    import HorizontalPanelHolderViewer = Io.VisionSDK.UserControls.BaseInterface.Viewers.UserControls.HorizontalPanelHolderViewer;
    import BasePanelHolderViewer = Com.Wui.Framework.UserControls.Primitives.BasePanelHolderViewer;
    import ScrollBar = Io.VisionSDK.UserControls.BaseInterface.Components.ScrollBar;

    export class Accordion extends Com.Wui.Framework.UserControls.BaseInterface.UserControls.Accordion {

        /**
         * @return {IScrollBar} This method should return class with
         * interface Com.Wui.Framework.Gui.Interfaces.Components.IScrollBar
         */
        protected getScrollBarClass() : any {
            return ScrollBar;
        }

        protected getPanelHolderViewerClass() : any {
            switch (this.GuiType()) {
            case AccordionType.VERTICAL:
                return VerticalPanelHolderViewer;
            case AccordionType.HORIZONTAL:
                return HorizontalPanelHolderViewer;
            default :
                return BasePanelHolderViewer;
            }
        }
    }
}
