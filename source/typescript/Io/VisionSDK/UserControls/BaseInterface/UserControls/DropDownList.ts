/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.BaseInterface.UserControls {
    "use strict";
    import DropDownListType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.DropDownListType;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import ScrollBar = Io.VisionSDK.UserControls.BaseInterface.Components.ScrollBar;
    import ToolTip = Io.VisionSDK.UserControls.BaseInterface.Components.ToolTip;

    export class DropDownList extends Com.Wui.Framework.UserControls.BaseInterface.UserControls.DropDownList {

        constructor($dropDownListType? : DropDownListType, $id? : string) {
            super($dropDownListType, $id);
            this.ShowField(true);
            this.ShowButton(false);
        }

        /**
         * @param {DropDownListType} [$dropDownListType] Specify type of element look and feel.
         * @return {DropDownListType} Returns type of element's look and feel.
         */
        public GuiType($dropDownListType? : DropDownListType) : DropDownListType {
            return <DropDownListType>super.GuiType($dropDownListType);
        }

        /**
         * @return {IToolTip} This method should return class with
         * interface Com.Wui.Framework.Gui.Interfaces.Components.IToolTip
         */
        protected getTitleClass() : any {
            return ToolTip;
        }

        /**
         * @return {IScrollBar} This method should return class with
         * interface Com.Wui.Framework.Gui.Interfaces.Components.IScrollBar
         */
        protected getScrollBarClass() : any {
            return ScrollBar;
        }

        protected guiTypeValueSetter($value : any) : any {
            return Property.EnumType(this.GuiType(), $value, DropDownListType, DropDownListType.PROPERTY);
        }

        protected styleClassNameSetterValidator($value : string) : boolean {
            if (!DropDownListType.Contains($value)) {
                return super.styleClassNameSetterValidator($value);
            }
            Echo.Println("Use GuiType method for set of drop down list type instead of StyleClassName method.");
            return false;
        }

        /**
         * Specify attributes of the instance after unserialization.
         */
        protected setInstanceAttributes() : void {
            super.setInstanceAttributes();
            this.ShowField(true);
            this.ShowButton(false);
        }
    }
}
