/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.BaseInterface.UserControls {
    "use strict";
    import BSPSelectorType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.BSPSelectorType;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import ToolTip = Io.VisionSDK.UserControls.BaseInterface.Components.ToolTip;
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import SelectBox = Io.VisionSDK.UserControls.BaseInterface.Components.SelectBox;
    import GuiObjectManager = Com.Wui.Framework.Gui.GuiObjectManager;
    import IEventsHandler = Com.Wui.Framework.Commons.Interfaces.IEventsHandler;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;

    export class BSPSelector extends Com.Wui.Framework.UserControls.Primitives.FormsObject {
        private guiType : BSPSelectorType;
        private width : number;
        private headerLabel : Label;
        private descriptionLabel : Label;
        private readonly bspList : SelectBox;

        public static TurnOn($element : BSPSelector, $manager : GuiObjectManager) : void {
            if (!$manager.IsActive($element)) {
                ElementManager.TurnOn($element.Id() + "_Status");
            }
        }

        public static TurnOff($element : BSPSelector, $manager : GuiObjectManager) : void {
            if (!$manager.IsActive($element) && !$manager.IsActive($element.bspList)) {
                ElementManager.TurnOff($element.Id() + "_Status");
            }
        }

        public static TurnActive($element : BSPSelector) : void {
            ElementManager.TurnActive($element.Id() + "_Status");
        }

        constructor($bspSelectorType? : BSPSelectorType, $id? : string) {
            super($id);
            this.width = 210;
            this.headerLabel = new Label();
            this.descriptionLabel = new Label();
            this.bspList = new SelectBox();
            this.bspList.Width(this.width);
            this.guiType = this.guiTypeValueSetter($bspSelectorType);
        }

        /**
         * @param {string} $header Specify text, which should be displayed as selector header.
         * @param {string} $description Specify text, which should be displayed as selector description.
         * @return {void}
         */
        public setText($header : string, $description : string) : void {
            this.headerLabel.Text($header);
            this.descriptionLabel.Text($description);
        }

        /**
         * @param {string} $value Add text, which should be displayed as selector item.
         * @return {void}
         */
        public Add($value : string) : void {
            this.bspList.Add($value);
        }

        /**
         * @return {void}
         */
        public Clear() : void {
            this.bspList.Clear();
        }

        public Value($value? : string) : string {
            return this.bspList.Value($value);
        }

        /**
         * @param {number} [$value] Specify element's width.
         * @return {number} Returns element's width value.
         */
        public Width($value? : number) : number {
            if (ObjectValidator.IsSet($value)) {
                if (this.width !== $value) {
                    this.width = Property.PositiveInteger(this.width, $value, 210);
                    if (this.IsCompleted()) {
                        ElementManager.setWidth(this.Id(), this.width);
                        ElementManager.setWidth(this.Id() + "_Header", this.width);
                        ElementManager.setWidth(this.Id() + "_Body", this.width);
                        ElementManager.setWidth(this.headerLabel.Id() + "_Text", this.width);
                        ElementManager.setWidth(this.descriptionLabel.Id() + "_Text", this.width - 15);
                    }
                    this.bspList.Width(this.width - 10);
                }
            }
            return this.width;
        }

        /**
         * @param {BSPSelectorType} [$bspSelectorType] Specify type of element look and feel.
         * @return {BSPSelectorType} Returns type of element's look and feel.
         */
        public GuiType($bspSelectorType? : BSPSelectorType) : BSPSelectorType {
            if (ObjectValidator.IsSet($bspSelectorType)) {
                this.guiType = this.guiTypeValueSetter($bspSelectorType);
                if (ElementManager.IsVisible(this.Id())) {
                    ElementManager.setClassName(this.Id() + "_Type", this.guiType.toString());
                }
            }
            return this.guiType;
        }

        protected guiTypeValueSetter($value : any) : any {
            return Property.EnumType(this.guiType, $value, BSPSelectorType, BSPSelectorType.ORANGE);
        }

        protected styleClassNameSetterValidator($value : string) : boolean {
            if (!BSPSelectorType.Contains($value)) {
                return super.styleClassNameSetterValidator($value);
            }
            Echo.Println("Use GuiType method for set of BSP Selector type instead of StyleClassName method.");
            return false;
        }

        protected innerCode() : IGuiElement {
            this.headerLabel.StyleClassName("HeaderLabel");
            this.descriptionLabel.StyleClassName("DescriptionLabel");
            this.bspList.Visible(false);
            this.bspList.Height(170);
            this.bspList.DisableCharacterNavigation();
            this.bspList.DisableAdvancedSelection();
            this.bspList.setInitSize(this.Width() - 10, 0);
            this.bspList.Width(this.Width() - 10);

            const showBSPList : IEventsHandler = ($eventArgs : EventArgs, $manager : GuiObjectManager) : void => {
                $eventArgs.StopAllPropagation();
                if (!this.bspList.Visible()) {
                    this.descriptionLabel.Visible(false);
                    this.bspList.Visible(true);
                }
                $manager.setHovered(this.bspList, true);
            };
            this.getEvents().setOnMouseOver(showBSPList);
            this.getEvents().setOnFocus(showBSPList);
            this.getEvents().setOnMouseOut(($eventArgs : EventArgs, $manager : GuiObjectManager) : void => {
                this.getEvents().FireAsynchronousMethod(() : void => {
                    if (!$manager.IsHovered(this.bspList)) {
                        this.bspList.Visible(false);
                    }
                }, false, 150);
                $manager.setHovered(this.bspList, false);
            });
            this.getEvents().setOnBlur(($eventArgs : EventArgs, $manager : GuiObjectManager) : void => {
                BSPSelector.TurnOn(this, $manager);
                this.bspList.Visible(false);
            });
            this.getEvents().setOnComplete(() : void => {
                ElementManager.setWidth(this.Id(), this.width);
                ElementManager.setWidth(this.headerLabel.Id() + "_Text", this.width);
                ElementManager.setWidth(this.descriptionLabel.Id() + "_Text", this.width - 15);
            });
            this.bspList.getEvents().setOnHide(($eventArgs : EventArgs, $manager : GuiObjectManager) : void => {
                this.getEvents().FireAsynchronousMethod(() : void => {
                    if (!$manager.IsHovered(this.bspList)) {
                        this.descriptionLabel.Visible(true);
                        BSPSelector.TurnOff(this, $manager);
                    }
                }, false, 150);
            });
            this.bspList.getEvents().setOnSelect(() : void => {
                const eventArgs : EventArgs = new EventArgs();
                eventArgs.Owner(this);
                this.getEventsManager().FireEvent(this, EventType.ON_CHANGE, eventArgs);
                this.getEventsManager().FireEvent(this.getClassName(), EventType.ON_CHANGE, eventArgs);
            });

            return super.innerCode();
        }

        protected innerHtml() : IGuiElement {
            return this.addElement(this.Id() + "_Type")
                .StyleClassName(this.GuiType()).GuiTypeTag(this.getGuiTypeTag())
                .Add(this.addElement(this.Id() + "_Status")
                    .StyleClassName(this.statusCss())
                    .Add(this.selectorElement())
                    .Add(this.addElement(this.Id() + "_Header").StyleClassName("Header").Width(this.width))
                    .Add(this.addElement(this.Id() + "_Body").StyleClassName("Body").Width(this.width)
                        .Add(this.headerLabel)
                        .Add(this.descriptionLabel)
                        .Add(this.bspList)
                    )
                );
        }

        /**
         * @return {IToolTip} This method should return class with
         * interface Com.Wui.Framework.Gui.Interfaces.Components.IToolTip
         */
        protected getTitleClass() : any {
            return ToolTip;
        }
    }
}
