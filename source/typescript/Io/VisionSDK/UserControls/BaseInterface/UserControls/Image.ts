/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.BaseInterface.UserControls {
    "use strict";
    import ImageType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.ImageType;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;
    import ImageOutputArgs = Com.Wui.Framework.Gui.ImageProcessor.ImageOutputArgs;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import ToolTip = Io.VisionSDK.UserControls.BaseInterface.Components.ToolTip;
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;

    export class Image extends Com.Wui.Framework.UserControls.BaseInterface.UserControls.Image {

        constructor($source : string, $imageType? : ImageType, $id? : string) {
            super($source, $imageType, $id);
        }

        /**
         * @param {ImageType} [$imageType] Specify type of element look and feel.
         * @return {ImageType} Returns type of element's look and feel.
         */
        public GuiType($imageType? : ImageType) : ImageType {
            return <ImageType>super.GuiType($imageType);
        }

        /**
         * @return {IToolTip} This method should return class with
         * interface Com.Wui.Framework.Gui.Interfaces.Components.IToolTip
         */
        protected getTitleClass() : any {
            return ToolTip;
        }

        /**
         * @return {IIcon} This method should return class with
         * interface Com.Wui.Framework.Gui.Interfaces.UserControls.IIcon
         */
        protected getLoadingSpinnerClass() : any {
            return Icon;
        }

        protected guiTypeValueSetter($value : any) : any {
            return Property.EnumType(this.GuiType(), $value, ImageType, "");
        }

        protected styleClassNameSetterValidator($value : string) : boolean {
            if (!ImageType.Contains($value)) {
                return super.styleClassNameSetterValidator($value);
            }
            Echo.Println("Use GuiType method for set of Image type instead of StyleClassName method.");
            return false;
        }

        protected innerCode() : IGuiElement {
            switch (this.Source()) {
            case ImageType.STATIC_BAR:
                this.GuiType(ImageType.GENERAL);
                if (ObjectValidator.IsEmptyOrNull(this.OutputArgs())) {
                    const outputArgs : ImageOutputArgs = new ImageOutputArgs();
                    outputArgs.setSize(1024, 15);
                    this.OutputArgs(outputArgs);
                }
                this.LoadingSpinnerEnabled(false);
                this.Enabled(false);
                this.getEvents().setOnComplete(() => {
                    this.getEvents().Clear(EventType.ON_MOUSE_OVER);
                });
                break;
            default:
                break;
            }

            return super.innerCode();
        }
    }
}
