/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.BaseInterface.UserControls {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import ToolTip = Io.VisionSDK.UserControls.BaseInterface.Components.ToolTip;
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import ImageButtonType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.ImageButtonType;
    import GuiObjectManager = Com.Wui.Framework.Gui.GuiObjectManager;
    import GuiOptionType = Com.Wui.Framework.Gui.Enums.GuiOptionType;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;
    import GeneralCssNames = Com.Wui.Framework.Gui.Enums.GeneralCssNames;
    import IconType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.IconType;

    export class InstallationItem extends Com.Wui.Framework.UserControls.Primitives.FormsObject {
        private value : boolean;
        private text : string;
        private width : number;
        private readonly discardButton : ImageButton;
        private infoIcon : Icon;
        private infoLabel : Label;
        private isNested : boolean;
        private isOptional : boolean;
        private isUpToDate : boolean;

        public static TurnOn($element : InstallationItem, $manager : GuiObjectManager) : void {
            if ($element.Enabled() && !$manager.IsActive($element) && !$manager.IsActive($element.discardButton)) {
                ElementManager.TurnOn($element.Id() + "_Status");
            }
        }

        public static TurnOff($element : InstallationItem, $manager : GuiObjectManager) : void {
            if ($element.Enabled() && !$manager.IsActive($element) && !$manager.IsActive($element.discardButton)) {
                ElementManager.TurnOff($element.Id() + "_Status");
            }
        }

        public static TurnActive($element : InstallationItem) : void {
            if ($element.Enabled()) {
                ElementManager.TurnActive($element.Id() + "_Status");
            }
        }

        constructor($id? : string) {
            super($id);

            this.value = true;
            this.width = 400;
            this.discardButton = new ImageButton(ImageButtonType.ROUND_OK);
            this.infoIcon = new Icon(IconType.INFO);
            this.infoLabel = new Label();
            this.isNested = false;
            this.isOptional = true;
            this.isUpToDate = false;
        }

        /**
         * @param {boolean} [$value] Specify, if item should be selected.
         * @return {boolean} Returns current selection state.
         */
        public Value($value? : boolean) : boolean {
            if (ObjectValidator.IsSet($value)) {
                this.value = Property.Boolean(this.value, $value);
                if (this.value) {
                    if (this.IsUpToDate()) {
                        this.discardButton.GuiType(ImageButtonType.ROUND_UPDATE);
                    } else {
                        this.discardButton.GuiType(ImageButtonType.ROUND_OK);
                    }
                    ElementManager.setClassName(this.Id() + "_Text", GeneralCssNames.ENABLE);
                } else {
                    this.discardButton.GuiType(ImageButtonType.ROUND_CLOSE);
                    ElementManager.setClassName(this.Id() + "_Text", GeneralCssNames.DISABLE);
                }
                const eventArgs : EventArgs = new EventArgs();
                eventArgs.Owner(this);
                this.getEventsManager().FireEvent(this, EventType.ON_CHANGE, eventArgs);
                this.getEventsManager().FireEvent(this.getClassName(), EventType.ON_CHANGE, eventArgs);
            }
            return this.value;
        }

        /**
         * @param {string} [$value] Specify text, which should be displayed as item value.
         * @return {string} Returns element's text value.
         */
        public Text($value? : string) : string {
            return this.text = Property.String(this.text, $value);
        }

        /**
         * @param {string} [$value] Specify text, which should be displayed as item's info.
         * @return {string} Returns element's info text value.
         */
        public Info($value? : string) : string {
            return this.infoLabel.Text($value);
        }

        /**
         * @param {number} [$value] Specify element's width.
         * @return {number} Returns element's width value.
         */
        public Width($value? : number) : number {
            if (ObjectValidator.IsSet($value)) {
                if (this.width !== $value) {
                    this.width = Property.PositiveInteger(this.width, $value, 400);
                    if (this.IsCompleted()) {
                        ElementManager.setWidth(this.Id() + "_Body", this.width);
                        ElementManager.setCssProperty(this.Id() + "_Info", "width", this.width);
                    }
                }
            }
            return this.width;
        }

        /**
         * @param {boolean} [$value] Specify, if element's width.
         * @return {boolean} Returns element's width value.
         */
        public IsNested($value? : boolean) : boolean {
            return this.isNested = Property.Boolean(this.isNested, $value);
        }

        /**
         * @param {boolean} [$value] Specify, if element's should be marked as optional.
         * @return {boolean} Returns element's optional flag.
         */
        public IsOptional($value? : boolean) : boolean {
            return this.isOptional = Property.Boolean(this.isOptional, $value);
        }

        /**
         * @param {boolean} [$value] Specify, if element's should behave as up to date.
         * @return {boolean} Returns element's up to date flag.
         */
        public IsUpToDate($value? : boolean) : boolean {
            return this.isUpToDate = Property.Boolean(this.isUpToDate, $value);
        }

        protected innerCode() : IGuiElement {
            this.getEvents().setOnClick(($eventArgs : EventArgs, $manager : GuiObjectManager) : void => {
                if (!$manager.IsHovered(this.discardButton) && !$manager.IsActive(this.discardButton)) {
                    if (!ElementManager.IsVisible(this.Id() + "_Info")) {
                        ElementManager.Show(this.Id() + "_Info");
                        const infoHeight : number = ElementManager.getElement(this.Id() + "_Info").offsetHeight;
                        const iconHeight : number = ElementManager.getElement(this.infoIcon.Id() + "_Icon").offsetHeight;
                        if (iconHeight <= infoHeight) {
                            this.infoIcon.Visible(true);
                            ElementManager.setCssProperty(this.infoIcon.Id() + "_Icon", "top",
                                Math.ceil((infoHeight - iconHeight) / 2));
                        } else {
                            this.infoIcon.Visible(false);
                        }
                    } else {
                        ElementManager.Hide(this.Id() + "_Info");
                    }
                }
            });

            this.discardButton.StyleClassName("DiscardIcon");
            this.getGuiOptions().Add(GuiOptionType.DISABLE);
            this.discardButton.getEvents().setOnClick(() : void => {
                this.Value(!this.Value());
            });

            if (this.Value()) {
                if (this.IsUpToDate()) {
                    this.discardButton.GuiType(ImageButtonType.ROUND_UPDATE);
                } else if (this.Value()) {
                    this.discardButton.GuiType(ImageButtonType.ROUND_OK);
                }
                this.discardButton.IsSelected(!this.isOptional);
            } else {
                this.discardButton.GuiType(ImageButtonType.ROUND_CLOSE);
            }

            return super.innerCode();
        }

        protected innerHtml() : IGuiElement {
            return this.addElement()
                .Add(this.addElement(this.Id() + "_Status")
                    .StyleClassName(this.statusCss())
                    .Add(this.addElement(this.Id() + "_Body").StyleClassName("Body")
                        .Width(this.width)
                        .Add(this.addElement().StyleClassName(this.IsNested() ? "Nested" : "")
                            .Add(this.selectorElement())
                            .Add(this.addElement(this.Id() + "_Text")
                                .StyleClassName(this.Value() ? GeneralCssNames.ENABLE : GeneralCssNames.DISABLE)
                                .Add(this.addElement().StyleClassName("Text")
                                    .Add(this.Text())
                                )
                            )
                            .Add(this.discardButton)
                        )
                    )
                    .Add(this.addElement(this.Id() + "_Info").StyleClassName("InfoEnvelop")
                        .Width(this.width)
                        .Visible(false)
                        .Add(this.infoIcon)
                        .Add(this.infoLabel)
                        .Add(this.addElement().setAttribute("clear", "both"))
                    )
                );
        }

        protected getTitleClass() : any {
            return ToolTip;
        }
    }
}
