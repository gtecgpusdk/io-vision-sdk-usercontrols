/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.BaseInterface.UserControls {
    "use strict";
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import MouseEventArgs = Com.Wui.Framework.Gui.Events.Args.MouseEventArgs;
    import IDropDownButtonItem = Io.VisionSDK.UserControls.Interfaces.IDropDownButtonItem;
    import IDropDownButtonArgs = Io.VisionSDK.UserControls.Interfaces.IDropDownButtonArgs;
    import IButton = Com.Wui.Framework.Gui.Interfaces.UserControls.IButton;
    import IDropDownList = Com.Wui.Framework.Gui.Interfaces.UserControls.IDropDownList;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import DropDownButtonType = Io.VisionSDK.UserControls.Enums.DropDownButtonType;
    import Property = Com.Wui.Framework.Commons.Utils.Property;

    export class DropDownButton
        extends Com.Wui.Framework.UserControls.Primitives.BaseGuiObject implements Io.VisionSDK.UserControls.Interfaces.IDropDownButton {
        private button : IButton;
        private readonly list : IDropDownList;
        private callbacks : ArrayList<any>;
        private isButtonFocusEnabled : boolean;
        private args : IDropDownButtonArgs;
        private guiType : DropDownButtonType;

        constructor($id? : string) {
            super($id);
            this.button = new (this.getButtonClass())();
            this.list = new (this.getDropDownListClass())();
            this.list.ShowButton(true);
            this.list.ShowField(true);
            this.callbacks = new ArrayList<any>();
            this.isButtonFocusEnabled = false;
            this.args = <IDropDownButtonArgs>{};
        }

        public Value($args? : IDropDownButtonArgs) : IDropDownButtonArgs {
            if (ObjectValidator.IsSet($args)) {
                this.args = $args;
                const handler : any = () : void => {
                    this.list.Clear();
                    this.callbacks.Clear();

                    this.list.Enabled($args.values.length > 1);
                    this.GuiType($args.type);
                    $args.values.forEach(($value : IDropDownButtonItem, $index : number) : void => {
                        if ($args.activeIndex === $index) {
                            this.button.Text($value.text);
                        } else if (ObjectValidator.IsEmptyOrNull($value.forceVisible) || $value.forceVisible) {
                            this.list.Add($value.text, $value.text, $value.styleClassName);
                        }
                        this.callbacks.Add($value.callback, $value.text);
                    });
                };
                if (this.list.IsCompleted()) {
                    handler();
                } else {
                    this.list.getEvents().setOnComplete(() : void => {
                        handler();
                    });
                }
            }
            return this.args;
        }

        public Width($value? : number) : number {
            this.button.Width($value);
            return this.list.Width($value);
        }

        public Enabled($value? : boolean) : boolean {
            this.button.Enabled($value);
            return this.list.Enabled($value);
        }

        public GuiType($dropDownButtonType? : any) : any {
            this.guiType = this.guiTypeValueSetter($dropDownButtonType);
            if (ObjectValidator.IsSet($dropDownButtonType)) {
                if (this.IsLoaded()) {
                    ElementManager.setClassName(this.Id() + "_Type", this.guiType.toString());
                    this.initFocusableStatus();
                }
            }
            return this.guiType;
        }

        protected guiTypeValueSetter($value : any) : any {
            return Property.EnumType(this.guiType, $value, DropDownButtonType, DropDownButtonType.ORANGE);
        }

        protected getButtonClass() : any {
            return Button;
        }

        protected getDropDownListClass() : any {
            return DropDownList;
        }

        protected innerCode() : IGuiElement {
            const listClass : any = this.getDropDownListClass();

            this.button.Visible(true);
            this.list.Visible(true);
            this.list.MaxVisibleItemsCount(-1);
            this.list.Enabled(false);

            this.getEvents().setOnComplete(() : void => {
                this.initFocusableStatus();
            });

            this.button.getEvents().setOnClick(($eventArgs : MouseEventArgs) : void => {
                $eventArgs.StopAllPropagation();
                if (this.isButtonFocusEnabled && this.list.getItemsCount() > 0) {
                    listClass.Focus(this.list);
                } else {
                    const callback : any = this.callbacks.getItem(this.button.Text());
                    if (ObjectValidator.IsFunction(callback)) {
                        callback();
                    }
                }
            });

            this.list.getEvents().setOnClick(($eventArgs : MouseEventArgs) : void => {
                $eventArgs.StopAllPropagation();
            });

            this.list.getEvents().setOnSelect(() : void => {
                const callback : any = this.callbacks.getItem(this.list.Value());
                if (ObjectValidator.IsFunction(callback)) {
                    callback();
                }
            });

            return super.innerCode();
        }

        protected innerHtml() : IGuiElement {
            return this.addElement(this.Id() + "_Type").StyleClassName(this.GuiType())
                .Add(this.addElement()
                    .StyleClassName("ButtonEnvelop")
                    .Add(this.button)
                )
                .Add(this.addElement()
                    .StyleClassName("ListEnvelop")
                    .Add(this.list)
                );
        }

        private initFocusableStatus() : void {
            this.isButtonFocusEnabled
                = ElementManager.getCssValue(this.list.Id() + "_FieldButton", "pointer-events", true) === "none";
        }
    }
}
