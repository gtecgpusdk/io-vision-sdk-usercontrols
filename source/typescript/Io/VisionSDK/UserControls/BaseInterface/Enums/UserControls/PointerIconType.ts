/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls {
    "use strict";

    export class PointerIconType extends Com.Wui.Framework.Commons.Primitives.BaseEnum {
        public static readonly OFF : string = "PointerOff";
        public static readonly ON : string = "PointerOn";
        public static readonly ACTIVCE : string = "PointerActive";
    }
}
