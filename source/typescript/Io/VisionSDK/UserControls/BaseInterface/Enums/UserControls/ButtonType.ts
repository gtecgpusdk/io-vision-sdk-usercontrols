/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls {
    "use strict";

    export class ButtonType extends Com.Wui.Framework.Commons.Primitives.BaseEnum {
        public static readonly WHITE_MEDIUM : string = "WhiteMedium";
        public static readonly RED_MEDIUM : string = "RedMedium";
        public static readonly GREEN_MEDIUM : string = "GreenMedium";
        public static readonly GREEN_GRAY_MEDIUM : string = "GreenGrayMedium";
        public static readonly GREEN_WHITE_MEDIUM : string = "GreenWhiteMedium";
        public static readonly GREEN_LOW_MEDIUM : string = "GreenLowMedium";
        public static readonly GRAY_SMALL : string = "GraySmall";
        public static readonly GRAY_MEDIUM : string = "GrayMedium";
        public static readonly TRANSPARENT : string = "Transparent";
    }
}
