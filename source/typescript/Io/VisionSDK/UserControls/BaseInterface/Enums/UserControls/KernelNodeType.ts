/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls {
    "use strict";

    export class KernelNodeType extends Com.Wui.Framework.Commons.Primitives.BaseEnum {
        public static readonly ABSOLUTE_DIFFERENCE : string = "AbsoluteDifference";
        public static readonly ACCUMULATE : string = "Accumulate";
        public static readonly ACCUMULATE_SQUARED : string = "AccumulateSquared";
        public static readonly ACCUMULATE_WEIGHTED : string = "AccumulateWeighted";
        public static readonly ARITHMETIC_ADDITION : string = "ArithmeticAddition";
        public static readonly ARITHMETIC_SUBTRACTION : string = "ArithmeticSubtraction";
        public static readonly BIT_DEPTH_CONVERT : string = "BitDepthConvert";
        public static readonly BITWISE_AND : string = "BitwiseAnd";
        public static readonly BITWISE_EXCLUSIVE_OR : string = "BitwiseExclusiveOr";
        public static readonly BITWISE_INCLUSIVE_OR : string = "BitwiseInclusiveOr";
        public static readonly BITWISE_NOT : string = "BitwiseNot";
        public static readonly BOX_FILTER : string = "BoxFilter";
        public static readonly CANNY_EDGE_DETECTOR : string = "CannyEdgeDetector";
        public static readonly CHANNEL_COMBINE : string = "ChannelCombine";
        public static readonly CHANNEL_EXTRACT : string = "ChannelExtract";
        public static readonly COLOR_CONVERT : string = "ColorConvert";
        public static readonly CUSTOM_CONVOLUTION : string = "CustomConvolution";
        public static readonly DILATE_IMAGE : string = "DilateImage";
        public static readonly EQUALIZE_HISTOGRAM : string = "EqualizeHistogram";
        public static readonly ERODE_IMAGE : string = "ErodeImage";
        public static readonly FAST_CORNER : string = "FastCorner";
        public static readonly GAUSSIAN_BLUR : string = "GaussianBlur";
        public static readonly GAUSSIAN_IMAGE_PYRAMID : string = "GaussianImagePyramid";
        public static readonly HALF_SCALE_GAUSSIAN : string = "HalfScaleGaussian";
        public static readonly HARRIS_CORNER : string = "HarrisCorner";
        public static readonly HISTOGRAM : string = "Histogram";
        public static readonly INTEGRAL_IMAGE : string = "IntegralImage";
        public static readonly MAGNITUDE : string = "Magnitude";
        public static readonly MEAN_AND_STANDARD : string = "MeanAndStandard";
        public static readonly MEDIAN_FILTER : string = "MedianFilter";
        public static readonly MIN_AND_MAX_LOCATION : string = "MinAndMaxLocation";
        public static readonly OPTICAL_FLOW : string = "OpticalFlow";
        public static readonly PHASE : string = "Phase";
        public static readonly PIXEL_WISE_MULTIPLICATION : string = "PixelWiseMultiplication";
        public static readonly REMAP : string = "Remap";
        public static readonly SCALE_IMAGE : string = "ScaleImage";
        public static readonly SOBEL : string = "Sobel";
        public static readonly TABLE_LOOK_UP : string = "TableLookUp";
        public static readonly THRESHOLD : string = "Threshold";
        public static readonly WARP_PERSPECTIVE : string = "WarpPerspective";
        public static readonly WARP_AFFINE : string = "WarpAffine";
        public static readonly NONLINEAR_FILTER : string = "NonLinearFilter";
        public static readonly LAPLACE_PYRAMID : string = "LaplacePyramid";
        public static readonly LAPLACE_PYRAMID_RECONSTRUCTION : string = "LaplacePyramidReconstruction";

        public static readonly DATA_OBJECT_COPY : string = "DataObjectCopy";
        public static readonly NON_MAXIMA_SUPPRESSION : string = "NonMaximaSuppression";
        public static readonly MIN : string = "Min";
        public static readonly MAX : string = "Max";
        public static readonly BILATERAL_FILTER : string = "BilateralFilter";
        public static readonly MATCH_TEMPLATE : string = "MatchTemplate";
        public static readonly LBP : string = "LBP";
        public static readonly HOG_CELLS : string = "HOGCells";
        public static readonly HOG_FEATURES : string = "HOGFeatures";
        public static readonly HOUGH_LINES_P : string = "HoughLinesP";
        public static readonly TENSOR_MULTIPLY : string = "TensorMultiply";
        public static readonly TENSOR_ADD : string = "TensorAdd";
        public static readonly TENSOR_SUBTRACT : string = "TensorSubtract";
        public static readonly TENSOR_TABLE_LOOK_UP : string = "TensorTableLookUp";
        public static readonly TENSOR_TRANSPOSE : string = "TensorTranspose";
        public static readonly TENSOR_CONVERT_BIT_DEPTH : string = "TensorConvertBitDepth";
        public static readonly TENSOR_MATRIX_MULTIPLY : string = "TensorMatrixMultiply";

        public static readonly ACTIVATION_LAYER : string = "ActivationLayer";
        public static readonly CONVOLUTION_LAYER : string = "ConvolutionLayer";
        public static readonly DECONVOLUTION_LAYER : string = "DeconvolutionLayer";
        public static readonly FULLY_CONNECTED_LAYER : string = "FullyConnectedLayer";
        public static readonly NORMALIZATION_LAYER : string = "NormalizationLayer";
        public static readonly POOLING_LAYER : string = "PoolingLayer";
        public static readonly ROI_POOLING_LAYER : string = "RoiPoolingLayer";
        public static readonly SOFTMAX_LAYER : string = "SoftmaxLayer";

        public static readonly IMAGE_INPUT : string = "ImageInput";
        public static readonly IMAGE_OUTPUT : string = "ImageOutput";
        public static readonly VIDEO_INPUT : string = "VideoInput";
        public static readonly VIDEO_OUTPUT : string = "VideoOutput";
        public static readonly CAMERA : string = "Camera";
        public static readonly DISPLAY : string = "Display";
        public static readonly DATA_INPUT : string = "DataInput";
        public static readonly DATA_OUTPUT : string = "DataOutput";

        public static readonly VISUAL_GRAPH : string = "VisualGraph";
        public static readonly VXCONTEXT : string = "VxContext";
        public static readonly VXGRAPH : string = "VxGraph";
        public static readonly VXGROUP : string = "VxGroup";
        public static readonly VXNODE : string = "VxNode";
        public static readonly VXDATA : string = "VXData";

        public static readonly DROP_AREA : string = "DropArea";
    }
}
