/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls {
    "use strict";

    export class IconType extends KernelNodeType {
        public static readonly SPINNER_WHITE_SMALL : string = "SpinnerWhiteSmall";
        public static readonly SPINNER_GRAY_SMALL : string = "SpinnerGraySmall";
        public static readonly SPINNER_GRAPHENE_SMALL : string = "SpinnerGrapheneSmall";
        public static readonly SPINNER_WHITE_BIG : string = "SpinnerWhiteBig";
        public static readonly SPINNER_GRAY_BIG : string = "SpinnerGrayBig";
        public static readonly SPINNER_GRAPHENE_BIG : string = "SpinnerGrapheneBig";
        public static readonly SPINNER_TRANSPARENT_BIG : string = "SpinnerTransparentBig";
        public static readonly DIRECTORY : string = "Directory";

        public static readonly REPORT_COLLECTDATA : string = "ReportCollectData";
        public static readonly REPORT_SEND : string = "ReportSend";
        public static readonly REPORT_SUCCESS : string = "ReportSuccess";
        public static readonly REPORT_ERROR : string = "ReportError";

        public static readonly INFO : string = "Info";
        public static readonly WIZARD_LOGO : string = "WizardLogo";

        public static readonly PROJECT_FOLDER : string = "ProjectFolder";
        public static readonly BUILD : string = "Build";
        public static readonly RUN_INLOOP : string = "RunInLoop";
        public static readonly STOP_LOOP : string = "StopLoop";
        public static readonly RUN_ONCE : string = "RunOnce";
        public static readonly SAVE : string = "Save";
        public static readonly LOAD : string = "Load";
        public static readonly TERMINAL : string = "Terminal";
        public static readonly HELP : string = "Help";
        public static readonly UNDO : string = "Undo";
        public static readonly REDO : string = "Redo";

        public static readonly NODE_BLOCKS : string = "NodeBlocks";
        public static readonly NODES_LIST : string = "NodesList";
        public static readonly SEARCH : string = "Search";

        public static readonly PROPERTIES_VIEW : string = "PropertiesView";
        public static readonly HIERARCHY_VIEW : string = "HierarchyView";
        public static readonly MIXED_VIEW : string = "MixedView";

        public static readonly PLUS : string = "Plus";
        public static readonly MINUS : string = "Minus";
    }
}
