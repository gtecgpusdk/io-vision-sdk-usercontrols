/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls {
    "use strict";

    export class NotificationMessageType extends Com.Wui.Framework.Commons.Primitives.BaseEnum {
        public static GENERAL_INFO : string = "GeneralInfo";
        public static GENERAL_WARNING : string = "GeneralWarning";
        public static GENERAL_ERROR : string = "GeneralError";
        public static GENERAL_SUCCESS : string = "GeneralSuccess";
    }
}
