/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls {
    "use strict";

    export class ImageButtonType extends Com.Wui.Framework.Commons.Primitives.BaseEnum {
        public static readonly APP_MAXIMIZE : string = "AppMaximize";
        public static readonly APP_CLOSE : string = "AppClose";
        public static readonly APP_MINIMIZE : string = "AppMinimize";
        public static readonly PLUS_GREEN : string = "PlusGreen";
        public static readonly GRAY_BOX : string = "GrayBox";
        public static readonly ROUND_CLOSE : string = "RoundClose";
        public static readonly ROUND_OK : string = "RoundOk";
        public static readonly ROUND_UPDATE : string = "RoundUpdate";
        public static readonly KERNEL_NODE : string = "KernelNode";
        public static readonly CROSS : string = "Cross";
        public static readonly ARROW_LEFT : string = "ArrowLeft";
        public static readonly ARROW_RIGHT : string = "ArrowRight";
        public static readonly ARROW_LEFT_WHITE : string = "ArrowLeftWhite";
        public static readonly ARROW_RIGHT_WHITE : string = "ArrowRightWhite";
        public static readonly TOOLBAR_ITEM : string = "ToolbarItem";
        public static readonly WIZARD_NAV : string = "WizardNav";
        public static readonly COMPASS : string = "Compass";
    }
}
