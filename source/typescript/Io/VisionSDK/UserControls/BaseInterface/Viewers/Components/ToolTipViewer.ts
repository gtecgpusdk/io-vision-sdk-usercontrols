/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.BaseInterface.Viewers.Components {
    "use strict";
    import BaseViewerArgs = Com.Wui.Framework.Gui.Primitives.BaseViewerArgs;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import AbstractGuiObject = Io.VisionSDK.UserControls.Primitives.AbstractGuiObject;
    import ToolTip = Io.VisionSDK.UserControls.BaseInterface.Components.ToolTip;

    export class ToolTipViewer extends Com.Wui.Framework.Gui.Primitives.BaseViewer {

        constructor($args? : BaseViewerArgs) {
            super($args);
            this.setInstance(new AbstractGuiObject("tooltip_placeholder"));
        }

        public getInstance() : AbstractGuiObject {
            return <AbstractGuiObject>super.getInstance();
        }

        protected normalImplementation() : void {
            this.getInstance().Title().Text("tool tip text");
        }

        /* dev:start */
        protected testImplementation() : string {
            this.normalImplementation();

            const object : AbstractGuiObject = this.getInstance();
            object.Title().Text("test " + StringUtils.NewLine() + StringUtils.NewLine() + "asd");
            object.StyleClassName("testCssClass");

            this.addTestButton("force show", () : void => {
                ToolTip.Show(object.Title());
            });
            this.addTestButton("force hide", () : void => {
                object.Title().Visible(false);
            });

            return "<style>.testCssClass {float: left; position: relative; top: 50px; left: 150px;}</style>";
        }

        /* dev:end */
    }
}
