/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.BaseInterface.Viewers.Components {
    "use strict";
    import BaseViewerArgs = Com.Wui.Framework.Gui.Primitives.BaseViewerArgs;
    import AbstractGuiObject = Com.Wui.Framework.UserControls.Primitives.AbstractGuiObject;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import CropBox = Io.VisionSDK.UserControls.BaseInterface.Components.CropBox;

    export class CropBoxViewer extends Com.Wui.Framework.Gui.Primitives.BaseViewer {

        constructor($args? : BaseViewerArgs) {
            super($args);
            this.setInstance(new AbstractGuiObject());
        }

        public getInstance() : AbstractGuiObject {
            return <AbstractGuiObject>super.getInstance();
        }

        protected normalImplementation() : void {
            if (ObjectValidator.IsEmptyOrNull(this.getInstance())) {
                this.setInstance(new AbstractGuiObject());
            }
        }

        /* dev:start */
        protected testImplementation() : string {
            this.setInstance(null);
            const object : CropBox = new CropBox();
            object.setDimensions(100, 100, 600, 600);
            // object.setSize(500, 500);
            // object.Enabled(false);
            return object.Draw();
        }

        /* dev:end */
    }
}
