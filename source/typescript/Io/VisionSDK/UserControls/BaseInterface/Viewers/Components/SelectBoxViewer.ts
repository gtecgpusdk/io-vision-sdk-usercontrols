/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.BaseInterface.Viewers.Components {
    "use strict";
    import BaseViewerArgs = Com.Wui.Framework.Gui.Primitives.BaseViewerArgs;
    import AbstractGuiObject = Com.Wui.Framework.UserControls.Primitives.AbstractGuiObject;
    import SelectBox = Io.VisionSDK.UserControls.BaseInterface.Components.SelectBox;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import DirectionType = Com.Wui.Framework.Gui.Enums.DirectionType;

    export class SelectBoxViewer extends Com.Wui.Framework.Gui.Primitives.BaseViewer {

        constructor($args? : BaseViewerArgs) {
            super($args);
            this.setInstance(new AbstractGuiObject("placeholder"));
        }

        public getInstance() : AbstractGuiObject {
            return <AbstractGuiObject>super.getInstance();
        }

        /* dev:start */
        protected testImplementation() : string {
            this.getInstance().Visible(false);

            const object : SelectBox = new SelectBox();
            object.StyleClassName("testCssClass");
            // object.Value("test value");

            // object.Visible(false);
            object.setInitSize(100, 20);

            // object.setOpenDirection(DirectionType.DOWN, DirectionType.LEFT);
            // object.setOpenDirection(DirectionType.UP, DirectionType.RIGHT);
            // object.setOpenDirection(DirectionType.UP, DirectionType.LEFT);

            object.Width(300);
            // object.Height(500);
            object.MaxVisibleItemsCount(3);
            // object.Clear();
            object.Add("18", 1);
            object.Add("35", 2, "withSeparator");
            object.Add("test4", 3);
            object.Add("test5", 4);
            object.Add("test7 with long visible text", "string value 2");
            object.Add("test6", "string value");

            // object.Select(3);
            // object.Select("test6");

            this.addTestButton("Show", () : void => {
                object.Visible(true);
            });
            this.addTestButton("Show", () : void => {
                object.Visible(true);
            });
            this.addTestButton("Hide", () : void => {
                object.Visible(false);
            });
            this.addTestButton("Add item", () : void => {
                object.Add("new text list item");
            });
            this.addTestButton("Clear", () : void => {
                object.Clear();
            });
            this.addTestButton("Set size", () : void => {
                object.Width(500);
            });
            this.addTestButton("Select item 2", () : void => {
                object.Select("35");
            });

            return "<style>" +
                ".testCssClass {position: relative; top: 500px; left: 500px; float: left;} " +
                ".withSeparator .Separator {border-bottom: 1px solid red;} " +
                "</style>" +
                object.Draw();
        }

        /* dev:end */
    }
}
