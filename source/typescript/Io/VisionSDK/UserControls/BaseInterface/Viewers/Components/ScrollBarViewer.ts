/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.BaseInterface.Viewers.Components {
    "use strict";
    import BaseViewerArgs = Com.Wui.Framework.Gui.Primitives.BaseViewerArgs;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import ScrollBar = Io.VisionSDK.UserControls.BaseInterface.Components.ScrollBar;
    import OrientationType = Com.Wui.Framework.Gui.Enums.OrientationType;
    import AbstractGuiObject = Com.Wui.Framework.UserControls.Primitives.AbstractGuiObject;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;

    export class ScrollBarViewer extends Com.Wui.Framework.Gui.Primitives.BaseViewer {

        constructor($args? : BaseViewerArgs) {
            super($args);
            this.setInstance(new AbstractGuiObject());
        }

        public getInstance() : AbstractGuiObject {
            return <AbstractGuiObject>super.getInstance();
        }

        protected normalImplementation() : void {
            if (ObjectValidator.IsEmptyOrNull(this.getInstance())) {
                this.setInstance(new AbstractGuiObject());
            }
        }

        /* dev:start */
        protected testImplementation() : string {
            this.setInstance(null);

            const scrollBar1 : ScrollBar = new ScrollBar(OrientationType.VERTICAL, "testScrollBar1");
            scrollBar1.StyleClassName("TestClassName");

            const scrollBar2 : ScrollBar = new ScrollBar(OrientationType.HORIZONTAL, "testScrollBar2");
            scrollBar2.Size(400);

            this.addTestButton("move to 50%", () : void => {
                ScrollBar.MoveTo(scrollBar1, 50);
            });
            this.addTestButton("move to 10%", () : void => {
                ScrollBar.MoveTo(scrollBar1, 10);
            });
            this.addTestButton("move to 90%", () : void => {
                ScrollBar.MoveTo(scrollBar1, 90);
            });

            return "<div style=\"width: 80%; position: relative; top: 50px; left: 50px; height: 400px;\">" +
                scrollBar1.Draw() +
                "<div style=\"float: left; padding-left: 20px;\">" + StringUtils.Space(1) + "</div>" +
                scrollBar2.Draw() +
                "</div>" +
                "<div style=\"clear: both;\"></div>";
        }

        /* dev:end */
    }
}
