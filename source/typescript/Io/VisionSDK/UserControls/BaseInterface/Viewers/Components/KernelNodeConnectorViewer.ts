/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.BaseInterface.Viewers.Components {
    "use strict";
    import BaseViewerArgs = Com.Wui.Framework.Gui.Primitives.BaseViewerArgs;
    import KernelNodeConnector = Io.VisionSDK.UserControls.BaseInterface.Components.KernelNodeConnector;
    import KernelNodeInterfaceType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.KernelNodeInterfaceType;

    export class KernelNodeConnectorViewer extends Com.Wui.Framework.Gui.Primitives.BaseViewer {

        constructor($args? : BaseViewerArgs) {
            super($args);
            this.setInstance(new KernelNodeConnector(-1));
        }

        public getInstance() : KernelNodeConnector {
            return <KernelNodeConnector>super.getInstance();
        }

        /* dev:start */
        protected testImplementation() : string {
            const object : KernelNodeConnector = this.getInstance();
            object.StyleClassName("testCssClass");
            this.addTestButton("Input type", () : void => {
                object.GuiType(KernelNodeInterfaceType.INPUT);
            });

            this.addTestButton("Output type", () : void => {
                object.GuiType(KernelNodeInterfaceType.OUTPUT);
            });

            return "<style>.testCssClass {position: relative; top: 50px; left: 200px;float: left; " +
                "background-color: #64FF00; padding: 40px;}</style>";
        }

        /* dev:end */
    }
}
