/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.BaseInterface.Viewers.UserControls {
    "use strict";
    import BaseViewerArgs = Com.Wui.Framework.Gui.Primitives.BaseViewerArgs;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import ProgressBar = Io.VisionSDK.UserControls.BaseInterface.UserControls.ProgressBar;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import GuiOptionType = Com.Wui.Framework.Gui.Enums.GuiOptionType;
    import ProgressBarEventArgs = Com.Wui.Framework.Gui.Events.Args.ProgressBarEventArgs;
    import Label = Com.Wui.Framework.UserControls.BaseInterface.UserControls.Label;
    import ProgressBarType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.ProgressBarType;

    export class ProgressBarViewer extends Com.Wui.Framework.Gui.Primitives.BaseViewer {

        constructor($args? : BaseViewerArgs) {
            super($args);
            this.setInstance(new ProgressBar());
        }

        public getInstance() : ProgressBar {
            return <ProgressBar>super.getInstance();
        }

        /* dev:start */
        protected testImplementation() : string {
            const object : ProgressBar = this.getInstance();
            object.getGuiOptions().Add(GuiOptionType.DISABLE);
            object.RangeStart(0);
            object.RangeEnd(100);
            object.Value(50);
            object.Width(200);
            object.Title().Text("title test");
            object.StyleClassName("testCssClass");
            // object.GuiType(ProgressBarType.MARQUEE);

            this.addTestButton("Disable", () : void => {
                object.Enabled(false);
            });
            this.addTestButton("Enable", () : void => {
                object.Enabled(true);
            });
            this.addTestButton("set 0", () : void => {
                object.Value(0);
            });
            this.addTestButton("set 50", () : void => {
                object.Value(50);
            });
            this.addTestButton("set 100", () : void => {
                object.Value(100);
            });
            this.addTestButton("Set size", () : void => {
                object.Width(400);
            });
            this.addTestButton("Marquee", () : void => {
                object.GuiType(ProgressBarType.MARQUEE);
            });
            this.addTestButton("General", () : void => {
                object.GuiType(ProgressBarType.GENERAL);
            });

            const label : Label = new Label("Progress 0 %", "progressShow");
            object.getEvents().setOnChange(($eventArgs : ProgressBarEventArgs) : void => {
                label.Text("Progress " + $eventArgs.Percentage() + " %");
            });

            return "<style>.testCssClass {position: relative; top: 50px; left: 200px; float: left;}</style>" +
                StringUtils.NewLine() +
                StringUtils.NewLine() +
                label.Draw() +
                StringUtils.NewLine();
        }

        /* dev:end */
    }
}
