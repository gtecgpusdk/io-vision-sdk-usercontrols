/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.BaseInterface.Viewers.UserControls {
    "use strict";
    import BaseViewerArgs = Com.Wui.Framework.Gui.Primitives.BaseViewerArgs;
    import CheckBox = Io.VisionSDK.UserControls.BaseInterface.UserControls.CheckBox;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import GuiOptionType = Com.Wui.Framework.Gui.Enums.GuiOptionType;

    export class CheckBoxViewer extends Com.Wui.Framework.Gui.Primitives.BaseViewer {

        constructor($args? : BaseViewerArgs) {
            super($args);
            this.setInstance(new CheckBox());
        }

        public getInstance() : CheckBox {
            return <CheckBox>super.getInstance();
        }

        protected normalImplementation() : void {
            const object : CheckBox = this.getInstance();
            object.Text("text");
        }

        /* dev:start */
        protected testImplementation() : string {
            const object : CheckBox = this.getInstance();
            // object.Checked(true);
            // object.Text("test checkbox");
            // object.Title().Text("title test");
            object.StyleClassName("testCssClass");
            object.getGuiOptions().Add(GuiOptionType.DISABLE);

            this.addTestButton("Disable", () : void => {
                object.Enabled(false);
            });
            this.addTestButton("Enable", () : void => {
                object.Enabled(true);
            });
            this.addTestButton("Error", () : void => {
                object.Error(true);
            });
            this.addTestButton("Normal", () : void => {
                object.Error(false);
            });
            this.addTestButton("Checked", () : void => {
                object.Checked(true);
            });
            this.addTestButton("Unchecked", () : void => {
                object.Checked(false);
            });
            this.addTestButton("Checked/Unchecked", () : void => {
                CheckBox.ToggleChecked(object);
            });
            this.addTestButton("Set text", () : void => {
                object.Text("new checkbox text");
            });

            return "<style>.testCssClass {position: relative; top: 50px; left: 200px; float: left;}</style>";
        }

        /* dev:end */
    }
}
