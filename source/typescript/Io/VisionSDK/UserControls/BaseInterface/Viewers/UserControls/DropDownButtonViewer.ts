/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.BaseInterface.Viewers.UserControls {
    "use strict";
    import BaseViewerArgs = Com.Wui.Framework.Gui.Primitives.BaseViewerArgs;
    import DropDownButton = Io.VisionSDK.UserControls.BaseInterface.UserControls.DropDownButton;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import DropDownButtonType = Io.VisionSDK.UserControls.Enums.DropDownButtonType;
    import IDropDownButtonArgs = Io.VisionSDK.UserControls.Interfaces.IDropDownButtonArgs;

    export class DropDownButtonViewer
        extends Com.Wui.Framework.Gui.Primitives.BaseViewer {

        constructor($args? : BaseViewerArgs) {
            super($args);
            this.setInstance(new DropDownButton());
        }

        public getInstance() : DropDownButton {
            return <DropDownButton>super.getInstance();
        }

        /* dev:start */
        protected testImplementation() : string {
            const object : DropDownButton = this.getInstance();
            object.StyleClassName("testCssClass");
            object.Width(200);

            this.addTestButton("Width 300", () : void => {
                object.Width(300);
            });
            this.addTestButton("Width 200", () : void => {
                object.Width(200);
            });
            this.addTestButton("Width 100", () : void => {
                object.Width(100);
            });

            const getButton : any = () : IDropDownButtonArgs => {
                return {
                    activeIndex: 0,
                    type       : ([
                        DropDownButtonType.ORANGE, DropDownButtonType.SKY_BLUE, DropDownButtonType.GREEN
                    ])[Math.floor(Math.random() * 3)],
                    values     : [
                        {
                            callback      : () : void => {
                                button.activeIndex = 0;
                                object.Value(button);
                                LogIt.Debug("Callback invoked for test value 1");
                            },
                            styleClassName: "testStyleClass",
                            text          : "test value 1"
                        },
                        {
                            callback: () : void => {
                                button.activeIndex = 1;
                                object.Value(button);
                                LogIt.Debug("Callback invoked for test value 2");
                            },
                            text    : "test value 2"
                        },
                        {
                            callback: () : void => {
                                button.activeIndex = 2;
                                object.Value(button);
                                LogIt.Debug("Callback invoked for test value 3");
                            },
                            text    : "test value 3"
                        }
                    ]
                };
            };

            let button : IDropDownButtonArgs = getButton();
            object.Value(button);

            this.addTestButton("To list", () : void => {
                object.Value({
                    activeIndex: 0,
                    type       : DropDownButtonType.GRAY_LIST_LARGE,
                    values     : [
                        {
                            callback      : () : void => {
                                LogIt.Debug("List opened");
                            },
                            styleClassName: "testStyleClass",
                            text          : "Open list"
                        },
                        {
                            callback: () : void => {
                                LogIt.Debug("Callback invoked for test value 1");
                            },
                            text    : "test value 1"
                        },
                        {
                            callback: () : void => {
                                LogIt.Debug("Callback invoked for test value 2");
                            },
                            text    : "test value 2"
                        }
                    ]
                });
            });

            this.addTestButton("To both", () : void => {
                button = getButton();
                object.Value(button);
            });

            this.addTestButton("To button", () : void => {
                object.Value({
                    activeIndex: 0,
                    type       : DropDownButtonType.GRAY_LIST_LARGE,
                    values     : [
                        {
                            callback      : () : void => {
                                LogIt.Debug("Button clicked");
                            },
                            styleClassName: "testStyleClass",
                            text          : "Invoke something"
                        }
                    ]
                });
            });

            return "<style>.testCssClass {padding: 75px}</style>";
        }

        /* dev:end */
    }
}
