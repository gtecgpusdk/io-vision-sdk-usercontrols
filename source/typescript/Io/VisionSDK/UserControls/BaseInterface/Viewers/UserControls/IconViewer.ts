/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.BaseInterface.Viewers.UserControls {
    "use strict";
    import BaseViewerArgs = Com.Wui.Framework.Gui.Primitives.BaseViewerArgs;
    import Icon = Io.VisionSDK.UserControls.BaseInterface.UserControls.Icon;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import IconType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.IconType;
    import KernelNodeType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.KernelNodeType;

    export class IconViewer extends Com.Wui.Framework.Gui.Primitives.BaseViewer {

        constructor($args? : BaseViewerArgs) {
            super($args);
            this.setInstance(new Icon(IconType.FAST_CORNER));
        }

        public getInstance() : Icon {
            return <Icon>super.getInstance();
        }

        /* dev:start */
        protected testImplementation() : string {
            // let clickCounter : number = 0;
            // const object : Icon = this.getInstance();
            // object.getEvents().setOnClick(() : void => {
            //     clickCounter++;
            //     Echo.Printf("icon has been clicked " + clickCounter + " times");
            // });
            // object.StyleClassName("testCssClass");
            //
            // this.addTestButton("Disable", () : void => {
            //     object.Enabled(false);
            // });
            // this.addTestButton("Normal", () : void => {
            //     object.Enabled(true);
            // });

            this.setInstance(null);
            let icons : string = "";
            IconType.getProperties().concat(KernelNodeType.getProperties()).forEach(($type : string) : void => {
                const icon : Icon = new Icon(IconType[$type]);
                icon.StyleClassName("testCssClass");
                icons += "<h4>" + $type + "</h4>" + icon.Draw() + "<hr>";
            });

            return "<style>hr {clear: both} .testCssClass {position: relative; top: -10px; float: left; " +
                "background-color: #64FF00; padding: 40px; clear: both;}</style>" + icons;
        }

        /* dev:end */
    }
}
