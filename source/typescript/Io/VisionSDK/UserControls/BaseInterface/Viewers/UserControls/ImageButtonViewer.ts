/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.BaseInterface.Viewers.UserControls {
    "use strict";
    import BaseViewerArgs = Com.Wui.Framework.Gui.Primitives.BaseViewerArgs;
    import ImageButton = Io.VisionSDK.UserControls.BaseInterface.UserControls.ImageButton;
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import GuiOptionType = Com.Wui.Framework.Gui.Enums.GuiOptionType;
    import ImageButtonType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.ImageButtonType;
    import IconType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.IconType;

    export class ImageButtonViewer extends Com.Wui.Framework.Gui.Primitives.BaseViewer {

        constructor($args? : BaseViewerArgs) {
            super($args);
            this.setInstance(new ImageButton(ImageButtonType.ARROW_RIGHT));
        }

        public getInstance() : ImageButton {
            return <ImageButton>super.getInstance();
        }

        /* dev:start */
        protected testImplementation() : string {
            let clickCounter : number = 0;
            const object : ImageButton = this.getInstance();
            // object.IconName(IconType.GAUSSIAN_BLUR);
            object.getEvents().setEvent(EventType.ON_CLICK, () : void => {
                clickCounter++;
                Echo.Printf("button has been clicked " + clickCounter + " times");
            });
            object.Title().Text("Image button tooltip text");
            object.StyleClassName("testCssClass");
            object.getGuiOptions().Add(GuiOptionType.DISABLE);

            this.addTestButton("Disable", () : void => {
                object.Enabled(false);
            });
            this.addTestButton("Enable", () : void => {
                object.Enabled(true);
            });
            this.addTestButton("Error", () : void => {
                object.Error(true);
            });
            this.addTestButton("Selected", () : void => {
                object.IsSelected(true);
            });
            this.addTestButton("Normal", () : void => {
                object.IsSelected(false);
                object.Error(false);
            });

            return "<style>.testCssClass {position: relative; top: 50px; left: 200px; float: left; " +
                "background-color: #64FF00; padding: 40px;}</style>";
        }

        /* dev:end */
    }
}
