/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.BaseInterface.Viewers.UserControls {
    "use strict";
    import BasePanelHolderViewerArgs = Com.Wui.Framework.UserControls.Primitives.BasePanelHolderViewerArgs;
    import UnitType = Com.Wui.Framework.Gui.Enums.UnitType;
    import PropagableNumber = Com.Wui.Framework.Gui.Structures.PropagableNumber;
    import HorizontalPanelHolder = Io.VisionSDK.UserControls.BaseInterface.UserControls.HorizontalPanelHolder;

    /* istanbul ignore next: this Viewer is used mainly as RuntimeTest */
    export class HorizontalPanelHolderViewer extends Com.Wui.Framework.UserControls.Primitives.BasePanelHolderViewer {
        /* dev:start */
        protected static getTestViewerArgs() : BasePanelHolderViewerArgs {
            const args : BasePanelHolderViewerArgs = new BasePanelHolderViewerArgs();
            args.HeaderText("Header Text");
            args.DescriptionText("Desc. Text");
            args.IsOpened(true);
            args.PrioritySize(new PropagableNumber({number: 400, unitType: UnitType.PX}));

            return args;
        }

        /* dev:end */

        constructor($args? : BasePanelHolderViewerArgs) {
            super($args);
            /* dev:start */
            this.setTestSubscriber(Com.Wui.Framework.UserControls.RuntimeTests.UserControls.HorizontalPanelHolderTest);
            /* dev:end */
        }

        public getInstance() : HorizontalPanelHolder {
            return <HorizontalPanelHolder>super.getInstance();
        }

        protected getInstanceClass() : any {
            return HorizontalPanelHolder;
        }
    }
}
