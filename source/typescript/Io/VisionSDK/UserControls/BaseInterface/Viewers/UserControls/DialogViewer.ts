/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.BaseInterface.Viewers.UserControls {
    "use strict";
    import BaseViewerArgs = Com.Wui.Framework.Gui.Primitives.BaseViewerArgs;
    import DialogViewerArgs = Com.Wui.Framework.UserControls.BaseInterface.ViewersArgs.UserControls.DialogViewerArgs;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import Dialog = Io.VisionSDK.UserControls.BaseInterface.UserControls.Dialog;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import DialogType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.DialogType;
    import ResizeableType = Com.Wui.Framework.Gui.Enums.ResizeableType;
    import IconType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.IconType;

    export class DialogViewer extends Com.Wui.Framework.Gui.Primitives.BaseViewer {

        constructor($args? : BaseViewerArgs) {
            super($args);
            this.setInstance(new Dialog(DialogType.BLUE));
        }

        public getInstance() : Dialog {
            return <Dialog>super.getInstance();
        }

        public ViewerArgs($args? : DialogViewerArgs) : DialogViewerArgs {
            return <DialogViewerArgs>super.ViewerArgs($args);
        }

        protected normalImplementation() : void {
            if (!ObjectValidator.IsEmptyOrNull(this.ViewerArgs())
                && !ObjectValidator.IsEmptyOrNull(this.ViewerArgs().PanelArgs())) {
                this.getInstance().PanelViewerArgs(this.ViewerArgs().PanelArgs());
            }
        }

        /* dev:start */
        protected testImplementation() : string {
            const object : Dialog = this.getInstance();
            const args : DialogViewerArgs = new DialogViewerArgs();
            args.Visible(true);
            this.ViewerArgs(args);

//            object.StyleClassName("testCssClass");
            object.Title().Text("Text filed tooltip text");
            object.Modal(false);
//            object.Visible(true);
            object.ResizeableType(ResizeableType.HORIZONTAL_AND_VERTICAL);
            object.Draggable(true);
//            object.AutoResize(true);
//            object.AutoCenter(true);
//            object.TopOffset(100);
//            object.MaxWidth(500);
//            object.MaxHeight(500);
//            object.Width(500);
//            object.Height(300);

            const object2 : Dialog = new Dialog(DialogType.GREEN);
            object2.Visible(false);
            // object2.Modal(true);
            object2.ResizeableType(ResizeableType.HORIZONTAL_AND_VERTICAL);
            object2.Draggable(true);
            object2.headerIcon.IconType(IconType.DIRECTORY);
            // object2.headerIcon.Visible(false);
            object2.headerText.Text("new message");
            // object2.headerText.Visible(true);

            this.addTestButton("Open dialog 1", () : void => {
                Dialog.Open(object);
            });
            this.addTestButton("Open dialog 2", () : void => {
                Dialog.Open(object2);
            });
            this.addTestButton("set header", () : void => {
                object2.headerText.Text("test test test test test test test");
            });

            return "<style>.testCssClass {position: absolute; top: 50px; left: 200px; float: left;}</style>" +
                StringUtils.NewLine() +
                StringUtils.NewLine() +
                object2.Draw();
        }

        /* dev:end */
    }
}
