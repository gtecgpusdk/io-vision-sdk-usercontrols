/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.BaseInterface.Viewers.UserControls {
    "use strict";
    import BaseViewerArgs = Com.Wui.Framework.Gui.Primitives.BaseViewerArgs;
    import DropDownList = Io.VisionSDK.UserControls.BaseInterface.UserControls.DropDownList;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import GuiOptionType = Com.Wui.Framework.Gui.Enums.GuiOptionType;
    import DropDownListType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.DropDownListType;

    export class DropDownListViewer extends Com.Wui.Framework.Gui.Primitives.BaseViewer {

        constructor($args? : BaseViewerArgs) {
            super($args);
            this.setInstance(new DropDownList(DropDownListType.PROPERTY));
        }

        public getInstance() : DropDownList {
            return <DropDownList>super.getInstance();
        }

        /* dev:start */
        protected testImplementation() : string {
            const object : DropDownList = this.getInstance();
            object.Title().Text("Tooltip text");
            object.StyleClassName("testCssClass");
            object.getGuiOptions().Add(GuiOptionType.DISABLE);
//            object.Value("test value");
            object.Hint("Write some text here ...");
//            object.Width(150);
            object.Width(300);
//            object.Height(500);
//            object.Enabled(false);
            object.MaxVisibleItemsCount(3);
//            object.Clear();
            object.Add("18", 1);
            object.Add("35", 2, "withSeparator");
            object.Add("test4", 3);
            object.Add("test5", 4);
            object.Add("test7 with long visible text", "string value 2");
            object.Add("test6", "string value");

//            object.Select(3);
//            object.Select("test6");

            this.addTestButton("Disable", () : void => {
                object.Enabled(false);
            });
            this.addTestButton("Enable", () : void => {
                object.Enabled(true);
            });
            this.addTestButton("Error", () : void => {
                object.Error(true);
            });
            this.addTestButton("Add item", () : void => {
                object.Add("new text list item");
            });
            this.addTestButton("Set size", () : void => {
                object.Width(500);
            });
            this.addTestButton("Select item 2", () : void => {
                object.Select("35");
            });

            const object2 : DropDownList = new DropDownList(DropDownListType.PROPERTY);
            object2.StyleClassName("testCssClass2");
            object2.Width(300);
            object2.MaxVisibleItemsCount(3);
//            object2.Clear();
            object2.Add("test 1");
            object2.Add("test 2");
            object2.Add("test 3");
            object2.Add("test 4");

            object2.Select(0);

            return "<style>" +
                ".testCssClass {padding: 10px; float: left; background-color: #DFDFDF; width: 320px; height: 50px;} " +
                ".withSeparator .Separator {border-bottom: 1px solid red;} " +
                ".testCssClass2 {padding: 10px; float: left; background-color: #DFDFDF; width: 320px; height: 50px;}" +
                "</style>" +
                object2.Draw();
        }

        /* dev:end */
    }
}
