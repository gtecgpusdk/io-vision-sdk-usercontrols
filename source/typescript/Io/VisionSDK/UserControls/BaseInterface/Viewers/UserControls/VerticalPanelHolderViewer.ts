/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.BaseInterface.Viewers.UserControls {
    "use strict";
    import BasePanelHolderViewerArgs = Com.Wui.Framework.UserControls.Primitives.BasePanelHolderViewerArgs;
    import UnitType = Com.Wui.Framework.Gui.Enums.UnitType;
    import PropagableNumber = Com.Wui.Framework.Gui.Structures.PropagableNumber;
    import VerticalPanelHolder = Io.VisionSDK.UserControls.BaseInterface.UserControls.VerticalPanelHolder;

    /* istanbul ignore next: this Viewer is used mainly as RuntimeTest */
    export class VerticalPanelHolderViewer extends Com.Wui.Framework.UserControls.Primitives.BasePanelHolderViewer {
        /* dev:start */
        protected static getTestViewerArgs() : BasePanelHolderViewerArgs {
            const args : BasePanelHolderViewerArgs = new BasePanelHolderViewerArgs();
            args.HeaderText("Vertical panel holder");
            args.DescriptionText("Vertical panel can be hidden by button or click");
            args.IsOpened(true);
            args.PrioritySize(new PropagableNumber({number : 500, unitType: UnitType.PX}));

            return args;
        }

        /* dev:end */

        constructor($args? : BasePanelHolderViewerArgs) {
            super($args);
            /* dev:start */
            this.setTestSubscriber(Com.Wui.Framework.UserControls.RuntimeTests.UserControls.VerticalPanelHolderTest);
            /* dev:end */
        }

        public getInstance() : VerticalPanelHolder {
            return <VerticalPanelHolder>super.getInstance();
        }

        protected getInstanceClass() : any {
            return VerticalPanelHolder;
        }
    }
}
