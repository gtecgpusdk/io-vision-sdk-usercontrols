/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.BaseInterface.Viewers.UserControls {
    "use strict";
    import BaseViewerArgs = Com.Wui.Framework.Gui.Primitives.BaseViewerArgs;
    import Button = Io.VisionSDK.UserControls.BaseInterface.UserControls.Button;
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import IconType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.IconType;
    import GuiOptionType = Com.Wui.Framework.Gui.Enums.GuiOptionType;
    import ButtonType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.ButtonType;

    export class ButtonViewer extends Com.Wui.Framework.Gui.Primitives.BaseViewer {

        constructor($args? : BaseViewerArgs) {
            super($args);
            this.setInstance(new Button(ButtonType.WHITE_MEDIUM));
        }

        public getInstance() : Button {
            return <Button>super.getInstance();
        }

        protected normalImplementation() : void {
            const object : Button = this.getInstance();
            object.Text("test text");
            object.Title().Text("Button tooltip text");
        }

        /* dev:start */
        protected testImplementation() : string {
            let clickCounter : number = 0;
            const object : Button = this.getInstance();
            object.getEvents().setEvent(EventType.ON_CLICK, () : void => {
                clickCounter++;
                object.Text("Finish");
                Echo.Printf("button has been clicked " + clickCounter + " times");
            });

            object.Text("Finish");
            object.Title().Text("Button tooltip text");

            // object.IconName(IconType.SPINNER_SMALL);
            object.StyleClassName("testCssClass");
            object.getGuiOptions().Add(GuiOptionType.DISABLE);
            object.getGuiOptions().Add(GuiOptionType.ACTIVED);

            this.addTestButton("Disable", () : void => {
                object.Enabled(false);
            });
            this.addTestButton("Enable", () : void => {
                object.Enabled(true);
            });
            this.addTestButton("Error", () : void => {
                object.Error(true);
            });
            this.addTestButton("Selected", () : void => {
                object.IsSelected(true);
            });
            this.addTestButton("Normal", () : void => {
                object.IsSelected(false);
                object.Error(false);
            });
            this.addTestButton("Set text", () : void => {
                object.Text("new button text");
            });
            this.addTestButton("Width 300", () : void => {
                object.Width(300);
            });
            this.addTestButton("Width 50", () : void => {
                object.Width(50);
            });
            this.addTestButton("Reset Width", () : void => {
                object.Width(-1);
            });

            return "<style>.testCssClass {position: relative; top: 50px; left: 200px; float: left;}</style>";
        }

        /* dev:end */
    }
}
