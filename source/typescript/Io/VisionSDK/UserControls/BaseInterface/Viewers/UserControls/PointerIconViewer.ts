/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.BaseInterface.Viewers.UserControls {
    "use strict";
    import BaseViewerArgs = Com.Wui.Framework.Gui.Primitives.BaseViewerArgs;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import PointerIcon = Io.VisionSDK.UserControls.BaseInterface.UserControls.PointerIcon;
    import PointerIconType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.PointerIconType;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;

    export class PointerIconViewer extends Com.Wui.Framework.Gui.Primitives.BaseViewer {

        constructor($args? : BaseViewerArgs) {
            super($args);
            this.setInstance(new PointerIcon());
        }

        public getInstance() : PointerIcon {
            return <PointerIcon>super.getInstance();
        }

        /* dev:start */
        protected testImplementation($instance : PointerIcon) : string {
            $instance.StyleClassName("testCssClass");
            $instance.Visible(true);
            $instance.getEvents().setOnMove(() : void => {
                LogIt.Debug("Moved");
            });
            $instance.getEvents().setOnClick(() : void => {
                LogIt.Debug("Click fired");
            });
            $instance.getEvents().setOnDoubleClick(() : void => {
                LogIt.Debug("DoubleClick fired");
            });
            $instance.getEvents().setOnDrag(() : void => {
                LogIt.Debug("Drag fired");
            });
            this.addTestButton("Click", () : void => {
                $instance.Click("Click");
            });
            this.addTestButton("DoubleClick", () : void => {
                $instance.DoubleClick("DoubleClick");
            });
            this.addTestButton("Drag", () : void => {
                $instance.Drag("Drag");
            });
            this.addTestButton("Normal", () : void => {
                $instance.Restore();
            });
            this.addTestButton("On", () : void => {
                $instance.IconType(PointerIconType.ON);
            });
            this.addTestButton("Hide", () : void => {
                $instance.Visible(false);
            });
            this.addTestButton("Move", () : void => {
                $instance.setPosition(500, 500);
            });
            this.addTestButton("MoveTo", () : void => {
                $instance.setPosition(0, 0);
                $instance.MoveTo(1000, 1000);
            });
            this.addTestButton("MoveTo2", () : void => {
                $instance.setPosition(0, 0);
                $instance.MoveTo(100, 500);
            });
            this.addTestButton("MoveTo3", () : void => {
                $instance.setPosition(500, 500);
                $instance.MoveTo(50, 50);
            });

            return "<style>.testCssClass {position: relative; top: 300px; left: 300px;}</style>";
        }

        /* dev:end */
    }
}
