/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.BaseInterface.Viewers.UserControls {
    "use strict";
    import BaseViewerArgs = Com.Wui.Framework.Gui.Primitives.BaseViewerArgs;
    import BSPSelector = Io.VisionSDK.UserControls.BaseInterface.UserControls.BSPSelector;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import BSPSelectorType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.BSPSelectorType;

    export class BSPSelectorViewer extends Com.Wui.Framework.Gui.Primitives.BaseViewer {

        constructor($args? : BaseViewerArgs) {
            super($args);
            this.setInstance(new BSPSelector());
        }

        public getInstance() : BSPSelector {
            return <BSPSelector>super.getInstance();
        }

        protected normalImplementation() : void {
            const object : BSPSelector = this.getInstance();
            object.Title().Text("BSPSelector");
        }

        /* dev:start */
        protected testImplementation() : string {
            const object : BSPSelector = this.getInstance();
            object.StyleClassName("testCssClass");
            // object.Width(400);

            object.Add("L4.1.15_2.1.0");
            object.Add("L3.14.52_1.1.0");
            object.Add("lMX6_L5.1.1_2.1.0");
            object.Add("L4.1.15_2.1.0");
            object.Add("L3.14.52_1.1.0");
            object.Add("lMX6_L5.1.1_2.1.0");
            object.Add("L4.1.15_2.1.0");
            object.Add("L3.14.52_1.1.0");
            object.Add("lMX6_L5.1.1_2.1.0");
            object.Add("L4.1.15_2.1.0");
            object.Add("L3.14.52_1.1.0");
            object.Add("lMX6_L5.1.1_2.1.0");
            object.setText("SUPPORTED", "Brief description of each version");

            // object.Title().Text("BSPselector tooltip text");

            object.getEvents().setOnChange(() : void => {
                Echo.Printf("Chosen BSP {0}", object.Value());
            });

            this.addTestButton("Orange type", () : void => {
                object.GuiType(BSPSelectorType.ORANGE);
            });
            this.addTestButton("Green type", () : void => {
                object.GuiType(BSPSelectorType.GREEN);
            });
            this.addTestButton("Blue type", () : void => {
                object.GuiType(BSPSelectorType.BLUE);
            });

            return "<style>.testCssClass {position: relative; top: 50px; left: 200px; float: left;}</style>";
        }

        /* dev:end */
    }
}
