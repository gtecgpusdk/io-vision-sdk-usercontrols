/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.BaseInterface.Viewers.UserControls {
    "use strict";
    import BaseViewerArgs = Com.Wui.Framework.Gui.Primitives.BaseViewerArgs;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import GuiOptionType = Com.Wui.Framework.Gui.Enums.GuiOptionType;
    import FormInput = Io.VisionSDK.UserControls.BaseInterface.UserControls.FormInput;
    import PropertyFormArgs = Io.VisionSDK.UserControls.Structures.PropertyFormArgs;
    import PropertyListFormArgs = Io.VisionSDK.UserControls.Structures.PropertyListFormArgs;
    import PropertyPickerFormArgs = Io.VisionSDK.UserControls.Structures.PropertyPickerFormArgs;

    export class FormInputViewer extends Com.Wui.Framework.Gui.Primitives.BaseViewer {

        constructor($args? : BaseViewerArgs) {
            super($args);
            this.setInstance(new FormInput(new PropertyFormArgs()));
        }

        public getInstance() : FormInput {
            return <FormInput>super.getInstance();
        }

        /* dev:start */
        protected testImplementation() : string {
            this.setInstance(null);

            const object1 : FormInput = new FormInput(new PropertyFormArgs());
            const args : PropertyFormArgs = <PropertyFormArgs>object1.Configuration();
            args.AddGuiOption(GuiOptionType.DISABLE);
            args.Name("Name: ");
            args.Value("test value");
            args.Width(300);
            args.TitleText("Title text");
//            args.Error(true);
//            args.Visible(false);
//            args.Enabled(false);

            this.addTestButton("Disable", () : void => {
                object1.Enabled(false);
            });
            this.addTestButton("Enable", () : void => {
                object1.Enabled(true);
            });
            this.addTestButton("Error", () : void => {
                object1.Error(true);
            });
            this.addTestButton("Normal", () : void => {
                object1.Error(false);
            });

            const object2 : FormInput = new FormInput(new PropertyListFormArgs());
            const args2 : PropertyListFormArgs = <PropertyListFormArgs>object2.Configuration();
            args2.Name("Form: ");
            args2.Width(300);
            args2.AddItem("test1");
            args2.AddItem("test2");
            args2.AddItem("test3");

            const object3 : FormInput = new FormInput(new PropertyPickerFormArgs());
            const args3 : PropertyPickerFormArgs = <PropertyPickerFormArgs>object3.Configuration();
            args3.Name("Form: ");
            args3.Width(300);

            return "<div style=\"position: relative; top: 50px; left: 200px; background-color: #DFDFDF; width: 306px; height: 500px;\">" +
                object1.Draw() +
                object2.Draw() +
                object3.Draw() +
                "</div>";
        }

        /* dev:end */
    }
}
