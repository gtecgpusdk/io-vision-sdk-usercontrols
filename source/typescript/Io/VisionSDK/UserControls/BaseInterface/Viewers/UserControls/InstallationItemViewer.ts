/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.BaseInterface.Viewers.UserControls {
    "use strict";
    import BaseViewerArgs = Com.Wui.Framework.Gui.Primitives.BaseViewerArgs;
    import InstallationItem = Io.VisionSDK.UserControls.BaseInterface.UserControls.InstallationItem;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;

    export class InstallationItemViewer extends Com.Wui.Framework.Gui.Primitives.BaseViewer {

        constructor($args? : BaseViewerArgs) {
            super($args);
            this.setInstance(new InstallationItem());
        }

        public getInstance() : InstallationItem {
            return <InstallationItem>super.getInstance();
        }

        protected normalImplementation() : void {
            const object : InstallationItem = this.getInstance();
            object.Title().Text("Installation Item text");
        }

        /* dev:start */
        protected testImplementation() : void {
            const object : InstallationItem = this.getInstance();
            object.StyleClassName("testCssClass");
            // object.Title().Text("Installation item tooltip text");
            object.Width(620);
            // object.IsNested(true);
            object.Text("This is the test install item");
            object.Info("This is info text for installation item</br>Estimated size: 100 MB");
        }

        /* dev:end */
    }
}
