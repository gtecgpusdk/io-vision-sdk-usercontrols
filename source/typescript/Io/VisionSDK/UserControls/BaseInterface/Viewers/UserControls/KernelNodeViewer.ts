/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.BaseInterface.Viewers.UserControls {
    "use strict";
    import BaseViewerArgs = Com.Wui.Framework.Gui.Primitives.BaseViewerArgs;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import KernelNode = Io.VisionSDK.UserControls.BaseInterface.UserControls.KernelNode;
    import KernelNodeType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.KernelNodeType;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;

    export class KernelNodeViewer extends Com.Wui.Framework.Gui.Primitives.BaseViewer {

        constructor($args? : BaseViewerArgs) {
            super($args);
            this.setInstance(new KernelNode(KernelNodeType.ACCUMULATE, -1, -1));
        }

        public getInstance() : KernelNode {
            return <KernelNode>super.getInstance();
        }

        /* dev:start */
        protected testImplementation() : string {
            const object : KernelNode = this.getInstance();
            object.StyleClassName("testCssClass");
            object.getEvents().setOnMouseEnter(() => {
                LogIt.Debug("Mouse entered node.");
            });
            object.getEvents().setOnMouseLeave(() => {
                LogIt.Debug("Mouse left node.");
            });

            this.addTestButton("Enable", () : void => {
                object.Enabled(true);
            });
            this.addTestButton("Disable", () : void => {
                object.Enabled(false);
            });
            this.addTestButton("Show input connectors", () : void => {
                object.HideConnectors();
                // object.ShowInputConnectors();
            });
            this.addTestButton("Show output connectors", () : void => {
                object.HideConnectors();
                object.ShowOutputConnectors();
            });
            this.addTestButton("Hide connectors", () : void => {
                object.HideConnectors();
            });
            this.addTestButton("Toggle select", () : void => {
                if (object.Selected()) {
                    object.Selected(false);
                } else {
                    object.Selected(true);
                }
            });

            return "<style>.testCssClass {position: relative; top: 50px; left: 200px;float: left; " +
                "background-color: #64FF00; padding: 40px;}</style>";
        }

        /* dev:end */
    }
}
