/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.BaseInterface.Viewers.UserControls {
    "use strict";
    import BaseViewerArgs = Com.Wui.Framework.Gui.Primitives.BaseViewerArgs;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import KernelNodeButton = Io.VisionSDK.UserControls.BaseInterface.UserControls.KernelNodeButton;
    import KernelNodeType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.KernelNodeType;
    import KernelNode = Io.VisionSDK.UserControls.BaseInterface.UserControls.KernelNode;

    export class KernelNodeButtonViewer extends Com.Wui.Framework.Gui.Primitives.BaseViewer {

        constructor($args? : BaseViewerArgs) {
            super($args);
            this.setInstance(new KernelNodeButton(new KernelNode(KernelNodeType.BITWISE_AND, -1, -1)));
        }

        public getInstance() : KernelNodeButton {
            return <KernelNodeButton>super.getInstance();
        }

        protected normalImplementation() : void {
            const object : KernelNodeButton = this.getInstance();
            object.Title().Text("Node title");
        }

        /* dev:start */
        protected testImplementation() : void {
            const object : KernelNodeButton = this.getInstance();
            object.StyleClassName("testCssClass");
            object.Width(620);
            object.Text("Name of node");
        }

        /* dev:end */
    }
}
