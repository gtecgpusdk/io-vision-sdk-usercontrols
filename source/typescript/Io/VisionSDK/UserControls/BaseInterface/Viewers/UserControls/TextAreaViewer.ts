/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.BaseInterface.Viewers.UserControls {
    "use strict";
    import BaseViewerArgs = Com.Wui.Framework.Gui.Primitives.BaseViewerArgs;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import GuiOptionType = Com.Wui.Framework.Gui.Enums.GuiOptionType;
    import TextArea = Io.VisionSDK.UserControls.BaseInterface.UserControls.TextArea;
    import TextAreaType = Com.Wui.Framework.UserControls.BaseInterface.Enums.UserControls.TextAreaType;

    export class TextAreaViewer extends Com.Wui.Framework.Gui.Primitives.BaseViewer {

        constructor($args? : BaseViewerArgs) {
            super($args);
            this.setInstance(new TextArea(TextAreaType.GENERAL));
        }

        public getInstance() : TextArea {
            return <TextArea>super.getInstance();
        }

        /* dev:start */
        protected testImplementation() : string {
            const object : TextArea = this.getInstance();
            object.Title().Text("Text filed tooltip text");
            object.StyleClassName("testCssClass");
            object.getGuiOptions().Add(GuiOptionType.DISABLE);
            object.Value("test value");
            object.Hint("Write some text here ...");
            object.Width(300);

            this.addTestButton("Disable", () : void => {
                object.Enabled(false);
            });
            this.addTestButton("Enable", () : void => {
                object.Enabled(true);
            });
            this.addTestButton("Error", () : void => {
                object.Error(true);
            });
            this.addTestButton("Normal", () : void => {
                object.Error(false);
            });
            this.addTestButton("Set text", () : void => {
                object.Value("new text area value");
            });

            return "<style>.testCssClass {position: relative; top: 50px; left: 200px; float: left;}</style>";
        }

        /* dev:end */
    }
}
