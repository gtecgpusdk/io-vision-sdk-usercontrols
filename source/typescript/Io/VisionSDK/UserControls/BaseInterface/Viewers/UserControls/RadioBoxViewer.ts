/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.BaseInterface.Viewers.UserControls {
    "use strict";
    import BaseViewerArgs = Com.Wui.Framework.Gui.Primitives.BaseViewerArgs;
    import RadioBox = Io.VisionSDK.UserControls.BaseInterface.UserControls.RadioBox;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import GuiOptionType = Com.Wui.Framework.Gui.Enums.GuiOptionType;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;

    export class RadioBoxViewer extends Com.Wui.Framework.Gui.Primitives.BaseViewer {

        constructor($args? : BaseViewerArgs) {
            super($args);
            this.setInstance(new RadioBox("groupName"));
        }

        public getInstance() : RadioBox {
            return <RadioBox>super.getInstance();
        }

        protected normalImplementation() : void {
            let object : RadioBox = this.getInstance();
            if (ObjectValidator.IsEmptyOrNull(object)) {
                this.setInstance(new RadioBox("groupName"));
                object = this.getInstance();
            }
            object.Text("text");
        }

        /* dev:start */
        protected testImplementation() : string {
            this.setInstance(null);
            const object1 : RadioBox = new RadioBox("testGroup", "item1");
            const object2 : RadioBox = new RadioBox("testGroup", "item2");
            const object3 : RadioBox = new RadioBox("testGroup", "item3");

//            object1.Checked(true);
            object1.Text("test radiobox 1");
            object1.Title().Text("title test");
            object1.getGuiOptions().Add(GuiOptionType.DISABLE);

            object2.Checked(true);
            object2.Text("test radiobox 2");
            object2.getGuiOptions().Add(GuiOptionType.DISABLE);

//            object3.Checked(true);
            object3.Text("test radiobox 3");
            object3.getGuiOptions().Add(GuiOptionType.DISABLE);

            this.addTestButton("Disable", () : void => {
                object1.Enabled(false);
            });
            this.addTestButton("Enable", () : void => {
                object1.Enabled(true);
            });
            this.addTestButton("Error", () : void => {
                object1.Error(true);
            });
            this.addTestButton("Normal", () : void => {
                object1.Error(false);
            });
            this.addTestButton("Checked", () : void => {
                object1.Checked(true);
            });
            this.addTestButton("Unchecked", () : void => {
                object1.Checked(false);
            });
            this.addTestButton("Checked/Unchecked", () : void => {
                RadioBox.ToggleChecked(object1);
            });
            this.addTestButton("Set text", () : void => {
                object1.Text("new checkbox text");
            });
            this.addTestButton("Disable group", () : void => {
                RadioBox.EnabledGroup("testGroup", false);
            });
            this.addTestButton("Enable group", () : void => {
                RadioBox.EnabledGroup("testGroup", true);
            });
            this.addTestButton("Error group", () : void => {
                RadioBox.ErrorGroup("testGroup", true);
            });
            this.addTestButton("Normal group", () : void => {
                RadioBox.ErrorGroup("testGroup", false);
            });

            return "<div style=\"position: relative; top: 50px; left: 200px; width: 500px;\">" +
                object1.Draw() + object2.Draw() + object3.Draw() +
                "</div>";
        }

        /* dev:end */
    }
}
