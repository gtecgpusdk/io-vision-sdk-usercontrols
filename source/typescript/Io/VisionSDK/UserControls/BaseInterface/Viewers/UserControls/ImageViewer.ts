/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.BaseInterface.Viewers.UserControls {
    "use strict";
    import BaseViewerArgs = Com.Wui.Framework.Gui.Primitives.BaseViewerArgs;
    import Image = Io.VisionSDK.UserControls.BaseInterface.UserControls.Image;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import ImageType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.ImageType;
    import ImageOutputArgs = Com.Wui.Framework.Gui.ImageProcessor.ImageOutputArgs;

    export class ImageViewer extends Com.Wui.Framework.Gui.Primitives.BaseViewer {

        constructor($args? : BaseViewerArgs) {
            super($args);
            this.setInstance(new Image(ImageType.STATIC_BAR));
        }

        public getInstance() : Image {
            return <Image>super.getInstance();
        }

        /* dev:start */
        protected testImplementation() : string {
            const object : Image = this.getInstance();
            object.StyleClassName("testCssClass");
            const args : ImageOutputArgs = new ImageOutputArgs();
            args.setSize(1040, 7);
            object.OutputArgs(args);

            return "<style>.testCssClass {position: relative; top: 50px; left: 200px; float: left;}</style>";
        }

        /* dev:end */
    }
}
