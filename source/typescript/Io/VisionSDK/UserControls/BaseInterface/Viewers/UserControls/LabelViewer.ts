/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.BaseInterface.Viewers.UserControls {
    "use strict";
    import BaseViewerArgs = Com.Wui.Framework.Gui.Primitives.BaseViewerArgs;
    import Label = Io.VisionSDK.UserControls.BaseInterface.UserControls.Label;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import GuiOptionType = Com.Wui.Framework.Gui.Enums.GuiOptionType;

    export class LabelViewer extends Com.Wui.Framework.Gui.Primitives.BaseViewer {

        constructor($args? : BaseViewerArgs) {
            super($args);
            this.setInstance(new Label());
        }

        public getInstance() : Label {
            return <Label>super.getInstance();
        }

        protected normalImplementation() : void {
            const object : Label = this.getInstance();
            object.Text("label:");
        }

        /* dev:start */
        protected testImplementation() : string {
            const object : Label = this.getInstance();
            object.Text("test label:");
            object.Title().Text("Label tooltip text");
            object.StyleClassName("testCssClass");
//            object.Enabled(false);
            object.getGuiOptions().Add(GuiOptionType.DISABLE);

            this.addTestButton("Disable", () : void => {
                object.Enabled(false);
            });
            this.addTestButton("Enable", () : void => {
                object.Enabled(true);
            });
            this.addTestButton("Show", () : void => {
                object.Visible(true);
            });
            this.addTestButton("Hide", () : void => {
                object.Visible(false);
            });
            this.addTestButton("Set text", () : void => {
                object.Text("new label text");
            });

            return "<style>.testCssClass {position: relative; top: 50px; left: 200px; float: left;}</style>";
        }

        /* dev:end */
    }
}
