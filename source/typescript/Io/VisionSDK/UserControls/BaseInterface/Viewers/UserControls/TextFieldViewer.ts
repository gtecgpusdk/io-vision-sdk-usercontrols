/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.BaseInterface.Viewers.UserControls {
    "use strict";
    import BaseViewerArgs = Com.Wui.Framework.Gui.Primitives.BaseViewerArgs;
    import TextField = Io.VisionSDK.UserControls.BaseInterface.UserControls.TextField;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import GuiOptionType = Com.Wui.Framework.Gui.Enums.GuiOptionType;
    import TextFieldType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.TextFieldType;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import AutocompleteOption = Com.Wui.Framework.UserControls.Structures.AutocompleteOption;

    export class TextFieldViewer extends Com.Wui.Framework.Gui.Primitives.BaseViewer {

        constructor($args? : BaseViewerArgs) {
            super($args);
            this.setInstance(new TextField(TextFieldType.GENERAL));
        }

        public getInstance() : TextField {
            return <TextField>super.getInstance();
        }

        /* dev:start */
        protected testImplementation() : string {
            const object : TextField = this.getInstance();
            object.Title().Text("Text filed tooltip text");
            object.StyleClassName("testCssClass");
            object.getGuiOptions().Add(GuiOptionType.DISABLE);
            object.Value("test value");
            object.Hint("Write some text here ...");
            object.Width(300);
//            object.Enabled(false);
//            object.setPasswordEnabled();
//            object.setOnlyNumbersAllowed();
//            object.setAutocompleteDisable();
//            object.LengthLimit(5);

            const autocompleteData : ArrayList<AutocompleteOption> = new ArrayList<AutocompleteOption>();
            for (let index : number = 0; index < 100; index++) {
                autocompleteData.Add(new AutocompleteOption("value " + index));
            }
            object.setAutocompleteData(autocompleteData);

            this.addTestButton("Disable", () : void => {
                object.Enabled(false);
            });
            this.addTestButton("Enable", () : void => {
                object.Enabled(true);
            });
            this.addTestButton("Error", () : void => {
                object.Error(true);
            });
            this.addTestButton("Normal", () : void => {
                object.Error(false);
            });
            this.addTestButton("Set text", () : void => {
                object.Value("new text filed value");
            });

            return "<style>.testCssClass {position: relative; top: 50px; left: 200px; float: left;}</style>";
        }

        /* dev:end */
    }
}
