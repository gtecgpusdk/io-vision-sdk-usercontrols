/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.BaseInterface.Viewers.UserControls {
    "use strict";
    import BaseViewerArgs = Com.Wui.Framework.Gui.Primitives.BaseViewerArgs;
    import ConnectorHolder = Io.VisionSDK.UserControls.BaseInterface.UserControls.ConnectorHolder;
    import KernelNodeInterfaceType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.KernelNodeInterfaceType;

    export class ConnectorHolderViewer extends Com.Wui.Framework.Gui.Primitives.BaseViewer {

        constructor($args? : BaseViewerArgs) {
            super($args);
            this.setInstance(new ConnectorHolder(KernelNodeInterfaceType.OUTPUT, null, null));
        }

        public getInstance() : ConnectorHolder {
            return <ConnectorHolder>super.getInstance();
        }

        protected normalImplementation() : void {
            const object : ConnectorHolder = this.getInstance();
        }

        /* dev:start */
        protected testImplementation() : string {
            const object : ConnectorHolder = this.getInstance();

            return "<style>.testCssClass {position: relative; top: 50px; left: 200px; float: left;}</style>";
        }

        /* dev:end */
    }
}
