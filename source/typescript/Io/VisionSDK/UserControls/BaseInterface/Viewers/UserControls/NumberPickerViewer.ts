/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.BaseInterface.Viewers.UserControls {
    "use strict";
    import BaseViewerArgs = Com.Wui.Framework.Gui.Primitives.BaseViewerArgs;
    import NumberPicker = Io.VisionSDK.UserControls.BaseInterface.UserControls.NumberPicker;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import GuiOptionType = Com.Wui.Framework.Gui.Enums.GuiOptionType;

    export class NumberPickerViewer extends Com.Wui.Framework.Gui.Primitives.BaseViewer {

        constructor($args? : BaseViewerArgs) {
            super($args);
            this.setInstance(new NumberPicker());
        }

        public getInstance() : NumberPicker {
            return <NumberPicker>super.getInstance();
        }

        /* dev:start */
        protected testImplementation() : string {
            const object : NumberPicker = this.getInstance();
            object.RangeStart(-100);
            object.RangeEnd(100);
            object.Value(50);
            object.Width(200);
            object.Text("{0} units");
            object.DecimalPlaces(1);
//            object.Title().Text("title test");
            object.StyleClassName("testCssClass");
            object.getGuiOptions().Add(GuiOptionType.DISABLE);
//            object.Enabled(false);

            this.addTestButton("Disable", () : void => {
                object.Enabled(false);
            });
            this.addTestButton("Enable", () : void => {
                object.Enabled(true);
            });
            this.addTestButton("set 0", () : void => {
                object.Value(0);
            });
            this.addTestButton("set 50", () : void => {
                object.Value(50);
            });
            this.addTestButton("set 100", () : void => {
                object.Value(100);
            });
            this.addTestButton("Set size", () : void => {
                object.Width(800);
            });
            this.addTestButton("Set text", () : void => {
                object.Text("{0} px");
            });

            return "<style>.testCssClass {position: relative; top: 50px; left: 200px; float: left; padding: 20px; " +
                "background-color: #DFDFDF; width: 320px; height: 50px;}</style>";
        }

        /* dev:end */
    }
}
