/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.BaseInterface.Viewers.UserControls {
    "use strict";
    import BaseViewerArgs = Com.Wui.Framework.Gui.Primitives.BaseViewerArgs;
    import InformativeButton = Io.VisionSDK.UserControls.BaseInterface.UserControls.InformativeButton;

    export class InformativeButtonViewer extends Com.Wui.Framework.Gui.Primitives.BaseViewer {

        constructor($args? : BaseViewerArgs) {
            super($args);
            this.setInstance(new InformativeButton());
        }

        public getInstance() : InformativeButton {
            return <InformativeButton>super.getInstance();
        }

        protected normalImplementation() : void {
            const object : InformativeButton = this.getInstance();
            object.Title().Text("Node title");
        }

        /* dev:start */
        protected testImplementation() : string {
            const object : InformativeButton = this.getInstance();
            object.StyleClassName("testCssClass");
            object.Text("Text text text");
            object.Description("Description description description");

            return "<style>.testCssClass {position: relative; top: 50px; left: 200px; width: 200px; height: 100px; float: left;}</style>";
        }

        /* dev:end */
    }
}
