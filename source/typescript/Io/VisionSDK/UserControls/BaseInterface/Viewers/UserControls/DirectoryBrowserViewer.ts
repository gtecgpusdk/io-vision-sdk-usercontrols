/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.BaseInterface.Viewers.UserControls {
    "use strict";
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import DirectoryBrowser = Io.VisionSDK.UserControls.BaseInterface.UserControls.DirectoryBrowser;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import IFileSystemFileProtocol = Com.Wui.Framework.Commons.Interfaces.IFileSystemFileProtocol;
    import FileSystemItemType = Com.Wui.Framework.Commons.Enums.FileSystemItemType;
    import IFileSystemDirectoryProtocol = Com.Wui.Framework.Commons.Interfaces.IFileSystemDirectoryProtocol;
    import FileSystemFilter = Com.Wui.Framework.Commons.Utils.FileSystemFilter;
    import IFileSystemDriveProtocol = Com.Wui.Framework.Commons.Interfaces.IFileSystemDriveProtocol;
    import BasePanelViewerArgs = Com.Wui.Framework.Gui.Primitives.BasePanelViewerArgs;

    export class DirectoryBrowserViewer extends Com.Wui.Framework.Gui.Primitives.BasePanelViewer {

        constructor($args? : BasePanelViewerArgs) {
            super($args);
            this.setInstance(new DirectoryBrowser());
        }

        public getInstance() : DirectoryBrowser {
            return <DirectoryBrowser>super.getInstance();
        }

        /* dev:start */
        protected testImplementation() : string {
            const object : DirectoryBrowser = this.getInstance();
            object.StyleClassName("testCssClass");
            const map : any = <any>[
                /* tslint:disable: object-literal-sort-keys */
                {
                    name: "Favorites",
                    type: FileSystemItemType.FAVORITES,
                    map : <IFileSystemDirectoryProtocol[]>[
                        {
                            name      : "Desktop",
                            type      : FileSystemItemType.DIRECTORY,
                            attributes: [],
                            map       : []
                        },
                        {
                            name      : "Downloads",
                            type      : FileSystemItemType.DIRECTORY,
                            attributes: [],
                            map       : []
                        }
                    ]
                },
                {
                    name: "Computer",
                    type: FileSystemItemType.MY_COMPUTER,
                    map : <IFileSystemDriveProtocol[]>[
                        {
                            name : "Primary",
                            value: "C:",
                            type : FileSystemItemType.DRIVE,
                            map  : [
                                <IFileSystemDirectoryProtocol>{
                                    name      : "boot",
                                    type      : FileSystemItemType.DIRECTORY,
                                    attributes: [],
                                    map       : [
                                        <IFileSystemDirectoryProtocol>{
                                            name      : "macrium",
                                            type      : FileSystemItemType.DIRECTORY,
                                            attributes: [],
                                            map       : []
                                        }
                                    ]
                                },
                                <IFileSystemDirectoryProtocol>{
                                    name      : "cygwin",
                                    type      : FileSystemItemType.DIRECTORY,
                                    attributes: [],
                                    map       : <IFileSystemDirectoryProtocol[]>[]
                                },
                                <IFileSystemDirectoryProtocol>{
                                    name      : "cygwin64",
                                    type      : FileSystemItemType.DIRECTORY,
                                    attributes: [],
                                    map       : <IFileSystemDirectoryProtocol[]>[]
                                },
                                <IFileSystemDirectoryProtocol>{
                                    name      : "Drivers",
                                    type      : FileSystemItemType.DIRECTORY,
                                    attributes: [],
                                    map       : <IFileSystemDirectoryProtocol[]>[]
                                },
                                <IFileSystemDirectoryProtocol>{
                                    name      : "InstallAnywhere",
                                    type      : FileSystemItemType.DIRECTORY,
                                    attributes: [FileSystemItemType.HIDDEN],
                                    map       : []
                                },
                                <IFileSystemFileProtocol>{
                                    name      : ".rnd",
                                    type      : FileSystemItemType.FILE,
                                    attributes: []
                                },
                                <IFileSystemFileProtocol>{
                                    name      : "HaxLogs.txt",
                                    type      : FileSystemItemType.FILE,
                                    attributes: []
                                }
                            ]
                        },
                        {
                            name : "WORK",
                            value: "D:",
                            type : FileSystemItemType.DRIVE,
                            map  : <IFileSystemDirectoryProtocol[]>[
                                {
                                    name      : "__DOKUMENTY__",
                                    type      : FileSystemItemType.DIRECTORY,
                                    attributes: [],
                                    map       : []
                                },
                                {
                                    name      : "__PROJECTS__",
                                    type      : FileSystemItemType.DIRECTORY,
                                    attributes: [],
                                    map       : <IFileSystemDirectoryProtocol[]>[
                                        {
                                            name      : "Freescale",
                                            type      : FileSystemItemType.DIRECTORY,
                                            attributes: [],
                                            map       : []
                                        }
                                    ]
                                },
                                {
                                    name      : "__TEMP__",
                                    type      : FileSystemItemType.DIRECTORY,
                                    attributes: [],
                                    map       : []
                                },
                                {
                                    name      : "Android CM",
                                    type      : FileSystemItemType.DIRECTORY,
                                    attributes: [],
                                    map       : []
                                }
                            ]
                        }
                    ]
                },
                {
                    name: "Network",
                    type: FileSystemItemType.NETWORK,
                    map : []
                }
                /* tslint:enable */
            ];
            object.setStructure(map);
            object.Path("C:/boot");
            // object.Filter(FileSystemFilter.HIDDEN_ALL, "*.log");

            object.Width(500);
            object.Height(350);

            object.getEvents().setOnChange(() : void => {
                Echo.Printf("selected: \"{0}\"", StringUtils.Replace(object.Path(), "/", "\\"));
            });

            object.getEvents().setOnDirectoryRequest(() : void => {
                const path : string = object.Path();
                if (path === "C:/cygwin") {
                    object.setStructure(<any>[
                        /* tslint:disable: object-literal-sort-keys */
                        <IFileSystemDirectoryProtocol>{
                            name      : "var",
                            type      : FileSystemItemType.DIRECTORY,
                            attributes: [],
                            map       : [
                                <IFileSystemDirectoryProtocol>{
                                    name      : "log",
                                    type      : FileSystemItemType.DIRECTORY,
                                    attributes: [],
                                    map       : <IFileSystemFileProtocol[]>[
                                        {
                                            name      : "setup.log",
                                            type      : FileSystemItemType.FILE,
                                            attributes: []
                                        },
                                        {
                                            name      : "setup.log.full",
                                            type      : FileSystemItemType.FILE,
                                            attributes: []
                                        }
                                    ]
                                }
                            ]
                        }
                        /* tslint:enable */
                    ], path);
                } else if (path === "D:/__DOKUMENTY__") {
                    object.setStructure(<any>[
                        /* tslint:disable: object-literal-sort-keys */
                        <IFileSystemDirectoryProtocol>{
                            name      : "Reports",
                            type      : FileSystemItemType.DIRECTORY,
                            attributes: [],
                            map       : []
                        },
                        <IFileSystemFileProtocol>{
                            name      : "FTF 2014.pod.xml",
                            type      : FileSystemItemType.FILE,
                            attributes: []
                        },
                        <IFileSystemFileProtocol>{
                            name      : "VisionSDK",
                            type      : FileSystemItemType.LINK,
                            attributes: []
                        }
                        /* tslint:enable */
                    ], path);
                } else {
                    object.setStructure([], "unknown");
                }
            });

            this.addTestButton("apply filter", () : void => {
                object.Filter(FileSystemFilter.DIRECTORIES);
            });

            return "<style>.testCssClass {position: relative; top: 10px; left: 10px; float: left;}</style>" +
                StringUtils.NewLine() + StringUtils.NewLine();
        }

        /* dev:end */
    }
}
