/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.BaseInterface.Viewers.UserControls {
    "use strict";
    import BaseViewerArgs = Com.Wui.Framework.Gui.Primitives.BaseViewerArgs;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import GuiOptionType = Com.Wui.Framework.Gui.Enums.GuiOptionType;
    import InputLabelType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.InputLabelType;
    import InputLabel = Io.VisionSDK.UserControls.BaseInterface.UserControls.InputLabel;

    export class InputLabelViewer extends Com.Wui.Framework.Gui.Primitives.BaseViewer {

        constructor($args? : BaseViewerArgs) {
            super($args);
            this.setInstance(new InputLabel(InputLabelType.PROPERTY));
        }

        public getInstance() : InputLabel {
            return <InputLabel>super.getInstance();
        }

        /* dev:start */
        protected testImplementation() : string {
            const object : InputLabel = this.getInstance();
            object.Title().Text("Text filed tooltip text");
//            object.IconName(IconType.BLACK_SQUARE);
            object.StyleClassName("testCssClass");
            object.getGuiOptions().Add(GuiOptionType.DISABLE);
            object.Text("test value");
            object.Width(300);
//            object.Enabled(false);

            this.addTestButton("Disable", () : void => {
                object.Enabled(false);
            });
            this.addTestButton("Enable", () : void => {
                object.Enabled(true);
            });
            this.addTestButton("Show", () : void => {
                object.Visible(true);
            });
            this.addTestButton("Hide", () : void => {
                object.Visible(false);
            });
            this.addTestButton("Error", () : void => {
                object.Error(true);
            });
            this.addTestButton("Normal", () : void => {
                object.Error(false);
            });
            this.addTestButton("Set text", () : void => {
                object.Text("new label text");
            });
            this.addTestButton("Set width", () : void => {
                object.Width(30);
            });

            return "<style>.testCssClass {position: relative; top: 50px; left: 200px; float: left;}</style>";
        }

        /* dev:end */
    }
}
