/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.BaseInterface.Viewers.UserControls {
    "use strict";
    import BaseViewerArgs = Com.Wui.Framework.Gui.Primitives.BaseViewerArgs;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import NotificationMessageType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.NotificationMessageType;
    import NotificationMessage = Io.VisionSDK.UserControls.BaseInterface.UserControls.NotificationMessage;

    export class NotificationMessageViewer extends Com.Wui.Framework.Gui.Primitives.BaseViewer {

        constructor($args? : BaseViewerArgs) {
            super($args);
            this.setInstance(new NotificationMessage());
        }

        public getInstance() : NotificationMessage {
            return <NotificationMessage>super.getInstance();
        }

        protected normalImplementation() : void {
            const object : NotificationMessage = this.getInstance();
            object.Text("message text");
            object.Prefix("message prefix: ");
        }

        /* dev:start */
        protected testImplementation() : string {
            const object : NotificationMessage = this.getInstance();

            object.Text("Test");
            object.Prefix("message prefix: ");
//            object.Title().Text("tooltip text");
            object.StyleClassName("testCssClass");
//            object.GuiType(NotificationMessageType.GENERAL_INFO);
            object.Width(600);

            this.addTestButton("Show", () : void => {
                object.Visible(true);
            });
            this.addTestButton("Hide", () : void => {
                object.Visible(false);
            });
            this.addTestButton("Info", () : void => {
                object.GuiType(NotificationMessageType.GENERAL_INFO);
            });
            this.addTestButton("Warning", () : void => {
                object.GuiType(NotificationMessageType.GENERAL_WARNING);
            });
            this.addTestButton("Error", () : void => {
                object.GuiType(NotificationMessageType.GENERAL_ERROR);
            });
            this.addTestButton("Success", () : void => {
                object.GuiType(NotificationMessageType.GENERAL_SUCCESS);
            });
            this.addTestButton("Width 500", () : void => {
                object.Width(500);
            });
            this.addTestButton("Width 100", () : void => {
                object.Width(100);
            });

            return "<style>.testCssClass {position: relative; top: 50px; left: 200px; float: left;}</style>";
        }

        /* dev:end */
    }
}
