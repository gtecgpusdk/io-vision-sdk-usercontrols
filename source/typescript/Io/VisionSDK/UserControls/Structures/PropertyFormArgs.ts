/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.Structures {
    "use strict";
    import TextFieldFormArgs = Com.Wui.Framework.UserControls.Structures.TextFieldFormArgs;
    import Property = Com.Wui.Framework.Commons.Utils.Property;

    export class PropertyFormArgs extends TextFieldFormArgs {
        private floatOnly : boolean;

        constructor() {
            super();
            this.floatOnly = false;
        }

        public FloatOnly($value? : boolean) : boolean {
            return this.floatOnly = Property.Boolean(this.floatOnly, $value);
        }
    }
}
