/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.Structures {
    "use strict";
    import IKernelNode = Io.VisionSDK.UserControls.Interfaces.IKernelNode;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import IVector = Io.VisionSDK.UserControls.Interfaces.IVector;

    export class KernelNodeInterface {
        public node : IKernelNode;
        public position : number;
        public dataType : string;
        private gridPosition : IVector;

        public GridPosition($value? : IVector) : IVector {
            if (ObjectValidator.IsSet($value)) {
                this.gridPosition = $value;
            }
            return ObjectValidator.IsEmptyOrNull(this.gridPosition) ? this.node.getGridPosition() : this.gridPosition;
        }
    }
}
