/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.Structures {
    "use strict";
    import BaseFormInputArgs = Com.Wui.Framework.UserControls.Structures.BaseFormInputArgs;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import Property = Com.Wui.Framework.Commons.Utils.Property;

    export class PropertyPickerFormArgs extends BaseFormInputArgs {
        private integerOnly : boolean;
        private size : number;
        private step : number;
        private rangeStart : any;
        private rangeEnd : any;

        constructor() {
            super();

            this.integerOnly = false;
            this.size = -1;
            this.step = -1;
            this.rangeStart = null;
            this.rangeEnd = null;
        }

        /**
         * @param {number} [$value] Specify parameter's value.
         * @return {number} Returns parameter's value.
         */
        public Value($value? : number) : number {
            if (ObjectValidator.IsSet($value) && ObjectValidator.IsDigit($value)) {
                super.Value($value);
            }
            return <number>super.Value();
        }

        /**
         * @param {boolean} [$value] Specify, if parameter's value should be only integer.
         * @return {boolean} Returns true, if parameter's value should be only integer, otherwise false.
         */
        public IntegerOnly($value? : boolean) : boolean {
            return this.integerOnly = Property.Boolean(this.integerOnly, $value);
        }

        /**
         * @param {number} [$value] Specify parameter's maximal length including decimal places count.
         * @return {number} Returns parameter's maximal length of value including decimal places count,
         * if size has been specified, otherwise -1.
         */
        public Size($value? : number) : number {
            return this.size = Property.Integer(this.size, $value, 1);
        }

        /**
         * @param {number} [$value] Specify minimal positive and non-zero step for change of current value.
         * @return {number} Returns minimal step value, if value step has been specified, otherwise -1.
         */
        public Step($value? : number) : number {
            if (ObjectValidator.IsDigit($value) && $value > 0) {
                this.step = $value;
            }
            return this.step;
        }

        public RangeStart($value? : number) : number;

        public RangeStart($value? : (...$args : any[]) => number) : (...$args : any[]) => number;

        /**
         * @param {number|Function} [$value] Specify parameter's minimal value by number of function witch returns number.
         * @return {null|number|Function} Returns parameter's minimal value if range has been specified, otherwise null.
         */
        public RangeStart($value? : any) : any {
            if (ObjectValidator.IsDigit($value) || ObjectValidator.IsFunction($value)) {
                this.rangeStart = $value;
            }
            return this.rangeStart;
        }

        public RangeEnd($value? : number) : number;

        public RangeEnd($value? : (...$args : any[]) => number) : (...$args : any[]) => number;

        /**
         * @param {number|Function} [$value] Specify parameter's maximal value by number of function witch returns number.
         * @return {null|number|Function} Returns parameter's maximal value if range has been specified, otherwise null.
         */
        public RangeEnd($value? : any) : any {
            if (ObjectValidator.IsDigit($value) || ObjectValidator.IsFunction($value)) {
                this.rangeEnd = $value;
            }
            return this.rangeEnd;
        }
    }
}
