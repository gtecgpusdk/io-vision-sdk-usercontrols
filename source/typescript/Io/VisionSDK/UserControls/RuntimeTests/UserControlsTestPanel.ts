/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Io.VisionSDK.UserControls.RuntimeTests {
    "use strict";
    import UserControls = Io.VisionSDK.UserControls.BaseInterface.UserControls;
    import Enums = Com.Wui.Framework.UserControls.BaseInterface.Enums.UserControls;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import ResizeableType = Com.Wui.Framework.Gui.Enums.ResizeableType;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;
    import BasePanel = Io.VisionSDK.UserControls.Primitives.BasePanel;

    export class UserControlsTestPanel extends BasePanel {
        public label1 : UserControls.Label;
        public label2 : UserControls.Label;
        public imageButton1 : UserControls.ImageButton;
        public imageButton2 : UserControls.ImageButton;
        public button1 : UserControls.Button;
        public button2 : UserControls.Button;
        public textField1 : UserControls.TextField;
        public textField2 : UserControls.TextField;
        public textArea1 : UserControls.TextArea;
        public textArea2 : UserControls.TextArea;
        public checkBox1 : UserControls.CheckBox;
        public checkBox2 : UserControls.CheckBox;
        public radioBox1 : UserControls.RadioBox;
        public radioBox2 : UserControls.RadioBox;
        public dropDownList1 : UserControls.DropDownList;
        public dropDownList2 : UserControls.DropDownList;
        public numberPicker1 : UserControls.NumberPicker;
        public numberPicker2 : UserControls.NumberPicker;
        public progressBar1 : UserControls.ProgressBar;
        public progressBar2 : UserControls.ProgressBar;
        public inputLabel1 : UserControls.InputLabel;
        public inputLabel2 : UserControls.InputLabel;

        constructor($id? : string) {
            super($id);

            this.label1 = new UserControls.Label("test label: ");
            this.label2 = new UserControls.Label("test label2");

            this.imageButton1 = new UserControls.ImageButton();
            this.imageButton2 = new UserControls.ImageButton(Enums.ImageButtonType.GREEN);

            this.button1 = new UserControls.Button();
            this.button2 = new UserControls.Button(Enums.ButtonType.RED);

            this.textField1 = new UserControls.TextField();
            this.textField2 = new UserControls.TextField(Enums.TextFieldType.BLUE);

            this.textArea1 = new UserControls.TextArea();
            this.textArea2 = new UserControls.TextArea();

            this.checkBox1 = new UserControls.CheckBox();
            this.checkBox2 = new UserControls.CheckBox();

            this.radioBox1 = new UserControls.RadioBox("testGroup");
            this.radioBox2 = new UserControls.RadioBox("testGroup");

            this.dropDownList1 = new UserControls.DropDownList();
            this.dropDownList2 = new UserControls.DropDownList();

            this.numberPicker1 = new UserControls.NumberPicker();
            this.numberPicker2 = new UserControls.NumberPicker();

            this.progressBar1 = new UserControls.ProgressBar();
            this.progressBar2 = new UserControls.ProgressBar();

            this.inputLabel1 = new UserControls.InputLabel();
            this.inputLabel2 = new UserControls.InputLabel(Enums.InputLabelType.RED, "label text 2");
        }

        protected innerCode() : IGuiElement {
            this.imageButton1.Title().Text("this is test image button");

            this.button1.IconName(Enums.IconType.RED_SQUARE);
            this.button1.Text("test button");

            this.button2.IconName(Enums.IconType.BLACK_SQUARE);
            this.button2.Text("test button 2");

            this.textField1.Hint("type some text here");
            this.textField1.Width(300);

            this.textField2.Value("this is textfield");
            this.textField2.Width(300);

            this.textArea1.Hint("type some text here");
            this.textArea1.Width(300);
            this.textArea1.Height(150);

            this.textArea2.Width(300);
            this.textArea2.Height(100);
            this.textArea2.LengthLimit(500);
            this.textArea2.ResizeableType(ResizeableType.HORIZONTAL_AND_VERTICAL);

            this.checkBox1.Text("check box item");

            this.checkBox2.Text(" ");
            this.checkBox2.Checked(true);
            this.checkBox2.Enabled(false);

            this.radioBox1.Text("radio item 1");
            this.radioBox1.Checked(true);

            this.radioBox2.Text("radio item 2");

            this.dropDownList1.Width(300);
            this.dropDownList1.Clear();
            this.dropDownList1.Add("item 1");
            this.dropDownList1.Add("item 2");
            this.dropDownList1.Add("item 3");
            this.dropDownList1.Add("item 4");
            this.dropDownList1.MaxVisibleItemsCount(2);
            this.dropDownList1.Hint("choose some item");
//            this.dropDownList1.Visible(false);

            this.dropDownList2.Width(300);
            this.dropDownList2.Height(300);
            this.dropDownList2.Clear();
            this.dropDownList2.Add("item 1");
            this.dropDownList2.Add("item 2");
            this.dropDownList2.Select(1);

            this.numberPicker1.Width(300);
            this.numberPicker1.Value(25);

            this.numberPicker2.Width(300);
            this.numberPicker2.Value(50);
            this.numberPicker2.RangeStart(-100);
            this.numberPicker2.RangeEnd(100);
            this.numberPicker2.DecimalPlaces(2);

            this.progressBar1.Width(300);
            this.progressBar1.Value(50);

            this.progressBar2.Width(300);
            this.progressBar2.RangeStart(-100);
            this.progressBar2.RangeEnd(100);
            this.progressBar2.Value(50);

            this.inputLabel1.Width(300);
            this.inputLabel2.Width(200);

            return super.innerCode().Add("<style>.UserControlsTestPanel >[guiType=\"Panel\"] >.Envelop >.Content {width: 600px;}</style>");
        }

        protected innerHtml() : IGuiElement {
            return this.addElement()

                .Add(this.Id())

                .Add(this.label1).Add(this.label2)

                .Add(this.imageButton1).Add(this.imageButton2).Add(StringUtils.NewLine())
                .Add(this.button1).Add(this.button2).Add(StringUtils.NewLine())

                .Add(this.textField1).Add(this.textField2).Add(StringUtils.NewLine())
                .Add(this.textArea1).Add(StringUtils.NewLine()).Add(this.textArea2).Add(StringUtils.NewLine())

                .Add(this.checkBox1).Add(this.checkBox2).Add(StringUtils.NewLine())
                .Add(this.radioBox1).Add(this.radioBox2).Add(StringUtils.NewLine())

                .Add(this.dropDownList1).Add(this.dropDownList2).Add(StringUtils.NewLine())

                .Add(this.numberPicker1).Add(this.numberPicker2).Add(StringUtils.NewLine())
                .Add(this.progressBar1).Add(this.progressBar2).Add(StringUtils.NewLine())

                .Add(this.inputLabel1).Add(this.inputLabel2);
        }
    }
}
/* dev:end */
