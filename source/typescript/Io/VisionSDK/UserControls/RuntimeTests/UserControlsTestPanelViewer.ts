/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Io.VisionSDK.UserControls.RuntimeTests {
    "use strict";
    import BasePanelViewerArgs = Com.Wui.Framework.Gui.Primitives.BasePanelViewerArgs;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;

    export class UserControlsTestPanelViewer extends Com.Wui.Framework.Gui.Primitives.BasePanelViewer {

        constructor($args? : BasePanelViewerArgs) {
            super($args);
            this.setInstance(new UserControlsTestPanel());
        }

        public getInstance() : UserControlsTestPanel {
            return <UserControlsTestPanel>super.getInstance();
        }

        protected normalImplementation() : void {
            const instance : UserControlsTestPanel = this.getInstance();
//            instance.Visible(false);
            instance.Scrollable(true);
            instance.Width(500);
            instance.Height(400);

//            if (!ObjectValidator.IsEmptyOrNull(this.ViewerArgs())) {
//                this.ViewerArgs().AsyncEnabled(false);
//            }
        }

        protected testImplementation() : string {
            const args : BasePanelViewerArgs = new BasePanelViewerArgs();
            args.AsyncEnabled(true);
            this.ViewerArgs(args);
            const instance : UserControlsTestPanel = this.getInstance();
            instance.Scrollable(true);
            instance.Width(500);
            instance.Height(500);
            return "";
        }
    }
}
/* dev:end */
