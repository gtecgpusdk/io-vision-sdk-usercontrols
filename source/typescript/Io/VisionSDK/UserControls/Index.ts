/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls {
    "use strict";
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import StaticPageContentManger = Com.Wui.Framework.Gui.Utils.StaticPageContentManager;
    import Viewers = Io.VisionSDK.UserControls.BaseInterface.Viewers;

    /**
     * Index request resolver class provides handling of web index page.
     */
    export class Index extends Com.Wui.Framework.Commons.HttpProcessor.Resolvers.BaseHttpResolver {

        protected resolver() : void {
            let output : string =
                "<div class=\"GuiInterface\">" +
                "<h1>Vision SDK UserControls Library</h1>" +
                "<h3>WUI Framework library focused on user controls with design for Vision SDK.</h3>" +
                "<div class=\"Index\">";

            output +=
                "<H3>Components</H3>" +
                StringUtils.NewLine() +
                "<a href=\"" + Viewers.Components.ToolTipViewer.CallbackLink(true) + "\">ToolTip</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + Viewers.Components.ScrollBarViewer.CallbackLink(true) + "\">ScrollBar</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + Viewers.Components.SelectBoxViewer.CallbackLink(true) + "\">SelectBox</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + Viewers.Components.KernelNodeConnectorViewer.CallbackLink(true) + "\">KernelNodeConnector</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + Viewers.Components.CropBoxViewer.CallbackLink(true) + "\">CropBox</a>" +

                "<H3>User Controls</H3>" +
                "<a href=\"" + Viewers.UserControls.LabelViewer.CallbackLink(true) + "\">Label</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + Viewers.UserControls.IconViewer.CallbackLink(true) + "\">Icon</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + Viewers.UserControls.ButtonViewer.CallbackLink(true) + "\">Button</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + Viewers.UserControls.ImageButtonViewer.CallbackLink(true) + "\">ImageButton</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + Viewers.UserControls.CheckBoxViewer.CallbackLink(true) + "\">CheckBox</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + Viewers.UserControls.RadioBoxViewer.CallbackLink(true) + "\">RadioBox</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + Viewers.UserControls.TextFieldViewer.CallbackLink(true) + "\">TextField</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + Viewers.UserControls.DirectoryBrowserViewer.CallbackLink(true) + "\">DirectoryBrowser</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + Viewers.UserControls.ImageViewer.CallbackLink(true) + "\">Image</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + Viewers.UserControls.ProgressBarViewer.CallbackLink(true) + "\">ProgressBar</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + Viewers.UserControls.BSPSelectorViewer.CallbackLink(true) + "\">BSP Selector</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + Viewers.UserControls.InstallationItemViewer.CallbackLink(true) + "\">Installation Item</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + Viewers.UserControls.InputLabelViewer.CallbackLink(true) + "\">InputLabel</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + Viewers.UserControls.FormInputViewer.CallbackLink(true) + "\">FormInput</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + Viewers.UserControls.DropDownListViewer.CallbackLink(true) + "\">DropDownList</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + Viewers.UserControls.NumberPickerViewer.CallbackLink(true) + "\">NumberPicker</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + Viewers.UserControls.KernelNodeViewer.CallbackLink(true) + "\">KernelNode</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + Viewers.UserControls.KernelNodeButtonViewer.CallbackLink(true) + "\">KernelNodeButton</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + Viewers.UserControls.DialogViewer.CallbackLink(true) + "\">Dialog</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + Viewers.UserControls.NotificationMessageViewer.CallbackLink(true) + "\">NotificationMessage</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + Viewers.UserControls.InformativeButtonViewer.CallbackLink(true) + "\">InformativeButton</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + Viewers.UserControls.TextAreaViewer.CallbackLink(true) + "\">TextArea</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + Viewers.UserControls.DropDownButtonViewer.CallbackLink(true) + "\">DropDownButton</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + Viewers.UserControls.PointerIconViewer.CallbackLink(true) + "\">PointerIcon</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + Viewers.UserControls.HorizontalPanelHolderViewer.CallbackLink(true) + "\">HorizontalPanelHolder</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + Viewers.UserControls.VerticalPanelHolderViewer.CallbackLink(true) + "\">VerticalPanelHolder</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + Viewers.UserControls.AccordionViewer.CallbackLink(true) + "\">Accordion</a>" +
                StringUtils.NewLine();

            /* dev:start */
            output +=
                "<H3>Runtime tests</H3>" +
                "<a href=\"" + Io.VisionSDK.UserControls.RuntimeTests.UserControlsTestPanelViewer
                    .CallbackLink(true) + "\">UserControlsTestPanel</a>" +
                StringUtils.NewLine();
            /* dev:end */

            output +=
                "</div>" +
                "</div>";

            output +=
                "<div class=\"Note\">" +
                "version: " + this.getEnvironmentArgs().getProjectVersion() +
                ", build: " + this.getEnvironmentArgs().getBuildTime() +
                "</div>" + StringUtils.NewLine(false) +
                "<div class=\"Logo\">" + StringUtils.NewLine(false) +
                "   <div class=\"WUI\"></div>" + StringUtils.NewLine(false) +
                "</div>";

            StaticPageContentManger.Clear();
            StaticPageContentManger.Title("Vision SDK - UserControls Index");
            StaticPageContentManger.BodyAppend(output);
            StaticPageContentManger.Draw();
        }
    }
}
