/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.Events {
    "use strict";
    import IKernelNodeEvents = Io.VisionSDK.UserControls.Interfaces.Events.IKernelNodeEvents;
    import IKernelNodeEventsHandler = Io.VisionSDK.UserControls.Interfaces.Events.IKernelNodeEventsHandler;
    import IMouseEventsHandler = Com.Wui.Framework.Gui.Interfaces.Events.IMouseEventsHandler;
    import KernelNodeEventType = Io.VisionSDK.UserControls.Enums.Events.KernelNodeEventType;
    import IConnectorHolderEvents = Io.VisionSDK.UserControls.Interfaces.Events.IConnectorHolderEvents;
    import IConnectorHolderEventsHandler = Io.VisionSDK.UserControls.Interfaces.Events.IConnectorHolderEventsHandler;
    import ConnectorHolderEventType = Io.VisionSDK.UserControls.Enums.Events.ConnectorHolderEventType;
    import Parent = Com.Wui.Framework.Gui.Events.ElementEventsManager;

    export class ElementEventsManager extends Parent implements IKernelNodeEvents, IConnectorHolderEvents {

        public setBeforeRemove($handler : IKernelNodeEventsHandler) : void {
            this.globalEvents.setEvent(this.owner, KernelNodeEventType.BEFORE_REMOVE, $handler);
        }

        public setOnMove($handler : IKernelNodeEventsHandler) : void {
            this.globalEvents.setEvent(this.owner, KernelNodeEventType.ON_MOVE, $handler);
        }

        public setOnConnectorMouseDown($handler : IKernelNodeEventsHandler) : void {
            this.globalEvents.setEvent(this.owner, KernelNodeEventType.ON_CONNECTOR_MOUSE_DOWN, $handler);
        }

        public setOnConnectorMouseUp($handler : IKernelNodeEventsHandler) : void {
            this.globalEvents.setEvent(this.owner, KernelNodeEventType.ON_CONNECTOR_MOUSE_UP, $handler);
        }

        public setOnConnectorMouseEnter($handler : IKernelNodeEventsHandler) : void {
            this.globalEvents.setEvent(this.owner, KernelNodeEventType.ON_CONNECTOR_MOUSE_ENTER, $handler);
        }

        public setOnConnectorMouseLeave($handler : IKernelNodeEventsHandler) : void {
            this.globalEvents.setEvent(this.owner, KernelNodeEventType.ON_CONNECTOR_MOUSE_LEAVE, $handler);
        }

        public setOnMouseEnter($handler : IMouseEventsHandler) : void {
            this.globalEvents.setEvent(this.owner, KernelNodeEventType.ON_MOUSE_ENTER, $handler);
        }

        public setOnMouseLeave($handler : IMouseEventsHandler) : void {
            this.globalEvents.setEvent(this.owner, KernelNodeEventType.ON_MOUSE_LEAVE, $handler);
        }

        public setOnTooltipChange($handler : IKernelNodeEventsHandler) : void {
            this.globalEvents.setEvent(this.owner, KernelNodeEventType.ON_TOOLTIP_CHANGE, $handler);
        }

        public setOnConnectorUpdate($handler : IConnectorHolderEventsHandler) : void {
            this.globalEvents.setEvent(this.owner, ConnectorHolderEventType.ON_CONNECTOR_UPDATE, $handler);
        }
    }
}
