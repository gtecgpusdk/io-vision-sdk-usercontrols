/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.Events.Args {
    "use strict";
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import KernelNodeInterfaceType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.KernelNodeInterfaceType;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;

    /**
     * KernelNodeEventArgs class provides args connected with KernelNode manipulation.
     */
    export class KernelNodeEventArgs extends EventArgs {
        private index : number;
        private connectorType : KernelNodeInterfaceType;
        private dataType : string;
        private tooltipText : string;
        private status : boolean;

        constructor() {
            super();
        }

        public Index($value? : number) : number {
            return this.index = Property.Integer(this.index, $value);
        }

        public ConnectorType($value? : KernelNodeInterfaceType) : KernelNodeInterfaceType {
            return this.connectorType = ObjectValidator.IsSet($value) ? $value : this.connectorType;
        }

        public DataType($value? : string) : string {
            return this.dataType = Property.String(this.dataType, $value);
        }

        public TooltipText($value? : string) : string {
            return this.tooltipText = Property.String(this.tooltipText, $value);
        }

        public Status($value? : boolean) : boolean {
            return this.status = Property.Boolean(this.status, $value);
        }
    }
}
