/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.Events.Args {
    "use strict";
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import KernelNodeInterfaceType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.KernelNodeInterfaceType;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;

    /**
     * ConnectorHolderEventArgs class provides args connected with ConnectorHolder manipulation.
     */
    export class ConnectorHolderEventArgs extends EventArgs {
        private fromIndex : number;
        private toIndex : number;
        private interfaceType : KernelNodeInterfaceType;

        constructor() {
            super();
        }

        public FromIndex($value? : number) : number {
            return this.fromIndex = Property.Integer(this.fromIndex, $value);
        }

        public ToIndex($value? : number) : number {
            return this.toIndex = Property.Integer(this.toIndex, $value);
        }

        public InterfaceType($value? : KernelNodeInterfaceType) : KernelNodeInterfaceType {
            return this.interfaceType = ObjectValidator.IsSet($value) ? $value : this.interfaceType;
        }
    }
}
