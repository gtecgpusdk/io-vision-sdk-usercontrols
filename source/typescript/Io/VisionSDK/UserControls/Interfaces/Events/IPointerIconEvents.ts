/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.Interfaces.Events {
    "use strict";
    import IMouseEventsHandler = Com.Wui.Framework.Gui.Interfaces.Events.IMouseEventsHandler;

    export interface IPointerIconEvents extends Com.Wui.Framework.Gui.Interfaces.Events.IFormsObjectEvents {
        setOnDrag($handler : IMouseEventsHandler) : void;

        setOnMove($handler : IMouseEventsHandler) : void;
    }
}
