/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.Interfaces.Events {
    "use strict";

    export interface IConnectorHolderEvents extends Com.Wui.Framework.Gui.Interfaces.Events.IBasePanelEvents {
        setOnConnectorUpdate($handler : IConnectorHolderEventsHandler) : void;

        setOnConnectorMouseUp($handler : IKernelNodeEventsHandler) : void;

        setOnConnectorMouseDown($handler : IKernelNodeEventsHandler) : void;
    }
}
