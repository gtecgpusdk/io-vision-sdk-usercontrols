/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.Interfaces.Events {
    "use strict";
    import IMouseEventsHandler = Com.Wui.Framework.Gui.Interfaces.Events.IMouseEventsHandler;

    export interface IKernelNodeEvents extends Com.Wui.Framework.Gui.Interfaces.Events.IBasePanelEvents {
        setOnMove($handler : IKernelNodeEventsHandler) : void;

        setOnConnectorMouseUp($handler : IKernelNodeEventsHandler) : void;

        setOnConnectorMouseDown($handler : IKernelNodeEventsHandler) : void;

        setOnConnectorMouseEnter($handler : IKernelNodeEventsHandler) : void;

        setOnConnectorMouseLeave($handler : IKernelNodeEventsHandler) : void;

        setOnMouseEnter($handler : IMouseEventsHandler) : void;

        setOnMouseLeave($handler : IMouseEventsHandler) : void;

        setOnTooltipChange($handler : IKernelNodeEventsHandler) : void;

        setBeforeRemove($handler : IKernelNodeEventsHandler) : void;
    }
}
