/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.Interfaces {
    "use strict";
    import Size = Com.Wui.Framework.Gui.Structures.Size;
    import ILabel = Com.Wui.Framework.Gui.Interfaces.UserControls.ILabel;

    export abstract class IAreaState {
        public readonly viewSize : Size;
        public readonly areaSize : Size;
        public readonly areaPos : IVector;
        public zoomMult : number;
        public readonly viewMousePos : IVector;
        public readonly viewMouseMove : IVector;
        public readonly viewMouseLastMove : IVector;
        public readonly viewMouseLastAxisChange : IVector;
    }

    export abstract class IAreaElements {
        public readonly envelopId : string;
        public readonly backgroundId : string;
        public readonly expandAreaId : string;
        public readonly collapseAreaId : string;
        public readonly dimensionsIndicator : ILabel;
    }

    export class IAreaResizeConstraints {
        public minX? : number;
        public maxX? : number;
        public minY? : number;
        public maxY? : number;
    }
}
