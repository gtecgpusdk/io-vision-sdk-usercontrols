/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.Interfaces {
    "use strict";
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import KernelNodeInterface = Io.VisionSDK.UserControls.Structures.KernelNodeInterface;

    export abstract class IConnection {
        public id : string;
        public stack : IConnectionStack;
        public input : Input;
        public output : Output;
    }

    export abstract class IErrorInput {
        public input : Input;
        public message : string;
    }

    export abstract class IConnectionStack {
        public id : string;
        public connections : ArrayList<IConnection>;
    }

    export class Output extends KernelNodeInterface {
        public inputs : ArrayList<Input>;
        public innerInput : Input;
        public delegator : Input;

        constructor() {
            super();
            this.inputs = new ArrayList<Input>();
        }
    }

    export class Input extends KernelNodeInterface {
        public output : Output;
        public connection : IConnection;
        public innerOutput : Output;
        public delegator : Output;
    }

    export class IConnectorVisibilityArgs {
        public sourceNode? : IKernelNode;
        public sourceConnector? : number;
        public validator? : IValidator;
        public dataType? : string;
        public scale? : number;
        public delegatedOutput? : Output;
    }

    export class IConnectionValidatorStatus {
        public status : boolean;
        public message : string;
        public sourceNode? : IKernelNode;
        public targetNode? : IKernelNode;
        public sourceIndex? : number;
        public targetIndex? : number;
    }

    export abstract class IInputValidatityInfo {
        public validInputs : ArrayList<Input>;
        public invalidInputs : ArrayList<IErrorInput>;
    }

    export abstract class INodeIntersection {
        public connection : IConnection;
        public node : IKernelNode;
    }

    export type IValidator = ($sourceNode : IKernelNode, $targetNode : IKernelNode, $sourceConnector : number, $targetConnector : number,
                              $correctAttributes? : boolean, $applyChanges? : boolean) => IConnectionValidatorStatus;
}
