/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.Interfaces {
    "use strict";

    export interface IDropDownButton extends Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiCommons {
        Value($args? : IDropDownButtonArgs) : IDropDownButtonArgs;
    }

    export abstract class IDropDownButtonArgs {
        public activeIndex : number;
        public values : IDropDownButtonItem[];
        public type : any;
    }

    export abstract class IDropDownButtonItem {
        public text : string;
        public styleClassName? : string;
        public forceVisible? : boolean;
        public callback : () => void;
    }
}
