/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.Interfaces {
    "use strict";
    import ElementOffset = Com.Wui.Framework.Gui.Structures.ElementOffset;
    import Size = Com.Wui.Framework.Gui.Structures.Size;
    import IVector = Io.VisionSDK.UserControls.Interfaces.IVector;

    export interface IAreaStrategy {

        setAreaStatus($value : string) : void;

        setMouseViewPosition($value : IVector, $clearMove? : boolean) : void;

        getMouseViewPosition() : IVector;

        getMouseAreaPosition() : IVector;

        getMouseAreaMove() : IVector;

        getMouseViewMove() : IVector;

        getMouseLastAreaMove() : IVector;

        getMouseLastViewMove() : IVector;

        getMouseLastViewAxisChange() : IVector;

        getPointViewPosition($point : IVector) : IVector;

        getViewSize() : Size;

        setViewSize($value : Size) : void;

        setAreaConstraints($minSizeX : number, $minSizeY : number) : void;

        IndicateAreaResize($direction : ElementOffset, $anchor : ElementOffset, $constraints? : IAreaResizeConstraints) : void;

        ApplyAreaResize($direction? : ElementOffset, $anchor? : ElementOffset) : void;

        CancelResize() : void;

        UpdateDimensionsIndicator($anchor? : ElementOffset, $size? : Size);

        getAreaResizeDirection() : ElementOffset;

        getResizeAnchor() : ElementOffset;

        getResizeAreaDimensions() : Size;

        setAreaSize($width : number, $height : number) : void;

        getAreaSize() : Size;

        setZoomPercentage($value : number, $isMouseRelative? : boolean) : void;

        getScale() : number;

        getZoomMultiplier() : number;

        ResetAreaPosition() : void;

        getAreaPosition() : IVector;

        setAreaPosition($offset : IVector) : void;

        getPointViewOverflow($point : IVector, $viewPadding? : number) : IVector;

        getMinOverflowPoint($points : IVector[], $viewPadding? : number) : IVector;

        getMaxOverflowPoint($points : IVector[], $viewPadding? : number) : IVector;

        ToggleScroll($vectorFunc? : () => IVector, $onIterationHandler? : () => void, $isMouseRelative? : boolean) : void;

        getPointScrollVector($point? : IVector, $round? : boolean, $maxSpeed? : number, $minSpeed? : number,
                             $viewPadding? : number) : IVector;

        getPointsScrollVector($points? : IVector[], $round? : boolean,
                              $maxSpeed? : number,
                              $minSpeed? : number, $viewPadding? : number) : IVector;

        IsPointsBoundableByView($points? : IVector[], $viewPadding? : number) : boolean;

        IsScrollActive() : boolean;

        IsResizeActive() : boolean;

        IsViewHovered() : boolean;
    }
}
