/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.Interfaces {
    "use strict";

    import KernelNodeInterfaceType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.KernelNodeInterfaceType;

    export interface IKernelNodeConnector extends Com.Wui.Framework.Gui.Interfaces.Primitives.IBaseGuiObject {
        Index($value? : number) : number;

        SuperSampleScale($value? : number) : number;

        GuiType($kernelNodeInterfaceType? : KernelNodeInterfaceType) : KernelNodeInterfaceType;

        Position($value? : IVector) : IVector;
    }
}
