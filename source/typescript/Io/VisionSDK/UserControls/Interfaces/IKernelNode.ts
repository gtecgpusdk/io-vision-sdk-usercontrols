/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
import ParameterRangeType = Io.VisionSDK.UserControls.Enums.ParameterRangeType;

namespace Io.VisionSDK.UserControls.Interfaces {
    "use strict";
    import ParameterRangeType = Io.VisionSDK.UserControls.Enums.ParameterRangeType;
    import ElementOffset = Com.Wui.Framework.Gui.Structures.ElementOffset;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import KernelNodeType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.KernelNodeType;
    import KernelNodeInterfaceType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.KernelNodeInterfaceType;
    import Size = Com.Wui.Framework.Gui.Structures.Size;
    import IKernelNodeEvents = Io.VisionSDK.UserControls.Interfaces.Events.IKernelNodeEvents;

    export interface IKernelNode extends Com.Wui.Framework.Gui.Interfaces.UserControls.IIcon {
        Move($position : IVector) : void;

        ParentNode($value? : IKernelNode) : IKernelNode;

        ChildNodes($value? : ArrayList<IKernelNode>) : ArrayList<IKernelNode>;

        AddInput($dataType : string) : void;

        AddOutput($dataType : string) : void;

        Help($value? : IKernelNodeHelp) : IKernelNodeHelp;

        IconType($type? : KernelNodeType) : KernelNodeType;

        Type($value? : string) : string;

        Selected($value? : boolean) : boolean;

        UniqueIdPrefix($value? : string) : string;

        UniqueId($value? : string) : string;

        NamePrefix($value? : string) : string;

        Name($value? : string) : string;

        getGroupId() : string;

        GroupOffset($value ? : ElementOffset) : ElementOffset;

        Column($value? : number) : number;

        Row($value? : number) : number;

        getOutput($position : number) : Output;

        getInput($position : number) : Input;

        getInputs() : ArrayList<Input>;

        getOutputs() : ArrayList<Output>;

        getInputConnectors() : ArrayList<IKernelNodeConnector>;

        getOutputConnectors() : ArrayList<IKernelNodeConnector>;

        Remove($removeRefs? : boolean) : void;

        TrackConnector($vector : IVector) : void;

        ShowInputConnectors($args? : IConnectorVisibilityArgs) : void;

        ShowOutputConnectors($args? : IConnectorVisibilityArgs) : void;

        HideConnectors() : void;

        IsConnectorsVisible() : boolean;

        ScaleVisibleConnectors($scale : number) : void;

        MoveInterface($fromIndex : number, $toIndex : number, $type : KernelNodeInterfaceType) : void;

        RemoveInterface($index : number, $type : KernelNodeInterfaceType) : void;

        getAttributes() : ArrayList<IKernelNodeAttribute>;

        setAttributeValue($name : string, $value : any) : void;

        setAttribute($value : IKernelNodeAttribute) : void;

        getGridPosition() : IVector;

        getAbsolutePosition() : IVector;

        getAbsoluteConnectorPosition($index : number, $type : KernelNodeInterfaceType) : IVector;

        GridSize($value? : Size) : Size;

        getEvents() : IKernelNodeEvents;

        setConnectorPosition($connector : IKernelNodeConnector, $index : number, $availableNum : number, $angleOffset : number,
                             $angleSpace : number) : void;

        RegisterEvents() : void;
    }

    export class IKernelNodeEnumAttributeItem {
        public text : string;
        public value : string;
    }

    export class IKernelNodeAttribute {
        public name : string;
        public type : string;
        public text : string;
        public value : any;
        public items? : Array<string | IKernelNodeEnumAttributeItem>;
        public format? : IKernelNodeAttributeFormat;
    }

    export class IKernelNodeAttributeFormat {
        public min : number;
        public max : number;
        public type? : ParameterRangeType;
        public step? : number;
        public decimalPlaces? : number;
        public readonly? : boolean;
        public hidden? : boolean;
    }

    export class IKernelNodeHelp {
        public name? : string = "";
        public tooltip? : string = "";
        public description? : string = "";
        public link? : string = "";
        public methodName? : string = "";
        public inputs? : string[] = [];
        public outputs? : string[] = [];
    }

    export class IKernelNodeNotifications {
        /* tslint:disable: variable-name */
        public no_input_connections : string;
        public no_compatible_node_connection_exists : string;
        /* tslint:enable */
    }
}
