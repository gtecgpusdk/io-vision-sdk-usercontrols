/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.Interfaces {
    "use strict";

    export abstract class IGridEnv {
        public readonly getNodePosition : ($row : number, $column : number) => IVector;
        public readonly nodeSize : number;
        public readonly nodeRadius : number;
        public readonly scrollAreaSize : number;
        public readonly selectScrollAreaSize : number;
        public readonly maxScrollSpeed : number;
        public readonly minGridSize : number;
    }

    export abstract class IGridElements {
        public dropAreaId : string;
        public dragHolderId : string;
    }
}
