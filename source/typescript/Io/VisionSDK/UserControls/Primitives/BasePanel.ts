/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.Primitives {
    "use strict";
    import ToolTip = Io.VisionSDK.UserControls.BaseInterface.Components.ToolTip;
    import ScrollBar = Io.VisionSDK.UserControls.BaseInterface.Components.ScrollBar;
    import IconType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.IconType;
    import Icon = Io.VisionSDK.UserControls.BaseInterface.UserControls.Icon;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;

    export class BasePanel extends Com.Wui.Framework.UserControls.Primitives.BasePanel {

        constructor($id? : string) {
            super($id);

            if (ObjectValidator.IsSet(this.loaderIcon)) {
                this.loaderIcon.IconType(IconType.SPINNER_WHITE_BIG);
            }
        }

        protected getTitleClass() : any {
            return ToolTip;
        }

        protected getLoaderIconClass() : any {
            return Icon;
        }

        protected getScrollBarClass() : any {
            return ScrollBar;
        }

        protected cssInterfaceName() : string {
            return BasePanel.ClassName();
        }
    }
}
