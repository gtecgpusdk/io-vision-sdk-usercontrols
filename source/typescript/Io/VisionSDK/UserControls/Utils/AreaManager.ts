/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.Utils {
    "use strict";
    import ElementOffset = Com.Wui.Framework.Gui.Structures.ElementOffset;
    import Size = Com.Wui.Framework.Gui.Structures.Size;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import DirectionType = Com.Wui.Framework.Gui.Enums.DirectionType;
    import EventsManager = Com.Wui.Framework.UserControls.Events.EventsManager;
    import IVector = Io.VisionSDK.UserControls.Interfaces.IVector;
    import IAreaElements = Io.VisionSDK.UserControls.Interfaces.IAreaElements;
    import IAreaState = Io.VisionSDK.UserControls.Interfaces.IAreaState;
    import IAreaResizeConstraints = Io.VisionSDK.UserControls.Interfaces.IAreaResizeConstraints;
    import IAreaStrategy = Io.VisionSDK.UserControls.Interfaces.IAreaStrategy;

    export class AreaManager extends Com.Wui.Framework.Commons.Primitives.BaseObject implements IAreaStrategy {
        private readonly state : IAreaState;
        private readonly elements : IAreaElements;
        private resizeDirection : ElementOffset;
        private resizeAnchor : ElementOffset;
        private resizeIdentity : string;
        private scrollHandle : number;
        private expandHideHandle : number;
        private defaultMinScrollSpeed : number = 0.5;
        private defaultMaxScrollSpeed : number = 15;
        private minSizeX : number = 250;
        private minSizeY : number = 600;
        private lastValidScrollVector : IVector;

        constructor($elements : IAreaElements) {
            super();
            this.elements = $elements;
            this.state = {
                areaPos                : {x: 0, y: 0},
                areaSize               : new Size(),
                viewMouseLastAxisChange: {x: 0, y: 0},
                viewMouseLastMove      : {x: 0, y: 0},
                viewMouseMove          : {x: 0, y: 0},
                viewMousePos           : {x: Number.NEGATIVE_INFINITY, y: Number.NEGATIVE_INFINITY},
                viewSize               : new Size(),
                zoomMult               : 1
            };
            this.state.areaSize.Width(this.minSizeX);
            this.state.areaSize.Height(this.minSizeY);
            this.resizeDirection = null;
            this.resizeAnchor = null;
            this.resizeIdentity = null;
            this.lastValidScrollVector = {x: 0, y: 0};
        }

        public setAreaStatus($value : string) : void {
            ElementManager.setClassName(this.elements.envelopId, $value);
        }

        public setMouseViewPosition($value : IVector, $clearMove : boolean = false) : void {
            if ($clearMove) {
                this.state.viewMouseMove.x = 0;
                this.state.viewMouseMove.y = 0;
            }
            this.state.viewMouseLastMove.x = $value.x - this.state.viewMousePos.x;
            this.state.viewMouseLastMove.y = $value.y - this.state.viewMousePos.y;
            if (this.state.viewMouseLastMove.x !== 0) {
                this.state.viewMouseLastAxisChange.x = this.state.viewMouseLastMove.x;
                this.state.viewMouseMove.x += this.state.viewMouseLastAxisChange.x;
            }
            if (this.state.viewMouseLastMove.y !== 0) {
                this.state.viewMouseLastAxisChange.y = this.state.viewMouseLastMove.y;
                this.state.viewMouseMove.y += this.state.viewMouseLastAxisChange.y;
            }
            this.state.viewMousePos.x = $value.x;
            this.state.viewMousePos.y = $value.y;
        }

        public getMouseViewPosition() : IVector {
            return {x: this.state.viewMousePos.x, y: this.state.viewMousePos.y};
        }

        public getMouseAreaPosition() : IVector {
            return {
                x: (-this.state.areaPos.x + this.state.viewMousePos.x) / this.state.zoomMult,
                y: (-this.state.areaPos.y + this.state.viewMousePos.y) / this.state.zoomMult
            };
        }

        public getMouseViewMove() : IVector {
            return {x: this.state.viewMouseMove.x, y: this.state.viewMouseMove.y};
        }

        public getMouseAreaMove() : IVector {
            return {x: this.state.viewMouseMove.x / this.state.zoomMult, y: this.state.viewMouseMove.y / this.state.zoomMult};
        }

        public getMouseLastViewMove() : IVector {
            return {x: this.state.viewMouseLastMove.x, y: this.state.viewMouseLastMove.y};
        }

        public getMouseLastAreaMove() : IVector {
            return {x: this.state.viewMouseLastMove.x / this.state.zoomMult, y: this.state.viewMouseLastMove.y / this.state.zoomMult};
        }

        public getMouseLastViewAxisChange() : IVector {
            return {x: this.state.viewMouseLastAxisChange.x, y: this.state.viewMouseLastAxisChange.y};
        }

        public getPointViewPosition($point : IVector) : IVector {
            return {
                x: $point.x * this.state.zoomMult + this.state.areaPos.x,
                y: $point.y * this.state.zoomMult + this.state.areaPos.y
            };
        }

        public setViewSize($value : Size) : void {
            this.state.viewSize.Width($value.Width());
            this.state.viewSize.Height($value.Height());
        }

        public getViewSize() : Size {
            const size : Size = new Size();
            size.Width(this.state.viewSize.Width());
            size.Height(this.state.viewSize.Height());
            return size;
        }

        public setAreaConstraints($minSizeX : number, $minSizeY : number) {
            this.minSizeX = $minSizeX;
            this.minSizeY = $minSizeY;
        }

        public IndicateAreaResize($direction : ElementOffset, $anchor : ElementOffset, $constraints? : IAreaResizeConstraints) : void {
            const identity : string = JSON.stringify($direction) + JSON.stringify($anchor);
            if (identity !== this.resizeIdentity) {
                this.resizeIdentity = identity;
                this.resizeDirection = new ElementOffset($direction.Top(), $direction.Left());
                this.resizeAnchor = $anchor;
                this.clearPosition(this.elements.backgroundId);
                this.clearPosition(this.elements.expandAreaId);
                this.clearPosition(this.elements.collapseAreaId);

                const leftResize : number = $direction.Left() * $anchor.Left();
                const topResize : number = $direction.Top() * $anchor.Top();

                const constraints : IAreaResizeConstraints = ObjectValidator.IsSet($constraints) ? $constraints :
                    {
                        maxX: Number.NEGATIVE_INFINITY,
                        maxY: Number.NEGATIVE_INFINITY,
                        minX: Number.POSITIVE_INFINITY,
                        minY: Number.POSITIVE_INFINITY
                    };

                const areaWidth : number = this.state.areaSize.Width();
                const areaHeight : number = this.state.areaSize.Height();
                if ($anchor.Left() > 0) {
                    this.resizeDirection.Left(this.resizeDirection.Left()
                        + Math.max(0, Math.max(this.minSizeX, constraints.maxX) - (areaWidth + leftResize)));
                } else if ($anchor.Left() < 0) {
                    this.resizeDirection.Left(this.resizeDirection.Left()
                        + Math.min(0, Math.min(areaWidth - this.minSizeX, constraints.minX) + leftResize));
                } else {
                    this.resizeDirection.Left(0);
                }

                if ($anchor.Top() > 0) {
                    this.resizeDirection.Top(this.resizeDirection.Top()
                        + Math.max(0, Math.max(this.minSizeY, constraints.maxY) - (areaHeight + topResize)));
                } else if ($anchor.Top() < 0) {
                    this.resizeDirection.Top(this.resizeDirection.Top()
                        + Math.min(0, Math.min(areaHeight - this.minSizeY, constraints.minY) + topResize));
                } else {
                    this.resizeDirection.Top(0);
                }

                const horizontalProperty : string = $anchor.Left() < 0 ? "left" : "right";
                const verticalProperty : string = $anchor.Top() < 0 ? "top" : "bottom";
                const boundedLeftResize : number = this.resizeDirection.Left() * this.resizeAnchor.Left();
                const boundedTopResize : number = this.resizeDirection.Top() * this.resizeAnchor.Top();
                const resize : any = ($areaId : string, $isHorizontal : boolean, $isVertical : boolean) : boolean => {
                    if ($isHorizontal) {
                        ElementManager.setCssProperty($areaId, horizontalProperty, -boundedLeftResize);
                    }
                    if ($isVertical) {
                        ElementManager.setCssProperty($areaId, verticalProperty, -boundedTopResize);
                    }
                    return $isHorizontal || $isVertical;
                };

                clearTimeout(this.expandHideHandle);
                this.expandHideHandle = null;
                if (resize(this.elements.expandAreaId, boundedLeftResize > 0, boundedTopResize > 0)) {
                    ElementManager.Show(this.elements.expandAreaId);
                }
                if (resize(this.elements.backgroundId, boundedLeftResize < 0, boundedTopResize < 0)) {
                    resize(this.elements.expandAreaId, boundedLeftResize < 0, boundedTopResize < 0);
                    ElementManager.Show(this.elements.expandAreaId);
                    ElementManager.Show(this.elements.collapseAreaId);
                }
                ElementManager.StopOpacityChange(this.elements.dimensionsIndicator);
                ElementManager.ChangeOpacity(this.elements.dimensionsIndicator, DirectionType.UP, 5);
            }
        }

        public ApplyAreaResize($direction : ElementOffset = this.resizeDirection, $anchor : ElementOffset = this.resizeAnchor) : void {
            if (!ObjectValidator.IsEmptyOrNull($direction) && ($direction.Left() !== 0 || $direction.Top() !== 0)) {
                const xOffset : number = $direction.Left() * $anchor.Left();
                const yOffset : number = $direction.Top() * $anchor.Top();
                const transformX : number = $anchor.Left() < 0 ? xOffset : 0;
                const transformY : number = $anchor.Top() < 0 ? yOffset : 0;
                this.setAreaPosition({
                    x: this.state.areaPos.x - transformX * this.state.zoomMult,
                    y: this.state.areaPos.y - transformY * this.state.zoomMult
                });
                const areaSize : Size = this.state.areaSize;
                this.setAreaSize(areaSize.Width() + xOffset, areaSize.Height() + yOffset);
            }
            this.clearPosition(this.elements.backgroundId);
            this.clearPosition(this.elements.expandAreaId);
            this.clearPosition(this.elements.collapseAreaId);

            this.expandHideHandle = EventsManager.getInstanceSingleton().FireAsynchronousMethod(() : void => {
                ElementManager.StopOpacityChange(this.elements.dimensionsIndicator);
                ElementManager.ChangeOpacity(this.elements.dimensionsIndicator, DirectionType.DOWN, 50);
            }, true, 1000);
            ElementManager.Hide(this.elements.collapseAreaId);
            this.resizeDirection = null;
            this.resizeAnchor = null;
        }

        public CancelResize() : void {
            this.ApplyAreaResize(new ElementOffset(), new ElementOffset());
        }

        public UpdateDimensionsIndicator($anchor : ElementOffset = null, $size : Size = this.getResizeAreaDimensions()) : void {
            this.elements.dimensionsIndicator.Text($size.Width() + " / " + $size.Height());
            const mouseAreaPos : IVector = this.getMouseAreaPosition();
            const getDistance : any = (fromX : number, fromY : number) : number => {
                return Math.sqrt(Math.pow(fromX - mouseAreaPos.x, 2) + Math.pow(fromY - mouseAreaPos.y, 2));
            };
            const topLeft : number = getDistance(0, 0);
            const topRight : number = getDistance(this.state.areaSize.Width(), 0);
            const bottomRight : number = getDistance(this.state.areaSize.Width(), this.state.areaSize.Height());
            const bottomLeft : number = getDistance(0, this.state.areaSize.Height());
            let className : string = "";
            if (!ObjectValidator.IsEmptyOrNull($anchor)) {
                let anchorLeft : number = $anchor.Left();
                if (anchorLeft === 0) {
                    anchorLeft = topLeft < topRight ? -1 : 1;
                }
                let anchorTop : number = $anchor.Top();
                if (anchorTop === 0) {
                    anchorTop = topLeft < bottomLeft ? -1 : 1;
                }
                if (anchorLeft < 0 && anchorTop < 0) {
                    className = "TopLeft";
                } else if (anchorLeft > 0 && anchorTop < 0) {
                    className = "TopRight";
                } else if (anchorLeft > 0 && anchorTop > 0) {
                    className = "BottomRight";
                } else {
                    className = "BottomLeft";
                }
            } else {
                switch (Math.min(topLeft, topRight, bottomRight, bottomLeft)) {
                case topLeft:
                    className = "TopLeft";
                    break;
                case topRight:
                    className = "TopRight";
                    break;
                case bottomRight:
                    className = "BottomRight";
                    break;
                case bottomLeft:
                    className = "BottomLeft";
                    break;
                }
            }
            ElementManager.setClassName(this.elements.dimensionsIndicator, className);
        }

        public getAreaResizeDirection() : ElementOffset {
            return this.resizeDirection;
        }

        public getResizeAnchor() : ElementOffset {
            return this.resizeAnchor;
        }

        public getResizeAreaDimensions() : Size {
            const dims : Size = this.state.areaSize;
            const returnVal : Size = new Size();
            returnVal.Width(dims.Width());
            returnVal.Height(dims.Height());
            if (!ObjectValidator.IsEmptyOrNull(this.resizeDirection)) {
                returnVal.Width(returnVal.Width() + this.resizeDirection.Left() * this.resizeAnchor.Left());
                returnVal.Height(returnVal.Height() + this.resizeDirection.Top() * this.resizeAnchor.Top());
            }
            return returnVal;
        }

        public setAreaSize($width : number, $height : number) : void {
            this.state.areaSize.Width($width);
            this.state.areaSize.Height($height);
            ElementManager.setSize(this.elements.envelopId, this.state.areaSize.Width(), this.state.areaSize.Height());
        }

        public getAreaSize() : Size {
            const size : Size = new Size();
            size.Width(this.state.areaSize.Width());
            size.Height(this.state.areaSize.Height());
            return size;
        }

        public setZoomPercentage($value : number, $isMouseRelative? : boolean) : void {
            const mouseAreaPos : IVector = this.getMouseAreaPosition();
            const getOffset : any = () : IVector => {
                return $isMouseRelative ?
                    {
                        x: mouseAreaPos.x * this.state.zoomMult,
                        y: mouseAreaPos.y * this.state.zoomMult
                    } :
                    {
                        x: this.state.viewSize.Width() / 2 * this.state.zoomMult,
                        y: this.state.viewSize.Height() / 2 * this.state.zoomMult
                    };
            };
            const beforeGraphOffset : IVector = getOffset();
            this.state.zoomMult = $value / 100;
            const afterGraphOffset : IVector = getOffset();

            this.setAreaPosition({
                x: this.state.areaPos.x + (beforeGraphOffset.x - afterGraphOffset.x),
                y: this.state.areaPos.y + (beforeGraphOffset.y - afterGraphOffset.y)
            });

            ElementManager.setZoom(this.elements.envelopId, $value);
            ElementManager.setZoom(this.elements.dimensionsIndicator.Id() + "_Text", 10000 / $value);
        }

        public getScale() : number {
            return 1 / this.state.zoomMult;
        }

        public getZoomMultiplier() : number {
            return this.state.zoomMult;
        }

        public ResetAreaPosition() : void {
            const offset : IVector = {
                x: (this.state.viewSize.Width() - this.state.areaSize.Width() * this.state.zoomMult) / 2,
                y: (this.state.viewSize.Height() - this.state.areaSize.Height() * this.state.zoomMult) / 2
            };
            if (offset.x + this.state.areaSize.Width() * this.state.zoomMult > this.state.viewSize.Width()
                || offset.y + this.state.areaSize.Height() * this.state.zoomMult > this.state.viewSize.Height()) {
                offset.x = 0;
                offset.y = 0;
            }
            this.setAreaPosition(offset);
        }

        public getAreaPosition() : IVector {
            return {x: this.state.areaPos.x, y: this.state.areaPos.y};
        }

        public setAreaPosition($position : IVector) {
            this.state.areaPos.y = $position.y;
            this.state.areaPos.x = $position.x;
            ElementManager.setCssProperty(this.elements.envelopId, "top", this.state.areaPos.y);
            ElementManager.setCssProperty(this.elements.envelopId, "left", this.state.areaPos.x);
        }

        public getPointViewOverflow($point : IVector, $viewPadding : number = 80) : IVector {
            const viewPosition : IVector = this.getPointViewPosition($point);
            const vector : IVector = {x: 0, y: 0};
            if (viewPosition.x - $viewPadding < 0) {
                vector.x = viewPosition.x - $viewPadding;
            } else if (viewPosition.x + $viewPadding - this.state.viewSize.Width() > 0) {
                vector.x = viewPosition.x - this.state.viewSize.Width() + $viewPadding;
            }
            if (viewPosition.y - $viewPadding < 0) {
                vector.y = viewPosition.y - $viewPadding;
            } else if (viewPosition.y + $viewPadding - this.state.viewSize.Height() > 0) {
                vector.y = viewPosition.y + $viewPadding - this.state.viewSize.Height();
            }
            return vector;
        }

        public getMaxOverflowPoint($points : IVector[], $viewPadding : number = 80) : IVector {
            let point : IVector = null;
            let maxOverflow : number = Number.NEGATIVE_INFINITY;
            $points.forEach(($point : IVector) : void => {
                const overflow : IVector = this.getPointViewOverflow($point, $viewPadding);
                const len : number = Math.sqrt(Math.pow(overflow.x, 2) + Math.pow(overflow.y, 2));
                if (len > maxOverflow) {
                    maxOverflow = len;
                    point = $point;
                }
            });
            return point;
        }

        public getMinOverflowPoint($points : IVector[], $viewPadding : number = 80) : IVector {
            let point : IVector = null;
            let minOverflow : number = Number.POSITIVE_INFINITY;
            $points.forEach(($point : IVector) : void => {
                const overflow : IVector = this.getPointViewOverflow($point, $viewPadding);
                const len : number = Math.sqrt(Math.pow(overflow.x, 2) + Math.pow(overflow.y, 2));
                if (len < minOverflow) {
                    minOverflow = len;
                    point = $point;
                }
            });
            return point;
        }

        public ToggleScroll($vectorFunc : () => IVector = () : IVector => {
            return {x: 0, y: 0};
        },                  $onIterationHandler? : () => void, $isMouseRelative : boolean = false) {
            const vector : IVector = $vectorFunc();
            if (Math.abs(vector.x) > 0.05 || Math.abs(vector.y) > 0.05) {
                const applyScroll : any = ($status : boolean = true) : void => {
                    if ($status) {
                        this.lastValidScrollVector = vector;
                    }
                    clearTimeout(this.scrollHandle);
                    const vectorFunc : any = $status ?
                        $vectorFunc :
                        () : IVector => {
                            this.lastValidScrollVector.x = this.lastValidScrollVector.x * 0.89250;
                            this.lastValidScrollVector.y = this.lastValidScrollVector.y * 0.89250;
                            return this.lastValidScrollVector;
                        };
                    const translatedVector : IVector = vectorFunc();
                    this.setAreaPosition({
                        x: this.state.areaPos.x + translatedVector.x,
                        y: this.state.areaPos.y + translatedVector.y
                    });
                    this.scrollHandle = EventsManager.getInstanceSingleton().FireAsynchronousMethod(() : void => {
                        this.ToggleScroll(vectorFunc, $onIterationHandler, $isMouseRelative);
                    }, 15);
                    if (ObjectValidator.IsSet($onIterationHandler)) {
                        $onIterationHandler();
                    }
                };

                if ($isMouseRelative) {
                    // Math.sign fails to transpile
                    const getSign : any = ($value : number) : number => {
                        if ($value === 0) {
                            return 0;
                        }
                        return Math.abs($value) === $value ? 1 : -1;
                    };
                    const lastAxisChange : IVector = this.getMouseLastViewAxisChange();
                    const xAxisScrollable : boolean = getSign(lastAxisChange.x) === getSign(-vector.x);
                    const yAxisScrollable : boolean = getSign(lastAxisChange.y) === getSign(-vector.y);
                    applyScroll(xAxisScrollable || yAxisScrollable);
                } else {
                    applyScroll();
                }
            } else {
                clearTimeout(this.scrollHandle);
                this.scrollHandle = null;
                this.lastValidScrollVector = {x: 0, y: 0};
                if (ObjectValidator.IsSet($onIterationHandler)) {
                    $onIterationHandler();
                }
            }
        }

        public getPointScrollVector($point : IVector = this.getMouseAreaPosition(), $round : boolean = true,
                                    $maxSpeed : number = this.defaultMaxScrollSpeed,
                                    $minSpeed : number = this.defaultMinScrollSpeed, $viewPadding : number = 80) : IVector {
            const scrollVector : IVector = this.getPointViewOverflow($point, $viewPadding);
            if (scrollVector.x !== 0 || scrollVector.y !== 0) {
                const top : number = scrollVector.y / 22.22975;
                const left : number = scrollVector.x / 22.22975;
                const speed : number = Math.sqrt(top * top + left * left);
                const speedMult : number = $maxSpeed / speed;
                scrollVector.x = -(speed < $minSpeed ? 0 : (speed > $maxSpeed ? left * speedMult : left));
                scrollVector.y = -(speed < $minSpeed ? 0 : (speed > $maxSpeed ? top * speedMult : top));
                if ($round) {
                    scrollVector.y = (scrollVector.y < 0 ? Math.floor : Math.ceil)(scrollVector.y);
                    scrollVector.x = (scrollVector.x < 0 ? Math.floor : Math.ceil)(scrollVector.x);
                }
            }
            return scrollVector;
        }

        public getPointsScrollVector($points : IVector[] = [this.getMouseAreaPosition()], $round : boolean = true,
                                     $maxSpeed : number = this.defaultMaxScrollSpeed,
                                     $minSpeed : number = this.defaultMinScrollSpeed, $viewPadding : number = 80) : IVector {
            let targetPoint : IVector = {x: 0, y: 0};
            if ($points.length !== 0) {
                if (this.IsPointsBoundableByView($points)) {
                    const totalVector : IVector = {x: 0, y: 0};
                    $points.forEach(($point : IVector) : void => {
                        const overflow : IVector = this.getPointViewOverflow($point, $viewPadding);
                        totalVector.x += overflow.x;
                        totalVector.y += overflow.y;
                    });
                    targetPoint.x = totalVector.x > 0 ? Number.NEGATIVE_INFINITY : Number.POSITIVE_INFINITY;
                    targetPoint.y = totalVector.y > 0 ? Number.NEGATIVE_INFINITY : Number.POSITIVE_INFINITY;
                    $points.forEach(($point : IVector) : void => {
                        targetPoint.x = (totalVector.x > 0 ? Math.max : Math.min)($point.x, targetPoint.x);
                        targetPoint.y = (totalVector.y > 0 ? Math.max : Math.min)($point.y, targetPoint.y);
                    });
                } else {
                    targetPoint = this.getMinOverflowPoint($points, $viewPadding);
                }
                return this.getPointScrollVector(targetPoint, $round, $maxSpeed, $minSpeed, $viewPadding);
            } else {
                return targetPoint;
            }
        }

        public IsPointsBoundableByView($points : IVector[] = [this.getMouseAreaPosition()], $viewPadding : number = 80) : boolean {
            let minX : number = Number.POSITIVE_INFINITY;
            let maxX : number = Number.NEGATIVE_INFINITY;
            let minY : number = Number.POSITIVE_INFINITY;
            let maxY : number = Number.NEGATIVE_INFINITY;
            $points.forEach(($point : IVector) : void => {
                if ($point.x < minX) {
                    minX = $point.x;
                }
                if ($point.x > maxX) {
                    maxX = $point.x;
                }
                if ($point.y < minY) {
                    minY = $point.y;
                }
                if ($point.y > maxY) {
                    maxY = $point.y;
                }
            });
            const width : number = (maxX - minX) * this.state.zoomMult;
            const height : number = (maxY - minY) * this.state.zoomMult;
            return this.state.viewSize.Width() - $viewPadding * 2 > width && this.state.viewSize.Height() - $viewPadding * 2 > height;
        }

        public IsScrollActive() : boolean {
            return !ObjectValidator.IsEmptyOrNull(this.scrollHandle);
        }

        public IsResizeActive() : boolean {
            return !ObjectValidator.IsEmptyOrNull(this.resizeDirection);
        }

        public IsViewHovered() : boolean {
            const viewOverflow : IVector = this.getPointViewOverflow(this.getMouseAreaPosition(), 0);
            return viewOverflow.x === 0 && viewOverflow.y === 0;
        }

        private clearPosition($id : string) : void {
            ElementManager.ClearCssProperty($id, "left");
            ElementManager.ClearCssProperty($id, "right");
            ElementManager.ClearCssProperty($id, "top");
            ElementManager.ClearCssProperty($id, "bottom");
        }
    }
}
