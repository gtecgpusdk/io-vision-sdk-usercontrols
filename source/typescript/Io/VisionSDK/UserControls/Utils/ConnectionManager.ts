/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.Utils {
    "use strict";
    import BaseObject = Com.Wui.Framework.Commons.Primitives.BaseObject;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import IConnection = Io.VisionSDK.UserControls.Interfaces.IConnection;
    import IConnectionStack = Io.VisionSDK.UserControls.Interfaces.IConnectionStack;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import ElementEventsManager = Io.VisionSDK.UserControls.Events.ElementEventsManager;
    import MouseEventArgs = Com.Wui.Framework.Gui.Events.Args.MouseEventArgs;
    import EventsManager = Com.Wui.Framework.UserControls.Events.EventsManager;
    import ToolTip = Io.VisionSDK.UserControls.BaseInterface.Components.ToolTip;
    import IErrorInput = Io.VisionSDK.UserControls.Interfaces.IErrorInput;
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import Size = Com.Wui.Framework.Gui.Structures.Size;
    import GuiObjectManager = Com.Wui.Framework.Gui.GuiObjectManager;
    import IValidator = Io.VisionSDK.UserControls.Interfaces.IValidator;
    import IKernelNode = Io.VisionSDK.UserControls.Interfaces.IKernelNode;
    import Output = Io.VisionSDK.UserControls.Interfaces.Output;
    import Input = Io.VisionSDK.UserControls.Interfaces.Input;
    import IConnectionValidatorStatus = Io.VisionSDK.UserControls.Interfaces.IConnectionValidatorStatus;
    import IVector = Io.VisionSDK.UserControls.Interfaces.IVector;
    import IInputValidatityInfo = Io.VisionSDK.UserControls.Interfaces.IInputValidatityInfo;
    import INodeIntersection = Io.VisionSDK.UserControls.Interfaces.INodeIntersection;
    import ConnectionStatus = Io.VisionSDK.UserControls.Enums.ConnectionStatus;
    import IKernelNodeConnector = Io.VisionSDK.UserControls.Interfaces.IKernelNodeConnector;
    import IGuiCommons = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiCommons;
    import IBounds = Io.VisionSDK.UserControls.Interfaces.IBounds;
    import IGridEnv = Io.VisionSDK.UserControls.Interfaces.IGridEnv;
    import KernelNodeInterfaceType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.KernelNodeInterfaceType;

    export class ConnectionManager extends BaseObject {
        private gridManager : GridManager;
        private readonly drawArea : string;
        private readonly connections : ArrayList<IConnection>;
        private readonly connectionStacks : ArrayList<IConnectionStack>;
        private readonly selectedConnections : ArrayList<IConnection>;
        private readonly errorInputs : ArrayList<IErrorInput>;
        private readonly stackTimeoutHandler : any;
        private validator : IValidator;
        private unstackedStack : IConnectionStack;
        private hoveredLine : string;
        private draggedLine : string;
        private isEnabled : boolean;
        private draggedOutput : Output;
        private defaultDragInput : Input;
        private isConnectorTracked : boolean;
        private inputValidity : IInputValidatityInfo;
        private dragValidator : () => boolean;
        private gridEnv : IGridEnv;
        private dragSetupHandler : ($sourceNode : IKernelNode, $index : number) => void;
        private activeLine : string;

        constructor($drawArea : string, $gridManager : GridManager) {
            super();
            this.gridManager = $gridManager;
            this.gridEnv = $gridManager.getEnvironment();
            this.drawArea = $drawArea;
            this.connections = new ArrayList<IConnection>();
            this.connectionStacks = new ArrayList<IConnectionStack>();
            this.selectedConnections = new ArrayList<IConnection>();
            this.errorInputs = new ArrayList<IErrorInput>();
            this.unstackedStack = null;
            this.hoveredLine = null;
            this.draggedLine = null;
            this.draggedOutput = null;
            this.defaultDragInput = null;
            this.isEnabled = true;
            this.isConnectorTracked = false;
            this.inputValidity = null;
            this.activeLine = null;

            this.stackTimeoutHandler = () : void => {
                if (ObjectValidator.IsEmptyOrNull(this.hoveredLine) ||
                    this.unstackedStack !== null && this.unstackedStack.connections.getItem(this.hoveredLine) === null) {
                    this.StackConnections();
                }
            };
            this.dragValidator = () : boolean => {
                return true;
            };
        }

        public getGridManager() : GridManager {
            return this.gridManager;
        }

        public Validator($value? : IValidator) : IValidator {
            if (ObjectValidator.IsSet($value)) {
                this.validator = ($sourceNode : IKernelNode, $targetNode : IKernelNode,
                                  $sourceConnector : number, $targetConnector : number,
                                  $correctAttributes? : boolean, $applyChanges? : boolean) : IConnectionValidatorStatus => {
                    if (this.isLoop($sourceNode, $targetNode)) {
                        return <IConnectionValidatorStatus>{
                            message: "Connection would cause loop.",
                            status : false
                        };
                    } else {
                        return $value($sourceNode, $targetNode, $sourceConnector, $targetConnector, $correctAttributes, $applyChanges);
                    }
                };
            }
            return this.validator;
        }

        public ValidateOutputs($args : IKernelNode | ArrayList<IKernelNode>) : void {
            if (!ObjectValidator.IsEmptyOrNull(this.Validator())) {
                const invalidInputs : ArrayList<IErrorInput> = new ArrayList<IErrorInput>();
                const validInputs : ArrayList<Input> = new ArrayList<Input>();
                let force : boolean = false;
                const validateInputs : any = ($node : IKernelNode) : void => {
                    $node.getOutputs().foreach(($output : Output) : void => {
                        $output.inputs.foreach(($input : Input) : void => {
                            const status : IConnectionValidatorStatus =
                                this.Validator()($output.node, $input.node, $output.position, $input.position, force, force);
                            if (status.status) {
                                validInputs.Add($input);
                            } else {
                                invalidInputs.Add(<IErrorInput>{
                                    input  : $input,
                                    message: "Incompatible input/output type."
                                    + (ObjectValidator.IsEmptyOrNull(status.message) ? "" : " " + status.message)
                                });
                            }
                        });
                    });
                };

                const args : ArrayList<IKernelNode> = ArrayList.ToArrayList($args);
                if (args.Length() === 1) {
                    force = true;
                }
                args.foreach(($node : IKernelNode) : void => {
                    validateInputs($node);
                });
                this.setConnectionValidity({validInputs, invalidInputs});
            }
        }

        public getErrorInputs() : ArrayList<IErrorInput> {
            return this.errorInputs;
        }

        public Resize($size : Size) : void {
            ElementManager.setSize(this.drawArea, $size.Width(), $size.Height());
        }

        public DragValidator($value? : () => boolean) : () => boolean {
            return this.dragValidator = ObjectValidator.IsSet($value) ? $value : this.dragValidator;
        }

        public DragSetupHandler($value? : ($sourceNode : IKernelNode, $index : number) => void)
            : ($sourceNode : IKernelNode, $index : number) => void {
            return this.dragSetupHandler = ObjectValidator.IsSet($value) ? $value : this.dragSetupHandler;
        }

        public DragConnection($sourceNode : IKernelNode, $index : number, $useInnerInterface? : boolean) : void {
            this.setNodeConnectionStatus($sourceNode, false);
            this.Enabled(false);
            this.StackConnections();
            this.draggedOutput = $useInnerInterface ?
                $sourceNode.getInputs().getItem($index).innerOutput :
                $sourceNode.getOutputs().getItem($index);
            this.draggedLine = this.DrawConnection(this.draggedOutput.GridPosition());
            this.PositionDraggedConnection();
            if (!$useInnerInterface) {
                $sourceNode.ScaleVisibleConnectors(this.gridManager.getScale());
            }
        }

        public ApplyDrag($input? : Input) : void {
            const input : Input = ObjectValidator.IsSet($input) ? $input : this.getDragTargetInput();
            if (!ObjectValidator.IsEmptyOrNull(input)) {
                const connection : IConnection = <IConnection>{
                    id    : this.draggedLine,
                    input,
                    output: this.draggedOutput
                };
                this.draggedOutput.inputs.Add(input);
                input.output = this.draggedOutput;
                input.connection = connection;

                if (ObjectValidator.IsFunction(this.Validator())) {
                    this.Validator()(input.output.node, input.node, input.output.position, input.position, true, true);
                }

                this.AddConnection(connection);
                this.setConnectionEndPoint(this.draggedLine, input.GridPosition());
                this.DeselectAll();
                this.SelectConnection(connection);
                if (!ObjectValidator.IsEmptyOrNull(connection.stack)) {
                    this.StackConnections();
                    this.UnstackConnections(connection.stack);
                }
                this.CancelDrag(false);
            } else {
                this.CancelDrag();
            }
        }

        public CancelDrag($removeConnection : boolean = true) : void {
            if ($removeConnection) {
                if (ObjectValidator.IsEmptyOrNull(this.defaultDragInput)) {
                    this.RemoveConnection(this.draggedLine);
                } else {
                    this.ApplyDrag(this.defaultDragInput);
                }
            }
            if (this.isConnectorTracked) {
                this.draggedOutput.node.HideConnectors();
            }
            this.draggedLine = null;
            this.draggedOutput = null;
            this.defaultDragInput = null;
            this.isConnectorTracked = false;
        }

        public IsDragActive() : boolean {
            return !ObjectValidator.IsEmptyOrNull(this.draggedLine);
        }

        public getDefaultDragInput() : Input {
            return this.defaultDragInput;
        }

        public PositionDraggedConnection() : void {
            if (!ObjectValidator.IsEmptyOrNull(this.draggedLine)) {
                const mouseAreaPos : IVector = this.gridManager.getMouseAreaPosition();
                const pos : IVector = {
                    x: mouseAreaPos.x,
                    y: mouseAreaPos.y
                };
                this.setConnectionEndPoint(this.draggedLine, pos);
                if (this.gridManager.Root() !== this.draggedOutput.node) {
                    const sourceNode : IKernelNode = this.draggedOutput.node;
                    const nodePos : IVector = sourceNode.getGridPosition();
                    sourceNode.HideConnectors();
                    sourceNode.TrackConnector({y: pos.y - nodePos.y, x: pos.x - nodePos.x});
                    this.isConnectorTracked = true;
                }
            }
        }

        public getDragSourceOutput() : Output {
            return this.draggedOutput;
        }

        public IsDragValid() : boolean {
            return this.dragValidator();
        }

        public getDragTargetInput() : Input {
            const activeConnectors : ArrayList<IKernelNodeConnector> = new ArrayList<IKernelNodeConnector>();
            GuiObjectManager.getInstanceSingleton().getActive().foreach(($type : IGuiCommons) : void => {
                if ($type.Implements(IKernelNodeConnector)) {
                    activeConnectors.Add(<IKernelNodeConnector>$type);
                }
            });
            if (activeConnectors.Length() === 1) {
                const connector : IKernelNodeConnector = activeConnectors.getFirst();
                if (connector.GuiType() === KernelNodeInterfaceType.INPUT) {
                    const node : IKernelNode = connector.Parent().Implements(IKernelNode) ?
                        <IKernelNode>connector.Parent() : this.gridManager.Root();
                    const index : number = connector.Index();
                    const input : Input = this.gridManager.Root() === node ?
                        node.getOutputs().getItem(index).innerInput : node.getInput(index);
                    if (ObjectValidator.IsEmptyOrNull(input.output)) {
                        return input;
                    }
                }
            }
            return null;
        }

        public Enabled($value? : boolean) : boolean {
            return this.isEnabled = Property.Boolean(this.isEnabled, $value);
        }

        public CreateNodeConnections($node : IKernelNode) {
            $node.getInputs().foreach(($input : Input) : void => {
                this.CreateConnection($input);
            });
            $node.getOutputs().foreach(($output : Output) : void => {
                $output.inputs.foreach(($input : Input) : void => {
                    this.CreateConnection($input);
                });
            });
        }

        public CreateConnection($input : Input) : string {
            if (ObjectValidator.IsEmptyOrNull($input.connection) && !ObjectValidator.IsEmptyOrNull($input.output)) {
                const lineId : string = this.DrawConnection($input.output.GridPosition(), $input.GridPosition());
                const connection : IConnection = <IConnection>{
                    id    : lineId,
                    input : $input,
                    output: $input.output
                };
                $input.connection = connection;

                this.AddConnection(connection);
                return lineId;
            }
        }

        public AddConnection($connection : IConnection) : void {
            let stack : IConnectionStack = null;
            this.connectionStacks.foreach(($stack : IConnectionStack) : boolean => {
                if (this.isConnectionsStacked($connection, $stack.connections.getFirst())) {
                    stack = $stack;
                    return false;
                }
            });

            if (stack === null) {
                this.connections.ToArray().forEach(($con : IConnection) : boolean => {
                    if (this.isConnectionsStacked($connection, $con)) {
                        stack = <IConnectionStack>{
                            connections: new ArrayList<IConnection>(),
                            id         : this.DrawConnection($con.output.GridPosition(), $con.input.GridPosition(), true)
                        };
                        $con.stack = stack;
                        stack.connections.Add($con, $con.id);
                        this.connectionStacks.Add(stack, stack.id);
                        return false;
                    }
                });
            }

            if (stack === null) {
                this.connections.Add($connection, $connection.id);
            } else {
                this.connections.Add($connection, $connection.id);
                stack.connections.Add($connection, $connection.id);
                $connection.stack = stack;

                stack.connections.foreach(($connection : IConnection) : void => {
                    ElementManager.getElement($connection.id).setAttribute("class", "DisabledLine");
                    ElementManager.getElement($connection.id + "_Envelop")
                        .setAttribute("class", "DisabledLineEnvelop");
                });
            }
        }

        public DrawConnection($from : IVector = null, $to : IVector = null, $isStacked : boolean = false,
                              $id? : string) : string {
            if (!ObjectValidator.IsEmptyOrNull($from) || !ObjectValidator.IsEmptyOrNull($to)) {
                const newLine : SVGLineElement = document.createElementNS("http://www.w3.org/2000/svg", "line");
                const id : string = ObjectValidator.IsSet($id) ? $id : "Line" + this.getUID();
                newLine.id = id;
                const newLineEnvelop : SVGLineElement = document.createElementNS("http://www.w3.org/2000/svg",
                    "line");
                newLineEnvelop.id = id + "_Envelop";

                let startPosition : IVector = $from;
                if (ObjectValidator.IsEmptyOrNull(startPosition)) {
                    startPosition = <IVector>$to;
                }
                let endPosition : IVector = $to;
                if (ObjectValidator.IsEmptyOrNull(endPosition)) {
                    endPosition = <IVector>$from;
                }

                if ($isStacked) {
                    newLine.setAttribute("class", "StackedLine");
                } else {
                    newLine.setAttribute("class", "Line");
                }
                newLineEnvelop.setAttribute("class", "LineEnvelop");

                const drawArea : HTMLElement = ElementManager.getElement(this.drawArea);
                drawArea.appendChild(newLine);
                drawArea.appendChild(newLineEnvelop);

                this.setConnectionStartPoint(id, startPosition);
                this.setConnectionEndPoint(id, endPosition);

                const events : ElementEventsManager = new ElementEventsManager(null, id + "_Envelop");

                const getConnection : any = () : IConnection => {
                    let connection : IConnection = this.connections.getItem(id);
                    if (connection === null && this.unstackedStack !== null) {
                        connection = this.unstackedStack.connections.getItem(id);
                    }
                    return connection;
                };

                if ($isStacked) {
                    events.setOnMouseOver(() : void => {
                        EventsManager.getInstanceSingleton().FireAsynchronousMethod(() : void => {
                            this.StackConnections();
                            if (this.isEnabled) {
                                const hoveredStack : IConnectionStack = this.connectionStacks.getItem(id);
                                if (!ObjectValidator.IsEmptyOrNull(hoveredStack)) {
                                    this.UnstackConnections(hoveredStack);
                                    EventsManager.getInstanceSingleton()
                                        .FireAsynchronousMethod(this.stackTimeoutHandler, 1000);
                                }
                            }
                        }, 10);
                    });
                } else {
                    let isDragReinitialized : boolean = false;
                    const selectConnection : any = ($eventArgs : MouseEventArgs) : void => {
                        EventsManager.getInstanceSingleton().FireAsynchronousMethod(() : void => {
                            if ($eventArgs.NativeEventArgs().ctrlKey) {
                                this.SelectConnection(this.getConnection(id),
                                    ObjectValidator.IsEmptyOrNull(this.selectedConnections.getItem(id)));
                            } else {
                                this.SelectConnection(this.getConnection(id), true);
                            }
                            this.activeLine = id;
                            isDragReinitialized = false;
                        }, 10);
                    };
                    events.setOnClick(selectConnection);
                    events.setOnMouseDown(($eventArgs : MouseEventArgs) : void => {
                        if ($eventArgs.NativeEventArgs().which === 1) {
                            isDragReinitialized = true;
                        }
                    });
                    events.setEvent("oncontextmenu", selectConnection);
                    events.setOnMouseOver(() : void => {
                        if (this.isEnabled) {
                            EventsManager.getInstanceSingleton().Clear(this.stackTimeoutHandler);
                            this.hoveredLine = id;
                            if (this.unstackedStack !== null && this.unstackedStack.connections.getItem(this.hoveredLine) === null) {
                                this.StackConnections();
                            }
                            const connection : IConnection = getConnection();
                            let text : string;

                            if (!ObjectValidator.IsEmptyOrNull(connection)) {
                                const isInputDelegated : boolean = !ObjectValidator.IsEmptyOrNull(connection.input.delegator);
                                const isOutputDelegated : boolean = !ObjectValidator.IsEmptyOrNull(connection.output.delegator);

                                text =
                                    (isOutputDelegated ? "InnerOutput " : "Output ") + connection.output.position +
                                    " of " + connection.output.node.Name() + "<br>" +
                                    (isInputDelegated ? "InnerInput " : "Input ") + connection.input.position +
                                    " of " + connection.input.node.Name();
                            }

                            if (ObjectValidator.IsEmptyOrNull(this.errorInputs.getItem(id))) {
                                ElementManager.getElement(id).setAttribute("class", "HoveredLine");
                            } else {
                                text += "<br><br>" + "Error : " + this.errorInputs.getItem(id).message;
                                ElementManager.getElement(id).setAttribute("class", "HoveredErrorLine");
                            }
                            ToolTip.ShowGlobal(text, 200, () : boolean => {
                                return id === this.hoveredLine;
                            });

                            EventsManager.getInstanceSingleton().FireAsynchronousMethod(() : void => {
                                this.showDataFlow(id);
                            }, 1000);
                        }
                    });

                    events.setOnMouseOut(($eventArgs : MouseEventArgs) : void => {
                        if (isDragReinitialized) {
                            const connection : IConnection = getConnection();
                            const output : Output = connection.output;
                            if (!$eventArgs.NativeEventArgs().ctrlKey) {
                                this.defaultDragInput = connection.input;
                                this.RemoveConnection(id);
                            }
                            this.DragSetupHandler()(output.node, output.position);
                        } else {
                            this.setConnectionStatus(id);
                            EventsManager.getInstanceSingleton().FireAsynchronousMethod(this.stackTimeoutHandler, 1000);
                        }
                        this.activeLine = null;
                        this.hoveredLine = null;
                        isDragReinitialized = false;
                    });
                }
                events.Subscribe();
                return newLine.id;
            }
            return null;
        }

        public MoveNodeConnections($node : IKernelNode, $position : IVector) : void {
            const moveConnections : any = ($connections : ArrayList<IConnection>) : void => {
                $connections.foreach(($connection : IConnection) : void => {
                    if ($connection.input.node === $node) {
                        this.setConnectionEndPoint($connection.id, $position);
                    } else if ($connection.output.node === $node) {
                        this.setConnectionStartPoint($connection.id, $position);
                    }
                });
            };

            moveConnections(this.connections);
            this.connectionStacks.foreach(($stack : IConnectionStack) : void => {
                const con : IConnection = $stack.connections.ToArray()[1]; // stacks create on second connection => direction of stack line
                if (con.input.node === $node) {
                    this.setConnectionEndPoint($stack.id, $position);
                } else if (con.output.node === $node) {
                    this.setConnectionStartPoint($stack.id, $position);
                }
            });
        }

        public getConnections() : ArrayList<IConnection> {
            return this.connections;
        }

        public getConnectionStacks() : ArrayList<IConnectionStack> {
            return this.connectionStacks;
        }

        public getConnection($id : string) : IConnection {
            return this.connections.getItem($id);
        }

        public setConnectionStartPoint($id : string, $position : IVector) : void {
            const line : SVGLineElement = <any>ElementManager.getElement($id);
            if (!ObjectValidator.IsEmptyOrNull(line)) {
                line.setAttribute("x1", $position.x.toString());
                line.setAttribute("y1", $position.y.toString());
                const lineEnvelop : SVGLineElement = <any>ElementManager.getElement($id + "_Envelop");
                lineEnvelop.setAttribute("x1", $position.x.toString());
                lineEnvelop.setAttribute("y1", $position.y.toString());
            }
        }

        public setConnectionEndPoint($id : string, $position : IVector) : void {
            const line : SVGLineElement = <any>ElementManager.getElement($id);
            if (!ObjectValidator.IsEmptyOrNull(line)) {
                line.setAttribute("x2", $position.x.toString());
                line.setAttribute("y2", $position.y.toString());
                const lineEnvelop : SVGLineElement = <any>ElementManager.getElement($id + "_Envelop");
                lineEnvelop.setAttribute("x2", $position.x.toString());
                lineEnvelop.setAttribute("y2", $position.y.toString());
            }
        }

        public setConnectionValidity($value : IInputValidatityInfo) : void {
            this.inputValidity = $value;
            this.StackConnections();
            $value.validInputs.foreach(($input : Input) : void => {
                this.errorInputs.RemoveAt(this.errorInputs.IndexOf(this.errorInputs.getItem($input.connection.id)));
                this.setConnectionStatus($input.connection.id);
            });
            $value.invalidInputs.foreach(($errorInput : IErrorInput) : void => {
                this.errorInputs.Add($errorInput, $errorInput.input.connection.id);
                this.setConnectionStatus($errorInput.input.connection.id);
            });
        }

        public getConnectionValidity() : IInputValidatityInfo {
            return this.inputValidity;
        }

        public setNodeConnectionStatus($node : IKernelNode | ArrayList<IKernelNode>, $status : ConnectionStatus = ConnectionStatus.INACTIVE,
                                       $force : boolean = false) {
            ArrayList.ToArrayList($node).foreach(($node : IKernelNode) : void => {
                $node.getInputs().foreach(($input : Input) : void => {
                    if (!ObjectValidator.IsEmptyOrNull($input.connection)) {
                        this.setConnectionStatus($input.connection.id, $status, $force);
                    }
                });
                $node.getOutputs().foreach(($output : Output) : void => {
                    $output.inputs.foreach(($input : Input) : void => {
                        if (!ObjectValidator.IsEmptyOrNull($input.connection)) {
                            this.setConnectionStatus($input.connection.id, $status, $force);
                        }
                    });
                });
            });
        }

        public setConnectionStatus($id : string, $status : ConnectionStatus = ConnectionStatus.INACTIVE, $force : boolean = false) : void {
            let stack : IConnectionStack = this.connectionStacks.getItem($id);
            let connection : IConnection = this.connections.getItem($id);
            if (!ObjectValidator.IsEmptyOrNull(stack) || !ObjectValidator.IsEmptyOrNull(connection)) {
                if (ObjectValidator.IsEmptyOrNull(stack) && !ObjectValidator.IsEmptyOrNull(connection.stack)) {
                    if (connection.stack !== this.unstackedStack) {
                        stack = connection.stack;
                        connection = null;
                    }
                }

                let statusClass : string;
                let element : HTMLElement = null;
                if (!ObjectValidator.IsEmptyOrNull(stack)) {
                    element = ElementManager.getElement(stack.id);
                    let isStackErr : boolean = false;
                    if ($status === ConnectionStatus.ERROR) {
                        isStackErr = true;
                    } else {
                        stack.connections.foreach(($connection : IConnection) : boolean => {
                            if (!ObjectValidator.IsEmptyOrNull(this.errorInputs.getItem($connection.id))) {
                                isStackErr = true;
                                return false;
                            }
                        });
                    }
                    if (!isStackErr) {
                        statusClass = $status === ConnectionStatus.ACTIVE || (this.isStackSelected(stack) && !$force) ?
                            "ActiveStackedLine" : "StackedLine";
                    } else {
                        statusClass = $status === ConnectionStatus.ACTIVE || (this.isStackSelected(stack) && !$force) ?
                            "ActiveErrorStackedLine" : "ErrorStackedLine";
                    }
                } else {
                    element = ElementManager.getElement(connection.id);
                    if (!ObjectValidator.IsEmptyOrNull(element)) {
                        if (ObjectValidator.IsEmptyOrNull(this.errorInputs.getItem($id)) && $status !== ConnectionStatus.ERROR) {
                            statusClass = $status === ConnectionStatus.ACTIVE ||
                            (!ObjectValidator.IsEmptyOrNull(this.selectedConnections.getItem($id)) && !$force) ?
                                "ActiveLine" : "Line";
                        } else {
                            statusClass = $status === ConnectionStatus.ACTIVE ||
                            (!ObjectValidator.IsEmptyOrNull(this.selectedConnections.getItem($id)) && !$force) ?
                                "ActiveErrorLine" : "ErrorLine";
                        }
                    }
                }
                element.setAttribute("class", statusClass);
            }
        }

        public RemoveSelected() : void {
            this.selectedConnections.ToArray().forEach(($connection : IConnection) : void => {
                this.RemoveConnection($connection.id);
            });
        }

        public RemoveAll($unlinkIO : boolean = true) : void {
            this.connections.ToArray().forEach(($connection : IConnection) : void => {
                this.RemoveConnection($connection.id, $unlinkIO);
            });
        }

        public RemoveNodeConnections($node : IKernelNode, $unlinkIO : boolean = true) : void {
            $node.getOutputs().ToArray().forEach(($output : Output) : void => {
                $output.inputs.ToArray().forEach(($input : Input) : void => {
                    if (!ObjectValidator.IsEmptyOrNull($input.connection)) {
                        this.RemoveConnection($input.connection.id, $unlinkIO);
                    }
                });
            });

            $node.getInputs().ToArray().forEach(($input : Input) : void => {
                if (!ObjectValidator.IsEmptyOrNull($input.connection)) {
                    this.RemoveConnection($input.connection.id, $unlinkIO);
                }
            });
        }

        public RemoveConnection($id : string, $unlinkIO : boolean = true) : void {
            let line : SVGLineElement = <any>ElementManager.getElement($id);
            if (!ObjectValidator.IsEmptyOrNull(line)) {
                line.remove();
                line = <any>ElementManager.getElement($id + "_Envelop");
                line.remove();
                ElementManager.CleanElementCache($id);
                ElementManager.CleanElementCache($id + "_Envelop");

                const connectionToRemove : IConnection = this.connections.getItem($id);
                if (!ObjectValidator.IsEmptyOrNull(connectionToRemove)) {
                    const connectionStack : IConnectionStack = connectionToRemove.stack;
                    if (!ObjectValidator.IsEmptyOrNull(connectionStack)) {
                        connectionStack.connections.RemoveAt(connectionStack.connections.IndexOf(connectionToRemove));
                        this.StackConnections();
                        this.UnstackConnections(connectionStack);
                        if (connectionStack.connections.Length() === 1) {
                            connectionStack.connections.getFirst().stack = null;
                            connectionStack.connections.Clear();
                            this.RemoveConnection(connectionStack.id);
                        }
                    }
                    this.connections.RemoveAt(this.connections.IndexOf(connectionToRemove));

                    if (!ObjectValidator.IsEmptyOrNull(connectionToRemove)) {
                        connectionToRemove.input.connection = null;
                        if (connectionToRemove.id === this.hoveredLine) {
                            this.hoveredLine = null;
                        }
                        this.selectedConnections.RemoveAt(this.selectedConnections.IndexOf(connectionToRemove));
                        this.errorInputs.RemoveAt(this.errorInputs.IndexOf(this.errorInputs.getItem(connectionToRemove.id)));

                        if ($unlinkIO) {
                            if (!ObjectValidator.IsEmptyOrNull(connectionToRemove.input.delegator)) {
                                connectionToRemove.input.delegator.innerInput.output = null;
                            } else if (!ObjectValidator.IsEmptyOrNull(connectionToRemove.output.delegator)) {
                                const innerOutputInputs : ArrayList<Input> = connectionToRemove.output.delegator.innerOutput.inputs;
                                innerOutputInputs.RemoveAt(innerOutputInputs.IndexOf(connectionToRemove.input));
                            }
                            const inputs : ArrayList<Input> = connectionToRemove.output.inputs;
                            inputs.RemoveAt(inputs.IndexOf(connectionToRemove.input));
                            connectionToRemove.input.output = null;
                        }
                    }
                } else {
                    this.connectionStacks.RemoveAt(this.connectionStacks.IndexOf(this.connectionStacks.getItem($id)));
                }
            }
        }

        public SelectByViewBounds($bounds : IBounds) : void {
            const zoomMult : number = this.gridManager.getZoomMultiplier();
            const areaBounds : IBounds = {
                maxX: $bounds.maxX / zoomMult,
                maxY: $bounds.maxY / zoomMult,
                minX: $bounds.minX / zoomMult,
                minY: $bounds.minY / zoomMult
            };
            const isLineIntersected : any = ($l1p1 : IVector, $l1p2 : IVector,
                                             $l2p1 : IVector, $l2p2 : IVector) : boolean => {
                const det : number = ($l1p2.x - $l1p1.x) * ($l2p2.y - $l2p1.y) -
                    ($l2p2.x - $l2p1.x) * ($l1p2.y - $l1p1.y);
                if (det === 0) {
                    return false;
                } else {
                    const lambda : number = (($l2p2.y - $l2p1.y) * ($l2p2.x - $l1p1.x)
                        + ($l2p1.x - $l2p2.x) * ($l2p2.y - $l1p1.y)) / det;
                    const gamma : number = (($l1p1.y - $l1p2.y) * ($l2p2.x - $l1p1.x)
                        + ($l1p2.x - $l1p1.x) * ($l2p2.y - $l1p1.y)) / det;
                    return lambda > 0 && lambda < 1 && gamma > 0 && gamma < 1;
                }
            };

            const handleConnections : any = ($connections : ArrayList<IConnection>) : void => {
                $connections.foreach(($connection : IConnection) : void => {
                    const p1 : IVector = $connection.input.GridPosition();
                    const p2 : IVector = $connection.output.GridPosition();

                    const toleranceOffset : number = 1;
                    const isPointInside : any = ($pointCoords : IVector) : boolean => {
                        return $pointCoords.y > areaBounds.minY - toleranceOffset
                            && $pointCoords.y < areaBounds.maxY + toleranceOffset
                            && $pointCoords.x > areaBounds.minX - toleranceOffset
                            && $pointCoords.x < areaBounds.maxX + toleranceOffset;
                    };

                    if (isPointInside(p1) || isPointInside(p2) ||
                        isLineIntersected(p1, p2, {y: areaBounds.minY, x: areaBounds.minX},
                            {y: areaBounds.maxY, x: areaBounds.minX}) ||
                        isLineIntersected(p1, p2, {y: areaBounds.minY, x: areaBounds.minX},
                            {y: areaBounds.minY, x: areaBounds.maxX}) ||
                        isLineIntersected(p1, p2, {y: areaBounds.minY, x: areaBounds.maxX},
                            {y: areaBounds.maxY, x: areaBounds.maxX}) ||
                        isLineIntersected(p1, p2, {y: areaBounds.maxY, x: areaBounds.maxX},
                            {y: areaBounds.maxY, x: areaBounds.minX})) {
                        this.SelectConnection($connection, true);
                    } else {
                        this.SelectConnection($connection, false);
                    }
                });
            };
            handleConnections(this.connections);
        }

        public getNodesIntersections($nodes : ArrayList<IKernelNode>) : ArrayList<INodeIntersection> {
            const intersections : ArrayList<INodeIntersection> = new ArrayList<INodeIntersection>();
            this.connections.foreach(($connection : IConnection) : void => {
                $nodes.foreach(($node : IKernelNode) : void => {
                    if (this.IsNodeIntersected($node, $connection)) {
                        intersections.Add({connection: $connection, node: $node});
                    }
                });
            });
            return intersections;
        }

        public getDragIntersections($nodes : ArrayList<IKernelNode>, $dragTarget : IKernelNode) : ArrayList<INodeIntersection> {
            const returnVal : ArrayList<INodeIntersection> = new ArrayList<INodeIntersection>();
            const connection : IConnection = {
                id    : this.draggedLine,
                input : null,
                output: this.draggedOutput,
                stack : null
            };
            connection.input = new Input();
            connection.input.GridPosition = () : IVector => {
                if (ObjectValidator.IsEmptyOrNull($dragTarget)) {
                    return this.gridManager.getMouseAreaPosition();
                } else {
                    const mouseAreaPos : IVector = this.gridManager.getMouseAreaPosition();
                    return this.gridEnv.getNodePosition(
                        Math.floor(mouseAreaPos.y / this.gridEnv.nodeSize),
                        Math.floor(mouseAreaPos.x / this.gridEnv.nodeSize));
                }
            };

            $nodes.foreach(($node : IKernelNode) : void => {
                if ($dragTarget !== $node) {
                    if (this.IsNodeIntersected($node, connection)) {
                        returnVal.Add({connection: null, node: $node});
                    }
                }
            });
            return returnVal;
        }

        public IsNodeIntersected($node : IKernelNode, $connection : IConnection) : boolean {
            if ($connection.output.node !== $node && $connection.input.node !== $node) {
                const absPos : IVector = $node.getAbsolutePosition();
                const point : IVector = this.gridEnv.getNodePosition(
                    Math.floor(absPos.y / this.gridEnv.nodeSize),
                    Math.floor(absPos.x / this.gridEnv.nodeSize));
                const dist : number = this.getDistanceToConnection(point, $connection);
                if (dist < this.gridEnv.nodeRadius) {
                    return true;
                }
            }
            return false;
        }

        public SelectConnections($connections : ArrayList<IConnection>, $status : boolean = true) : void {
            $connections.ToArray().forEach(($connection : IConnection) : void => {
                if ($status) {
                    this.SelectConnection($connection, true);
                } else {
                    this.SelectConnection($connection, false);
                }
            });
        }

        public SelectNodeConnections($node : IKernelNode, $status : boolean = true) : void {
            $node.getOutputs().foreach(($output : Output) : void => {
                $output.inputs.foreach(($input : Input) : void => {
                    this.SelectConnection($input.connection, $status);
                });
            });
            $node.getInputs().foreach(($input : Input) : void => {
                this.SelectConnection($input.connection, $status);
            });
        }

        public SelectConnection($connection : IConnection, $status : boolean = true) : void {
            if (!ObjectValidator.IsEmptyOrNull($connection)) {
                if ($status) {
                    this.selectedConnections.Add($connection, $connection.id);
                } else if (this.selectedConnections.Contains($connection)) {
                    this.selectedConnections.RemoveAt(this.selectedConnections.IndexOf($connection));
                }
                this.setConnectionStatus($connection.id);
            }
        }

        public SelectAll() : void {
            this.SelectConnections(this.connections, true);
        }

        public DeselectAll() : void {
            this.SelectConnections(this.selectedConnections, false);
        }

        public getSelectedConnections() : ArrayList<IConnection> {
            return this.selectedConnections;
        }

        public setDragStatus($value : boolean) : void {
            if ($value) {
                ElementManager.getElement(this.draggedLine).setAttribute("class", "Line");
            } else {
                ElementManager.getElement(this.draggedLine).setAttribute("class", "ErrorLine");
            }
        }

        public getHoveredLine() : string {
            return this.hoveredLine;
        }

        public UnstackConnections($stack : IConnectionStack) : void {
            if (this.unstackedStack === null) {
                this.unstackedStack = $stack;
                const connection : IConnection = $stack.connections.getFirst();
                const startPos : IVector = connection.output.GridPosition();
                const endPos : IVector = connection.input.GridPosition();
                const vector : IVector
                    = {y: endPos.y - startPos.y, x: endPos.x - startPos.x};
                const magnitude : number = Math.sqrt(Math.pow(vector.x, 2) + Math.pow(vector.y, 2));
                const rotatedUnitX : number = -vector.y / magnitude;
                const rotatedUnitY : number = vector.x / magnitude;
                const lineOffset : number = 10;
                let initOffset : number = -(($stack.connections.Length() - 1) * lineOffset) / 2;

                ElementManager.getElement($stack.id + "_Envelop")
                    .setAttribute("class", "DisabledLineEnvelop");
                ElementManager.getElement($stack.id)
                    .setAttribute("class", "DisabledLine");

                $stack.connections.foreach(($connection : IConnection) : void => {
                    this.setConnectionStartPoint($connection.id, {
                        x: startPos.x + (initOffset * rotatedUnitX),
                        y: startPos.y + (initOffset * rotatedUnitY)
                    });
                    this.setConnectionEndPoint($connection.id, {
                        x: endPos.x + (initOffset * rotatedUnitX),
                        y: endPos.y + (initOffset * rotatedUnitY)
                    });
                    ElementManager.getElement($connection.id + "_Envelop").setAttribute("class", "LineEnvelop");
                    this.setConnectionStatus($connection.id);
                    initOffset += lineOffset;
                });

                EventsManager.getInstanceSingleton().FireAsynchronousMethod(this.stackTimeoutHandler, 1000);
            }
        }

        public StackConnections() : void {
            if (this.unstackedStack !== null) {
                const stackElement : HTMLElement = ElementManager.getElement(this.unstackedStack.id + "_Envelop");
                if (!ObjectValidator.IsEmptyOrNull(stackElement)) {
                    this.resetConnectionsPosition(this.unstackedStack.connections);
                    stackElement.setAttribute("class", "LineEnvelop");
                    this.setConnectionStatus(this.unstackedStack.id);
                    this.hideConnections(this.unstackedStack.connections);
                }
                this.unstackedStack = null;
            }
        }

        private isConnectionsStacked($connection1 : IConnection, $connection2 : IConnection) {
            if (!(ObjectValidator.IsEmptyOrNull($connection1.output.delegator) &&
                ObjectValidator.IsEmptyOrNull($connection2.output.delegator) &&
                ObjectValidator.IsEmptyOrNull($connection1.input.delegator) &&
                ObjectValidator.IsEmptyOrNull($connection2.input.delegator))) {
                return $connection1.output === $connection2.output && $connection1.input.node === $connection2.input.node
                    && ObjectValidator.IsEmptyOrNull($connection1.input.delegator);
            }
            return ($connection1.output.node === $connection2.output.node && $connection1.input.node === $connection2.input.node
                || $connection1.input.node === $connection2.output.node && $connection1.output.node === $connection2.input.node);
        }

        private isStackSelected($stack : IConnectionStack) : boolean {
            let returnVal : boolean = false;
            $stack.connections.foreach(($connection : IConnection) : boolean => {
                if (!ObjectValidator.IsEmptyOrNull(this.selectedConnections.getItem($connection.id))) {
                    returnVal = true;
                    return false;
                }
            });
            return returnVal;
        }

        private resetConnectionsPosition($connections : ArrayList<IConnection>) : void {
            $connections.foreach(($connection : IConnection) : void => {
                const startPos : IVector = $connection.output.GridPosition();
                const endPos : IVector = $connection.input.GridPosition();
                this.setConnectionStartPoint($connection.id, startPos);
                this.setConnectionEndPoint($connection.id, endPos);
            });
        }

        private hideConnections($connections : ArrayList<IConnection>) : void {
            $connections.foreach(($connection : IConnection) : void => {
                ElementManager.getElement($connection.id + "_Envelop")
                    .setAttribute("class", "DisabledLineEnvelop");
                ElementManager.getElement($connection.id)
                    .setAttribute("class", "DisabledLine");
            });
        }

        private isLoop($startNode : IKernelNode, $outputNode : IKernelNode) : boolean {
            let isLoop : boolean = false;
            $outputNode.getOutputs().foreach(($output : Output) : boolean => {
                $output.inputs.foreach(($input : Input) : boolean => {
                    if ($input.node === $startNode) {
                        isLoop = true;
                    } else {
                        if (this.isLoop($startNode, $input.node)) {
                            isLoop = true;
                        }
                    }
                    return !isLoop;
                });
                return !isLoop;
            });
            return isLoop;
        }

        private getDistanceToConnection($point : IVector, $connection : IConnection) : number {
            const nodeAbs1 : IVector = $connection.output.node.getAbsolutePosition();
            const nodeAbs2 : IVector = ObjectValidator.IsEmptyOrNull($connection.input.node) ?
                $connection.input.GridPosition() : $connection.input.node.getAbsolutePosition();
            const line1 : IVector = this.gridEnv.getNodePosition(
                Math.floor(nodeAbs1.y / this.gridEnv.nodeSize),
                Math.floor(nodeAbs1.x / this.gridEnv.nodeSize));
            const line2 : IVector = ObjectValidator.IsEmptyOrNull($connection.input.node) ? nodeAbs2 :
                this.gridEnv.getNodePosition(
                    Math.floor(nodeAbs2.y / this.gridEnv.nodeSize),
                    Math.floor(nodeAbs2.x / this.gridEnv.nodeSize));
            let dist : number = this.getSqueredDistance(line1, line2);
            if (dist === 0) {
                dist = this.getSqueredDistance($point, line1);
            }
            const lengthMult : number = Math.max(0, Math.min(1, (
                ($point.x - line1.x) * (line2.x - line1.x) +
                ($point.y - line1.y) * (line2.y - line1.y)) / dist));
            return Math.sqrt(this.getSqueredDistance($point, {
                x: line1.x + lengthMult * (line2.x - line1.x),
                y: line1.y + lengthMult * (line2.y - line1.y)
            }));
        }

        private getSqueredDistance($p1 : IVector, $p2 : IVector) : number {
            return Math.pow($p1.x - $p2.x, 2) + Math.pow($p1.y - $p2.y, 2);
        }

        private showDataFlow($lineId : string, $dashLength : number = 12, $dashSpace : number = 0, $dashOffset : number = 0) : void {
            if (!ObjectValidator.IsEmptyOrNull(this.connections.getItem($lineId))) {
                if (this.hoveredLine === $lineId && ObjectValidator.IsEmptyOrNull(this.activeLine)) {
                    ElementManager.getElement($lineId)
                        .setAttribute("stroke-dasharray", $dashLength + "," + $dashSpace);
                    ElementManager.getElement($lineId)
                        .setAttribute("stroke-dashoffset", $dashOffset.toString());
                    EventsManager.getInstanceSingleton().FireAsynchronousMethod(() : void => {
                        const length = Math.max($dashLength - 0.25, 9);
                        const space = Math.min($dashSpace + 0.25, 3);
                        this.showDataFlow($lineId,
                            length, space, space === 3 ? $dashOffset - .25 : 0);
                    }, 15);
                } else {
                    ElementManager.getElement($lineId)
                        .setAttribute("stroke-dasharray", "");
                    ElementManager.getElement($lineId)
                        .setAttribute("stroke-dashoffset", "");
                }
            }
        }
    }
}
