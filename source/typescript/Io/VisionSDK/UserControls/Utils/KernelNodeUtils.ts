/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.Utils {
    "use strict";
    import BaseObject = Com.Wui.Framework.Commons.Primitives.BaseObject;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import KernelNodeType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.KernelNodeType;
    import ElementOffset = Com.Wui.Framework.Gui.Structures.ElementOffset;
    import Size = Com.Wui.Framework.Gui.Structures.Size;
    import KernelNode = Io.VisionSDK.UserControls.BaseInterface.UserControls.KernelNode;
    import IKernelNode = Io.VisionSDK.UserControls.Interfaces.IKernelNode;
    import Input = Io.VisionSDK.UserControls.Interfaces.Input;
    import Output = Io.VisionSDK.UserControls.Interfaces.Output;
    import IKernelNodeAttribute = Io.VisionSDK.UserControls.Interfaces.IKernelNodeAttribute;
    import DragNodeType = Io.VisionSDK.UserControls.Enums.DragNodeType;
    import InterfaceDataType = Io.VisionSDK.UserControls.Enums.InterfaceDataType;
    import IVector = Io.VisionSDK.UserControls.Interfaces.IVector;

    export class KernelNodeUtils extends BaseObject {
        private readonly gridManager : GridManager;
        private readonly connectionManager : ConnectionManager;
        private readonly registry : KernelNodeRegistry;

        public constructor($gridManager : GridManager, $connectionManager : ConnectionManager, $registry : KernelNodeRegistry) {
            super();
            this.gridManager = $gridManager;
            this.connectionManager = $connectionManager;
            this.registry = $registry;
        }

        public Copy($nodes : ArrayList<IKernelNode> | IKernelNode, $copySubnodes : boolean = true, $updateIdentity : boolean = true,
                    $assignIdentity : boolean = true) : ArrayList<IKernelNode> {
            const handlers : any[] = [];
            const copiedNodes : ArrayList<IKernelNode> = new ArrayList<IKernelNode>();

            const getNode : any = ($node : IKernelNode) : IKernelNode => {
                let node : IKernelNode = copiedNodes.getItem($node.Id());
                if (ObjectValidator.IsEmptyOrNull(node)) {
                    node = this.registry.getNodeByUniqueId($node.UniqueId());
                }
                return node;
            };

            const copyNode : any = ($node : IKernelNode) : IKernelNode => {
                const nodeCopy : IKernelNode = new KernelNode($node.IconType(), $node.Row(), $node.Column());
                nodeCopy.Type($node.Type());
                nodeCopy.UniqueIdPrefix($node.UniqueIdPrefix());
                nodeCopy.NamePrefix($updateIdentity ? (<any>$node.Name()).replace(/([0-9]*)$/, "") : $node.NamePrefix());
                nodeCopy.Name($updateIdentity ? "" : $node.Name());
                nodeCopy.UniqueId($updateIdentity ? "" : $node.UniqueId());
                nodeCopy.Help($node.Help());
                copiedNodes.Add(nodeCopy, $node.Id());

                $node.getAttributes().foreach(($attribute : IKernelNodeAttribute) : void => {
                    const attrCopy : IKernelNodeAttribute = JSON.parse(JSON.stringify($attribute));
                    if ($updateIdentity) {
                        if (ObjectValidator.IsString(attrCopy.value)) {
                            attrCopy.value = StringUtils.Replace(attrCopy.value, $node.Name(), "{nodeName}");
                        } else if (ObjectValidator.IsArray(attrCopy.value)) {
                            attrCopy.value = JSON.stringify(attrCopy.value);
                        }
                    }
                    nodeCopy.setAttribute(attrCopy);
                });
                $node.getInputs().foreach(($input : Input) : void => {
                    nodeCopy.AddInput($input.dataType);
                });
                $node.getOutputs().foreach(($output : Output) : void => {
                    nodeCopy.AddOutput($output.dataType);
                });

                if (!ObjectValidator.IsEmptyOrNull($node.ParentNode())) {
                    nodeCopy.ParentNode(getNode($node.ParentNode()));
                }
                nodeCopy.ChildNodes($copySubnodes ? this.Copy($node.ChildNodes(), $copySubnodes, $updateIdentity) : $node.ChildNodes());
                nodeCopy.ChildNodes().foreach(($child : IKernelNode) : void => {
                    $child.ParentNode(nodeCopy);
                });

                nodeCopy.GridSize().Width($node.GridSize().Width());
                nodeCopy.GridSize().Height($node.GridSize().Height());
                nodeCopy.GroupOffset($node.GroupOffset());

                handlers.push(() : void => {
                    $node.getOutputs().foreach(($output : Output) : void => {
                        const output : Output = nodeCopy.getOutput($output.position);
                        if (!ObjectValidator.IsEmptyOrNull($output.innerInput)) {
                            output.innerInput = new Input();
                            output.innerInput.position = $output.innerInput.position;
                            output.innerInput.node = nodeCopy;
                            output.innerInput.delegator = output;
                            output.innerInput.dataType = $output.innerInput.dataType;

                            const sourceOutput : Output = $output.innerInput.output;
                            if (!ObjectValidator.IsEmptyOrNull(sourceOutput)) {
                                const innerNode : IKernelNode = getNode(sourceOutput.node);
                                if (innerNode !== nodeCopy) {
                                    output.innerInput.output = innerNode.getOutput(sourceOutput.position);
                                }
                            }
                        }

                        $output.inputs.foreach(($input : Input) : void => {
                            const inputNode : IKernelNode = copiedNodes.getItem($input.node.Id());
                            if (!ObjectValidator.IsEmptyOrNull(inputNode) && inputNode !== $output.node.ParentNode()) {
                                const input : Input = inputNode.getInput($input.position);
                                output.dataType = $output.dataType;
                                input.dataType = $input.dataType;
                                input.output = output;
                                output.inputs.Add(input);
                            }
                        });
                    });

                    $node.getInputs().foreach(($input : Input) : void => {
                        const inputNode : IKernelNode = getNode($input.node);
                        if (!ObjectValidator.IsEmptyOrNull($input.innerOutput)) {
                            const input : Input = inputNode.getInput($input.position);
                            input.innerOutput = new Output();
                            input.innerOutput.position = $input.innerOutput.position;
                            input.innerOutput.node = nodeCopy;
                            input.innerOutput.delegator = input;
                            input.innerOutput.dataType = $input.innerOutput.dataType;

                            $input.innerOutput.inputs.foreach(($input : Input) : void => {
                                const innerNode = getNode($input.node);
                                const innerNodeInput : Input = innerNode === nodeCopy ?
                                    innerNode.getOutput($input.position).innerInput : innerNode.getInput($input.position);
                                innerNodeInput.output = input.innerOutput;
                                input.innerOutput.inputs.Add(innerNodeInput);
                            });
                        }
                    });

                    const parent : IKernelNode = $node.ParentNode();
                    if (!ObjectValidator.IsEmptyOrNull(parent) && parent.Type() === KernelNodeType.VXGROUP) {
                        $node.getInputs().foreach(($input : Input) : void => {
                            if (!ObjectValidator.IsEmptyOrNull($input.output) && !ObjectValidator.IsEmptyOrNull($input.output.delegator)) {
                                const inputNode : IKernelNode = getNode($input.node);
                                const input : Input = inputNode.getInput($input.position);
                                input.output = parent.getInput($input.output.position).innerOutput;
                                input.output.inputs.RemoveAt(input.output.inputs.IndexOf($input));
                                input.output.inputs.Add(input);
                            }
                        });

                        $node.getOutputs().foreach(($output : Output) : void => {
                            $output.inputs.foreach(($input : Input) : void => {
                                if (!ObjectValidator.IsEmptyOrNull($input.delegator)) {
                                    const outputNode : IKernelNode = getNode($output.node);
                                    const output : Output = outputNode.getOutput($output.position);
                                    const innerInput : Input = parent.getOutput($input.position).innerInput;
                                    output.inputs.RemoveAt(output.inputs.IndexOf($input));
                                    output.inputs.Add(innerInput);
                                    innerInput.output = output;
                                }
                            });
                        });
                    }
                });
                return nodeCopy;
            };

            const nodes : ArrayList<IKernelNode> = ArrayList.ToArrayList($nodes);
            nodes.foreach(($node : IKernelNode) : void => {
                copyNode($node);
            });
            handlers.forEach(($handler : any) : void => {
                $handler();
            });
            if ($assignIdentity) {
                this.registry.AssignIdentity(copiedNodes);
            }
            return copiedNodes;
        }

        public GroupNodes($callback? : ($group : IKernelNode) => void) : void {
            const root : IKernelNode = this.gridManager.Root();
            const nodes : ArrayList<IKernelNode> = this.gridManager.getSelectedNodes();
            let anchor : IKernelNode = this.gridManager.DragAnchorNode();

            const isGroupValid : any = () : boolean => {
                let status : boolean = true;
                if (ObjectValidator.IsEmptyOrNull(nodes) ||
                    root.Type() !== KernelNodeType.VXGRAPH) {
                    status = false;
                } else {
                    this.gridManager.getSelectedNodes().foreach(($node : IKernelNode) : boolean => {
                        if ($node.Type() === KernelNodeType.VXGROUP) {
                            status = false;
                            return false;
                        }
                    });
                }
                return status;
            };

            const defaultGroupSize : Size = new Size();
            defaultGroupSize.Width(9);
            defaultGroupSize.Height(9);
            const axisPadding : number = 4;

            if (isGroupValid()) {
                let topCorrection : number = null;
                let leftCorrection : number = null;

                const centerGroup : any = ($groupNode : IKernelNode) : void => {
                    const nodes : ArrayList<IKernelNode> = $groupNode.ChildNodes();

                    let left : number = Number.POSITIVE_INFINITY;
                    let right : number = Number.NEGATIVE_INFINITY;
                    let top : number = Number.POSITIVE_INFINITY;
                    let bottom : number = Number.NEGATIVE_INFINITY;
                    nodes.foreach(($node : IKernelNode) : void => {
                        if ($node.Column() < left) {
                            left = $node.Column();
                        }
                        if ($node.Column() > right) {
                            right = $node.Column();
                        }
                        if ($node.Row() < top) {
                            top = $node.Row();
                        }
                        if ($node.Row() > bottom) {
                            bottom = $node.Row();
                        }
                    });
                    const width : number = right - left + 1;
                    const height : number = bottom - top + 1;

                    defaultGroupSize.Width(Math.max(width + axisPadding, defaultGroupSize.Width()));
                    const maxInnerWidth : number = defaultGroupSize.Width() - axisPadding;
                    const relativeLeftCorrection : number = axisPadding + maxInnerWidth - width;
                    leftCorrection = Math.ceil(relativeLeftCorrection / 2) - left;
                    defaultGroupSize.Width(defaultGroupSize.Width() + relativeLeftCorrection % 2);

                    defaultGroupSize.Height(Math.max(height + axisPadding, defaultGroupSize.Height()));
                    const maxInnerHeight : number = defaultGroupSize.Height() - axisPadding;
                    const relativeTopCorrection : number = axisPadding + maxInnerHeight - height;
                    topCorrection = Math.ceil(relativeTopCorrection / 2) - top;
                    defaultGroupSize.Height(defaultGroupSize.Height() + relativeTopCorrection % 2);

                    nodes.foreach(($node : IKernelNode) : void => {
                        $node.Column($node.Column() + leftCorrection);
                        $node.Row($node.Row() + topCorrection);
                    });

                    $groupNode.Row(top);
                    $groupNode.Column(left);
                };

                const groupNode : IKernelNode = new KernelNode(KernelNodeType.VXGROUP, -1, -1);
                groupNode.UniqueIdPrefix("vxGroup");
                groupNode.NamePrefix("vxGroup");
                groupNode.Type(KernelNodeType.VXGROUP);
                groupNode.GroupOffset(new ElementOffset(topCorrection, leftCorrection));
                groupNode.ParentNode(root);

                const copiedNodes : ArrayList<IKernelNode> = this.Copy(nodes, false, false);
                copiedNodes.foreach(($node : IKernelNode) : void => {
                    groupNode.ChildNodes().Add($node);
                    $node.ParentNode(groupNode);
                });

                centerGroup(groupNode);
                groupNode.GridSize(defaultGroupSize);

                const handlers : any[] = [];
                nodes.foreach(($node : IKernelNode) : void => {
                    $node.getInputs().foreach(($input : Input) : void => {
                        if (!ObjectValidator.IsEmptyOrNull($input.output) && !nodes.Contains($input.output.node)) {
                            const prevOutput : Output = $input.output;
                            handlers.push(() : void => {
                                anchor.AddInput(InterfaceDataType.DELEGATED);
                                const input : Input = anchor.getInputs().getLast();
                                const innerNode : IKernelNode = this.registry.getNodeByUniqueId($input.node.UniqueId());
                                const innerNodeInput : Input = innerNode.getInput($input.position);
                                const outputNode : IKernelNode = this.registry.getNodeByUniqueId(prevOutput.node.UniqueId());
                                const output : Output = outputNode.getOutput(prevOutput.position);

                                output.inputs.Add(input);
                                input.output = output;

                                input.innerOutput = new Output();
                                input.innerOutput.position = input.position;
                                input.innerOutput.node = anchor;
                                input.innerOutput.inputs.Add(innerNodeInput);
                                input.innerOutput.delegator = input;
                                input.innerOutput.dataType = InterfaceDataType.DELEGATED;
                                innerNodeInput.output = input.innerOutput;

                                this.connectionManager.CreateConnection(input);
                            });
                        }
                    });

                    $node.getOutputs().foreach(($output : Output) : void => {
                        $output.inputs.foreach(($input : Input) : void => {
                            if (!nodes.Contains($input.node)) {
                                handlers.push(() : void => {
                                    anchor.AddOutput(InterfaceDataType.DELEGATED);
                                    const output : Output = anchor.getOutputs().getLast();
                                    const innerNode : IKernelNode = this.registry.getNodeByUniqueId($output.node.UniqueId());
                                    const innerNodeOutput : Output = innerNode.getOutput($output.position);
                                    const inputNode : IKernelNode = this.registry.getNodeByUniqueId($input.node.UniqueId());
                                    const input : Input = inputNode.getInput($input.position);

                                    output.inputs.Add(input);
                                    input.output = output;

                                    output.innerInput = new Input();
                                    output.innerInput.position = output.position;
                                    output.innerInput.node = anchor;
                                    output.innerInput.output = innerNodeOutput;
                                    output.innerInput.delegator = output;
                                    output.innerInput.dataType = InterfaceDataType.DELEGATED;
                                    innerNodeOutput.inputs.Add(output.innerInput);

                                    this.connectionManager.CreateConnection(input);
                                });
                            }
                        });
                    });
                });

                this.gridManager.RemoveSelected();
                this.connectionManager.RemoveSelected();

                this.registry.AssignIdentity(groupNode);
                this.gridManager.DragNodes(groupNode, groupNode, DragNodeType.RETAIN_ON_CANCEL_RELATIVE, () : void => {
                    anchor = groupNode;
                    this.gridManager.SelectNode(anchor);
                    handlers.forEach(($handler : any) : void => {
                        $handler();
                    });
                    const mousePos : IVector = this.gridManager.getMouseAreaPosition();
                    this.connectionManager.MoveNodeConnections(anchor, {y: mousePos.y, x: mousePos.x});
                    if (ObjectValidator.IsSet($callback)) {
                        $callback(anchor);
                    }
                });
            }
        }

        public UnGroupNodes($callback? : ($group : IKernelNode) => void) : void {
            let anchor : IKernelNode = this.gridManager.DragAnchorNode();

            const groupNode : IKernelNode = this.gridManager.getSelectedNode();
            if (!ObjectValidator.IsEmptyOrNull(groupNode) && groupNode.Type() === KernelNodeType.VXGROUP) {
                const copiedNodes : ArrayList<IKernelNode> = this.Copy(groupNode.ChildNodes(), false, false);
                copiedNodes.foreach(($node : IKernelNode) : void => {
                    $node.Column($node.Column() - $node.GroupOffset().Left());
                    $node.Row($node.Row() - $node.GroupOffset().Top());
                });

                const handlers : any = [];
                groupNode.getInputs().foreach(($input : Input) : void => {
                    $input.innerOutput.inputs.foreach(($innerNodeInput : Input) : void => {
                        const prevOutput : Output = $input.output;
                        handlers.push(() : void => {
                            const inputNode : IKernelNode = this.registry.getNodeByUniqueId($innerNodeInput.node.UniqueId());
                            const outputNode : IKernelNode = this.registry.getNodeByUniqueId(prevOutput.node.UniqueId());
                            const input : Input = inputNode.getInput($innerNodeInput.position);
                            const output : Output = outputNode.getOutput(prevOutput.position);
                            input.output = output;
                            output.inputs.Add(input);
                            this.connectionManager.CreateConnection(input);
                        });
                    });
                });

                groupNode.getOutputs().foreach(($output : Output) : void => {
                    const innerNodeOutput : Output = $output.innerInput.output;
                    if (!ObjectValidator.IsEmptyOrNull(innerNodeOutput)) {
                        $output.inputs.foreach(($input : Input) : void => {
                            handlers.push(() : void => {
                                const inputNode : IKernelNode = this.registry.getNodeByUniqueId($input.node.UniqueId());
                                const outputNode : IKernelNode = this.registry.getNodeByUniqueId(innerNodeOutput.node.UniqueId());
                                const input : Input = inputNode.getInput($input.position);
                                const output : Output = outputNode.getOutput(innerNodeOutput.position);
                                input.output = output;
                                output.inputs.ToArray().forEach(($input : Input) : void => {
                                    if ($input.node.Type() === KernelNodeType.VXGROUP) {
                                        output.inputs.RemoveAt(output.inputs.IndexOf($input));
                                    }
                                });
                                output.inputs.Add(input);
                                this.connectionManager.CreateConnection(input);
                            });
                        });
                    }
                });

                this.gridManager.RemoveSelected();
                this.connectionManager.RemoveSelected();

                this.registry.AssignIdentity(copiedNodes);
                this.gridManager.DragNodes(copiedNodes, copiedNodes.getFirst(), DragNodeType.RELATIVE, () : void => {
                    anchor = copiedNodes.getFirst();

                    this.gridManager.SelectNode(anchor);
                    handlers.forEach(($handler : any) : void => {
                        $handler();
                    });
                    const mousePos : IVector = this.gridManager.getMouseAreaPosition();
                    this.connectionManager.MoveNodeConnections(anchor, {y: mousePos.y, x: mousePos.x});
                    if (ObjectValidator.IsSet($callback)) {
                        $callback(anchor);
                    }
                });
            }
        }
    }
}
