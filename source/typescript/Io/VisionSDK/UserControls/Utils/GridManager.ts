/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.Utils {
    "use strict";
    import ElementOffset = Com.Wui.Framework.Gui.Structures.ElementOffset;
    import Size = Com.Wui.Framework.Gui.Structures.Size;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import GeneralCssNames = Com.Wui.Framework.Gui.Enums.GeneralCssNames;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import ValueProgressEventArgs = Com.Wui.Framework.Gui.Events.Args.ValueProgressEventArgs;
    import ProgressType = Com.Wui.Framework.Gui.Enums.ProgressType;
    import DirectionType = Com.Wui.Framework.Gui.Enums.DirectionType;
    import ValueProgressManager = Com.Wui.Framework.Gui.Utils.ValueProgressManager;
    import EventsManager = Com.Wui.Framework.UserControls.Events.EventsManager;
    import KernelNodeType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.KernelNodeType;
    import IKernelNode = Io.VisionSDK.UserControls.Interfaces.IKernelNode;
    import IGuiCommons = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiCommons;
    import DragNodeType = Io.VisionSDK.UserControls.Enums.DragNodeType;
    import BasePanel = Io.VisionSDK.UserControls.Primitives.BasePanel;
    import IGridElements = Io.VisionSDK.UserControls.Interfaces.IGridElements;
    import TimeoutManager = Com.Wui.Framework.Commons.Events.TimeoutManager;
    import IVector = Io.VisionSDK.UserControls.Interfaces.IVector;
    import IGridEnv = Io.VisionSDK.UserControls.Interfaces.IGridEnv;
    import Reflection = Com.Wui.Framework.Commons.Utils.Reflection;
    import IBounds = Io.VisionSDK.UserControls.Interfaces.IBounds;
    import IAreaStrategy = Io.VisionSDK.UserControls.Interfaces.IAreaStrategy;

    export class GridManager extends Com.Wui.Framework.Commons.Primitives.BaseObject implements IAreaStrategy {
        private static readonly dragOffset : number = -99999;
        public areaManager : IAreaStrategy;
        private readonly parent : BasePanel;
        private readonly elements : IGridElements;
        private readonly env : IGridEnv;
        private root : IKernelNode;
        private readonly nodes : ArrayList<IKernelNode>;
        private readonly selectedNodes : ArrayList<IKernelNode>;
        private readonly highlightedNodes : ArrayList<IKernelNode>;
        private nodesMatrix : string[][];
        private isGridVisible : boolean;
        private dragAnchorNode : IKernelNode;
        private isDragSnapped : boolean;
        private isDragLoading : boolean;
        private onDragLoadHandler : any;
        private dragValidator : () => boolean;
        private resizeIdentity : string;

        constructor($parent : BasePanel, $elements : IGridElements, $gridEnv : IGridEnv, $areaManager : IAreaStrategy) {
            super();
            this.parent = $parent;
            this.elements = $elements;
            this.env = $gridEnv;
            this.areaManager = $areaManager;
            this.areaManager.setAreaConstraints(
                this.env.minGridSize * this.env.nodeSize,
                this.env.minGridSize * this.env.nodeSize);
            this.nodes = new ArrayList<IKernelNode>();
            this.selectedNodes = new ArrayList<IKernelNode>();
            this.highlightedNodes = new ArrayList<IKernelNode>();
            this.nodesMatrix = [];
            this.isGridVisible = false;
            this.dragAnchorNode = null;
            this.isDragSnapped = false;
            this.isDragLoading = false;
            this.onDragLoadHandler = null;
            this.resizeIdentity = null;

            this.dragValidator = () : boolean => {
                return false;
            };
        }

        public getEnvironment() : IGridEnv {
            return this.env;
        }

        public Root($value? : IKernelNode) : IKernelNode {
            return this.root = ObjectValidator.IsSet($value) ? $value : this.root;
        }

        public getNodes() : ArrayList<IKernelNode> {
            return this.nodes;
        }

        public IndicateAreaResize($direction : ElementOffset, $anchor : ElementOffset) : void {
            const identity : string = JSON.stringify($direction) + JSON.stringify($anchor);
            if (identity !== this.resizeIdentity) {
                this.resizeIdentity = identity;
                let minX : number = Number.POSITIVE_INFINITY;
                let maxX : number = Number.NEGATIVE_INFINITY;
                let minY : number = Number.POSITIVE_INFINITY;
                let maxY : number = Number.NEGATIVE_INFINITY;
                this.nodes.foreach(($node : IKernelNode) : void => {
                    if ($node.Column() >= 0 && $node.Row() >= 0) {
                        if ($node.Column() < minX) {
                            minX = $node.Column();
                        }
                        if ($node.Column() > maxX) {
                            maxX = $node.Column();
                        }
                        if ($node.Row() < minY) {
                            minY = $node.Row();
                        }
                        if ($node.Row() > maxY) {
                            maxY = $node.Row();
                        }
                    }
                });
                const rowResize : number = $direction.Top() > 0 ? Math.ceil($direction.Top() / this.env.nodeSize - 0.5) :
                    Math.floor($direction.Top() / this.env.nodeSize + 0.5);
                const colResize : number = $direction.Left() > 0 ? Math.ceil($direction.Left() / this.env.nodeSize - 0.5) :
                    Math.floor($direction.Left() / this.env.nodeSize + 0.5);
                const directionPx : ElementOffset
                    = new ElementOffset(rowResize * this.env.nodeSize, colResize * this.env.nodeSize);
                /* tslint:disable: object-literal-sort-keys */
                this.areaManager.IndicateAreaResize(directionPx, $anchor,
                    {
                        minX: minX * this.env.nodeSize,
                        maxX: (maxX + 1) * this.env.nodeSize,
                        minY: minY * this.env.nodeSize,
                        maxY: (maxY + 1) * this.env.nodeSize
                    });
                /* tslint:enable */
                this.GridVisible(true);
                this.UpdateDimensionsIndicator();
            }
        }

        public ApplyAreaResize($direction? : ElementOffset, $anchor? : ElementOffset) : void {
            if (ObjectValidator.IsEmptyOrNull($direction)) {
                const resizeDir : ElementOffset = this.areaManager.getAreaResizeDirection();
                if (ObjectValidator.IsEmptyOrNull(resizeDir)) {
                    $direction = null;
                } else {
                    $direction = new ElementOffset(resizeDir.Top(), resizeDir.Left());
                }
            }
            if (!ObjectValidator.IsEmptyOrNull($direction)) {
                if (ObjectValidator.IsEmptyOrNull($anchor)) {
                    $anchor = this.areaManager.getResizeAnchor();
                }
                const dims : Size = this.GridDims();
                const yOffset : number = $direction.Left() * $anchor.Left();
                const xOffset : number = $direction.Top() * $anchor.Top();
                const rowOffset : number = yOffset / this.env.nodeSize;
                const colOffset : number = xOffset / this.env.nodeSize;
                dims.Width(dims.Width() + rowOffset);
                dims.Height(dims.Height() + colOffset);
                this.areaManager.ApplyAreaResize($direction, $anchor);
                const transformX : number = $anchor.Left() < 0 ? rowOffset : 0;
                const transformY : number = $anchor.Top() < 0 ? colOffset : 0;
                if (transformX !== 0 || transformY !== 0) {
                    this.nodes.foreach(($node : IKernelNode) : void => {
                        this.ApplyMove($node, new ElementOffset($node.Row() + transformY, $node.Column() + transformX));
                    });
                }
                this.GridVisible(false);
            }
        }

        public CancelResize() : void {
            this.ApplyAreaResize(new ElementOffset(), new ElementOffset());
            this.GridVisible(false);
        }

        public UpdateDimensionsIndicator($anchor? : ElementOffset, $size? : Size) : void {
            if (ObjectValidator.IsEmptyOrNull($size)) {
                const areaDims : Size = this.getResizeAreaDimensions();
                $size = new Size();
                $size.Height(areaDims.Height() / this.env.nodeSize);
                $size.Width(areaDims.Width() / this.env.nodeSize);
            }
            this.areaManager.UpdateDimensionsIndicator($anchor, $size);
        }

        public getAreaResizeDirection() : ElementOffset {
            return this.areaManager.getAreaResizeDirection();
        }

        public IndicateGridDragResize() : void {
            const overflow : ElementOffset = this.getDragGridOverflow();
            const anchor : ElementOffset = new ElementOffset(overflow.Top() < 0 ? -1 : 1, overflow.Left() < 0 ? -1 : 1);
            this.IndicateAreaResize(overflow, anchor);
        }

        public getGridResizeDirection() : ElementOffset {
            const resizePx : ElementOffset = this.areaManager.getAreaResizeDirection();
            return new ElementOffset(resizePx.Top() / this.env.nodeSize,
                resizePx.Left() / this.env.nodeSize);
        }

        public getResizeAnchor() : ElementOffset {
            return this.areaManager.getResizeAnchor();
        }

        public getResizeAreaDimensions() : Size {
            return this.areaManager.getResizeAreaDimensions();
        }

        public DragValidator($value? : () => boolean) : () => boolean {
            return this.dragValidator = ObjectValidator.IsSet($value) ? $value : this.dragValidator;
        }

        public DragNodes($nodes : IKernelNode | ArrayList<IKernelNode>, $anchorType : IKernelNode | KernelNodeType, $type : DragNodeType,
                         $callback? : () => void) : ArrayList<IKernelNode> {
            const nodes : ArrayList<IKernelNode> = ArrayList.ToArrayList($nodes);
            const createNodes : boolean = !(nodes.IsEmpty() || nodes.getFirst().IsLoaded());
            const autoAnchor : boolean = !ObjectValidator.IsEmptyOrNull($anchorType) &&
                !Reflection.getInstance().Implements(<IKernelNode>$anchorType, IKernelNode);
            const anchor : any = <any>$anchorType;

            if ($type !== DragNodeType.PASTE) {
                nodes.foreach(($node : IKernelNode) : void => {
                    if (autoAnchor && $node.Type() === anchor ||
                        !autoAnchor && $node.Column() === anchor.Column() && $node.Row() === anchor.Row()) {
                        this.dragAnchorNode = $node;
                    }
                });
            }

            if (createNodes) {
                const nodePerRow : number = Math.ceil(Math.sqrt(nodes.Length()));
                this.SelectNodes(this.selectedNodes, false);
                this.SelectNodes(nodes, true);
                let index : number = 0;
                let loadedNum : number = 0;

                const asyncLoader : TimeoutManager = new TimeoutManager();
                this.isDragLoading = true;
                nodes.foreach(($node : IKernelNode) : void => {
                    if (!($type === DragNodeType.PASTE || $type === DragNodeType.RETAIN_ON_CANCEL_RELATIVE)) {
                        if ($type === DragNodeType.RELATIVE) {
                            $node.Row(GridManager.dragOffset + $node.Row());
                            $node.Column(GridManager.dragOffset + $node.Column());
                        } else {
                            $node.Row(GridManager.dragOffset + Math.floor(index / nodePerRow));
                            $node.Column(GridManager.dragOffset + index % nodePerRow);
                        }
                    }

                    $node.Visible(true);
                    $node.getEvents().setOnLoad(() : void => {
                        loadedNum++;
                        if (loadedNum === nodes.Length()) {
                            this.isDragLoading = false;
                            if (this.IsDragActive() || $type === DragNodeType.PASTE) {
                                nodes.foreach(($node : IKernelNode) : void => {
                                    const sourceNode : Node = ElementManager.getElement($node.Id() + "_GuiWrapper").parentNode;
                                    const targetDiv : Node = ElementManager.getElement(this.elements.dropAreaId);
                                    const temp : Node = document.createElement("div");
                                    targetDiv.appendChild(temp);
                                    temp.parentNode.insertBefore(sourceNode, temp);
                                    temp.parentNode.removeChild(temp);
                                    if ($type === DragNodeType.PASTE || $type === DragNodeType.RETAIN_ON_CANCEL_RELATIVE) {
                                        const dims : Size = this.GridDims();
                                        if ($node.Column() + 1 > dims.Width()) {
                                            dims.Width($node.Column() + 1);
                                        }
                                        if ($node.Row() + 1 > dims.Height()) {
                                            dims.Height($node.Row() + 1);
                                        }
                                        this.MoveNode($node, this.env.getNodePosition($node.Row(), $node.Column()));
                                    } else {
                                        this.MoveNode($node, {x: GridManager.dragOffset, y: GridManager.dragOffset});
                                    }
                                    if ($type !== DragNodeType.PASTE) {
                                        this.SelectNode($node, true);
                                    }
                                    this.RegisterNode($node);
                                });
                                if ($type !== DragNodeType.PASTE) {
                                    this.GridVisible(true);
                                    this.PositionDraggedNodes();
                                }

                                if (ObjectValidator.IsFunction(this.onDragLoadHandler)) {
                                    this.onDragLoadHandler();
                                }
                                if (ObjectValidator.IsFunction($callback)) {
                                    $callback();
                                }
                            } else {
                                nodes.foreach(($node : IKernelNode) : void => {
                                    this.RemoveNode($node);
                                });
                            }
                        }
                    });
                    $node.ParentNode(this.root);
                    this.root.ChildNodes().Add($node);
                    asyncLoader.Add(() : void => {
                        this.parent.AddChild($node);
                    });
                    index++;
                });
                asyncLoader.Execute();
            } else {
                this.PositionDraggedNodes();
                if ($type !== DragNodeType.PASTE) {
                    this.GridVisible(true);
                }
                if (ObjectValidator.IsFunction($callback)) {
                    $callback();
                }
            }
            return nodes;
        }

        public setDragStatus($value : string) : void {
            ElementManager.setClassName(this.elements.dragHolderId, $value);
        }

        public PositionDraggedNodes($moveOffset : ElementOffset = new ElementOffset()) : void {
            const mouseAreaPos : IVector = this.areaManager.getMouseAreaPosition();
            if (this.isDragLoading) {
                const dragPos : IVector = this.env.getNodePosition(this.dragAnchorNode.Row(), this.dragAnchorNode.Column());
                const posOffset : IVector = {
                    x: mouseAreaPos.x - dragPos.x,
                    y: mouseAreaPos.y - dragPos.y
                };
                this.positionGroupIndicator(posOffset);
            } else {
                if (this.selectedNodes.IsEmpty() || !this.selectedNodes.Contains(this.dragAnchorNode)) {
                    this.SelectNodes(this.selectedNodes, false);
                    this.SelectNode(this.dragAnchorNode);
                }

                const snapNode : () => boolean = () : boolean => {
                    if (this.IsDragActive() && this.IsDropPointHovered() && this.IsDragValid() && !this.areaManager.IsScrollActive()) {
                        if (!this.isDragSnapped) {
                            this.isDragSnapped = true;
                            const manipulatorArgs : ValueProgressEventArgs
                                = ValueProgressManager.get(this.elements.dragHolderId + "_NodeSnap");
                            manipulatorArgs.Owner(this.elements.dragHolderId);
                            manipulatorArgs.DirectionType(DirectionType.UP);
                            manipulatorArgs.ProgressType(ProgressType.LINEAR);
                            manipulatorArgs.Step(20);
                            manipulatorArgs.RangeStart(0);
                            manipulatorArgs.RangeEnd(100);
                            manipulatorArgs.CurrentValue(0);
                            manipulatorArgs.ChangeEventType(this.elements.dragHolderId + "_NodeSnapChange");
                            EventsManager.getInstanceSingleton().setEvent(this.elements.dragHolderId + "_NodeSnap",
                                this.elements.dragHolderId + "_NodeSnapChange",
                                ($eventArgs : ValueProgressEventArgs) : void => {
                                    if (this.isDragSnapped && !ObjectValidator.IsEmptyOrNull(this.dragAnchorNode)) {
                                        const posOffset : IVector = this.getHoveredNodeOffset($eventArgs.CurrentValue());
                                        this.positionDraggedNodes(posOffset);
                                        this.positionGroupIndicator(posOffset);
                                    }
                                });
                            ValueProgressManager.Execute(manipulatorArgs);
                        }
                        return true;
                    } else {
                        this.isDragSnapped = false;
                        return false;
                    }
                };

                if (!ObjectValidator.IsEmptyOrNull(this.dragAnchorNode) &&
                    ((!this.isDragSnapped && !ObjectValidator.IsEmptyOrNull($moveOffset)
                        && Math.abs($moveOffset.Left()) + Math.abs($moveOffset.Top()) > 1) || !snapNode())) {
                    this.isDragSnapped = false;
                    EventsManager.getInstanceSingleton().FireAsynchronousMethod(snapNode, true,
                        100);
                    const posOffset : IVector = this.env.getNodePosition(this.dragAnchorNode.Row(), this.dragAnchorNode.Column());
                    const targetPos : IVector = {
                        x: mouseAreaPos.x - posOffset.x,
                        y: mouseAreaPos.y - posOffset.y
                    };
                    this.positionDraggedNodes(targetPos);
                    this.positionGroupIndicator(targetPos);
                }
            }
        }

        public IsDragActive() : boolean {
            return !ObjectValidator.IsEmptyOrNull(this.dragAnchorNode);
        }

        public IsDragLoaded() : boolean {
            return !this.isDragLoading;
        }

        public IsDragSnapped() : boolean {
            return this.isDragSnapped;
        }

        public getHoveredNodeOffset($percentage : number = 100) : IVector {
            const mouseAreaPos : IVector = this.areaManager.getMouseAreaPosition();
            const targetPoint : IVector = this.env.getNodePosition(this.getHoveredRow(), this.getHoveredColumn());
            const startPoint : IVector = {
                x: mouseAreaPos.x,
                y: mouseAreaPos.y
            };
            const position : IVector = {
                x: startPoint.x + (targetPoint.x - startPoint.x)
                    * $percentage / 100,
                y: startPoint.y + (targetPoint.y - startPoint.y)
                    * $percentage / 100
            };
            const posOffset : IVector = this.env.getNodePosition(this.dragAnchorNode.Row(), this.dragAnchorNode.Column());
            return {x: position.x - posOffset.x, y: position.y - posOffset.y};
        }

        public CancelDrag() : void {
            if (this.IsDragActive()) {
                ElementManager.Hide(this.elements.dragHolderId);
                this.selectedNodes.ToArray().forEach(($node : IKernelNode) : void => {
                    if ($node.Row() > -1 && $node.Column() > -1) {
                        $node.Error(false);
                        $node.Selected(true);
                        this.MoveNode($node, this.env.getNodePosition($node.Row(), $node.Column()));
                    } else {
                        this.RemoveNode($node, true, true);
                    }
                });
                this.dragAnchorNode = null;
                this.GridVisible(false);
            }
        }

        public getDragGridOverflow() : ElementOffset {
            let minRow : number = Number.POSITIVE_INFINITY;
            let maxRow : number = Number.NEGATIVE_INFINITY;
            let minCol : number = Number.POSITIVE_INFINITY;
            let maxCol : number = Number.NEGATIVE_INFINITY;
            const move : ElementOffset = this.getMove();
            this.selectedNodes.foreach(($node : IKernelNode) : void => {
                const row : number = $node.Row() + move.Top();
                const col : number = $node.Column() + move.Left();
                if (row < minRow) {
                    minRow = row;
                }
                if (row > maxRow) {
                    maxRow = row;
                }
                if (col < minCol) {
                    minCol = col;
                }
                if (col > maxCol) {
                    maxCol = col;
                }
            });
            maxRow += 1;
            maxCol += 1;
            const dims : Size = this.GridDims();
            const vertical : number = minRow < 0 ? minRow
                : (maxRow > dims.Height() ? maxRow - dims.Height() : 0);
            const horizontal : number = minCol < 0 ? minCol
                : (maxCol > dims.Width() ? maxCol - dims.Width() : 0);
            return new ElementOffset(vertical * this.env.nodeSize, horizontal * this.env.nodeSize);
        }

        public MoveNode($node : IKernelNode, $offset : IVector) : void {
            $node.Move($offset);
        }

        public IsDragValid() : boolean {
            return this.dragValidator();
        }

        public getInvalidDraggedNodes() : ArrayList<IKernelNode> {
            const invalidDrag : ArrayList<IKernelNode> = new ArrayList<IKernelNode>();
            const gridOffset : ElementOffset = this.getMove();
            this.selectedNodes.foreach(($node : IKernelNode) : void => {
                const node : IKernelNode
                    = this.getNodeAtPosition($node.Row() + gridOffset.Top(), $node.Column() + gridOffset.Left());
                if (!ObjectValidator.IsEmptyOrNull(node) && !this.selectedNodes.Contains(node)) {
                    invalidDrag.Add($node);
                }
            });
            return invalidDrag;
        }

        public ApplyMove($node : IKernelNode, $to : ElementOffset) : void {
            if (!ObjectValidator.IsEmptyOrNull(this.nodesMatrix[$node.Row()])
                && $node.UniqueId() === this.nodesMatrix[$node.Row()][$node.Column()]) {
                this.nodesMatrix[$node.Row()][$node.Column()] = null;
            }
            $node.Column($to.Left());
            $node.Row($to.Top());

            this.MoveNode($node, this.env.getNodePosition($node.Row(), $node.Column()));
            if (ObjectValidator.IsEmptyOrNull(this.nodesMatrix[$node.Row()])) {
                this.nodesMatrix[$node.Row()] = [];
            }
            this.nodesMatrix[$node.Row()][$node.Column()] = $node.UniqueId().toString();
        }

        public ApplyDrag($callback? : ($success : boolean) => void) : void {
            const handler : any = () : void => {
                if (this.IsDragValid()) {
                    const gridOffset : ElementOffset = this.getMove();
                    const applyMove : any = ($node : IKernelNode) : void => {
                        if (!ObjectValidator.IsEmptyOrNull(this.nodesMatrix[$node.Row()])
                            && this.nodesMatrix[$node.Row()][$node.Column()] === $node.UniqueId()) {
                            this.nodesMatrix[$node.Row()][$node.Column()] = null;
                        }
                        $node.Row($node.Row() + gridOffset.Top());
                        $node.Column($node.Column() + gridOffset.Left());
                        if (ObjectValidator.IsEmptyOrNull(this.nodesMatrix[$node.Row()])) {
                            this.nodesMatrix[$node.Row()] = [];
                        }
                        this.nodesMatrix[$node.Row()][$node.Column()] = $node.UniqueId();
                        this.MoveNode($node, this.env.getNodePosition($node.Row(), $node.Column()));
                    };

                    ElementManager.Hide(this.elements.dragHolderId);
                    this.selectedNodes.foreach(($node : IKernelNode) : void => {
                        applyMove($node);
                    });
                    if (ObjectValidator.IsSet($callback)) {
                        $callback(true);
                    }
                    this.GridVisible(false);
                    this.dragAnchorNode = null;
                } else if (ObjectValidator.IsSet($callback)) {
                    $callback(false);
                }
                this.onDragLoadHandler = null;
            };
            if (this.isDragLoading) {
                this.onDragLoadHandler = handler;
            } else {
                handler();
            }
        }

        public getMove() : ElementOffset {
            if (this.IsDragActive()) {
                return new ElementOffset(this.getHoveredRow() - this.dragAnchorNode.Row(),
                    this.getHoveredColumn() - this.dragAnchorNode.Column());
            } else {
                return new ElementOffset();
            }
        }

        public DragAnchorNode($value? : IKernelNode) : IKernelNode {
            return this.dragAnchorNode = ObjectValidator.IsSet($value) ? $value : this.dragAnchorNode;
        }

        public RegisterNode($node : IKernelNode) : void {
            if (ObjectValidator.IsEmptyOrNull(this.nodesMatrix[$node.Row()])) {
                this.nodesMatrix[$node.Row()] = [];
            }
            this.nodesMatrix[$node.Row()][$node.Column()] = $node.UniqueId();
            this.nodes.Add($node, $node.UniqueId());
            $node.RegisterEvents();
        }

        public ClearReferences() : void {
            this.selectedNodes.Clear();
            this.nodes.Clear();
            this.nodesMatrix = [];
        }

        public RemoveAll() : void {
            this.nodes.ToArray().forEach(($node : IKernelNode) : void => {
                this.RemoveNode($node);
            });
        }

        public RemoveNode($node : IKernelNode, $clearIdentity : boolean = true, $removeFromTree : boolean = true) : void {
            if ($removeFromTree) {
                $node.ChildNodes().ToArray().forEach(($node : IKernelNode) : void => {
                    this.RemoveNode($node, $clearIdentity, $removeFromTree);
                });
                $node.ParentNode().ChildNodes().RemoveAt($node.ParentNode().ChildNodes().IndexOf($node));
            }
            if ($removeFromTree && !ObjectValidator.IsEmptyOrNull(this.nodesMatrix[$node.Row()])) {
                this.nodesMatrix[$node.Row()][$node.Column()] = null;
            }
            $node.Remove($clearIdentity);

            const parent : IGuiCommons = $node.Parent();
            if (!ObjectValidator.IsEmptyOrNull(parent)) {
                parent.getChildElements().RemoveAt(parent.getChildElements().IndexOf($node));
            }
            this.selectedNodes.RemoveAt(this.selectedNodes.IndexOf($node));
            this.nodes.RemoveAt(this.nodes.IndexOf($node));
        }

        public GridDims($value? : Size) : Size {
            const root : IKernelNode = this.root;
            if (ObjectValidator.IsEmptyOrNull(root)) {
                return new Size();
            }
            if (ObjectValidator.IsSet($value)) {
                this.areaManager.ApplyAreaResize(
                    new ElementOffset(
                        (root.GridSize().Width() - $value.Width()) * this.env.nodeSize,
                        (root.GridSize().Height() - $value.Height()) * this.env.nodeSize),
                    new ElementOffset(1, 1));
                root.GridSize($value);
            }
            return root.GridSize();
        }

        public setGridSize($value : Size) : void {
            this.setAreaSize($value.Width() * this.env.nodeSize, $value.Height() * this.env.nodeSize);
        }

        public GridVisible($value? : boolean) : boolean {
            if (ObjectValidator.IsSet($value)) {
                if (this.isGridVisible !== $value) {
                    this.isGridVisible = Property.Boolean(this.isGridVisible, $value);
                    if (this.isGridVisible) {
                        this.setAreaStatus(GeneralCssNames.ON);
                        this.nodes.foreach(($node : IKernelNode) : void => {
                            $node.Enabled(false);
                        });
                    } else {
                        this.setAreaStatus(GeneralCssNames.OFF);
                        this.nodes.foreach(($node : IKernelNode) : void => {
                            $node.Enabled(true);
                        });
                    }
                }
            }
            return this.isGridVisible;
        }

        public SelectAll() : void {
            this.SelectNodes(this.nodes, true);
        }

        public DeselectAll() : void {
            this.SelectNodes(this.selectedNodes, false);
        }

        public SelectNodesByIds($ids : string[], $scrollTo : boolean = true) : void {
            if (!ObjectValidator.IsEmptyOrNull($ids)) {
                const nodes : ArrayList<IKernelNode> = new ArrayList<IKernelNode>();
                $ids.forEach(($id : string) : void => {
                    const node : IKernelNode = this.getNodeFromId($id);
                    if (!ObjectValidator.IsEmptyOrNull(node)) {
                        nodes.Add(node);
                    }
                });
                this.SelectNodes(nodes, true);
                if ($scrollTo) {
                    const positions : IVector[] = [];
                    this.selectedNodes.foreach(($node : IKernelNode) : void => {
                        positions.push($node.getAbsolutePosition());
                    });
                    this.areaManager.ToggleScroll(() : IVector => {
                        return this.areaManager.getPointsScrollVector(positions, false, this.env.selectScrollAreaSize);
                    });
                }
            }
        }

        public SelectByViewBounds($bounds : IBounds) : void {
            const zoomMult : number = this.areaManager.getZoomMultiplier();
            const areaBounds : IBounds = {
                maxX: $bounds.maxX / zoomMult,
                maxY: $bounds.maxY / zoomMult,
                minX: $bounds.minX / zoomMult,
                minY: $bounds.minY / zoomMult
            };
            this.nodes.foreach(($node : IKernelNode) : void => {
                const pos : IVector = this.env.getNodePosition($node.Row(), $node.Column());
                const nodeCenterOffset : number = 20;
                if (pos.x > areaBounds.minX - nodeCenterOffset
                    && pos.x < areaBounds.maxX + nodeCenterOffset
                    && pos.y > areaBounds.minY - nodeCenterOffset
                    && pos.y < areaBounds.maxY + nodeCenterOffset) {
                    this.SelectNode($node, true);
                } else {
                    this.SelectNode($node, false);
                }
            });
        }

        public SelectNodes($nodes : ArrayList<IKernelNode>, $status : boolean = true) : void {
            $nodes.ToArray().forEach(($node : IKernelNode) : void => {
                this.SelectNode($node, $status);
            });
        }

        public SelectNode($node : IKernelNode, $status : boolean = true) : void {
            if (!ObjectValidator.IsEmptyOrNull($node) && $node !== this.root) {
                if ($node.Selected() !== $status) {
                    $node.Selected($status);
                    if ($status) {
                        this.selectedNodes.Add($node, $node.UniqueId());
                    } else if (this.selectedNodes.Contains($node)) {
                        this.selectedNodes.RemoveAt(this.selectedNodes.IndexOf($node));
                    }
                }
            }
        }

        public IsNodeSelected($node : IKernelNode) : boolean {
            return !ObjectValidator.IsEmptyOrNull(this.nodes.getItem($node.UniqueId()));
        }

        public getSelectedNodes() : ArrayList<IKernelNode> {
            return this.selectedNodes;
        }

        public getSelectedNode() : IKernelNode {
            return this.selectedNodes.Length() === 1 ? this.selectedNodes.getFirst() : null;
        }

        public getSelectedNodesIds() : string[] {
            const copy : ArrayList<IKernelNode> = ArrayList.ToArrayList(this.selectedNodes);
            copy.SortByKeyUp();
            return <string[]>copy.getKeys();
        }

        public RemoveSelected() : void {
            this.selectedNodes.ToArray().forEach(($node : IKernelNode) : void => {
                this.RemoveNode($node);
            });
        }

        public HighlightNodes($status : boolean, ...$nodes : IKernelNode[]) : void {
            if (ObjectValidator.IsEmptyOrNull($nodes)) {
                if (!$status) {
                    this.highlightedNodes.foreach(($node : IKernelNode) : void => {
                        if (ObjectValidator.IsEmptyOrNull(this.selectedNodes.getItem($node.UniqueId()))) {
                            $node.Selected(false);
                        }
                    });
                    this.highlightedNodes.Clear();
                }
            } else {
                $nodes.forEach(($node : IKernelNode) : void => {
                    if ($status) {
                        this.highlightedNodes.Add($node, $node.UniqueId());
                    } else {
                        this.highlightedNodes.RemoveAt(this.highlightedNodes.IndexOf($node));
                    }
                    $node.Selected($status);
                });
            }
        }

        public getHighlightedNodes() : ArrayList<IKernelNode> {
            return this.highlightedNodes;
        }

        public IsDropPointHovered() : boolean {
            const mouseAreaPos : IVector = this.areaManager.getMouseAreaPosition();
            const row : number = Math.floor(mouseAreaPos.y / this.env.nodeSize);
            const col : number = Math.floor(mouseAreaPos.x / this.env.nodeSize);
            const nodePos : IVector = this.env.getNodePosition(row, col);
            return Math.sqrt(Math.pow(nodePos.x - mouseAreaPos.x, 2)
                + Math.pow(nodePos.y - mouseAreaPos.y, 2)) < this.env.nodeRadius * 1.2;
        }

        public IsInsideGrid($row? : number, $col? : number) : boolean {
            const row : number = ObjectValidator.IsSet($row) ? $row : this.getHoveredRow();
            const col : number = ObjectValidator.IsSet($col) ? $col : this.getHoveredColumn();

            const matrixSize : Size = this.GridDims();
            return col >= 0 && col < matrixSize.Width()
                && row >= 0 && row < matrixSize.Height();
        }

        public getNodeAtPosition($row : number, $column : number) : IKernelNode {
            if (!ObjectValidator.IsEmptyOrNull($row) && !ObjectValidator.IsEmptyOrNull($column) &&
                !ObjectValidator.IsEmptyOrNull(this.nodesMatrix[$row])) {
                return this.nodes.getItem(this.nodesMatrix[$row][$column]);
            }
            return null;
        }

        public getNodeFromId($id : string) : IKernelNode {
            return this.nodes.getItem($id);
        }

        public getHoveredNode() : IKernelNode {
            return this.getNodeAtPosition(this.getHoveredRow(), this.getHoveredColumn());
        }

        public getHoveredRow() : number {
            return Math.floor(this.areaManager.getMouseAreaPosition().y / this.env.nodeSize);
        }

        public getHoveredColumn() : number {
            return Math.floor(this.areaManager.getMouseAreaPosition().x / this.env.nodeSize);
        }

        public IsViewHovered() : boolean {
            return this.areaManager.IsViewHovered();
        }

        public ToggleScroll($vectorFunc : () => IVector = () : IVector => {
            return {x: 0, y: 0};
        },                  $onIterationHandler? : () => void, $isMouseRelative? : boolean) : void {
            this.areaManager.ToggleScroll($vectorFunc, $onIterationHandler, $isMouseRelative);
        }

        public getPointScrollVector($point? : IVector, $round? : boolean, $maxSpeed? : number, $minSpeed? : number,
                                    $viewPadding : number = this.env.scrollAreaSize) : IVector {
            return this.areaManager.getPointScrollVector($point, $round, $maxSpeed, $minSpeed, $viewPadding);
        }

        public getPointsScrollVector($points? : IVector[], $round? : boolean, $maxSpeed? : number, $minSpeed? : number,
                                     $viewPadding : number = this.env.scrollAreaSize) : IVector {
            return this.areaManager.getPointsScrollVector($points, $round, $maxSpeed, $minSpeed, $viewPadding);
        }

        public IsPointsBoundableByView($points? : IVector[], $viewPadding : number = this.env.scrollAreaSize) : boolean {
            return this.areaManager.IsPointsBoundableByView($points, $viewPadding);
        }

        public IsScrollActive() : boolean {
            return this.areaManager.IsScrollActive();
        }

        public IsResizeActive() : boolean {
            return this.areaManager.IsResizeActive();
        }

        public ResetAreaPosition() : void {
            this.areaManager.ResetAreaPosition();
        }

        public getAreaPosition() : IVector {
            return this.areaManager.getAreaPosition();
        }

        public setAreaPosition($offset : IVector) {
            this.areaManager.setAreaPosition($offset);
        }

        public setZoomPercentage($value : number, $isMouseRelative? : boolean) : void {
            this.areaManager.setZoomPercentage($value, $isMouseRelative);
            this.getNodes().foreach(($node : IKernelNode) : void => {
                $node.ScaleVisibleConnectors($value);
            });
        }

        public getScale() : number {
            return this.areaManager.getScale();
        }

        public getZoomMultiplier() : number {
            return this.areaManager.getZoomMultiplier();
        }

        public getPointViewOverflow($point : IVector,
                                    $viewPadding : number = this.env.scrollAreaSize) : IVector {
            return this.areaManager.getPointViewOverflow($point, $viewPadding);
        }

        public getMaxOverflowPoint($points : IVector[], $viewPadding : number = this.env.scrollAreaSize) : IVector {
            return this.areaManager.getMaxOverflowPoint($points, $viewPadding);
        }

        public getMinOverflowPoint($points : IVector[], $viewPadding : number = this.env.scrollAreaSize) : IVector {
            return this.areaManager.getMinOverflowPoint($points, $viewPadding);
        }

        public setAreaStatus($value : string) : void {
            this.areaManager.setAreaStatus($value);
        }

        public setMouseViewPosition($value : IVector, $clearMove : boolean = false) : void {
            this.areaManager.setMouseViewPosition($value, $clearMove);
        }

        public getMouseViewPosition() : IVector {
            return this.areaManager.getMouseViewPosition();
        }

        public getMouseAreaPosition() : IVector {
            return this.areaManager.getMouseAreaPosition();
        }

        public getMouseViewMove() : IVector {
            return this.areaManager.getMouseViewMove();
        }

        public getMouseAreaMove() : IVector {
            return this.areaManager.getMouseAreaMove();
        }

        public getMouseLastViewMove() : IVector {
            return this.areaManager.getMouseLastViewMove();
        }

        public getMouseLastAreaMove() : IVector {
            return this.areaManager.getMouseLastAreaMove();
        }

        public getMouseLastViewAxisChange() : IVector {
            return this.areaManager.getMouseLastViewAxisChange();
        }

        public getPointViewPosition($point : IVector) : IVector {
            return this.areaManager.getPointViewPosition($point);
        }

        public getViewSize() : Size {
            return this.areaManager.getViewSize();
        }

        public setViewSize($value : Size) : void {
            this.areaManager.setViewSize($value);
        }

        public setAreaConstraints($minSizeX : number, $minSizeY : number) : void {
            this.areaManager.setAreaConstraints($minSizeX, $minSizeY);
        }

        public setAreaSize($width : number, $height : number) : void {
            this.areaManager.setAreaSize($width, $height);
            ElementManager.setSize(this.elements.dropAreaId, $width, $height);
        }

        public getAreaSize() : Size {
            return this.areaManager.getAreaSize();
        }

        private positionDraggedNodes($offset : IVector) : void {
            this.selectedNodes.foreach(($node : IKernelNode) : void => {
                const pos : IVector = $node.getGridPosition();
                this.MoveNode($node, {x: pos.x + $offset.x, y: pos.y + $offset.y});
            });
        }

        private positionGroupIndicator($offset : IVector) : void {
            if (this.selectedNodes.Length() > 1) {
                let minX : number = Number.POSITIVE_INFINITY;
                let maxX : number = Number.NEGATIVE_INFINITY;
                let minY : number = Number.POSITIVE_INFINITY;
                let maxY : number = Number.NEGATIVE_INFINITY;
                const offset : number = 50;
                this.selectedNodes.foreach(($node : IKernelNode) : void => {
                    const nodePos : IVector = $node.getGridPosition();
                    minY = Math.min(minY, nodePos.y + $offset.y - offset);
                    maxY = Math.max(maxY, nodePos.y + $offset.y + offset);
                    minX = Math.min(minX, nodePos.x + $offset.x - offset);
                    maxX = Math.max(maxX, nodePos.x + $offset.x + offset);
                });
                const borderOffset : number
                    = ElementManager.getCssIntegerValue(this.elements.dragHolderId, "border-width");
                ElementManager.setCssProperty(this.elements.dragHolderId, "top", minY - borderOffset);
                ElementManager.setCssProperty(this.elements.dragHolderId, "left", minX - borderOffset);
                ElementManager.setCssProperty(this.elements.dragHolderId, "height", maxY - minY);
                ElementManager.setCssProperty(this.elements.dragHolderId, "width", maxX - minX);
                ElementManager.Show(this.elements.dragHolderId);
            }
        }
    }
}
