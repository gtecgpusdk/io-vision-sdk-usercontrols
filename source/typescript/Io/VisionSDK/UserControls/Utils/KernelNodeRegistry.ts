/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.UserControls.Utils {
    "use strict";
    import BaseObject = Com.Wui.Framework.Commons.Primitives.BaseObject;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import KernelNodeType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.KernelNodeType;
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import IKernelNode = Io.VisionSDK.UserControls.Interfaces.IKernelNode;
    import IKernelNodeAttribute = Io.VisionSDK.UserControls.Interfaces.IKernelNodeAttribute;

    export class KernelNodeRegistry extends BaseObject {
        private uniqueNames : ArrayList<IKernelNode>;
        private uniqueIds : ArrayList<IKernelNode>;
        private uniqueIdsCounters : ArrayList<number>;
        private applicationDirectory : string;

        public constructor($applicationDirectory : string) {
            super();
            this.ClearState();
            this.applicationDirectory = $applicationDirectory;
        }

        public AssignUniqueId($node : IKernelNode) : void {
            const prefix : string = $node.UniqueIdPrefix();
            if (ObjectValidator.IsEmptyOrNull(this.uniqueIdsCounters.getItem(prefix))) {
                this.uniqueIdsCounters.Add(0, prefix);
            }
            const id : string = $node.UniqueId();
            if (!ObjectValidator.IsEmptyOrNull(id)) {
                this.uniqueIds.Add($node, id);

                const lastIndex : any = StringUtils.Replace(id, prefix, "");
                if (ObjectValidator.IsInteger(lastIndex)) {
                    const index : number = Number(lastIndex);
                    if (index > this.uniqueIdsCounters.getItem(prefix)) {
                        this.uniqueIdsCounters.Add(index, prefix);
                    }
                }
            } else {
                $node.UniqueId(this.generateAssignableUniqueId(prefix));
                this.uniqueIds.Add($node, $node.UniqueId());
            }
        }

        public AssignName($node : IKernelNode) : void {
            const name : string = $node.Name();
            if (!ObjectValidator.IsEmptyOrNull(name)) {
                this.uniqueNames.Add($node, name);
            } else {
                $node.Name(this.generateAssignableName($node.NamePrefix(), $node.Type() !== KernelNodeType.VISUAL_GRAPH));
                this.uniqueNames.Add($node, $node.Name());
            }
        }

        public IsNameAvailable($value : string) : boolean {
            return ObjectValidator.IsSet($value) && ObjectValidator.IsEmptyOrNull(this.uniqueNames.getItem($value));
        }

        public AssignIdentity($nodes : ArrayList<IKernelNode> | IKernelNode) {
            ArrayList.ToArrayList($nodes).foreach(($node : IKernelNode) : void => {
                this.AssignName($node);
                this.AssignUniqueId($node);

                $node.getAttributes().foreach(($attribute : IKernelNodeAttribute) : void => {
                    const attributeCopy : IKernelNodeAttribute = JSON.parse(JSON.stringify($attribute));
                    if ($attribute.type !== "Array") {
                        attributeCopy.value = StringUtils.Replace(attributeCopy.value, "{nodeName}", $node.Name());
                        attributeCopy.value = StringUtils.Replace(attributeCopy.value, "{appPath}", this.applicationDirectory);
                    }
                    $node.setAttribute(attributeCopy);
                });
                this.AssignIdentity($node.ChildNodes());
            });
        }

        public UnAssignIdentity($node : IKernelNode) : void {
            this.UnassignName($node.Name());
            this.UnassignUniqueId($node.UniqueId());
        }

        public getNodeByUniqueId($value : string) : IKernelNode {
            return this.uniqueIds.getItem($value);
        }

        public getNodeByName($value : string) : IKernelNode {
            return this.uniqueNames.getItem($value);
        }

        public UnassignName($value : string) : void {
            this.uniqueNames.RemoveAt(this.uniqueNames.IndexOf(this.uniqueNames.getItem($value)));
        }

        public UnassignUniqueId($value : string) : void {
            this.uniqueIds.RemoveAt(this.uniqueIds.IndexOf(this.uniqueIds.getItem($value)));
        }

        public ApplicationDirectory($value? : string) : string {
            return this.applicationDirectory = Property.String(this.applicationDirectory, $value);
        }

        public ClearState() : void {
            this.uniqueNames = new ArrayList<IKernelNode>();
            this.uniqueIds = new ArrayList<IKernelNode>();
            this.uniqueIdsCounters = new ArrayList<number>();
        }

        private generateAssignableName($value : string, $indexFromZero : boolean) : string {
            let index = 0;
            let assigned : string = $value;
            if ($indexFromZero) {
                assigned += index;
                index += 1;
            }

            while (!this.IsNameAvailable(assigned)) {
                assigned = $value + index;
                index += 1;
            }
            return assigned;
        }

        private generateAssignableUniqueId($value : string) : string {
            let assigned : string = $value + this.uniqueIdsCounters.getItem($value);
            while (!ObjectValidator.IsEmptyOrNull(this.uniqueIds.getItem(assigned))) {
                assigned = $value + this.uniqueIdsCounters.getItem($value);
                this.uniqueIdsCounters.Add(this.uniqueIdsCounters.getItem($value) + 1, $value);
            }
            return assigned;
        }
    }
}
