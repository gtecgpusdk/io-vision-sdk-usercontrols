# io-vision-sdk-usercontrols v2019.1.0

> WUI Framework library focused on user controls with design for Vision SDK.

## Requirements

This library does not have any special requirements but it depends on the 
[WUI Builder](https://bitbucket.org/wuiframework/com-wui-framework-builder). See the WUI Builder requirements before you build this project.

## Project build

The project build is fully automated. For more information about the project build, see the 
[WUI Builder](https://bitbucket.org/wuiframework/com-wui-framework-builder) documentation.

## Documentation

This project provides automatically-generated documentation in [TypeDoc](http://typedoc.org/) from the TypeScript source by running the 
`wui docs` command from the {projectRoot} folder.

> NOTE: The documentation is accessible also from the {projectRoot}/build/target/docs/index.html file after a successful creation.

## History

### v2019.1.0
Added new icons for OVX 1.2. Responsive design enhancements. Usage of SASS mixins.
### v2019.0.2
WUI Core update.
### v2019.0.1
Fixed textfield behaviour in disabled an selected mode. Added data flow animation. Fixed connection reconnect and drop area scroll direction.
### v2019.0.0
Refactoring of namespaces and UI managers. Stability bug fixes focused on Vision Studio drag-and-drop features.
### v2018.3.0
WUI Core update. Performance enhancements. Refactoring based on support for nodes grouping.
### v2018.2.0
Integrate new Accordions. KernelButton and DirectoryBrowser stability bug fixes. Convert docs to TypeDoc.
### v2018.1.3
WUI Core update.
### v2018.1.2
KernelNode API update reflecting new history manager requirements. Stability bug fixes and clean up.
### v2018.1.1
Cumulative GUI stability bug fixes. Added PointerIcon suitable for mouse emulation.
### v2018.1.0
Update of WUI Core supporting multithreading. Bug fix for BSP selector. Added global configurable tooltip. Update of Kernel node icons. 
Zoom related enhancements. Added DropDownList mixin.
### v2018.0.5
Added user controls required by nodes selector. Added connection point tracking. Added icons for VideoIO nodes. 
Bug fixes connected with onchange feedback from properties tab.
### v2018.0.4
Added style for DropDownList without field. Bug fixes for disabled TextArea and DropDownList.
### v2018.0.3
Fixed scrolling and events emitting issues. Added new icon for toolbar. Update of validation feedback.
### v2018.0.2
Added new icons for OpenVX 1.1. Added notifications user control based on graphical design.
### v2018.0.1
Update of toolbar icons. Fixed issues connected with events emitted by updated of user controls values.
### v2018.0.0
Added new icons. KernelNodes connectors converted to linked objects. Stability bug fixes. Changed version format.
### v1.2.2
Added new icons dedicated for toolbar. Added support for consumption of connection error messages 
and text representation of enum values in properties tab.
### v1.2.1
Stability bug fixes for KernelNodes connections. Added ability to consume external connections validator.
### v1.2.0
Refactoring of KernelNodes. Update of SCR and history ordering. Added simple Dialog user control. Change of SCM domain.
### v1.1.2
Update of user controls required by Vision SDK Studio.
### v1.1.1
Added new icons, DropDownList and NumberPicker. Update of dependencies.
### v1.1.0
Added new icon and clean up required by Vision SDK Studio.
### v1.0.4
Added fixed version for dependencies. Implemented user controls required by Vision SDK Studio.
### v1.0.3
Update of TypeScript syntax. Added user controls required by GPU SDK Installer and Vision SDK Studio.
### v1.0.2
WUI namespaces refactoring 
### v1.0.1
Namespace refactoring from Vision SDK to GPU SDK 
### v1.0.0
Initial release

## License

This software is owned or controlled by NXP Semiconductors. 
Use of this software is governed by the BSD-3-Clause License distributed with this material.
  
See the `LICENSE.txt` file distributed for more details.

---

Author Jakub Cieslar, 
Copyright (c) 2016 [Freescale Semiconductor, Inc.](http://freescale.com/), 
Copyright (c) 2017-2019 [NXP](http://nxp.com/)
